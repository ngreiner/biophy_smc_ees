# -*- coding: utf-8 -*


##############################################################################################################
# Class IaOD3RecruitmentBuilder.
#
# Created on: 2 January 2020
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
##############################################################################################################


from biophy_smc_ees.recruitment_builders.RecruitmentBuilder import RecruitmentBuilder
from biophy_smc_ees.neural_entities.fibers.IaFiberOD3 import IaFiberOD3
from biophy_smc_ees.simulations.IaOD3Simulation import IaOD3Simulation
from biophy_smc_ees.stims.IaFiberOD3EextStim import IaFiberOD3EextStim


class IaOD3RecruitmentBuilder(RecruitmentBuilder):
    """
    This class refines the RecruitmentBuilder class to treat with IaFibers and extracellular stimulation.

    Instance variables defined here:
        _neuralobj_type: str: identifier of neural object type.         -- overrides RecruitmentBuilder._neuralobj_type
        _stim_type: str: identifier of stimulation type.                -- overrides RecruitmentBuilder._stim_type
        _NeuralObj: NeuralEntity-derived class.                         -- overrides RecruitmentBuilder._NeuralObj
        _Simu: Simulation-derived class.                                -- overrides RecruitmentBuilder._Simu
        _Stim: Stimulation-derived class.                               -- overrides RecruitmentBuilder._Stim

    Methods defined here:
        __init__(self, smc, **kwargs)       -- extends RecruitmentBuild.__init__

    """

    def __init__(self, smc, **kwargs):
        """Class-constructor method.

        Same argument list as that of the mother class RecruitmentBuilder.

        """

        super(IaOD3RecruitmentBuilder, self).__init__(smc, **kwargs)

        self._neuralobj_type = 'IaOD3'
        self._stim_type = 'eext'

        self._NeuralObj = IaFiberOD3
        self._Simu = IaOD3Simulation
        self._Stim = IaFiberOD3EextStim

##############################################################################################################
