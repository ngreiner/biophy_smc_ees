# -*- coding: utf-8 -*


##############################################################################################################
# Class RecruitmentBuilder.
#
# Created on: 8 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
##############################################################################################################


import os
import time

import numpy as np
import pandas

from biophy_smc_ees.utils.decorators import print_exec
from biophy_smc_ees.utils import utilities
from biophy_smc_ees.utils.FileLock import FileLock


class RecruitmentBuilder(object):
    """
    This class regroups attributes and methods allowing to automatize the execution of simulations to evaluate the
    recruitment of neural entities of a sensorimotor circuit in response to various types of stimulations.

    Instance variables defined here:
        _neuralobj_type     : str   : identifier of neural object type ('Ia', 'II', 'MN', ...).
        _stim_type          : str   : identifier of stimulation type ('eext', 'stat_syn_from_nfib', ...).
        _NeuralObj          : NeuralEntity-derived class    : corresponding to _neuralobj_type.
        _Simu               : Simulation-derived class      : corresponding to _neuralobj_type.
        _Stim               : Stimulation-derived class     : corresponding to _stim_type.
        _smc                : SMC object                    : sensorimotor circuit.
        _gidxs              : list of int                   : global indexes of neural entities.
        _combis             : list of str/int/list/dict     : descriptors of stimulation combinations.
        _scombis            : list of str/int/list/dict     : str-encrypted _combis.
        _amps               : list of int/float             : stimulation amplitudes.
        _neuralobj_params   : dict  : parameters to build instances of the _NeuralObj class.
        _simu_params        : dict  : parameters to build instances of the _Simu class.
        _stim_params        : dict  : parameters to build instances of the _Stim class.
        _runperm            : int   : level of permission to run new simulations.
        _date_created       : str   : date of creation of the RecruitmentBuilder in the format 'yyyymmdd_hhmmss'.
        _size_groups        : int   : size of groups for parallel computations.
        _skip_when_recr     : bool  : indicator whether 2nd simulation-skipping mechanism is used or not.

    Methods defined here:
        __init__(self, smc, **kwargs)
        __repr__(self)
        update_stim_params(self, **kwargs)
        run_recr_serial(self)
        _comp_recr_gidx(self, gidx)
        _enable_ecr(self, records, scombis, amps)
        _make_folder(self, gidx)
        _get_records_path(self, gidx)

    """

    def __init__(self, smc, **kwargs):
        """Class-constructor method.

        Arguments:
            smc : SMC object    : sensorimotor circuit of the entities of which the recruitments are calculated.

        Keyword arguments:
            gidxs               : list of int                   : global indexes of neural entities.
            combis              : list of str/int/list/dict     : descriptors of stimulation combinations.
            amps                : list of int/float             : stimulation amplitudes.
            neuralobj_params    : dict  : parameters to build instances of the _NeuralObj class.
            simu_params         : dict  : parameters to build instances of the _Simu class.
            stim_params         : dict  : parameters to build instances of the _Stim class.
            runperm             : int   : level of permission to run recruitment calculations.
            size_groups         : int   : size of groups for parallel computations.
            skip_when_recr      : bool  : indicator whether 2nd simulation-skipping mechanism is used or not.

        """

        self._neuralobj_type = ''
        self._stim_type = ''
        self._NeuralObj = None
        self._Simu = None
        self._Stim = None

        self._smc = smc
        self._gidxs = None
        self._combis = None
        self._scombis = None
        self._amps = None

        self._neuralobj_params = {}
        self._simu_params = {}
        self._stim_params = {}

        self._runperm = 1
        self._date_created = '{:d}{:02d}{:02d}_{:02d}{:02d}{:02d}'.format(*time.localtime()[0 : 6])
        self._size_groups = 50
        self._skip_when_recr = False

        try:
            self._smc = kwargs['smc']
        except:
            pass

        try:
            self._gidxs = kwargs['gidxs']
        except:
            pass

        try:
            self._combis = kwargs['combis']
            self._scombis = [utilities.combi_2_str(combi) for combi in self._combis]
        except:
            pass

        try:
            self._amps = kwargs['amps']
        except:
            pass

        try:
            self._neuralobj_params = kwargs['neuralobj_params']
        except:
            pass

        try:
            self._simu_params = kwargs['simu_params']
        except:
            pass

        try:
            self._stim_params = kwargs['stim_params']
        except:
            pass

        try:
            self._runperm = kwargs['runperm']
        except:
            pass

        try:
            self._size_groups = kwargs['size_groups']
        except:
            pass

        try:
            self._skip_when_recr = kwargs['skip_when_recr']
        except:
            pass

    def __repr__(self):
        """Return the string representation of the RecruitmentBuilder."""
        return '{}_{}'.format(self.__class__.__name__, self._date_created)

    ##############################################################################################################
    # The methods below are mutators of various class attributes.
    ##############################################################################################################

    def update_stim_params(self, **kwargs):
        """Update the dictionary _stim_params with the input stim_params."""
        self._stim_params.update(**kwargs)

    ##############################################################################################################
    # The methods below deal with running NEURON simulations and exporting the results.
    ##############################################################################################################

    @print_exec
    def run_recr_serial(self):
        """Build and export the recruitments using a serial approach.

        The entities whose recruitments shall be built are treated individually, and sequentially.
        For each of them, the method _comp_recr_gidx performs the necessary simulations and export the new records.

        """

        # Build log files folder.
        os.makedirs(os.path.join('.', 'log_files', '{}'.format(self)), exist_ok=True)

        # Loop over gidxs.
        gidxs = list(self._gidxs)

        while gidxs:

            # Pop next gidx from list.
            gidx = gidxs.pop(0)

            # Run recruitment for gidx.
            self._make_folder(gidx)
            with FileLock(self._get_records_path(gidx)) as file_lock:

                # If gidx is already locked, which happens when it is involved in computations supervised by another
                # RecruitmentBuilder, push back gidx at the end of the list.
                if not file_lock.acquired:
                    gidxs.append(gidx)

                # Otherwise run computations on gidx.
                else:
                    self._comp_recr_gidx(gidx)

    @print_exec
    def _comp_recr_gidx(self, gidx):
        """Orchestrate the recruitment calculations of a single entity.

        Arguments:
            gidx    : int   : global index of entity.

        The method proceeds as follows:

            1. Load the records of the entity.

            2. For each (combi, amp, stim_type) configuration of the RecruitmentBuilder, perform new recruitment
            calculations if required/allowed (which is determined on the basis of the entity records and of the
            RecruitmentBuilder's _combis, _amps, _stim_type, and _runperm: see method _enable_recr). Two additional
            mechanisms are implemented to reduce the number of simulations to be ran: the first skips a simulation if
            the stimulation it involves is 'empty' for a given (combi, amp) configuration; the second skips the
            simulations involving a stimulation whose amp is higher than an amp that already recruited the entity
            with the same combi. This latter mechanism is disabled by default but can be enabled by setting
            _skip_when_recr to True.

            3. Export the entity's updated records.

        """

        # Console message.
        print('Running computations for {} #{:d} in process #{:d}...'.format(self._neuralobj_type, gidx, os.getpid()))

        # Prepare log file.
        log = ['{} #{:d}\n'.format(self._neuralobj_type, gidx)]
        file_path = os.path.join('.', 'log_files', '{}'.format(self), '{}_{:d}.log'.format(self._neuralobj_type, gidx))

        # Load records of entity.
        records = self._smc.load_records(self._neuralobj_type, gidx=gidx)

        # Check whether entity needs recruitment calculations for the RecruitmentBuilder's _combis and _amps.
        # If not write log file and return.
        if not self._enable_recr(records, self._scombis, self._amps):

            log.append('Skipped computations for combis {} (not enabled).'.format(' | '.join(self._scombis)))
            with open(file_path, 'a') as f:
                f.write('\n'.join(log + ['\n', '\n']))

            return

        # Otherwise, build NeuralObj, Simu and Stim objects, run the necessary simulations, and export the updated
        # records of entity.
        new_records = []

        neuralobj = self._NeuralObj(smc=self._smc, gidx=gidx, **self._neuralobj_params)
        simu = self._Simu(neuralobj, **self._simu_params)
        stim = self._Stim(neuralobj, **self._stim_params)
        simu.init()

        # Loop over combis.
        for combi, scombi in zip(self._combis, self._scombis):

            # Check whether entity needs recruitment calculations for the current combi and the RecruitmentBuilder's
            # _amps. If not, skip computations.
            if not self._enable_recr(records, [scombi], self._amps):
                log.append('Skipped computations for combi {} (not enabled).'.format(scombi))
                continue

            # Otherwise, reset combi and move forward.
            stim.reset(combi=combi)
            log.append('Running computations for combi {}...'.format(scombi))

            # Loop over amplitudes. We are going to pop the amplitudes from their list, so we make a copy of that list.
            amps = list(self._amps)

            while amps:

                amp = amps.pop(0)

                # Check whether entity needs recruitment calculations for the current combi and amp.
                # If not, skip calculations.
                if not self._enable_recr(records, [scombi], [amp]):
                    log.append('\tSkipped amp = {:.1f} (not enabled).'.format(amp))
                    continue

                # Otherwise, reset stim amp and move forward.
                stim.reset(amp=amp)

                # Check whether stim is empty. If yes, skip calculations, assign recr=0.
                if stim.isempty():
                    simu.reset()
                    recr = simu.get_recr()
                    recr['combi'] = scombi
                    recr['amp'] = amp
                    recr['stim_type'] = self._stim_type
                    new_records.append(recr)
                    log.append('\tSkipped amp = {:.1f} (Stim was empty)'.format(amp))
                    continue

                # Otherwise, run simulation.
                stim.activate()
                simu.reset()
                simu.run()
                recr = simu.get_recr()
                recr['combi'] = scombi
                recr['amp'] = amp
                recr['stim_type'] = self._stim_type
                recr['delay'] = recr['delay'].apply(lambda x: max(x - stim.delay(), 0.0))
                new_records.append(recr)
                log.append('\tComputed amp = {:.1f}. Recruitment:\n{}'.format(amp, recr.drop(
                    columns=['combi', 'amp']).to_string()))

                # If _skip_when_recr is True, and if the current amp recruited the entity, skip calculations
                # for subsequent amps and assign to these amps replicas of the recruitment values obtained with the
                # current amp.
                if self._skip_when_recr and np.any(recr['recr'].values):
                    log.append('\tSkipped subsequent simulations: declared {} recruited.'.format(self._neuralobj_type))
                    for amp in amps:
                        recr = recr.copy()
                        recr['amp'] = amp
                        new_records.append(recr)
                    break

        # Update records with new_records and export updated records if needed.
        if new_records:
            # Updating the original records with the new records consists in two operations: (i) overwriting the
            # entries of the original records with the entries of the new records which have similar (combi, amp,
            # stim_type) combinations, and (ii) adding the entries of the new records whose (combi, amp, stim_type)
            # combinations cannot be found in any entry of the original records. The easiest way to achieve this is
            # to add to the new records the entries of the old records whose (combi, amp, stim_type) are not in the
            # new records. This is achieved below.
            configs = set((df.loc[0, 'combi'], df.loc[0, 'amp'], df.loc[0, 'stim_type']) for df in new_records)
            for idx, record in records.iterrows():
                if (record['combi'], record['amp'], record['stim_type']) not in configs:
                    new_records.append(pandas.DataFrame(record).T)
            new_records = pandas.concat(new_records, ignore_index=True)
            if not new_records.equals(records):
                self._smc.save_records(new_records, self._neuralobj_type, gidx=gidx)

        # Write log file.
        filepath = os.path.join('.', 'log_files', '{}'.format(self), '{}_{:d}.log'.format(self._neuralobj_type, gidx))
        with open(filepath, 'a') as f:
            f.write('\n'.join(log + ['\n', '\n']))

    def _enable_recr(self, records, scombis, amps):
        """Return a boolean indicating whether recruitment calculations are enabled or not.

        Arguments:
            records : pandas.DataFrame  : recruitment records of a neural entity.
            scombis : list of str       : str-encrypted stimulation combination descriptors.
            amps    : list of int/float : stimulation amplitudes.

        Return:
            outbool : bool  : indicator whether recruitment calculations are enabled or not.

        The policy for delivering clearance is as follows:
            1. If running permission is minimal (_runperm==0), outbool=False.
            2. If maximal (_runperm==2), outbool=True.
            3. If any one (combi, amp, stim_type) configuration formed with the input combis and amps and
            with the RecruitmentBuilder's _stim_type is missing from the input records, outbool=True.
            4. If none is missing, and if running permission is low (_runperm==1), outbool=False. Otherwise,
            _runperm=='i', and authorization is given upon asking to user: outbool=prompt_confirm(msg).

        """

        # If running permission is minimal (=0) always forbid running simulations. If maximal (=2), always enable.
        if self._runperm == 0:
            return False
        elif self._runperm == 2:
            return True

        # Otherwise, check if any one (combi, amp, stim_type) configuration formed with the input combis and amps and
        # with the RecruitmentBuilder's _stim_type is missing from the input records. If yes, give clearance.
        for scombi in scombis:
            for amp in amps:
                if records[(records.combi == scombi) & (records.amp == amp) &
                           (records.stim_type == self._stim_type)].empty:
                    return True

        # If the code below is reached, no entry is missing in the records.
        # If running permission is low (=1), forbid running new simulations.
        if self._runperm == 1:
            return False

        # Otherwise, running permission is set on interactive (='i'), and permission to run new simulations will be
        # granted only after asking to user.
        else:
            return utilities.prompt_confirm('Amplitudes already in records for currently accessed configuration.'
                                            'Do you want to run new computations nonetheless? (y/n)\n')

    def _make_folder(self, gidx):
        """Make the folder where the records file of the input gidx should be stored."""
        os.makedirs(self._smc.get_folder_path(self._neuralobj_type, gidx=gidx), exist_ok=True)

    def _get_records_path(self, gidx):
        """Return the path of the records file of the input gidx."""
        return self._smc.get_records_path(self._neuralobj_type, gidx=gidx)

##############################################################################################################
