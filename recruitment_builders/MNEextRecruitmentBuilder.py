# -*- coding: utf-8 -*


##############################################################################################################
# Class MNEextRecruitmentBuilder.
#
# Created on: 10 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
##############################################################################################################


from biophy_smc_ees.recruitment_builders.RecruitmentBuilder import RecruitmentBuilder
from biophy_smc_ees.neural_entities.cells.Motoneuron import Motoneuron
from biophy_smc_ees.simulations.MNSimulation import MNSimulation
from biophy_smc_ees.stims.MNEextStim import MNEextStim


class MNEextRecruitmentBuilder(RecruitmentBuilder):
    """
    This class refines the RecruitmentBuilder class to treat with Motoneurons and extracellular stimulation.

    Instance variables defined here:
        _neuralobj_type: str: identifier of neural object type.         -- overrides RecruitmentBuilder._neuralobj_type
        _stim_type: str: identifier of stimulation type.                -- overrides RecruitmentBuilder._stim_type
        _NeuralObj: NeuralEntity-derived class.                         -- overrides RecruitmentBuilder._NeuralObj
        _Simu: Simulation-derived class.                                -- overrides RecruitmentBuilder._Simu
        _Stim: Stimulation-derived class.                               -- overrides RecruitmentBuilder._Stim

    Methods defined here:
        __init__(self, smc, **kwargs)       -- extends RecruitmentBuild.__init__

    """

    def __init__(self, smc, **kwargs):
        """Class-constructor method.

        Same argument list as that of the mother class RecruitmentBuilder.

        """

        super(MNEextRecruitmentBuilder, self).__init__(smc, **kwargs)

        self._neuralobj_type = 'MN'
        self._stim_type = 'eext'

        self._NeuralObj = Motoneuron
        self._Simu = MNSimulation
        self._Stim = MNEextStim

##############################################################################################################
