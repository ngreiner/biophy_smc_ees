# -*- coding: utf-8 -*


###########################################################################
# Class MNStatSynFromNFibStim.
#
# Created on: 12 February 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from scipy import random as rand
from neuron import h

from biophy_smc_ees.stims.MNStatSynStim import MNStatSynStim

h.load_file('stdrun.hoc')


class MNStatSynFromNFibStim(MNStatSynStim):
    """
    This class refines the MNStatSynStim class to represent synaptic actions exerted by a prescribed number of fibers.

    Class attributes defined here:
        _nfib       : 1     : default number of fibers providing synapses.
        _p_connec   : 1.0   : default connection probability.
        _lam_nsyn   : 9.6   : default mean number of synapses per fiber (used as Poisson distribution parameter).

    Methods defined here:
        __init__(self, **kwargs)        -- extends MNStatSynStim.__init__
        reset(self, **kwargs)           -- extends MNStatSynStim.reset
        _set_syns(self)                 -- overrides MNStatSynStim._set_syns
        _set_hsyns(self)                -- overrides MNStatSynStim._set_hsyns

    """

    _nfib = 1           # default number of fibers providing synapses.
    _p_connec = 1.0     # default connection probability.
    _lam_nsyn = 9.6     # default mean number of synapses per fiber (used as Poisson distribution parameter).

    def __init__(self, mn, **kwargs):
        """Class constructor method.

        Arguments:
            mn  : Motoneuron object : to which the synaptic stimulation is applied.

        Keyword arguments:
            delay       : float             : synapse onsets' delay (in ms).
            tau         : float             : synapse time constant (in ms).
            gmax        : float             : synapse maximal conductance (in pS).
            e           : float             : synapse reversal potential (in mV).
            syn_distrib : str               : mode for the synapses distribution.
            amp         : int               : number of fibers providing synapses (alternative to nfib).
            nfib        : int               : number of fibers providing synapses.
            p_connec    : float in [0, 1]   : connection probability.
            lam_nsyn    : float             : mean number of synapses per fiber (Poisson distribution parameter).

        """

        super(MNStatSynFromNFibStim, self).__init__(mn, **kwargs)

        try:
            self._nfib = kwargs['amp']
        except:
            pass

        try:
            self._nfib = kwargs['nfib']
        except:
            pass

        try:
            self._p_connec = kwargs['p_connec']
        except:
            pass

        try:
            self._lam_nsyn = kwargs['lam_nsyn']
        except:
            pass

        self._set_syns()

    def _set_syns(self):
        """Build the synapse descriptors syns."""

        self.syns = []

        # We loop over the total number of fibers providing synapses.
        for _ in range(self._nfib):

            # For each fiber, the number of synapses is obtained by drawing from a Poisson distribution.
            # The dendritic sections targeted by these synapses are determined according to the specified mode of
            # selection.
            nsyn = rand.poisson(self._lam_nsyn)
            if nsyn == 0: nsyn = 1
            dsecs = self.mn.dtree.pick_dsecs(k=nsyn, mode=self._syn_distrib)

            # A random jittering delay is added on top of the basic onset delay of each synapse.
            # This delay is log-normally distributed, with mean=1ms and std=0.5ms.
            onsets = [self._delay + rand.lognormal(-0.47, 0.37) for _ in dsecs]

            # There's only some probability that the fiber provides synapses.
            if rand.random() < self._p_connec:
                self.syns += zip(dsecs, onsets)

    def _set_hsyns(self):
        """Build the actual HOC synapses hsyns."""
        self.hsyns = [self._build_alpha_syn(syn[0].hsec, onset=syn[1]) for syn in self.syns]

    def reset(self, **kwargs):
        """Reset the MNStatSynFromNFibStim.

        Keyword arguments:
            delay               : float             : synapse onsets' delay (in ms).
            tau                 : float             : synapse time constant (in ms).
            gmax                : float             : synapse maximal conductance (in pS).
            e                   : float             : synapse reversal potential (in mV).
            syn_distrib         : str               : mode for the synapses distribution.
            seed                : int               : new seed for the random number generator.
            combi               : dict              :
            combi['lam_nsyn']   : float             : mean number of synapses per fiber.
            combi['p_connec']   : float in [0, 1]   : connection probability.
            combi['gmax']       : float             : synapse maximal conductance (in pS).
            amp                 : int               : number of fibers providing synapses (alternative to nfib).
            nfib                : int               : number of fibers providing synapses.
            p_connec            : float in [0, 1]   : connection probability.
            lam_nsyn            : float             : mean number of synapses per fiber (Poisson distribution).

        """

        super(MNStatSynFromNFibStim, self).reset(**kwargs)

        try:
            self._lam_nsyn = kwargs['combi']['lam_nsyn']
        except:
            pass

        try:
            self._p_connec = kwargs['combi']['p_connec']
        except:
            pass

        try:
            self._gmax = kwargs['combi']['gmax']
        except:
            pass

        try:
            self._nfib = kwargs['amp']
        except:
            pass

        try:
            self._nfib = kwargs['nfib']
        except:
            pass

        try:
            self._p_connec = kwargs['p_connec']
        except:
            pass

        try:
            self._lam_nsyn = kwargs['lam_nsyn']
        except:
            pass

        self._set_syns()

###########################################################################
