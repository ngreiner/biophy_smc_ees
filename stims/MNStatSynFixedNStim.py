# -*- coding: utf-8 -*


###########################################################################
# Class MNStatSynFixedNStim.
#
# Created on: 18 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from scipy import random as rand
from neuron import h

from biophy_smc_ees.stims.MNStatSynStim import MNStatSynStim

h.load_file('stdrun.hoc')


class MNStatSynFixedNStim(MNStatSynStim):
    """
    This class refines the MNStatSynStim class to represent synaptic actions exerted by a prescribed number of synapses.

    Class variables defined here:
        _nsyn   : 1 : default number of synapses.

    Methods defined here:
        __init__(self, **kwargs)        -- extends MNStatSynStim.__init__
        _set_syns(self)                 -- overrides MNStatSynStim._set_syns
        _set_hsyns(self)                -- overrides MNStatSynStim._set_hsyns
        reset(self, **kwargs)           -- extends MNStatSynStim.reset

    """

    _nsyn = 1

    def __init__(self, mn, **kwargs):
        """Class constructor method.

        Arguments:
            mn  : Motoneuron object : to which the synaptic stimulation is applied.

        Keyword arguments:
            delay   : float : synapse onsets' delay (in ms).
            tau     : float : synapse time constant (in ms).
            gmax    : float : synapse maximal conductance (in pS).
            e       : float : synapse reversal potential (in mV).
            syn_distrib : str   : mode for the synapses distribution.
            amp     : int   : number of synapses (alternative to nsyn).
            nsyn    : int   : number of synapses.

        """

        super(MNStatSynFixedNStim, self).__init__(mn, **kwargs)

        try:
            self._nsyn = kwargs['amp']
        except:
            pass

        try:
            self._nsyn = kwargs['nsyn']
        except:
            pass

        self._set_syns()

    def _set_syns(self):
        """Build the synapse descriptors syns.

        The delay of activation of the synapses are calculated as the sum of the base delay _delay and a jitter
        representing the stochastic duration of neurotransmitter release. This stochastic duration is hardcoded to be
        lognormally distributed with mean=1.0ms and std=0.5ms.

        """
        dsecs = self.mn.dtree.pick_dsecs(self._nsyn, mode=self._syn_distrib)
        onsets = [self._delay + rand.lognormal(-0.47, 0.37) for _ in range(self._nsyn)]
        self.syns = zip(dsecs, onsets)

    def _set_hsyns(self):
        """Build the actual HOC synapses hsyns."""
        self.hsyns = [self._build_alpha_syn(syn[0].hsec, onset=syn[1]) for syn in self.syns]

    def reset(self, **kwargs):
        """Reset the MNStatSynFixedNStim.

        Keyword arguments:
            delay   : float : synapse onsets' delay (in ms).
            tau     : float : synapse time constant (in ms).
            gmax    : float : synapse maximal conductance (in pS).
            e       : float : synapse reversal potential (in mV).
            syn_distrib : str   : mode for the synapses distribution.
            amp     : int   : number of synapses (alternative to nsyn).
            nsyn    : int   : number of synapses.

        """

        super(MNStatSynFixedNStim, self).reset(**kwargs)

        try:
            self._nsyn = kwargs['amp']
        except:
            pass

        try:
            self._nsyn = kwargs['nsyn']
        except:
            pass

        self._set_syns()

###########################################################################
