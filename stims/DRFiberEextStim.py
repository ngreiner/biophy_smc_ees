# -*- coding: utf-8 -*


###########################################################################
# Class DRFiberEextStim.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np

from neuron import h

from biophy_smc_ees.stims.FiberEextStim import FiberEextStim

h.load_file('stdrun.hoc')

class DRFiberEextStim(FiberEextStim):
    """
    This class refines the FiberEextStim class to represent extracellular stimulations applied to DRFibers.

    Methods defined here:
        get_eexts(self, **kwargs)               -- overrides FiberEextStim.get_eexts
        _reset_eext_amp(self, amp)              -- overrides FiberEextStim._reset_eext_amp
        _set_eext_uniform(self, **kwargs)       -- overrides FiberEextStim._set_eext_uniform
        _set_eext_from_file(self)               -- overrides FiberEextStim._set_eext_from_file
        _read_elec_eext(self, elec)             -- overrides FiberEextStim._read_elec_eext

    """

    def get_eexts(self):
        """Return the electric potential arrays of the DRFiber branches as a list of arrays.

        Return:
            eexts   : list of 1D numpy.arrays.

        Description: the list is formed by placing the potential array of the DRbranch in a list.

        """
        return [self.eext['DRbranch']]

    def _reset_eext_amp(self, amp):
        """Reset the eext of the DRFiberEextStim according to the new input amplitude.

        Arguments:
            amp : float : stimulation amplitude (in uA).

        """
        self.eext['DRbranch'] = self.eext['DRbranch'] * (amp / self._amp)
        self._amp = amp

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform distribution.

        Keyword arguments:
            v1  : float : vext value of the uniform distribution.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        DRFiber during the stimulation pulse. This method assigns it with a uniform distribution. Its value is given
        by `v1` if provided, by _amp otherwise.

        """
        try:
            amp = kwargs['v1']
            self._amp = amp
        except:
            amp = self._amp

        self.eext['DRbranch'] = amp * np.ones(len(self.fiber.DRbranch.get_sections()))

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        DRFiber during the stimulation pulse. This method builds eext from textfiles. If attribute _combi is a str or
        an int, eext is obtained for a single electrode. Otherwise, _combi is the list/dict of activation ratios for
        a collection of electrodes.

        If combi is a list, and eext[i] is the potential distribution of elec #i, we have:
            eext = sum_i (combi[i] * eext[i + 1]) for i index of combi.
        Note: eext is evaluated for i + 1 because electrodes are indexed starting from 1 while Python lists are
        indexed starting from 0.

        If _combi is a dict, we have:
            eext = sum_elec (_combi[elec] * eext[elec]) for elec key of _combi.

        """

        # If combi is str or int, eext is obtained for a single electrode.
        if isinstance(self._combi, str) or isinstance(self._combi, int):
            eext = self._read_elec_eext(self._combi)

        # If _combi is a list, it is the list of the activation ratios for each electrode, in which case we need to read
        # eext from the textfiles for each electrode and perform a linear summation using these ratios.
        elif isinstance(self._combi, list):
            eext = dict()
            eext['DRbranch'] = 0.0
            for i, w in enumerate(self._combi):
                if w == 0.0: continue
                tmp = self._read_elec_eext(i + 1)
                eext['DRbranch'] += tmp['DRbranch'] * w

        # Otherwise, combi is a dictionnary of activation ratios for a collection of electrodes, in which case we
        # need to read eext from the textfiles for these electrodes and perform a linear summation using these ratios.
        else:
            eext = dict()
            eext['DRbranch'] = 0.0
            for s, w in self._combi.items():
                tmp = self._read_elec_eext(s)
                eext['DRbranch'] += tmp['DRbranch'] * w

        # Multiply the obtained vector by the stimulation amplitude. Assign to attribute eext.
        self.eext['DRbranch'] = eext['DRbranch'] * self._amp

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : dict  : the extracellular potential distribution elicited by the input elec along the DRbranch of
                              the DRFiber for a normalized current amplitude of 1uA.

        Description: there is only one key to eext, which is 'DRbranch'. The value attached to it is a numpy array
        giving the potentials at the compartments of the DRbranch of the DRFiber.

        """

        dir_data = self.fiber.smc().dir_data
        musc = self.fiber.musc()
        seg = self.fiber.seg()
        fidx = self.fiber.fidx()

        # Read content of file.
        if isinstance(elec, int):
            file_path = os.path.join(dir_data, musc, seg, 'DRFiber{:d}'.format(fidx), 'potentials',
                                    'elec{:d}.txt'.format(elec))
        else:
            file_path = os.path.join(dir_data, musc, seg, 'DRFiber{:d}'.format(fidx), 'potentials',
                                    '{}.txt'.format(elec))
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # Form eext dictionnary.
        eext = dict()
        eext['DRbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])

        return eext

###########################################################################
