# -*- coding: utf-8 -*


###########################################################################
# Class FiberEextStim.
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# Date created: 25 June 2018
# Last modified: 25 June 2018
#
###########################################################################


import numpy as np
from neuron import h

from biophy_smc_ees.stims.Stim import Stim

h.load_file('stdrun.hoc')


class FiberEextStim(Stim):
    """
    This class refines the Stim class to represent extracellular stimulations applied to fibers.

    Class variables defined here:
        _eext_type  : 'file'    : default identifier of type of extracellular potential distribution.

    Instance variables defined here:
        fiber   : Fiber object  : to which the simulation is applied.
        eext    : dict  : one key per Fiber branch. Each key is the array of the extracellular potentials at each
                          compartment of the corresponding branch.
        _combi  : str/int/dict  : descriptor of stimulation combination.

    Methods defined here:
        __init__(self, branch, **kwargs)    -- extends Stim.__init__ method
        __repr__(self)                      -- overrides Stim.__repr__ method
        combi(self, *args)
        eext_type(self, *args)
        get_eexts(self)
        reset(self, **kwargs)
        _reset_eext_amp(self, amp)
        _set_eext(self, **kwargs)
        _set_eext_uniform(self, **kwargs)
        _set_eext_from_file(self)
        _read_elec_eext(self, elec)
        _switch_on(self)
        _switch_off(self)
        activate(self)                      -- overrides Stim.activate method
        isempty(self)                       -- overrides Stim.isempty

    """

    _eext_type = 'file'

    def __init__(self, fiber, **kwargs):
        """Class constructor method.

        Arguments:
            fiber: Fiber object.

        Keyword arguments:
            amp     : int/float : amplitude of stimulation.
            delay   : float     : delay of stimulation onset (in ms).
            dur     : float     : duration of stimulation onset (in ms).
            combi   : int/str/dict  : descriptor of stimulation combination.
            eext_type   : str   : identifier of type of extracellular potential distribution.

        """

        super(FiberEextStim, self).__init__(**kwargs)

        self.fiber = fiber
        self.eext = dict()
        self._stim_vecs = []

        self._combi = None

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        try:
            self.eext_type(kwargs['eext_type'])
        except:
            pass

        try:
            self._set_eext()
        except:
            pass

    def __repr__(self):
        """Return the string representation of the FiberEextStim."""
        return '{}.{}'.format(self.__class__.__name__, self.fiber.name)

    def combi(self, *args):
        """Return (without args) or set and return (with args) the attribute _combi."""
        if len(args) > 0:
            self._combi = args[0]
        return self._combi

    def eext_type(self, *args):
        """Return (without args) or set and return (with args) the attribute _eext_type."""
        if len(args) > 0:
            self._eext_type = args[0]
        return self._eext_type

    def get_eexts(self):
        """Return the electric potential arrays of the Fiber branches as a list of arrays.

        Purely virtual method (overridden in children classes).

        """
        return []

    def reset(self, **kwargs):
        """Reset the FiberEextStim.

        Keyword arguments:
            delay   : float : stimulation delay (in ms).
            dur     : float : stimulation duration (in ms).
            combi   : str/list/dict : descriptor of stimulation combination.
            amp     : float : stimulation amplitude (in uA).

        """

        try:
            self.delay(kwargs['delay'])
        except:
            pass

        try:
            self.dur(kwargs['dur'])
        except:
            pass

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        if 'combi' not in kwargs.keys() and 'amp' in kwargs.keys():
            self._reset_eext_amp(kwargs['amp'])

        elif 'combi' in kwargs.keys() and 'amp' not in kwargs.keys():
            self.combi(kwargs['combi'])
            self._set_eext()

        elif 'combi' in kwargs.keys() and 'amp' in kwargs.keys():
            self.combi(kwargs['combi'])
            self.amp(kwargs['amp'])
            self._set_eext()

        else:
            pass

    def _reset_eext_amp(self, amp):
        """Reset the eext of the FiberEextStim according to the new input amplitude.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _set_eext(self, **kwargs):
        """Set the attribute eext.

        Keyword arguments:
             v1 : float : eext value of every section of the Fiber if _eext_type='uniform'.

        Description: eext is the dictionnary of the extracellular potential distributions along each branch of the
        Fiber during the stimulation pulse.

        """
        if self._eext_type == 'file':
            self._set_eext_from_file()
        elif self._eext_type == 'uniform':
            self._set_eext_uniform(**kwargs)

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform potential distribution.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _switch_on(self):
        """Assign the values of eext to the e_extracellular batteries of the Fiber compartments."""

        branches = self.fiber.get_branches()
        eexts = self.get_eexts()

        for branch, eext in zip(branches, eexts):
            for i, sec in enumerate(branch.get_sections()):
                sec.e_extracellular = eext[i]

        h.cvode.re_init()

    def _switch_off(self):
        """Assign 0.0 to all the e_extracellular batteries of the Fiber compartments."""

        branches = self.fiber.get_branches()

        for branch in branches:
            for sec in branch.get_sections():
                sec.e_extracellular = 0.0

        h.cvode.re_init()

    # def activate(self):
    #     """Perform the specific actions to activate the stimulation.
    #
    #     Here it consists of pushing two events in NEURON's integrator event queue:
    #         1 at time t=delay to switch on the extracellular batteries
    #         1 at time t=delay+dur to switch off the extracellular batteries
    #
    #     """
    #     h.cvode.event(self._delay, self._switch_on)
    #     h.cvode.event(self._delay + self._dur, self._switch_off)

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Here it consists in building vectors to be played into the extracellular mechanism of the Fiber sections.

        """
        self._set_stim_vecs()

    def _set_stim_vecs(self):
        """Build the vectors to be played in the Fiber's sections' extracellular mechanism.

        The vectors implement finite rise and fall time of the extracellular potential distribution.
        Rise and fall times are both set to _dur/100.

        (delay + dur/100)     (delay + dur)
                 |                |
                 __________________
                /                  \
               /                    \
        ______/                      \___________________________

              |                      |
           delay                    (delay + dur + dur/100)

        """
        self._stim_vecs = []

        branches = self.fiber.get_branches()
        eexts = self.get_eexts()

        # Time points
        t1 = self._delay
        t2 = self._delay + self._dur * 0.01
        t3 = self._delay + self._dur
        t4 = self._delay + self._dur * 1.01
        t5 = self._delay + self._dur * 1.02
        tvec = h.Vector([0.0, t1, t2, t3, t4, t5])

        # Unitary voltage vector.
        vvec = np.asarray([0.0, 0.0, 1.0, 1.0, 0.0, 0.0])

        for branch, eext in zip(branches, eexts):
            for i, sec in enumerate(branch.get_sections()):
                self._stim_vecs.append(h.Vector(eext[i] * vvec))
                self._stim_vecs[-1].play(sec(0.5)._ref_e_extracellular, tvec, 1)
        self._stim_vecs.append(tvec)

    def isempty(self):
        """Return a boolean indicating whether the FiberEextStim is empty or not.

         This is the case if eext is empty.

         """
        return len(self.eext) == 0

###########################################################################
