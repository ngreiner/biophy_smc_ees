# -*- coding: utf-8 -*


###########################################################################
# Class IaFiberEextStim.
#
# Created on: 25 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np

from neuron import h

from biophy_smc_ees.stims.FiberEextStim import FiberEextStim

h.load_file('stdrun.hoc')


class IaFiberEextStim(FiberEextStim):
    """
    This class refines the FiberEextStim class to represent extracellular stimulations applied to IaFibers.

    Methods defined here:
        get_eexts(self, **kwargs)               -- overrides FiberEextStim.get_eexts
        _reset_eext_amp(self, amp)              -- overrides FiberEextStim._reset_eext_amp
        _set_eext_uniform(self, **kwargs)       -- overrides FiberEextStim._set_eext_uniform
        _set_eext_from_file(self)               -- overrides FiberEextStim._set_eext_from_file
        _read_elec_eext(self, elec)             -- overrides FiberEextStim._read_elec_eext
    """

    def get_eexts(self):
        """Return the electric potential arrays of the IaFiber branches as a list of arrays.

        Return:
            eexts   : list of 1D numpy.arrays.

        Description: the list is formed by taking the potential arrays of the Abranch, Dbranch, DRbranch and the Cols
        consecutively in this order.
        """
        return [self.eext['Abranch'], self.eext['Dbranch'], self.eext['DRbranch']] + self.eext['Cols']

    def _reset_eext_amp(self, amp):
        """Reset the eext of the IaFiberEextStim according to the new input amplitude.

        Arguments:
            amp : float : stimulation amplitude (in uA).

        """
        self.eext['Abranch'] = self.eext['Abranch'] * (amp / self._amp)
        self.eext['Dbranch'] = self.eext['Dbranch'] * (amp / self._amp)
        self.eext['DRbranch'] = self.eext['DRbranch'] * (amp / self._amp)
        for i, eext in enumerate(self.eext['Cols']):
            self.eext['Cols'][i] = eext * (amp / self._amp)
        self._amp = amp

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform distribution.

        Keyword arguments:
            v1  : float : value of the uniform distribution.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        IaFiber during the stimulation pulse. This method assigns it with a uniform distribution. Its value is given
        by `v1` if provided, by _amp otherwise.

        """
        try:
            amp = kwargs['v1']
        except:
            amp = self._amp
        self.eext['Abranch'] = amp * np.ones(len(self.fiber.Abranch.get_sections()))
        self.eext['Dbranch'] = amp * np.ones(len(self.fiber.Dbranch.get_sections()))
        self.eext['DRbranch'] = amp * np.ones(len(self.fiber.DRbranch.get_sections()))
        self.eext['Cols'] = [amp * np.ones(len(Col.get_sections())) for Col in self.fiber.Cols]

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        IaFiber during the stimulation pulse. This method builds eext from textfiles. If attribute _combi is a str or
        an int, eext is obtained for a single electrode. Otherwise, _combi is the list/dict of activation ratios for
        a collection of electrodes.

        If combi is a list, and eext[i] is the potential distribution of elec #i, we have:
            eext = sum_i (combi[i] * eext[i + 1]) for i index of combi.
        Note: eext is evaluated for i + 1 because electrodes are indexed starting from 1 while Python lists are
        indexed starting from 0.

        If _combi is a dict, we have:
            eext = sum_elec (_combi[elec] * eext[elec]) for elec key of _combi.

        """

        # If attribute _combi is str or int, eext is obtained for a single electrode.
        if isinstance(self._combi, str) or isinstance(self._combi, int):
            eext = self._read_elec_eext(self._combi)

        # If _combi is a list, it is the list of the activation ratios for each electrode, in which case we need to read
        # eext from the textfiles for each electrode and perform a linear summation using these ratios.
        elif isinstance(self._combi, list):
            eext = dict()
            eext['Abranch'] = 0.0
            eext['Dbranch'] = 0.0
            eext['DRbranch'] = 0.0
            eext['Cols'] = [0.0 for _ in range(len(self.fiber.Cols))]
            for i, w in enumerate(self._combi):
                if w == 0.0: continue
                tmp = self._read_elec_eext(i + 1)
                eext['Abranch'] += tmp['Abranch'] * w
                eext['Dbranch'] += tmp['Dbranch'] * w
                eext['DRbranch'] += tmp['DRbranch'] * w
                for n in range(len(self.fiber.Cols)):
                    eext['Cols'][n] += tmp['Cols'][n] * w

        # Otherwise, _combi is a dictionnary of activation ratios for a collection of electrodes, in which case we
        # need to read eext from the textfiles for these electrodes and perform a linear summation using these ratios.
        else:
            eext = dict()
            eext['Abranch'] = 0.0
            eext['Dbranch'] = 0.0
            eext['DRbranch'] = 0.0
            eext['Cols'] = [0.0 for _ in range(len(self.fiber.Cols))]
            for s, w in self._combi.items():
                tmp = self._read_elec_eext(s)
                eext['Abranch'] += tmp['Abranch'] * w
                eext['Dbranch'] += tmp['Dbranch'] * w
                eext['DRbranch'] += tmp['DRbranch'] * w
                for n in range(len(self.fiber.Cols)):
                    eext['Cols'][n] += tmp['Cols'][n] * w

        # Multiply the obtained vector by the stimulation amplitude. Assign to attribute eext.
        self.eext['Abranch'] = eext['Abranch'] * self._amp
        self.eext['Dbranch'] = eext['Dbranch'] * self._amp
        self.eext['DRbranch'] = eext['DRbranch'] * self._amp
        self.eext['Cols'] = [elt * self._amp for elt in eext['Cols']]

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : dict  : the extracellular potential distributions elicited by the input elec along the branches
                              of the IaFiber for a normalized current amplitude of 1uA.

        Description: the keys of eext are 'Abranch', 'Dbranch', 'DRbranch' and 'Cols'. The values attached to
        'Abranch', 'Dbranch' and 'DRbranch' are numpy arrays giving the potentials at the compartments of the
        Abranch, Dbranch and DRbranch of the IaFiber. The value attached to 'Cols' is a list of such numpy arrays,
        one for each collateral of the IaFiber.

        """

        dir_data = self.fiber.smc().dir_data
        musc = self.fiber.musc()
        seg = self.fiber.seg()
        fidx = self.fiber.fidx()

        # Read content of file.
        if isinstance(elec, int):
            file_path = os.path.join(dir_data, musc, seg, 'IaFiber{:d}'.format(fidx), 'potentials',
                                     'elec{:d}.txt'.format(elec))
        else:
            file_path = os.path.join(dir_data, musc, seg, 'IaFiber{:d}'.format(fidx), 'potentials',
                                     '{}.txt'.format(elec))
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # Form eext dictionnary.
        eext = dict()
        eext['Abranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['Dbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['DRbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['Cols'] = [np.array([float(e) for e in file_lines.pop(0).split()]) for _ in range(len(self.fiber.Cols))]

        return eext

###########################################################################
