# -*- coding: utf-8 -*


###########################################################################
# Class MNStatSynFromIaStim.
#
# Created on: 19 September 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from scipy import random as rand
from neuron import h
import pandas

from biophy_smc_ees.stims.MNStatSynStim import MNStatSynStim

h.load_file('stdrun.hoc')


class MNStatSynFromIaStim(MNStatSynStim):
    """
    This class refines the MNStatSynStim class to represent synaptic actions exerted onto motoneurons following the
    direct extracellular activation of Ia-fibers.

    Class variables defined here:
        _lam_nsyn       : 9.6   : default mean number of synapses per Ia-fiber (Poisson distribution parameter).
        _connec_type    : 'fcd' : default identifier of connectivity type between Ia-fibers and motoneurons.

    Instance variables defined here:
        _combi      : int/str/dict      : descriptor of Ia-fibers' extracellular stimulation combination.
        _amp        : float             : amplitude of Ia-fibers' extracellular stimulation (in uA).
        _syn_index  : pandas.DataFrame  : global index of synapses of the Motoneuron.

    Methods defined here:
        __init__(self, **kwargs)        -- extends MNStatSynStim.__init__
        combi(self, *args)
        amp(self, *args)
        _set_syn_index(self)
        _set_syns(self)                 -- overrides MNStatSynStim._set_syns
        _set_hsyns(self)                -- overrides MNStatSynStim._set_hsyns
        reset(self, **kwargs)           -- extends MNStatSynStim.reset

    """

    _lam_nsyn = 9.6         # mean number of synapses per Ia-MN contact system (Poisson distribution parameter).
    _connec_type = 'fcd'    # identifier of connectivity type between Ia-fibers and motoneurons.

    def __init__(self, mn, **kwargs):
        """Class constructor method.

        Arguments:
            mn  : Motoneuron object : to which the synaptic stimulation is applied.

        Keyword arguments:
            delay       : float         : synapse onsets' delay (in ms).
            tau         : float         : synapse time constant (in ms).
            gmax        : float         : synapse maximal conductance (in pS).
            e           : float         : synapse reversal potential (in mV).
            syn_distrib : str           : mode for the synapses distribution.
            lam_nsyn    : float         : mean number of synapses per Ia-fiber (Poisson distribution parameter).
            connec_type : str           : identifier of connectivity type between Ia-fibers and motoneurons.
            combi       : int/str/dict  : descriptor of Ia-fibers' extracellular stimulation combination.
            amp         : float         : amplitude of Ia-fibers' extracellular stimulation (in uA).

        """

        super(MNStatSynFromIaStim, self).__init__(mn, **kwargs)

        self._combi = None
        self._amp = None
        self._syn_index = None

        try:
            self._lam_nsyn = kwargs['lam_nsyn']
        except:
            pass

        try:
            self._connec_type = kwargs['connec_type']
        except:
            pass

        try:
            self._combi = kwargs['combi']
        except:
            pass

        try:
            self._amp = kwargs['amp']
        except:
            pass

        self._set_syn_index()
        self._set_syns()

    def combi(self, *args):
        """Return (without args) or set and return (with args) the attribute _combi."""
        if len(args) > 0:
            self._combi = args[0]
        return self._combi

    def amp(self, *args):
        """Return (without args) or set and return (with args) the attribute _amp."""
        if len(args) > 0:
            self._amp = args[0]
        return self._amp

    def _set_syn_index(self):
        """Set the global synapse index of the Motoneuron.

        The global synapse index is formed by retrieving all the Ia-fibers providing synapses to the Motoneuron,
        and determining the number and position of the synapses supplied by each of them. We keep track of the
        collateral with which each Ia-fiber supplies its synapses, for each collateral has a specific propagation
        delay, which influences the delay of onset of the synapses it supplies.

        """
        self._syn_index = pandas.DataFrame(columns=['Ia_gidx', 'Col', 'idsec'])
        syns = []

        Ia_inputs = self.mn.smc().get_Ia_inputs_2_MN(gidx=self.mn.gidx())

        for row in Ia_inputs.itertuples():
            nsyn = rand.poisson(self._lam_nsyn)
            if nsyn == 0: nsyn = 1
            idsecs = self.mn.dtree.pick_idsecs(k=nsyn, mode=self._syn_distrib)
            syns.append(pandas.DataFrame(dict(Ia_gidx=[row.Ia_gidx] * nsyn, Col=[row.Col] * nsyn, idsec=idsecs)))

        if syns:
            self._syn_index = pandas.concat(syns, ignore_index=True)

    def _set_syns(self):
        """Build the synapse descriptors syns.

        The synapse descriptors which are built concern only a subgroup of the total pool of synapses referenced by
        the global synapse index _syn_index. They are those synapses whose supplying Ia-fiber collaterals were
        recruited at the amplitude _amp with the extracellular stimulation combination _combi.

        The delay of activation of the synapses are calculated as the sum of the base delay _delay, the propagation
        delay of their respective supplying collaterals, and a stochastic delay representing the neurotransmitter
        release. This stochastic delay is hardcoded to be lognormally distributed with mean=1.0ms and std=0.5ms.

        """

        self.syns = []

        # Unpack mn's synapse index for convenience.
        syn_index = self._syn_index

        # Retrieve the gidxs of the Ia-fibers innervating the Motoneuron.
        gidxs = syn_index['Ia_gidx'].drop_duplicates().tolist()

        # Build the synapses supplied by each Ia-fiber sequentially.
        for gidx in gidxs:

            # First, load the Ia-fiber recruitment records and keep only rows of interest.
            # The latter are those rows whose stim_type is 'eext' (for extracellular stimulation), whose (combi, amp)
            # pair match the present MNStatSynFromIaStim's own (combi, amp) pair, and whose recruitment value is 1.0.
            # In practice, this will result either in an empty records (in the case where the combi, amp pair didn't
            # recruit the Ia-fiber), either in a records with ~10 entries, 1 for each of the collaterals of the
            # Ia-fiber, which are usually recruited all at once when the Ia-fiber is recruited.
            records = self.mn.smc().load_records('Ia', gidx=gidx)
            records = records[(records['combi'] == self._combi) & (records['amp'] == self._amp) &
                              (records['stim_type'] == 'eext') & (records['recr'] == 1)]

            # If no such records exist, pass to next Ia-fiber.
            if records.empty:
                continue

            # Retrieve the collateral of the Ia-fiber that supplies synapses to the MN, and retrieve the indexes of the
            # dendritic sections it innervates from the global synapse index.
            tmp = syn_index.loc[syn_index['Ia_gidx'] == gidx]
            Col = tmp['Col'].values[0]
            idsecs = tmp['idsec'].values

            # Retrieve the propagation delay of the collateral.
            Col_delay = records.loc[records['Col'] == Col, 'delay'].iat[0]

            # Form the (dsec, delay) pairs describing the synapses.
            # A jitter lognormally distributed is introduced in the onset delay of each synapse, representing the
            # stochastic duration of neurotransmitter release (mean=1.0ms, std=0.5ms).
            for idsec in idsecs:
                dsec = self.mn.dtree.dsecs[idsec]
                release_delay = rand.lognormal(-0.47, 0.37)
                delay = self._delay + Col_delay + release_delay
                self.syns.append((dsec, delay))

    def _set_hsyns(self):
        """Build the actual HOC synapses.

        Under the fiber-count-dependent excitability hypothesis (_connec_type == 'fcd'), all the synapses have the
        same maximal conductance given by the attribute _gmax. In this case, the excitability of a Motoneuron is
        indeed fiber-count-dependent, because a Motoneuron contacted by a larger number of Ia-fibers will embed a larger
        total number of synapses in its dendritic tree, thus a larger total synaptically activable conductance,
        and thus likely greater synaptic currents.

        Under the iso-excitability hypothesis (_connec_type == 'iso'), the maximal conductance of the synapses is
        adjusted such that the product nsyn_max * gmax = 300 * _gmax, where gmax is the adjusted maximal conductance,
        _gmax the unadjusted maximal conductance, and nsyn_max is the total number of synapses received by the
        Motoneuron. The number 300 is the estimation by Segev et al., 'Computer Simulation of Group Ia EPSPs Using
        Morphologically Realistic Models of Cat Motoneurons', 1990, of the total number of synaptic boutons formed by
        medial gastrocnemius Ia-fibers onto a typical medial gastrocnemius motoneuron of the cat that can be
        activated simultaneously (which occur when all the Ia-fibers of the medial gastrocnemius are recruited
        simultaneously).
        In this case, regardless of the number of fibers contacting the Motoneuron, and thus regardless of the total
        number of synapses decorating its dendritic tree, its total synaptically activable conductance is fixed,
        dictated by _gmax.

        """

        if self._connec_type == 'fcd':
            gmax = self._gmax
        elif self._connec_type == 'iso':
            gmax = self._gmax * 300.0 / len(self._syn_index)
        else:
            raise ValueError('Invalid value for attribute _mode: {}'.format(self._connec_type))

        self.hsyns = [self._build_alpha_syn(syn[0].hsec, onset=syn[1], gmax=gmax) for syn in self.syns]

    def reset(self, **kwargs):
        """Reset the MNStatSynFromIaStim.

        Keyword arguments:
            delay       : float         : synapse onsets' delay (in ms).
            tau         : float         : synapse time constant (in ms).
            gmax        : float         : synapse maximal conductance (in pS).
            e           : float         : synapse reversal potential (in mV).
            syn_distrib : str           : mode for the synapses distribution.
            lam_nsyn    : float         : mean number of synapses per Ia-fiber (Poisson distribution parameter).
            connec_type : str           : identifier of connectivity type between Ia-fibers and motoneurons.
            combi       : int/str/dict  : descriptor of Ia-fibers' extracellular stimulation combination.
            amp         : float         : amplitude of Ia-fibers' extracellular stimulation (in uA).

        """

        super(MNStatSynFromIaStim, self).reset(**kwargs)

        try:
            self._lam_nsyn = kwargs['lam_nsyn']
        except:
            pass

        try:
            self._connec_type = kwargs['connec_type']
        except:
            pass

        try:
            self._combi = kwargs['combi']
        except:
            pass

        try:
            self._amp = kwargs['amp']
        except:
            pass

        self._set_syns()

###########################################################################
