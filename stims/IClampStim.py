# -*- coding: utf-8 -*


###########################################################################
# Class IClampStim.
#
# Created on: 18 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from neuron import h

from biophy_smc_ees.stims.Stim import Stim

h.load_file('stdrun.hoc')


class IClampStim(Stim):
    """
    This class refines the Stim class to represent current-clamp stimulations materialized by HOC IClamp objects
    inserted into specific HOC sections.

    Class variables defined here:
        _amp    : 1.0   : default amplitude (in nA).
        _loc    : 0.5   : default location of IClamp in the HOC section.

    Instance variables defined here:
        hsec: h.Section object: into which the IClamp is inserted.
        IClamp: h.IClamp object: the inserted IClamp.

    Methods:
        __init__(self, **kwargs)        -- extends Stim.__init__
        __repr__(self)                  -- overrides Stim.__repr__
        reset(self, **kwargs)
        _set_IClamp(self)
        activate(self)                  -- overrides Stim.activate
        isempty(self)                   -- overrides Stim.isempty

    """

    _amp = 1.0      # default IClamp amplitude (in nA).
    _loc = 0.5      # default location of IClamp on hsec (in the range [0, 1]).

    def __init__(self, hsec, **kwargs):
        """Class constructor method.

        Keyword arguments:
            amp     : float             : amplitude of stimulation (in nA).
            delay   : float             : delay of stimulation onset (in ms).
            dur     : float             : duration of stimulation onset (in ms).
            loc     : float in [0, 1]   : location of IClamp in the HOC section.

        """

        super(IClampStim, self).__init__(**kwargs)

        self.hsec = hsec
        self.IClamp = None

        try:
            self._loc = kwargs['loc']
        except:
            pass

    def __repr__(self):
        """Return the string representation of the IClampStim."""
        return 'IClampStim.{}'.format(self.hsec.name())

    def reset(self, **kwargs):
        """Reset the IClampStim.

        Keyword arguments:
            amp     : float             : amplitude of stimulation (in nA).
            delay   : float             : synapse onsets' delay (in ms).
            dur     : float             : duration of stimulation onset (in ms).
            loc     : float in [0, 1]   : location of IClamp in the HOC section.

        """

        self.IClamp = None

        try:
            self._amp = kwargs['amp']
        except:
            pass

        try:
            self._delay = kwargs['delay']
        except:
            pass

        try:
            self._dur = kwargs['dur']
        except:
            pass

        try:
            self._loc = kwargs['loc']
        except:
            pass

    def _set_IClamp(self):
        """Build the h.IClamp object IClamp."""
        self.IClamp = h.IClamp(self.hsec(self._loc))
        self.IClamp.delay = self._delay
        self.IClamp.dur = self._dur
        self.IClamp.amp = self._amp

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Here it consists in building the IClamp object.

        """
        self._set_IClamp()
        h.cvode.re_init()

    def isempty(self):
        """Return a boolean indicating whether the IClampStim is empty or not.

        This is the case if IClamp is None.

        """
        return self.IClamp is None

###########################################################################
