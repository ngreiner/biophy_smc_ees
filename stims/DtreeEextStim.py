# -*- coding: utf-8 -*


###########################################################################
# Class DtreeEextStim.
#
# Created on: 17 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np
from neuron import h

from biophy_smc_ees.stims.Stim import Stim

h.load_file('stdrun.hoc')


class DtreeEextStim(Stim):
    """
    This class refines the Stim class to represent extracellular stimulations applied to DendTrees.

    Class variables defined here:
        _eext_type  : 'file'    : default identifier of type of extracellular potential distribution.

    Instance variables defined here:
        dtree   : DendTree object   : to which the stimulation is applied.
        eext    : 1D numpy.array    : one value per dendritic section.
        _combi  : str/int/dict      : descriptor of stimulation combination.

    Methods defined here:
        __init__(self, branch, **kwargs)    -- extends Stim.__init__ method
        __repr__(self)                      -- overrides Stim.__repr__ method
        combi(self, *args)
        eext_type(self, *args)
        get_eexts(self)
        reset(self, **kwargs)
        _reset_eext_amp(self, amp)
        _set_eext(self, **kwargs)
        _set_eext_uniform(self, **kwargs)
        _set_eext_from_file(self)
        _read_elec_eext(self, elec)
        _switch_on(self)
        _switch_off(self)
        activate(self)                      -- overrides Stim.activate method
        isempty(self)                       -- overrides Stim.isempty

    """

    _eext_type = 'file'

    def __init__(self, dtree, **kwargs):
        """Class constructor method.

        Arguments:
            dtree: DendTree object.

        Keyword arguments:
            amp         : int/float     : amplitude of stimulation.
            delay       : float         : delay of stimulation onset (in ms).
            dur         : float         : duration of stimulation onset (in ms).
            combi       : int/str/dict  : descriptor of stimulation combination.
            eext_type   : str           : identifier of type of extracellular potential distribution.

        """

        super(DtreeEextStim, self).__init__(**kwargs)

        self.dtree = dtree
        self.eext = None

        self._combi = None

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        try:
            self.eext_type(kwargs['eext_type'])
        except:
            pass

        try:
            self._set_eext()
        except:
            pass

    def combi(self, *args):
        """Return (without args) or set and return (with args) the attribute _combi."""
        if len(args) > 0:
            self._combi = args[0]
        return self._combi

    def eext_type(self, *args):
        """Return (without args) or set and return (with args) the attribute _eext_type."""
        if len(args) > 0:
            self._eext_type = args[0]
        return self._eext_type

    def reset(self, **kwargs):
        """Reset the DtreeEextStim.

        Keyword arguments:
            delay   : float         : stimulation delay (in ms).
            dur     : float         : stimulation duration (in ms).
            combi   : str/list/dict : descriptor of stimulation combination.
            amp     : float         : stimulation amplitude (in uA).

        """

        try:
            self.delay(kwargs['delay'])
        except:
            pass

        try:
            self.dur(kwargs['dur'])
        except:
            pass

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        if 'combi' not in kwargs.keys() and 'amp' in kwargs.keys():
            self._reset_eext_amp(kwargs['amp'])

        elif 'combi' in kwargs.keys() and 'amp' not in kwargs.keys():
            self.combi(kwargs['combi'])
            self._set_eext()

        elif 'combi' in kwargs.keys() and 'amp' in kwargs.keys():
            self.combi(kwargs['combi'])
            self.amp(kwargs['amp'])
            self._set_eext()

        else:
            pass

    def _reset_eext_amp(self, amp):
        """Reset the eext of the DtreeEextStim according to the new input amplitude.

        Arguments:
            amp : float : stimulation amplitude (in uA).

        """
        self.eext = self.eext / self._amp * amp
        self._amp = amp

    def _set_eext(self, **kwargs):
        """Set the attribute eext.

        Parameters:
             v1 : float : eext value of every section of the DendTree if _eext_type='uniform'.

        Description: eext is the array of the extracellular potentials at the sections of the DendTree during the
        stimulation pulse.

        """
        if self._eext_type == 'file':
            self._set_eext_from_file()
        elif self._eext_type == 'uniform':
            self._set_eext_uniform(**kwargs)

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            coords  : N*3 numpy.array   : coordinates where extracellular potentials were evaluated.
            eext    : 1D numpy.array    : the extracellular potentials elicited by the input elec at the given
                                          coordinates, for a normalized current amplitude of 1uA.
        """
        dirData = self.dtree.cell.smc().dirData
        musc = self.dtree.cell.musc()
        seg = self.dtree.cell.seg()
        fidx = self.dtree.cell.fidx()

        # Derive file path.
        if isinstance(elec, int):
            filePath = os.path.join(dirData, musc, seg, 'MN{:d}'.format(fidx), 'dtree', 'potentials',
                                    'elec{:d}.txt'.format(elec))
        else:
            filePath = os.path.join(dirData, musc, seg, 'MN{:d}'.format(fidx), 'dtree', 'potentials',
                                    '{}.txt'.format(elec))

        # Read content of file.
        txt = np.loadtxt(filePath)
        coords = txt[:, 0 : 3]
        eext = txt[:, 3]

        return coords, eext

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform distribution.

        Keyword arguments:
            v1  : float : value of the uniform distribution.

        Description: eext is the array of the extracellular potentials at the sections of the DendTree during the
        stimulation pulse. This method assigns it with a uniform distribution. Its value is given by `v1` if
        provided, by _amp otherwise.

        """
        try:
            amp = kwargs['v1']
            self._amp = amp
        except:
            amp = self._amp
        self.eext = amp * np.ones(len(self.dtree.dsecs))

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Description: eext is the array of the extracellular potentials at the compartments of the DendTree during the
        stimulation pulse. This method builds eext from textfiles. If attribute _combi is a str or an int,
        eext is obtained for a single electrode. Otherwise, combi is the list/dict of activation ratios for a collection
        of electrodes.

        If _combi is a list, and eext[i] is the potential distribution of elec #i, we have:
            eext = sum_i (_combi[i] * eext[i + 1]) for i index of _combi.
        Note: eext is evaluated for i + 1 because electrodes are indexed starting from 1 while Python lists are
        indexed starting from 0.

        If _combi is a dict, we have:
            eext = sum_elec (_combi[elec] * eext[elec]) for elec key of combi.

        The two latter are not implemented yet.

        """

        # If attribute combi is None, eext is obtained for a single electrode described by the attribute elec.
        if isinstance(self._combi, str) or isinstance(self._combi, int):
            coords, eext = self._read_elec_eext(self._combi)
        else:
            raise TypeError('Incorrect type for _combi (or not implemented yet).')

        # Initialize eext.
        self.eext = np.zeros((len(self.dtree.dsecs), 1))

        # Fill eext.
        for isec, dsec in enumerate(self.dtree.dsecs):
            c3d = dsec.get_center3d()
            tmp = np.sqrt(np.sum(np.square(coords - c3d), axis=1))
            idxs = np.zeros((3, 1), dtype=int)
            dists = np.zeros((3, 1), dtype=float)
            for i in range(3):
                dists[i] = np.amin(tmp)
                idxs[i] = np.argmin(tmp)
                tmp[idxs[i]] = np.Inf
            vals = eext[idxs]
            ws = np.reciprocal(dists) / np.sum(np.reciprocal(dists))
            self.eext[isec] = np.sum(np.multiply(ws, vals)) * self._amp

    def _switch_on(self):
        """Assign the values of eext to the e_extracellular batteries of the DendTree's sections."""
        for dsec, e in zip(self.dtree.dsecs, self.eext):
            dsec.hsec.e_extracellular = e

    def _switch_off(self):
        """Assign 0.0 to the e_extracellular batteries of the DendTree's sections."""
        for dsec in self.dtree.dsecs:
            dsec.hsec.e_extracellular = 0.0

    # def activate(self):
    #     """Perform the specific actions to activate the stimulation.
    #
    #     Here it consists of pushing two events in NEURON's integrator event queue:
    #         1 at time t=delay to switch on the extracellular batteries
    #         1 at time t=delay+dur to switch off the extracellular batteries
    #
    #     """
    #     h.cvode.event(self._delay, self._switch_on)
    #     h.cvode.event(self._delay + self._dur, self._switch_off)

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Here it consists in building vectors to be played into the extracellular mechanism of the dtree sections.

        """
        self._set_stim_vecs()

    def _set_stim_vecs(self):
        """Build the vectors to be played in the sections' extracellular mechanism.

        The vectors implement finite rise and fall time of the extracellular potential distribution.
        Rise and fall times are both set to _dur/100.

        (delay + dur/100)     (delay + dur)
                 |                |
                 __________________
                /                  \
               /                    \
        ______/                      \___________________________

              |                      |
           delay                    (delay + dur + dur/100)

        """
        self._stim_vecs = []

        # Time vector of variations of the extracellular potential distribution.
        t1 = self._delay
        t2 = self._delay + self._dur * 0.01
        t3 = self._delay + self._dur
        t4 = self._delay + self._dur * 1.01
        t5 = self._delay + self._dur * 1.02
        tvec = h.Vector([0.0, t1, t2, t3, t4, t5])

        # Unitary voltage vector.
        vvec = np.asarray([0.0, 0.0, 1.0, 1.0, 0.0, 0.0])

        # Build stim vectors.
        for dsec, e in zip(self.dtree.dsecs, self.eext):
            self._stim_vecs.append(h.Vector(e * vvec))
            self._stim_vecs[-1].play(dsec.hsec(0.5)._ref_e_extracellular, tvec, 1)
        self._stim_vecs.append(tvec)

    def isempty(self):
        """Return a boolean indicating whether the DtreeEextStim is empty or not.

        This is the case if eext is None.

        """
        return self.eext is None

###########################################################################
