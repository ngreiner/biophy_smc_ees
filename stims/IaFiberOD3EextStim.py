# -*- coding: utf-8 -*


###########################################################################
# Class IaFiberOD3EextStim.
#
# Created on: 2 January 2020
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np

from neuron import h

from biophy_smc_ees.stims.IaFiberEextStim import IaFiberEextStim

h.load_file('stdrun.hoc')


class IaFiberOD3EextStim(IaFiberEextStim):
    """
    This class inherits from the IaFiberEextStim class to represent extracellular stimulations applied to IaFiberOD3s.

    Methods defined here:
        _read_elec_eext(self, elec)         -- overrides IaFiberEextStim._read_elec_eext

    """

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution from the appropriate textfile.

        Parameters:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : dict  : the extracellular potential distributions elicited by the input elec along the branches
                              of the IaFiberOD3 for a normalized current amplitude of 1uA.

        Description: the keys of eext are 'Abranch', 'Dbranch', 'DRbranch' and 'Cols'. The values attached to
        'Abranch', 'Dbranch' and 'DRbranch' are numpy arrays giving the potentials at the compartments of the
        Abranch, Dbranch and DRbranch of the IaFiberOD3. The value attached to 'Cols' is a list of such numpy arrays,
        one for each collateral of the IaFiberOD3.
        """

        dir_data = self.fiber.smc().dir_data
        musc = self.fiber.musc()
        seg = self.fiber.seg()
        fidx = self.fiber.fidx()

        # Read content of file.
        if isinstance(elec, int):
            file_path = os.path.join(dir_data, musc, seg, 'IaFiberOD3{:d}'.format(fidx), 'potentials',
                                     'elec{:d}.txt'.format(elec))
        else:
            file_path = os.path.join(dir_data, musc, seg, 'IaFiberOD3{:d}'.format(fidx), 'potentials',
                                     '{}.txt'.format(elec))
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # Form eext dictionnary.
        eext = dict()
        eext['Abranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['Dbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['DRbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['Cols'] = [np.array([float(e) for e in file_lines.pop(0).split()]) for _ in range(len(self.fiber.Cols))]

        return eext

###########################################################################
