# -*- coding: utf-8 -*


###########################################################################
# Class CSTFiberEextStim.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np

from neuron import h

from biophy_smc_ees.stims.FiberEextStim import FiberEextStim

h.load_file('stdrun.hoc')


class CSTFiberEextStim(FiberEextStim):
    """
    This class refines the FiberEextStim class to represent extracellular stimulations applied to CSTFibers.

    Methods overridden:
        get_eexts(self, **kwargs)               -- overrides FiberEextStim.get_eexts
        _reset_eext_amp(self, amp)              -- overrides FiberEextStim._reset_eext_amp
        _set_eext_uniform(self, **kwargs)       -- overrides FiberEextStim._set_eext_uniform
        _set_eext_from_file(self)               -- overrides FiberEextStim._set_eext_from_file
        _read_elec_eext(self, elec)             -- overrides FiberEextStim._read_elec_eext

    """

    def get_eexts(self):
        """Return the electric potential arrays of the CSTFiber branches as a list of arrays.

        Return:
            eexts   : list of 1D numpy.arrays.

        Description: the list is formed by taking the potential arrays of the CSTbranch and of the collaterals
        consecutively in this order.

        """
        return [self.eext['CSTbranch']] + self.eext['Cols']

    def _reset_eext_amp(self, amp):
        """Reset the eext of the CSTFiberEextStim according to the input new amplitude.

        Arguments:
            amp : float : stimulation amplitude (in uA).

        """
        self.eext['CSTbranch'] = self.eext['CSTbranch'] * (amp / self._amp)
        for i, eext in enumerate(self.eext['Cols']):
            self.eext['Cols'][i] = eext * (amp / self._amp)
        self._amp = amp

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform distribution.

        Keyword arguments:
            v1  : float : vext value of the uniform distribution.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        CSTFiber during the stimulation pulse. This method assigns it with a uniform distribution. Its value is given
        by `v1` if provided, by _amp otherwise.

        """
        try:
            amp = kwargs['v1']
            self._amp = amp
        except:
            amp = self._amp
        self.eext['CSTbranch'] = amp * np.ones(len(self.fiber.CSTbranch.get_sections()))
        self.eext['Cols'] = [amp * np.ones(len(Col.get_sections())) for Col in self.fiber.Cols]

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Description: eext is the dictionnary of the extracellular potential distributions along the branches of the
        CSTFiber during the stimulation pulse. This method builds eext from textfiles. If attribute combi is a str or
        an int, eext is obtained for a single electrode. Otherwise, combi is the list/dict of activation ratios for
        a collection of electrodes.

        If combi is a list, and eext[i] is the potential distribution of elec #i, we have:
            eext = sum_i (combi[i] * eext[i + 1]) for i index of combi.
        Note: eext is evaluated for i + 1 because electrodes are indexed starting from 1 while Python lists are
        indexed starting from 0.

        If combi is a dict, we have:
            eext = sum_elec (combi[elec] * eext[elec]) for elec key of combi.

        """

        # If attribute combi is None, eext is obtained for a single electrode described by the attribute elec.
        if isinstance(self._combi, str) or isinstance(self._combi, int):
            eext = self._read_elec_eext(self._combi)

        # If _combi is a list, it is the list of the activation ratios for each electrode, in which case we need to read
        # eext from the textfiles for each electrode and perform a linear summation using these ratios.
        elif isinstance(self._combi, list):
            eext = dict()
            eext['CSTbranch'] = 0.0
            eext['Cols'] = [0.0 for _ in range(len(self.fiber.Cols))]
            for i, w in enumerate(self._combi):
                if w == 0.0: continue
                tmp = self._read_elec_eext(i + 1)
                eext['CSTbranch'] += tmp['CSTbranch'] * w
                for n in range(len(self.fiber.Cols)):
                    eext['Cols'][n] += tmp['Cols'][n] * w

        # Otherwise, combi is a dictionnary of activation ratios for a collection of electrodes, in which case we
        # need to read eext from the textfiles for these electrodes and perform a linear summation using these ratios.
        else:
            eext = dict()
            eext['CSTbranch'] = 0.0
            eext['Cols'] = [0.0 for _ in range(len(self.fiber.Cols))]
            for s, w in self._combi.items():
                tmp = self._read_elec_eext(s)
                eext['CSTbranch'] += tmp['CSTbranch'] * w
                for n in range(len(self.fiber.Cols)):
                    eext['Cols'][n] += tmp['Cols'][n] * w

        # Multiply the obtained vector by the stimulation amplitude. Assign to attribute eext.
        self.eext['CSTbranch'] = eext['CSTbranch'] * self._amp
        self.eext['Cols'] = [elt * self._amp for elt in eext['Cols']]

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : dict  : the extracellular potential distributions elicited by the input elec along the CST branch
                              and along the collaterals of the CSTFiber for a normalized current amplitude of 1uA.

        Description: the keys of eext are 'CSTbranch' and 'Cols'. The value attached to 'CSTbranch' is a numpy array
        giving the potentials at the compartments of the CSTbranch of the CSTFiber. The value attached to 'Cols' is
        a list of such numpy arrays, one for each collateral of the CSTFiber.

        """

        dir_data = self.fiber.smc().dir_data
        musc = self.fiber.musc()
        seg = self.fiber.seg()
        fidx = self.fiber.fidx()

        # Read content of file.
        if isinstance(elec, int):
            file_path = os.path.join(dir_data, musc, seg, 'CSTFiber{:d}'.format(fidx), 'potentials',
                                     'elec{:d}.txt'.format(elec))
        else:
            file_path = os.path.join(dir_data, musc, seg, 'CSTFiber{:d}'.format(fidx), 'potentials',
                                     '{}.txt'.format(elec))
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # Form eext dictionnary.
        eext = dict()
        eext['CSTbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])
        eext['Cols'] = [np.array([float(e) for e in file_lines.pop(0).split()]) for _ in range(len(self.fiber.Cols))]

        return eext

###########################################################################
