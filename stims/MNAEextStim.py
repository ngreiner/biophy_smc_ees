# -*- coding: utf-8 -*


###########################################################################
# Class MNAEextStim.
#
# Created on: 18 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np
from neuron import h

from biophy_smc_ees.stims.Stim import Stim

h.load_file('stdrun.hoc')


class MNAEextStim(Stim):
    """
    This class refines the Stim class to represent extracellular stimulations applied to MotorAxons (the cellular
    compartments, not the fully-fledged neural entities).

    Class variables defined here:
        _eext_type  : 'file'    : default identifier of type of extracellular potential distribution.

    Instance variables defined here:
        axon    : cell_comps.MotorAxon object    : to which the stimulation is applied.
        eext    : 1D-numpy.array    : extracellular potential distribution along MotorAxon.
        _combi  : int/str/dict      : descriptor of stimulation combination.

    Methods defined here:
        __init__(self, axon, **kwargs)      -- extends Stim.__init__ method
        __repr__(self)                      -- overrides Stim.__repr__ method
        combi(self, *args)
        eext_type(self, *args)
        reset(self, **kwargs)
        _reset_eext_amp(self, amp)
        _set_eext(self, **kwargs)
        _set_eext_uniform(self, **kwargs)
        _set_eext_from_file(self)
        _read_elec_eext(self, elec)
        _switch_on(self)
        _switch_off(self)
        activate(self)                      -- overrides Stim.activate method
        isempty(self)                       -- overrides Stim.isempty

    """

    _eext_type = 'file'

    def __init__(self, axon, **kwargs):
        """Class constructor method.

        Arguments:
            axon    : cell_comps.MotorAxon object    : to which the stimulation is applied.

        Keyword arguments:
            amp     : int/float : amplitude of stimulation.
            delay   : float     : delay of stimulation onset (in ms).
            dur     : float     : duration of stimulation onset (in ms).
            combi   : int/str/dict  : descriptor of stimulation combination.
            eext_type   : str   : identifier of type of extracellular potential distribution.

        """

        super(MNAEextStim, self).__init__(**kwargs)

        self.axon = axon
        self.eext = None

        self._combi = None

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        try:
            self.eext_type(kwargs['eext_type'])
        except:
            pass

        try:
            self._set_eext()
        except:
            pass

    def __repr__(self):
        """Return the string representation of the MNAEextStim."""
        return 'MNAEextStim.{}'.format(self.axon.name)

    def combi(self, *args):
        """Return (without args) or set and return (with args) the attribute _combi."""
        if len(args) > 0:
            self._combi = args[0]
        return self._combi

    def eext_type(self, *args):
        """Return (without args) or set and return (with args) the attribute _eext_type."""
        if len(args) > 0:
            self._eext_type = args[0]
        return self._eext_type

    def reset(self, **kwargs):
        """Reset the MNAEextStim object.

        Keyword arguments:
            delay   : float : stimulation delay (in ms).
            dur     : float : stimulation duration (in ms).
            combi   : str/list/dict : descriptor of stimulation combination.
            amp     : float : stimulation amplitude (in uA).

        """

        try:
            self.delay(kwargs['delay'])
        except:
            pass

        try:
            self.dur(kwargs['dur'])
        except:
            pass

        try:
            self.combi(kwargs['combi'])
        except:
            pass

        if 'combi' not in kwargs.keys() and 'amp' in kwargs.keys():
            self._reset_eext_amp(kwargs['amp'])

        elif 'combi' in kwargs.keys() and 'amp' not in kwargs.keys():
            self.combi(kwargs['combi'])
            self._set_eext()

        elif 'combi' in kwargs.keys() and 'amp' in kwargs.keys():
            self.combi(kwargs['combi'])
            self.amp(kwargs['amp'])
            self._set_eext()

        else:
            pass

    def _reset_eext_amp(self, amp):
        """Reset the eext of the MNAEextStim according to the new input amplitude.

        Arguments:
            amp : float : stimulation amplitude (in uA).

        """
        self.eext = self.eext / self._amp * amp
        self._amp = amp

    def _set_eext(self, **kwargs):
        """Set the attribute eext.

        Keyword arguments:
             v1 : float : eext value of every section of the MotorAxon if _eext_type='uniform'.

        Description: eext is the array of the extracellular potentials at the compartments of the MotorAxon during the
        stimulation pulse.

        """
        if self._eext_type == 'file':
            self._set_eext_from_file()
        elif self._eext_type == 'uniform':
            self._set_eext_uniform(**kwargs)

    def _set_eext_uniform(self, **kwargs):
        """Set the attribute eext with a uniform distribution.

        Keyword arguments:
            v1  : float : value of the uniform distribution.

        Description: eext is the array of the extracellular potentials at the compartments of the MotorAxon during the
        stimulation pulse. This method assigns it with a uniform distribution. Its value is given by `v1` if
        provided, by _amp otherwise.

        """
        try:
            amp = kwargs['v1']
            self._amp = amp
        except:
            amp = self._amp

        self.eext = amp * np.ones(len(self.axon.VRbranch.get_sections()))

    def _set_eext_from_file(self):
        """Set the attribute eext using potential distributions from textfiles.

        Description: eext is the array of the extracellular potentials at the compartments of the MotorAxon during the
        stimulation pulse. This method builds eext from textfiles. If attribute combi is a str or an int,
        eext is obtained for a single electrode. Otherwise, combi is the list/dict of activation ratios for a collection
        of electrodes.

        If combi is a list, and eext[i] is the potential distribution of elec #i, we have:
            eext = sum_i (combi[i] * eext[i + 1]) for i index of combi.
        Note: eext is evaluated for i + 1 because electrodes are indexed starting from 1 while Python lists are
        indexed starting from 0.

        If combi is a dict, we have:
            eext = sum_elec (combi[elec] * eext[elec]) for elec key of combi.

        """

        # If attribute _combi is str or int, eext is obtained for a single electrode.
        if isinstance(self._combi, str) or isinstance(self._combi, int):
            eext = self._read_elec_eext(self._combi)

        # If _combi is a list, it is the list of the activation ratios for each electrode, in which case we need to read
        # eext from the textfiles for each electrode and perform a linear summation using these ratios.
        elif isinstance(self._combi, list):
            eext = 0.0
            for i, w in enumerate(self._combi):
                if w == 0.0: continue
                tmp = self._read_elec_eext(i + 1)
                eext += tmp * w

        # Otherwise, combi is a dictionnary of activation ratios for a collection of electrodes, in which case we
        # need to read eext from the textfiles for these electrodes and perform a linear summation using these ratios.
        else:
            eext = 0.0
            for s, w in self._combi.items():
                tmp = self._read_elec_eext(s)
                eext += tmp * w

        # Multiply the obtained vector by the stimulation amplitude. Assign to attribute eext.
        self.eext = eext * self._amp

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : 1D numpy.array    : the extracellular potentials of the sections of the MotorAxon elicited by the
                                          input elec, for a normalized current amplitude of 1uA.
        """

        dirData = self.axon.cell.smc().dirData
        musc = self.axon.cell.musc()
        seg = self.axon.cell.seg()
        fidx = self.axon.cell.fidx()

        # Derive file path.
        if isinstance(elec, int):
            filePath = os.path.join(dirData, musc, seg, 'MN{:d}'.format(fidx), 'axon', 'potentials',
                                    'elec{:d}.txt'.format(elec))
        else:
            filePath = os.path.join(dirData, musc, seg, 'MN{:d}'.format(fidx), 'axon', 'potentials',
                                    '{}.txt'.format(elec))

        # Read file content and return.
        eext = np.loadtxt(filePath)
        return eext

    def _switch_on(self):
        """Assign the values of eext to the e_extracellular batteries of the MotorAxon's sections."""
        for sec, e in zip(self.axon.get_sections(), self.eext):
            sec.e_extracellular = e

    def _switch_off(self):
        """Assign 0.0 to the e_extracellular batteries of the MotorAxon's sections."""
        for sec in self.axon.get_sections():
            sec.e_extracellular = 0.0

    # def activate(self):
    #     """Perform the specific actions to activate the stimulation.
    #
    #     Here it consists of pushing two events in NEURON's integrator event queue:
    #         1 at time t=delay to switch on the extracellular batteries
    #         1 at time t=delay+dur to switch off the extracellular batteries
    #
    #     """
    #     h.cvode.event(self._delay, self._switch_on)
    #     h.cvode.event(self._delay + self._dur, self._switch_off)

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Here it consists in building vectors to be played into the extracellular mechanism of the axon sections.

        """
        self._set_stim_vecs()

    def _set_stim_vecs(self):
        """Build the vectors to be played in the sections' extracellular mechanism.

        The vectors implement finite rise and fall time of the extracellular potential distribution.
        Rise and fall times are both set to _dur/100.

        (delay + dur/100)     (delay + dur)
                 |                |
                 __________________
                /                  \
               /                    \
        ______/                      \___________________________

              |                      |
           delay                    (delay + dur + dur/100)

        """
        self._stim_vecs = []

        # Time points
        t1 = self._delay
        t2 = self._delay + self._dur * 0.01
        t3 = self._delay + self._dur
        t4 = self._delay + self._dur * 1.01
        t5 = self._delay + self._dur * 1.02
        tvec = h.Vector([0.0, t1, t2, t3, t4, t5])

        # Unitary voltage vector.
        vvec = np.asarray([0.0, 0.0, 1.0, 1.0, 0.0, 0.0])

        # Build stim vectors.
        for sec, e in zip(self.axon.get_sections(), self.eext):
            self._stim_vecs.append(h.Vector(e * vvec))
            self._stim_vecs[-1].play(sec(0.5)._ref_e_extracellular, tvec, 1)
        self._stim_vecs.append(tvec)

    def isempty(self):
        """Return a boolean indicating whether the MNAEextStim is empty or not.

        This is the case if eext is None.

        """
        return self.eext is None

###########################################################################
