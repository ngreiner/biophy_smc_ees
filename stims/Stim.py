# -*- coding: utf-8 -*


###########################################################################
# Class Stim.
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# Date created: 25 June 2018
# Last modified: 25 June 2018
#
###########################################################################


class Stim(object):
    """
    This class regroups attributes and methods describing a stimulation applied to a neural entity.

    Class variables defined here:
        _amp    : -200.0    : default amplitude of stimulation (in uA).
        _delay  : 5.0       : default delay of stimulation onset (in ms).
        _dur    : 0.2       : default stimulation duration (in ms).

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        amp(self, *args)
        delay(self, *args)
        dur(self, *args)
        activate(self)
        isempty(self)

    """

    _amp = - 200.0     # uA
    _delay = 5.0       # ms
    _dur = 0.2         # ms

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            amp     : float : amplitude of stimulation (in uA).
            delay   : float : delay of stimulation onset (in ms).
            dur     : float : duration of stimulation onset (in ms).

        """

        try:
            self.amp(kwargs['amp'])
        except:
            pass

        try:
            self.delay(kwargs['delay'])
        except:
            pass

        try:
            self.dur(kwargs['dur'])
        except:
            pass

    def __repr__(self):
        """Return the string representation of the Stim."""
        return '{}_{:d}'.format(self.__class__.__name__, id(self))

    def amp(self, *args):
        """Return (without args) or set and return (with args) the attribute _amp."""
        if len(args) > 0:
            self._amp = args[0]
        return self._amp

    def delay(self, *args):
        """Return (without args) or set and return (with args) the attribute _delay."""
        if len(args) > 0:
            self._delay = args[0]
        return self._delay

    def dur(self, *args):
        """Return (without args) or set and return (with args) the attribute _dur."""
        if len(args) > 0:
            self._dur = args[0]
        return self._dur

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Purely virtual method (overridden in subclasses).

        """
        pass

    def isempty(self):
        """Return a boolean indicating whether the Stim is empty or not.

        Default is True (overridden in subclasses).

        """
        return True

###########################################################################
