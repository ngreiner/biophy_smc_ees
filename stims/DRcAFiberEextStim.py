# -*- coding: utf-8 -*


###########################################################################
# Class DRcAFiberEextStim.
#
# Created on: 25 November 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np

from neuron import h

from biophy_smc_ees.stims.DRFiberEextStim import DRFiberEextStim

h.load_file('stdrun.hoc')

class DRcAFiberEextStim(DRFiberEextStim):
    """
    This class refines the DRFiberEextStim class to represent extracellular stimulations applied to DRcAFibers.

    Methods defined here:
        _read_elec_eext(self, elec)             -- overrides DRFiberEextStim._read_elec_eext

    """

    def _read_elec_eext(self, elec):
        """Read the extracellular potential distribution elicited by the input elec from the appropriate textfile.

        Arguments:
            elec    : str/int   : name/index of electrode.

        Return:
            eext    : dict  : the extracellular potential distribution elicited by the input elec along the DRcAbranch
                              of the DRcAFiber for a normalized current amplitude of 1uA.

        Description: there is only one key to eext, which is 'DRbranch'. The value attached to it is a numpy array
        giving the potentials at the compartments of the DRbranch of the DRcAFiber.

        """

        dir_data = self.fiber.smc().dir_data
        musc = self.fiber.musc()
        seg = self.fiber.seg()
        fidx = self.fiber.fidx()

        # Read content of file.
        if isinstance(elec, int):
            file_path = os.path.join(dir_data, musc, seg, 'DRcAFiber{:d}'.format(fidx), 'potentials',
                                    'elec{:d}.txt'.format(elec))
        else:
            file_path = os.path.join(dir_data, musc, seg, 'DRcAFiber{:d}'.format(fidx), 'potentials',
                                    '{}.txt'.format(elec))
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # Form eext dictionnary.
        eext = dict()
        eext['DRbranch'] = np.array([float(e) for e in file_lines.pop(0).split()])

        return eext

###########################################################################
