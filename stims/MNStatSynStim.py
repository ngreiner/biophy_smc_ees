# -*- coding: utf-8 -*


###########################################################################
# Class MNStatSynStim.
#
# Created on: 12 February 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from scipy import random as rand
from neuron import h
from biophy_smc_ees.utils.utilities import gen_seed

h.load_file('stdrun.hoc')


class MNStatSynStim(object):
    """
    This class regroups attributes and methods to describe a static synaptic stimulation applied to a motoneuron.

    Class variables defined here:
        _delay  : 5.0       : default synapse onsets' delay (in ms).
        _tau    : 0.2       : default synapse time constant (in ms).
        _gmax   : 5.0       : default synapse maximal conductance (in pS).
        _e      : 0.0       : default synapse reversal potential (in mV).
        _syn_distrib    : 'groups'  : default mode for the synapses distribution.

    Instance variables defined here:
        mn      : Motoneuron object     : to which the synaptic stimulation is applied.
        syns    : list of tuples        : describing the synapses.
        hsyns   : list of HOC synapses  : actual synapses.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        delay(self, *args)
        reset(self, **kwargs)
        isempty(self)
        activate(self)
        _set_syns(self)
        _set_hsyns(self)
        _build_alpha_syn(self, **kwargs)

    """

    _delay = 5.0                # default synapse onsets' delay (in ms).
    _tau = 0.2                  # default synapse time constant (in ms).
    _gmax = 5.0                 # default synapse maximal conductance (in pS).
    _e = 0.0                    # default synapse reversal potential (in mV).
    _syn_distrib = 'groups'     # default mode for the synapses distribution.

    def __init__(self, mn, **kwargs):
        """Class constructor method.

        Arguments:
            mn  : Motoneuron object : to which the synaptic stimulation is applied.

        Keyword arguments:
            delay   : float : synapse onsets' delay (in ms).
            tau     : float : synapse time constant (in ms).
            gmax    : float : synapse maximal conductance (in pS).
            e       : float : synapse reversal potential (in mV).
            syn_distrib : str   : mode for the synapses distribution.

        """

        self.mn = mn
        self.syns = []
        self.hsyns = []

        try:
            self._delay = kwargs['delay']
        except:
            pass

        try:
            self._tau = kwargs['tau']
        except:
            pass

        try:
            self._gmax = kwargs['gmax']
        except:
            pass

        try:
            self._e = kwargs['e']
        except:
            pass

        try:
            self._syn_distrib = kwargs['syn_distrib']
        except:
            pass

        try:
            rand.seed(gen_seed(mn.smc(), mn.__class__.__name__, mn.gidx()))
        except:
            pass

        try:
            rand.seed(kwargs['seed'])
        except:
            pass

    def __repr__(self):
        """Return the string representation of the MNStatSynStim."""
        return '{}.{}'.format(self.__class__.__name__, self.mn.name)

    def delay(self, *args):
        """Return (without args) or set and return (with args) the attribute _delay."""
        if len(args) > 0:
            self._delay = args[0]
        return self._delay

    def _set_syns(self):
        """Build the synapse descriptors syns.

        Purely virtual method (overridden in children classes)."""
        pass

    def _set_hsyns(self):
        """Build the actual HOC synapses hsyns.

        Purely virtual method (overridden in children classes)."""
        pass

    def _build_alpha_syn(self, hsec, **kwargs):
        """Build an AlphaSynapse object on a HOC section.

        Arguments:
            hsec    : HOC section   : at the center of which the synapse shall be built.

        Keyword arguments:
            onset   : float : time of onset of conductance transient (in ms).
            tau     : float : synapse time constant (in ms).
            gmax    : float : synapse maximal conductance (in nS).
            e       : float : synapse reversal potential (in mV).

        Return:
            syn: AlphaSynapse object.
        """

        try:
            onset = kwargs['onset']
        except:
            onset = self._delay

        try:
            tau = kwargs['tau']
        except:
            tau = self._tau

        try:
            gmax = kwargs['gmax']
        except:
            gmax = self._gmax

        try:
            e = kwargs['e']
        except:
            e = self._e

        hsyn = h.AlphaSynapse(hsec(0.5))
        hsyn.onset = onset
        hsyn.tau = tau
        hsyn.gmax = gmax * 1e-3
        hsyn.e = e

        return hsyn

    def reset(self, **kwargs):
        """Reset the MNStatSynStim.

        Keyword arguments:
            delay       : float : synapse onsets' delay (in ms).
            tau         : float : synapse time constant (in ms).
            gmax        : float : synapse maximal conductance (in pS).
            e           : float : synapse reversal potential (in mV).
            syn_distrib : str   : mode for the synapses distribution.
            seed        : int   : new seed for the random number generator.

        An attempt to seed the random number generator using the Motoneuron's gidx is made first, before an attempt
        to seed it from the eventual dedicated input argument is made.

        """

        try:
            self._delay = kwargs['delay']
        except:
            pass

        try:
            self._tau = kwargs['tau']
        except:
            pass

        try:
            self._gmax = kwargs['gmax']
        except:
            pass

        try:
            self._e = kwargs['e']
        except:
            pass

        try:
            self._syn_distrib = kwargs['syn_distrib']
        except:
            pass

        try:
            rand.seed(gen_seed(self.mn.smc(), self.mn.__class__.__name__, self.mn.gidx()))
        except:
            pass

        try:
            rand.seed(kwargs['seed'])
        except:
            pass

    def activate(self):
        """Perform the specific actions to activate the stimulation.

        Here it consists in building the HOC synapses.

        """
        self._set_hsyns()
        h.cvode.re_init()

    def isempty(self):
        """Return a boolean indicating whether the stim is empty or not.

        This is the case if syns is empty.

        """
        return len(self.syns) == 0

###########################################################################
