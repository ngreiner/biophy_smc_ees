# -*- coding: utf-8 -*


#############################################################################
# Class SomaSec.
#
# Date created: 7 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np

from biophy_smc_ees.neural_entities.cell_secs.CellSec import CellSec


class SomaSec(CellSec):
    """
    This class refines the CellSec class to represent somatic sections.

    Methods defined here:
        __init__(self, **kwargs)        -- extends CellSec.__init__
        get_Gm(self)                    -- overrides CellSec.get_Gm
        get_Gin(self)

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            comp    : str               : indicator of the compartment of the cell to which the CellSec belongs.
            name    : str               : name of the CellSec.
            par     : CellSec object    : parent CellSec of the CellSec.

        """
        super(SomaSec, self).__init__(comp='soma', **kwargs)

    def get_Gm(self):
        """Return the membrane specific conductance of the SomaSec (in S / um^2)."""
        try:
            return self.hsec.g_pas * 1e-8
        except:
            return self.hsec.gl_motoneuron * 1e-8

    def get_Gin(self):
        """Return the total passive input conductance of the SomaSec (in Ohm)."""
        return self.get_Gm() * np.pi * self.hsec.diam * self.hsec.L

#############################################################################
