# -*- coding: utf-8 -*


#############################################################################
# Class DendStem.
#
# Date created: 8 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.cell_secs.DendSec import DendSec


class DendStem(DendSec):
    """
    This class refines the DendSec class to represent dendritic stems (first sections of dendrites).

    Class variables defined here:
        isastem : True  :   indicator that the DendSec is a DendStem.

    Methods defined here:
        __init__(self, soma, **kwargs)      -- extends DendSec.__init__

    Dendritic stems are distinct from non-stem dendritic sections in that they may have siblings. Instead of being
    connected to the far extremity of the DendStem section (its 1 end in NEURON), siblings of a DendStem are
    connected to the proximal end of the DendStem section (its 0 end in NEURON). This peculiarity comes from the swc
    file format itself, in which branches sometimes originate from the first point of such stems instead of
    originating from the soma.

    """

    isastem = True

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            name    : str               : name of the DendStem.
            par     : CellSec object    : parent CellSec of this DendSec.

        """
        super(DendStem, self).__init__(**kwargs)
        self.siblings = []

#############################################################################
