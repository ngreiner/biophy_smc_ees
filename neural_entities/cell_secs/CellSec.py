# -*- coding: utf-8 -*


#############################################################################
# Class CellSec.
#
# Created on: 8 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
import weakref
from neuron import h


class CellSec(object):
    """
    This class regroups attributes and methods describing a section of a cellular compartment (dendritic tree or soma).

    Instance variables defined here:
        comp    : str               : indicator of the compartment of the cell to which the CellSec belongs.
        name    : str               : name of the CellSec.
        par     : weakref.ref       : weakref to the parent CellSec object of the CellSec.
        hsec    : h.Section object  : HOC section implementing the biophysical characteristics of the CellSec.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        get_D(self)
        get_L(self)
        get_area(self)
        get_n3d(self)
        get_diam3ds(self)
        get_x3ds(self)
        get_y3ds(self)
        get_z3ds(self)
        get_p3ds(self)
        get_l3ds(self)
        get_center3d(self)
        get_Rm(self)
        get_Ri(self)
        get_lambda(self)
        get_L_elec(self)
        get_generalized_L_elec(self)

    The CellSec class is a base class providing a container for a HOC section embedded in a model of cell.

    The CellSec class provides methods to extract geometrical and biophysical information about the HOC section it
    encapsulates. Besides, the CellSec class provides a mechanism to easily keep track of parenthood relationships
    between different such sections (the 'par' attribute).

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            comp    : str               : indicator of the compartment of the cell to which the CellSec belongs.
            name    : str               : name of the CellSec.
            par     : CellSec object    : parent CellSec of the CellSec.

        """
        self.name = '{}_{:d}'.format(self.__class__.__name__, id(self))
        self.comp = None
        self.par = lambda: None
        self.hsec = None

        try:
            self.name = kwargs['name']
        except:
            pass

        try:
            self.comp = kwargs['comp']
        except:
            pass

        try:
            self.par = weakref.ref(kwargs['par'])
        except:
            pass

    def __repr__(self):
        """Return the string representation of the CellSec."""
        return self.name

    ##############################################################################################################
    # Accessors of geometric parameters of the CellSec.
    ##############################################################################################################

    def get_D(self):
        """Return the diameter of the CellSec (in um)."""
        return self.hsec.diam

    def get_L(self):
        """Return the length of the CellSec (in um)."""
        return self.hsec.L

    def get_area(self):
        """Return the area of the CellSec (in um^2)."""
        return self.hsec(0.5).area()

    def get_n3d(self):
        """Return the number of 3D-points defining the geometry of the CellSec."""
        return int(h.n3d(sec=self.hsec))

    def get_diam3ds(self):
        """Return the diameters of the 3D-points defining the geometry of the CellSec (in um)."""
        n = self.get_n3d()
        diam3ds = np.zeros(n)
        for i in range(n):
            diam3ds[i] = h.diam3d(i, sec=self.hsec)
        return diam3ds

    def get_x3ds(self):
        """Return the x-coordinates of the 3D-points defining the geometry of the CellSec (in um)."""
        n = self.get_n3d()
        x3ds = np.zeros(n)
        for i in range(n):
            x3ds[i] = h.x3d(i, sec=self.hsec)
        return x3ds

    def get_y3ds(self):
        """Return the y-coordinates of the 3D-points defining the geometry of the CellSec (in um)."""
        n = self.get_n3d()
        y3ds = np.zeros(n)
        for i in range(n):
            y3ds[i] = h.y3d(i, sec=self.hsec)
        return y3ds

    def get_z3ds(self):
        """Return the z-coordinates of the 3D-points defining the geometry of the CellSec (in um)."""
        n = self.get_n3d()
        z3ds = np.zeros(n)
        for i in range(n):
            z3ds[i] = h.z3d(i, sec=self.hsec)
        return z3ds

    def get_p3ds(self):
        """Return the coordinates of the 3D-points defining the geometry of the CellSec (in um)."""
        x3ds = self.get_x3ds()
        y3ds = self.get_y3ds()
        z3ds = self.get_z3ds()
        p3ds = np.zeros((x3ds.size, 3))
        p3ds[:, 0] = x3ds
        p3ds[:, 1] = y3ds
        p3ds[:, 2] = z3ds
        return p3ds

    def get_l3ds(self):
        """Return the lengths of the segments joining the 3D-points defining the geometry of the CellSec (in um)."""
        p3ds = self.get_p3ds()
        l3ds = np.linalg.norm(np.diff(p3ds, axis=0), axis=1)
        return l3ds

    def get_center3d(self):
        """Return the coordinates of the center point of the CellSec (in um).

        This is the average between the first and last 3d-points defining the geometry of the CellSec.

        """
        p3ds = self.get_p3ds()
        return (p3ds[0, :] + p3ds[-1, :]) / 2.0

    ##############################################################################################################
    # Accessors of biophysical parameters of the DendSec.
    ##############################################################################################################

    def get_Gm(self):
        """Return the membrane specific conductance of the CellSec (in S / um^2).

        Purely virtual method (overridden in children classes).

        """
        return np.nan

    def get_Rm(self):
        """Return the membrane specific resistance of the CellSec (in Ohm * um^2)."""
        return 1.0 / self.get_Gm()

    def get_Ri(self):
        """Return the axoplasmic resistivity of the CellSec (in Ohm * um)."""
        return self.hsec.Ra * 1e4

    def get_lambda(self):
        """Return the space constant ('lambda') of the CellSec (in um).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        return np.sqrt(self.get_D() * self.get_Rm() / (4 * self.get_Ri()))

    def get_L_elec(self):
        """Return the electrotonic length of the CellSec (adimensional).

        Description: see 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        return self.get_L() / self.get_lambda()

    def get_generalized_L_elec(self):
        """Return the generalized electrotonic length of the CellSec (adimensional), obtained using its 3d info.

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """

        # Retrieve geometric information of CellSec.
        npts = self.get_n3d()
        ds = self.get_diam3ds()
        ls = self.get_l3ds()

        # We will need the function below.
        # f computes the generalized electrotonic length of a single portion of section of length l with linear
        # variation of diameter from d1 to d2.
        def f(l, d1, d2):
            if d1 == d2:
                return np.sqrt(4 * self.get_Ri() / self.get_Rm()) * l / d1 ** 0.5
            else:
                return np.sqrt(4 * self.get_Ri() / self.get_Rm()) * l * 2.0 * (d2 ** 0.5 - d1 ** 0.5) / (d2 - d1)

        # Compute generalized electrotonic length of the DendSec.
        gen_L_elec = 0.0
        for i in range(npts - 1):
            gen_L_elec += f(ls[i], ds[i], ds[i + 1])

        return gen_L_elec

#############################################################################
