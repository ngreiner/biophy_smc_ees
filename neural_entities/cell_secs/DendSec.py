# -*- coding: utf-8 -*


#############################################################################
# Class DendSec.
#
# Created on: 8 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np

from biophy_smc_ees.neural_entities.cell_secs.CellSec import CellSec


class DendSec(CellSec):
    """
    This class refines the CellSec class to represent dendritic sections.

    Class variables defined here:
        isastem     : False : indicator that the DendSec is not a DendStem.
        siblings    : None  : siblings of the DendSec (is overridden in class DendStem).

    Instance variables defined here:
        lchild  : DendSec object    : left child of the DendSec.
        rchild  : DendSec object    : right child of the DendSec.

    Methods defined here:
        __init__(self, **kwargs)                    -- extends CellSec.__init__
        get_dist_from_soma(self)
        get_longest_path_length(self)
        get_Gm(self)                                -- overrides CellSec.get_Gm
        get_B1(self)
        get_B0(self)
        get_Ginf(self)
        get_Gin(self)
        get_elec_dist_from_soma(self)
        rename_offspring(self, basename=None)

    The DendSec class refines the machinery of the CellSec class to keep track of parenthood relationships by
    introducing the 'lchild' and 'rchild' attributes, respectively standing for 'left child' and 'right child'.

    As such, the DendSec class features the essential characteristics of a binary tree node. In fact,
    DendSec instances are meant to be assembled to form dendritic trees (see class DendTree) which are (almost)
    binary trees (but see DendStem class).

    Besides the DendSec class provides methods to inspect the electrotonic properties of the dendritic sections it
    represents and of the dendritic trees in which they may be inserted by leveraging these parenthood relationships.

    """

    isastem = False
    siblings = None

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            name    : str               : name of the DendSec.
            par     : CellSec object    : parent CellSec of the DendSec.

        """
        super(DendSec, self).__init__(comp='dend', **kwargs)
        self.lchild = None
        self.rchild = None

    ##############################################################################################################
    # Accessors of geometric parameters of the DendSec.
    ##############################################################################################################

    def get_dist_from_soma(self):
        """Return the distance from soma of the DendSec (in um).

        Here the distance from soma of the DendSec is understood to be the distance of its middle point from the soma.

        """
        if self.isastem:
            return self.get_L() / 2.0
        else:
            return self.get_L() / 2.0 + self.par().get_dist_from_soma() + self.par().get_L() / 2.0

    def get_longest_path_length(self):
        """Return the length of the longest dendritic path originating from the DendSec (in um).

        Here the longest dendritic path originating from the DendSec is understood to be the longest dendritic path
        originating from its middle point.

        """
        if self.lchild is None:
            return self.get_L() / 2.0
        elif self.rchild is None:
            return self.get_L() / 2.0 + self.lchild.get_L() / 2.0 + self.lchild.get_longest_path_length()
        else:
            return max(self.get_L() / 2.0 + self.lchild.get_L() / 2.0 + self.lchild.get_longest_path_length(),
                       self.get_L() / 2.0 + self.rchild.get_L() / 2.0 + self.rchild.get_longest_path_length())

    ##############################################################################################################
    # Accessors of biophysical parameters of the DendSec.
    ##############################################################################################################

    def get_Gm(self):
        """Return the membrane specific conductance of the DendSec (in S / um^2)."""
        return self.hsec.g_pas * 1e-8

    def get_B1(self):
        """Return the parameter B1 of the DendSec (adimensional).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        if self.lchild is None:
            return 0.0
        else:
            B21 = self.lchild.get_B0() * np.power(self.lchild.get_D() / self.get_D(), 1.5)
            if self.rchild is None:
                B22 = 0.0
            else:
                B22 = self.rchild.get_B0() * np.power(self.rchild.get_D() / self.get_D(), 1.5)
            return B21 + B22

    def get_B0(self):
        """Return the parameter B0 of the DendSec (adimensional).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        B1 = self.get_B1()
        L = self.get_L_elec()
        return (B1 + np.tanh(L)) / (1 + B1 * np.tanh(L))

    def get_Ginf(self):
        """Return the reference input conductance of the DendSec (in Ohm).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        ri = 4 * self.get_Ri() / (np.pi * self.get_D() ** 2)
        lam = self.get_lambda()
        return 1 / (ri * lam)

    def get_Gin(self):
        """Return the input conductance of the DendSec (in Ohm).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        return self.get_Ginf() * self.get_B0()

    def get_elec_dist_from_soma(self):
        """Return the electrotonic distance from soma of the DendSec (adimensional).

        Here, the electrotonic distance from soma of a DendSec is understood to be the electrotonic distance from
        soma of its middle point. Besides, the definition which is adopted here for the electrotonic distance from
        soma of any point in a dendritic tree is similar to that employed in 'Horseradish Peroxidase Study of the
        Spatial and Electrotonic Distribution of Group Ia Synapses on Type-Identified Ankle Extensor Motoneurons in
        the Cat', Burke and Glenn, The Journal of Comparative Neurology, 1996. It is the sum of the electrotonic
        lengths of every segment joining the soma to the considered point.

        """
        if self.isastem:
            return self.get_L_elec() / 2.0
        else:
            return self.get_L_elec() / 2.0 + self.par().get_elec_dist_from_soma() + self.par().get_L_elec() / 2.0

    ##############################################################################################################
    # Misc.
    ##############################################################################################################

    def rename_offspring(self, basename=None):
        """Rename the offspring of the DendSec.

        Keyword arguments:
            basename: str: basename for offspring's names.

        If basename is not passed in argument, it defaults to self.name.

        """
        if basename is None:
            basename = self.name

        if self.lchild:
            self.lchild.name = basename + '0'
            self.lchild.rename_offspring()
        if self.rchild:
            self.rchild.name = basename + '1'
            self.rchild.rename_offspring()

#############################################################################
