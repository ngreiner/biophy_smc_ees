# -*- coding: utf-8 -*


#############################################################################
# Class Cell.
#
# Created on: 26 February 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.NeuralEntity import NeuralEntity


class Cell(NeuralEntity):
    """
    This class refines the NeuralEntity class to represent neural cells.

    Instance variables defined here:
        soma    : Soma object.

    Methods defined here:
        __init__(self, **kwargs)        -- extends NeuralEntity.__init__

    The Cell class is an abstract class defining the attributes and methods common to all its derived classes. This
    consists only in the __init__method and the attribute soma.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """
        super(Cell, self).__init__(**kwargs)
        self.soma = None

#############################################################################
