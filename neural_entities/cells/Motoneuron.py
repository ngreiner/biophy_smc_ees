# -*- coding: utf-8 -*


#############################################################################
# Class Motoneuron.
#
# Created on: 4 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from scipy import random as rand

from biophy_smc_ees.neural_entities.cells.Cell import Cell
from biophy_smc_ees.neural_entities.cell_comps.Soma import Soma
from biophy_smc_ees.neural_entities.cell_comps.MultiTapSoma import MultiTapSoma
from biophy_smc_ees.neural_entities.cell_comps.DendTree import DendTree
from biophy_smc_ees.neural_entities.cell_comps.MotorAxon import MotorAxon
from biophy_smc_ees.utils.resources import swc_files
from biophy_smc_ees.utils.utilities import gen_seed


class Motoneuron(Cell):
    """
    This class refines the Cell class to represent exclusively motoneurons.

    Instance variables defined here:
        dtree       : DendTree object.
        axon        : cell_comps.MotorAxon object.
        _with_dtree : bool  : indicator whether the Motoneuron possesses a DendTree or not.
        _with_axon  : bool  : indicator whether the Motoneuron possesses a MotorAxon or not.
        _morpho     : dict  : morphological parameters of the Motoneuron.
        _model      : dict  : model specifications for the Motoneuron.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Cell.__init__
        diam(self, *args)
        soma_model(self, *args)
        soma_morpho(self, *args)
        with_axon(self, *args)
        axon_model(self, *args)
        axon_nN(self, *args)
        axon_diam(self, *args)
        with_dtree(self, *args)
        dtree_model(self, *args)
        swc_file(self, *args)
        extra_mech(self, *args)
        _read_morpho_info(self)
        _build(self)
        _create_compartments(self)
        _connect_dtree(self)
        _connect_axon(self)
        reset(self, **kwargs)           -- extends NeuralEntity.reset
        _ready_2_build(self)            -- overrides NeuralEntity._ready_2_build

    We represent a motoneuron as composed of a soma, a dendritic tree and an axon.

    The soma can be represented either by a single cylindrical compartment, either by a series of truncated conical
    compartments (one for each dendrite plus one for the axon) as is done in 'Extracellular Stimulation of Central
    Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al., Journal of
    Neurophysiology, 2002.

    The dendritic tree is a binary tree of dendritic sections (see DendTree class) described by a swc file (see
    http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html for a
    description of the swc format).

    The axon is represented by a single unbranching myelinated nerve branch comprising an explicit initial segment.

    The biophysical properties of every HOC section composing the motoneuron are taken from 'Extracellular
    Stimulation of Central Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output', McIntyre et
    al., Journal of Neurophysiology, 2002. Several models are available for the axon (see MotorBranch class). We also
    prepared the basic machinery to allow specifying a model for the soma and dendritic tree, but there is only one
    model available for each at present, and trying to specify one with the dedicated keywords of the Motoneuron
    constructor will have no effect.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Usage:
            mn = Motoneuron(smc=some_smc, gidx=some_gidx)
            mn = Motoneuron(smc=some_smc, info=(musc, seg, fidx))
            mn = Motoneuron(smc=some_smc, musc=some_musc, seg=some_seg, fidx=some_fidx)
            mn = Motoneuron(diam=diam, soma_morpho=soma_morpho, with_axon=with_axon, axon_model=axon_model,
            axon_nN=axon_nN, axon_diam=axon_diam, with_dtree=with_dtree, dtree_model=dtree_model, swc_file=swc_file,
             extra_mech=extra_mech)

        Keyword arguments:
            smc         : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc        : str           : name of the muscle to which the NeuralEntity belongs.
            seg         : str           : name of the segment in which the NeuralEntity is positioned.
            fidx        : int           : index of the NeuralEntity in the (musc, seg) family.
            info        : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx        : int           : global index of the NeuralEntity in the sensorimotor circuit.
            diam        : float         : diameter of the Motoneuron.
            soma_model  : str           : identifier of model of soma of the Motoneuron.
            soma_morpho : str           : identifier of morphology of soma of the Motoneuron.
            with_axon   : bool          : indicator whether axon should be built or not.
            axon_model  : str           : identifier of model of axon of the Motoneuron.
            axon_nN     : int           : number of nodes of Ranvier of axon of the Motoneuron.
            axon_diam   : float         : diameter of axon of the Motoneuron.
            with_dtree  : bool          : indicator whether dtree should be built or not.
            dtree_model : str           : identifier of model of dtree of the Motoneuron.
            swc_file    : str           : path to swc file.
            extra_mech  : bool          : indicator whether extracellular mechanism should be used or not.

        There are 2 ways to build a Motoneuron:

        1. Specify its identity via the specification of an SMC object and appropriate identification information (
        its global index, or the triplet of its musc, seg, and family index in the (musc, seg) family -- see class
        NeuralEntity). If the specified information is valid, the morphological parameters necessary to build the
        Motoneuron are read from textfiles.

        2. Specify directly these morphological parameters via the dedicated keywords.

        If none are specified, the instantiation of the Motoneuron object will succeed, but the Motoneuron (its
        compartments) will not effectively be built. A subsequent call to reset with the appropriate keyword
        arguments will be necessary to do so.

        By default, the Motoneuron is expected to embed an axon and a dendritic tree, and specifying the diameter of
        the Motoneuron, the diameter of its axon, and the number of nodes of Ranvier of its axon is necessary to
        build the Motoneuron. A path to a swc file describing the dendritic tree is also necessary to build the
        Motoneuron, but a default swc file is assigned to the Motoneuron at instantiation time, so specifying one is
        not required, though allowed. This default assignment makes use of a pseudo-random number generator. This
        pseudo-random number generator is initialized with a seed determined based on the Motoneuron's global index.
        If the Motoneuron doesn't have one, the pseudo-random generator seed is left unmodified.

        """

        super(Motoneuron, self).__init__(**kwargs)

        # Seed random generator for Motoneuron if applicable.
        if self._smc is not None and self._gidx is not None:
            rand.seed(gen_seed(self._smc.name, self.__class__.__name__, self._gidx))

        # Public attributes.
        self.dtree = None
        self.axon = None

        # Private attributes.
        self._with_dtree = True
        self._with_axon = True

        self._morpho = dict()
        self._morpho['diam'] = None
        self._morpho['soma'] = 'multitap'
        self._morpho['axon'] = dict()
        self._morpho['axon']['nN'] = None
        self._morpho['axon']['diam'] = None
        self._morpho['dtree'] = dict()
        self._morpho['dtree']['swc_file'] = rand.choice(swc_files)

        self._model = dict()
        self._model['soma'] = None
        self._model['axon'] = 'ModelD'
        self._model['dtree'] = None
        self._model['extra_mech'] = False

        # Parse inputs.
        try:
            self.diam(kwargs['diam'])
        except:
            pass

        try:
            self.soma_model(kwargs['soma_model'])
        except:
            pass

        try:
            self.soma_morpho(kwargs['soma_morpho'])
        except:
            pass

        try:
            self.with_axon(kwargs['with_axon'])
        except:
            pass

        try:
            self.axon_model(kwargs['axon_model'])
        except:
            pass

        try:
            self.axon_nN(kwargs['axon_nN'])
        except:
            pass

        try:
            self.axon_diam(kwargs['axon_diam'])
        except:
            pass

        try:
            self.with_dtree(kwargs['with_dtree'])
        except:
            pass

        try:
            self.dtree_model(kwargs['dtree_model'])
        except:
            pass

        try:
            self.swc_file(kwargs['swc_file'])
        except:
            pass

        try:
            self.extra_mech(kwargs['extra_mech'])
        except:
            pass

        if self._has_ID():
            self._read_morpho_info(diam=self.diam())

        if self._ready_2_build():
            self._build()

    ##############################################################################################################
    # Mutators and accessors.
    ##############################################################################################################

    def diam(self, *args):
        """Return (without args) or set and return (with args) the value of _morpho['diam'].

        Optional arguments:
            diam    : float : Motoneuron diameter (in um).

        Additional actions:
            Set _need_2_build to True if _morpho['diam'] is modified.

        """
        if len(args) > 0:
            if self._morpho['diam'] != args[0]:
                self._morpho['diam'] = args[0]
                self._need_2_build = True
        return self._morpho['diam']

    def soma_model(self, *args):
        """Return (without args) or set and return (with args) the value of _model['soma'].

        Optional arguments:
            soma_model  : str   : identifier of soma model.

        Additional actions:
            Set _need_2_build to True if _model['soma'] is modified.

        """
        if len(args) > 0:
            if self._model['soma'] != args[0]:
                self._model['soma'] = args[0]
                self._need_2_build = True
        return self._model['soma']

    def soma_morpho(self, *args):
        """Return (without args) or set and return (with args) the value of _morpho['soma'].

        Optional arguments:
            soma_morpho : str   : identifier of model of soma of the Motoneuron.

        Additional actions:
            Set _need_2_build to True if _morpho['soma'] is modified.

        """
        if len(args) > 0:
            if self._morpho['soma'] != args[0]:
                self._morpho['soma'] = args[0]
                self._need_2_build = True
        return self._morpho['soma']

    def with_axon(self, *args):
        """Return (without args) or set and return (with args) the attribute _with_axon.

        Optional arguments:
            with_axon   : bool  : indicator of whether axon should be built or not.

        Additional actions:
            Set _need_2_build to True if _with_axon is modified.

        """
        if len(args) > 0:
            if self._with_axon != args[0]:
                self._with_axon = args[0]
                self._need_2_build = True
        return self._with_axon

    def axon_model(self, *args):
        """Return (without args) or set and return (with args) the value of _model['axon'].

        Optional arguments:
            axon_model  : str   : identifier of axon model.

        Additional actions:
            Set _need_2_build to True if _model['axon'] is modified.

        """
        if len(args) > 0:
            if self._model['axon'] != args[0]:
                self._model['axon'] = args[0]
                self._need_2_build = True
        return self._model['axon']

    def axon_nN(self, *args):
        """Return (without args) or set and return (with args) the value of _morpho['axon']['nN'].

        Optional arguments:
            axon_nN : int   : number of nodes of Ranvier of axon.

        Additional actions:
            Set _need_2_build to True if _morpho['axon']['nN'] is modified.

        """
        if len(args) > 0:
            if 'axon' not in self._morpho.keys():
                self._morpho['axon'] = dict()
            if self._morpho['axon']['nN'] != args[0]:
                self._morpho['axon']['nN'] = args[0]
                self._need_2_build = True
        return self._morpho['axon']['nN']

    def axon_diam(self, *args):
        """Return (without args) or set and return (with args) the value of _morpho['axon']['diam'].

        Optional arguments:
            axon_diam   : float : axon diameter (in um).

        Additional actions:
            Set _need_2_build to True if _morpho['axon']['diam'] is modified.

        """
        if len(args) > 0:
            if 'axon' not in self._morpho.keys():
                self._morpho['axon'] = dict()
            if self._morpho['axon']['diam'] != args[0]:
                self._morpho['axon']['diam'] = args[0]
                self._need_2_build = True
        return self._morpho['axon']['diam']

    def with_dtree(self, *args):
        """Return (without args) or set and return (with args) the attribute _with_dtree.

        Optional arguments:
            with_dtree  : bool  : indicator of whether dtree should be built or not.

        Additional actions:
            Set _need_2_build to True if _with_dtree is modified.

        """
        if len(args) > 0:
            if self._with_axon != args[0]:
                self._with_axon = args[0]
                self._need_2_build = True
        return self._with_axon

    def dtree_model(self, *args):
        """Return (without args) or set and return (with args) the value of _model['dtree'].

        Optional arguments:
            dtree_model : str   : identifier of dtree model.

        Additional actions:
            Set _need_2_build to True if _model['dtree'] is modified.

        """
        if len(args) > 0:
            if self._model['dtree'] != args[0]:
                self._model['dtree'] = args[0]
                self._need_2_build = True
        return self._model['dtree']

    def swc_file(self, *args):
        """Return (without args) or set and return (with args) the value of _morpho['dtree']['swc_file'].

        Optional arguments:
            swc_file    : str   : path to swc file.

        Additional actions:
            Set _need_2_build to True if _morpho['dtree']['swc_file'] is modified.

        """
        if len(args) > 0:
            if self._morpho['dtree']['swc_file'] != args[0]:
                self._morpho['dtree']['swc_file'] = args[0]
                self._need_2_build = True
        return self._morpho['dtree']['swc_file']

    def extra_mech(self, *args):
        """Return (without args) or set and return (with args) the value of _model['extra_mech'].

        Optional arguments:
            extra_mech  : bool  : indicator of whether extracellular mechanism shall be inserted in the Motoneuron.

        Additional actions:
            Set _need_2_build to True if _model['extra_mech'] is modified.

        """
        if len(args) > 0:
            if self._model['extra_mech'] != args[0]:
                self._model['extra_mech'] = args[0]
                self._need_2_build = True
        return self._model['extra_mech']

    def _read_morpho_info(self, diam=None):
        """Read the morphology-related attributes of the Motoneuron from the appropriate textfiles.

        Keyword arguments:
            diam    : float : Motoneuron diameter.

        If diam is not passed in argument, the value assigned to _morpho['diam'] is read from textfile.
        Otherwise, it is the value passed in argument.
        This method sets _need_2_read_info to False.

        """

        MN_folder = os.path.join(self._smc.dir_data, self._musc, self._seg, 'MN{:d}'.format(self._fidx))

        # General morphology (diameter).
        if diam is None:
            file_path = os.path.join(MN_folder, 'morpho.txt')
            with open(file_path, 'r') as file_ID:
                file_lines = file_ID.readlines()
            diam = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self.diam(diam)

        # Axon morphology.
        if self._with_axon:
            filePath = os.path.join(MN_folder, 'axon', 'morpho.txt')
            with open(filePath, 'r') as file_ID:
                file_lines = file_ID.readlines()
            diam = float(file_lines.pop(0)) * 1e3  # To get it in um.
            nN = int(file_lines.pop(0))
            self.axon_diam(diam)
            self.axon_nN(nN)

        # DendTree morphology.
        if self._with_dtree:
            try:
                dtree_folder = os.path.join(MN_folder, 'dtree')
                swc_file_name = [file_name for file_name in os.listdir(dtree_folder) if file_name.endswith('.swc')][0]
                swc_file_path = os.path.join(dtree_folder, swc_file_name)
                self.swc_file(swc_file_path)
            except:
                pass

        # Info is read.
        self._need_2_read_info = False

    def _build(self):
        """Build the Motoneuron."""
        self._create_compartments()
        self.soma.build()
        if self._with_dtree:
            self.dtree.build()
            self._connect_dtree()
        if self._with_axon:
            self.axon.build()
            self._connect_axon()
        self._need_2_build = False

    def _create_compartments(self):
        """Create the soma, dendritic tree, and axon objects."""
        if self._morpho['soma'] == 'simple':
            self.soma = Soma(cell=self)
        elif self._morpho['soma'] == 'multitap':
            self.soma = MultiTapSoma(cell=self)
        if self._with_dtree:
            self.dtree = DendTree(cell=self)
        if self._with_axon:
            self.axon = MotorAxon(cell=self)

    def _connect_dtree(self):
        """Connect the dendritic tree to the soma."""
        stems = self.dtree.get_stems()
        if self._morpho['soma'] == 'simple':
            for stem in stems:
                stem.par = self.soma.somasec
                stem.hsec.connect(self.soma.somasec.hsec, 1, 0)
        elif self._morpho['soma'] == 'multitap':
            for tsec, stem in zip(self.soma.tsecs[1:], stems):
                stem.hsec.connect(tsec.hsec, 1, 0)

    def _connect_axon(self):
        """Connect the axon to the soma."""
        if self._morpho['soma'] == 'simple':
            self.axon.VRbranch.inisegs[0].connect(self.soma.somasec.hsec, 0, 0)
        elif self._morpho['soma'] == 'multitap':
            self.axon.VRbranch.inisegs[0].connect(self.soma.tsecs[0].hsec, 1, 0)

    def reset(self, **kwargs):
        """Reset the Motoneuron.

        Keyword arguments:
            smc         : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc        : str           : name of the muscle to which the NeuralEntity belongs.
            seg         : str           : name of the segment through which the NeuralEntity is running.
            fidx        : int           : index of the NeuralEntity in the (musc, seg) family.
            gidx        : int           : global index of the NeuralEntity.
            diam        : float         : diameter of the Motoneuron (in um).
            swc_file    : str           : path to swc file.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """

        super(Motoneuron, self).reset(**kwargs)

        try:
            self.diam(kwargs['diam'])
        except:
            pass

        try:
            self.swc_file(kwargs['swc_file'])
        except:
            pass

        if self._need_2_read_info and self._has_ID():
            self._read_morpho_info(diam=self.diam())

        if self._need_2_build and self._ready_2_build():
            self._build()

    def adjust_el(self):
        """Adjust the leakage reversal potential of the Motoneuron's active compartments to reach steady-state."""
        self.soma.adjust_el()
        if self._with_axon:
            self.axon.adjust_el()

    def _ready_2_build(self):
        """Return a boolean indicating whether the Motoneuron is ready to be built or not."""
        if self._morpho['diam'] is None: return False

        if self._with_dtree:
            if self._morpho['dtree']['swc_file'] is None: return False
            else: pass

        if self._with_axon:
            if self._morpho['axon']['diam'] is None: return False
            elif self._morpho['axon']['nN'] is None: return False
            else: pass

        return True

#############################################################################
