# -*- coding: utf-8 -*


###########################################################################
# Class SMC.
#
# Created on: 4 May 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import os
import numpy as np
import pandas

from biophy_smc_ees.utils import utilities


class SMC(object):
    """
    This class regroups attributes and methods describing the composition and connectivity of a sensorimotor circuit.

    Class variables defined here:
        wperm   : 1 : default indicator of writing permission level.

    Instance variables defined here:
        dir_data        : str               : path to SMC dataset directory.
        dir_res         : str               : path to SMC results directory.
        name            : str               : name of SMC.
        muscs           : list of str       : names of the muscles of the SMC.
        musc_count      : int               : count of muscles of the SMC.
        segs            : list              : names of the spinal segments of the SMC.
        seg_count       : int               : count of spinal segments of the SMC.
        Ia_distrib      : 2D numpy.array    : Ia_distrib[i,j] = count of Ias of musc i running through seg j.
        II_distrib      : 2D numpy.array    : II_distrib[i,j] = count of IIs of musc i running through seg j.
        DCcB_distrib    : 2D numpy.array    : DCcB_distrib[i,j] = count of DCcBs of musc i running through seg j.
        DRcA_distrib    : 2D numpy.array    : DRcA_distrib[i,j] = count of DRcAs of musc i running through seg j.
        CST_distrib     : 2D numpy.array    : CST_distrib[i,j] = count of CSTs of musc i running through seg j.
        ST_distrib      : 2D numpy.array    : ST_distrib[i,j] = count of STs of musc i running through seg j.
        MN_distrib      : 2D numpy.array    : MN_distrib[i,j] = count of MNs of musc i running through seg j.
        Ia_index        : pandas.DataFrame  : relational structure linking Ia gidxs, muscs, segs, and fidxs.
        II_index        : pandas.DataFrame  : relational structure linking II gidxs, muscs, segs, and fidxs.
        DCcB_index      : pandas.DataFrame  : relational structure linking DCcB gidxs, muscs, segs, and fidxs.
        DRcA_index      : pandas.DataFrame  : relational structure linking DRcA gidxs, muscs, segs, and fidxs.
        CST_index       : pandas.DataFrame  : relational structure linking CST gidxs, muscs, segs, and fidxs.
        ST_index        : pandas.DataFrame  : relational structure linking ST gidxs, muscs, segs, and fidxs.
        IaOD3_index     : pandas.DataFrame  : relational structure linking IaOD3 gidxs, muscs, segs, and fidxs.
        MN_index        : pandas.DataFrame  : relational structure linking MN gidxs, muscs, segs, and fidxs.
        Ia_MN_connec    : pandas.DataFrame  : relational structure listing the connections between Ias and MNs.
        II_MN_connec    : pandas.DataFrame  : relational structure listing the connections between IIs and MNs.
        CST_MN_connec   : pandas.DataFrame  : relational structure listing the connections between CSTs and MNs.
        _with_Ias       : bool              : indicator whether SMC possesses Ias or not.
        _with_IIs       : bool              : indicator whether SMC possesses IIs or not.
        _with_DCcBs     : bool              : indicator whether SMC possesses DCcBs or not.
        _with_DRcAs     : bool              : indicator whether SMC possesses DRcAs or not.
        _with_CSTs      : bool              : indicator whether SMC possesses CSTs or not.
        _with_STs       : bool              : indicator whether SMC possesses STs or not.
        _with_IaOD3s    : bool              : indicator whether SMC possesses IaOD3s or not.
        _with_MNs       : bool              : indicator whether SMC possesses MNs or not.
        _wperm          : int               : indicator of writing permission level.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        get_info(self)
        _read_dir_data(self)
        get_distrib(self, entity)
        get_index(self, entity)
        get_musc_segs(self, entity, musc)
        get_Ia_musc_segs(self, musc)
        get_II_musc_segs(self, musc)
        get_DCcB_musc_segs(self, musc)
        get_DRcA_musc_segs(self, musc)
        get_CST_musc_segs(self, musc)
        get_ST_musc_segs(self, musc)
        get_MN_musc_segs(self, musc)
        get_obj_count(self, entity, musc=None, seg=None)
        get_Ia_count(self, musc=None, seg=None)
        get_II_count(self, musc=None, seg=None)
        get_DCcB_count(self, musc=None, seg=None)
        get_DRcA_count(self, musc=None, seg=None)
        get_CST_count(self, musc=None, seg=None)
        get_ST_count(self, musc=None, seg=None)
        get_MN_count(self, musc=None, seg=None)
        get_fidxs(self, entity, musc, seg)
        get_gidxs(self, entity, musc=None, seg=None)
        get_Ia_gidxs(self, musc=None, seg=None)
        get_II_gidxs(self, musc=None, seg=None)
        get_DCcB_gidxs(self, musc=None, seg=None)
        get_DRcA_gidxs(self, musc=None, seg=None)
        get_CST_gidxs(self, musc=None, seg=None)
        get_ST_gidxs(self, musc=None, seg=None)
        get_MN_gidxs(self, musc=None, seg=None)
        info_2_gidx(self, entity, musc, seg, fidx)
        Ia_info_2_gidx(self, musc, seg, fidx)
        II_info_2_gidx(self, musc, seg, fidx)
        DCcB_info_2_gidx(self, musc, seg, fidx)
        DRcA_info_2_gidx(self, musc, seg, fidx)
        CST_info_2_gidx(self, musc, seg, fidx)
        ST_info_2_gidx(self, musc, seg, fidx)
        IaOD3_info_2_gidx(self, musc, seg, fidx)
        MN_info_2_gidx(self, musc, seg, fidx)
        gidx_2_info(self, entity, gidx)
        Ia_gidx_2_info(self, gidx)
        II_gidx_2_info(self, gidx)
        DCcB_gidx_2_info(self, gidx)
        DRcA_gidx_2_info(self, gidx)
        CST_gidx_2_info(self, gidx)
        ST_gidx_2_info(self, gidx)
        IaOD3_gidx_2_info(self, gidx)
        MN_gidx_2_info(self, gidx)
        get_Ia_inputs_2_MN(self, **kwargs)
        load_obj(self, obj_name, entity, **kwargs)
        load_records(self, entity, **kwargs)
        save_obj(self, obj, obj_name, entity, **kwargs)
        save_records(self, records, entity, **kwargs)
        _enable_save_obj(self, obj_name, entity, **kwargs)
        build_empty_records(entity)                                 -- static
        get_folder_path(self, entity, **kwargs)
        make_folder(self, folder)
        _make_dir_res(self)
        expand_entity_name(entity)                                  -- static
        collapse_entity_name(entity)                                -- static


    The SMC class provides control over a sensorimotor circuit dataset. A sensorimotor circuit dataset is composed of
    a certain number of neural entities, which are described by their morphology and interconnectivity,
    and which populate a spinal cord model. In addition, distributions of extracellular potential along these neural
    entities during electrical stimulation of the spinal cord are included in the sensorimotor circuit dataset. These
    distributions can be used to evaluate the recruitment of the neural entities with a neuro-biophysical solver (we
    use NEURON).

    The information of a sensorimotor circuit dataset is stored in two folders: the first folder, dir_data,
    holds the information about the composition of the sensorimotor circuit, the connectivity between its neural
    entities, and the morphology and the electric potential distributions along these neural entities; the second
    folder, dir_res, holds the results of the neuro-biophysical simulations.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            dir_data    : str       : path to SMC dataset directory.
            dir_res     : str       : path to SMC results directory.
            name        : str       : name of SMC.
            wperm       : int/str   : indicator of writing permission level.

        The keyword argument dir_data must be specified to instantiate an SMC object. It should be a valid path
        to a sensorimotor circuit dataset directory.

        If the keyword argument dir_res is specified and does not yet exist in the file system, an attempt is made to
        build the directory.

        """

        self.dir_data = None
        self.dir_res = None
        self.name = None
        self.muscs = None
        self.musc_count = None
        self.segs = None
        self.seg_count = None
        self.Ia_distrib = None
        self.II_distrib = None
        self.DCcB_distrib = None
        self.DRcA_distrib = None
        self.CST_distrib = None
        self.ST_distrib = None
        self.IaOD3_distrib = None
        self.MN_distrib = None
        self.Ia_index = None
        self.II_index = None
        self.DCcB_index = None
        self.DRcA_index = None
        self.CST_index = None
        self.ST_index = None
        self.IaOD3_index = None
        self.MN_index = None
        self.Ia_MN_connec = None
        self.II_MN_connec = None
        self.CST_MN_connec = None

        self._with_Ias = False
        self._with_IIs = False
        self._with_DCcBs = False
        self._with_DRcAs = False
        self._with_CSTs = False
        self._with_STs = False
        self._with_IaOD3s = False
        self._with_MNs = False
        self._wperm = 1

        try:
            self.dir_data = kwargs['dir_data']
        except:
            pass

        try:
            self.dir_res = kwargs['dir_res']
        except:
            pass

        try:
            self.name = kwargs['name']
        except:
            try:
                (h, t) = os.path.split(self.dir_data)
                self.name = t if t else os.path.split(h)[1]
            except:
                pass

        try:
            self._wperm = kwargs['wperm']
        except:
            pass

        self._read_dir_data()

        if self.dir_res is not None:
            self._make_dir_res()

    def __repr__(self):
        """Return the string representation of the SMC."""
        return self.name

    def get_info(self):
        """Return a string giving information about the SMC."""
        string = 'SMC object with following attributes:\n'
        string += ' - python id:{:d}\n'.format(id(self))
        string += ' - muscs: {}\n'.format(', '.join(self.muscs))
        string += ' - musc_count: {:d}\n'.format(self.musc_count)
        string += ' - segs: {}\n'.format(', '.join(self.segs))
        string += ' - seg_count: {:d}\n'.format(self.seg_count)
        string += ' - Ia_distrib:\n'
        for i in range(self.musc_count):
            string += '\t{}\n'.format('  '.join([str(elt).rjust(4) for elt in self.Ia_distrib[i, :]]))
        string += ' - Ia_count: {:d}\n'.format(self.Ia_count)
        string += ' - MN_distrib:\n'
        for i in range(self.musc_count):
            string += '\t{}\n'.format('  '.join([str(elt).rjust(4) for elt in self.MN_distrib[i, :]]))
        string += ' - MN_count: {:d}\n'.format(self.n_MNs)
        return string

    def _read_dir_data(self):
        """Read the content of the dataset directory of the SMC and sets its other attributes accordingly."""

        # Read composition file of SMC.
        compo_file_path = os.path.join(self.dir_data, 'composition.txt')
        with open(compo_file_path, 'r') as file_ID:
            lines = file_ID.readlines()
            lines = [line.split() for line in lines]

        # Muscles.
        flag = lines.pop(0).pop(0)
        assert(flag == 'Muscles')
        self.muscs = lines.pop(0)
        self.musc_count = len(self.muscs)

        # Segments.
        flag = lines.pop(0).pop(0)
        assert(flag == 'Segments')
        self.segs = lines.pop(0)
        self.seg_count = len(self.segs)

        # Entities.
        while lines:
            flag = lines.pop(0).pop(0)

            if flag == 'Ia_distrib':
                self._with_Ias = True
                self.Ia_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                             dtype=int)
                self.Ia_count = np.sum(self.Ia_distrib)

            elif flag == 'II_distrib':
                self._with_IIs = True
                self.II_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                             dtype=int)
                self.n_IIs = np.sum(self.II_distrib)

            elif flag == 'DCcB_distrib':
                self._with_DCcBs = True
                self.DCcB_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                               dtype=int)
                self.n_DCcBs = np.sum(self.DCcB_distrib)

            elif flag == 'DRcA_distrib':
                self._with_DRcAs = True
                self.DRcA_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                               dtype=int)
                self.n_DRcAs = np.sum(self.DRcA_distrib)

            elif flag == 'CST_distrib':
                self._with_CSTs = True
                self.CST_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                              dtype=int)
                self.n_CSTs = np.sum(self.CST_distrib)

            elif flag == 'ST_distrib':
                self._with_STs = True
                self.ST_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                             dtype=int)
                self.n_STs = np.sum(self.ST_distrib)

            elif flag == 'IaOD3_distrib':
                self._with_IaOD3s = True
                self.IaOD3_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                             dtype=int)
                self.n_IaOD3s = np.sum(self.IaOD3_distrib)

            elif flag == 'MN_distrib':
                self._with_MNs = True
                self.MN_distrib = np.asarray([[int(w) for w in lines.pop(0)] for _ in range(self.musc_count)],
                                             dtype=int)
                self.n_MNs = np.sum(self.MN_distrib)

        # Read Ia_index and Ia_connec files.
        if self._with_Ias:
            Ia_index_file_path = os.path.join(self.dir_data, 'Ia_index.txt')
            self.Ia_index = pandas.read_csv(Ia_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.Ia_index.index = range(1, len(self.Ia_index) + 1)
            Ia_MN_connec_file_path = os.path.join(self.dir_data, 'Ia_connec.txt')
            self.Ia_MN_connec = pandas.read_csv(
                Ia_MN_connec_file_path, sep=' ', header=None, names=['Ia_gidx', 'MN_gidx', 'Col'])
            self.Ia_MN_connec.index = range(1, len(self.Ia_MN_connec) + 1)

        # Read II_index and II_connec files.
        if self._with_IIs:
            II_index_file_path = os.path.join(self.dir_data, 'II_index.txt')
            self.II_index = pandas.read_csv(II_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.II_index.index = range(1, len(self.II_index) + 1)
            II_MN_connec_file_path = os.path.join(self.dir_data, 'II_connec.txt')
            self.II_MN_connec = pandas.read_csv(
                II_MN_connec_file_path, sep=' ', header=None, names=['II_gidx', 'MN_gidx', 'Col'])
            self.II_MN_connec.index = range(1, len(self.II_MN_connec) + 1)

        # Read DCcB_index file.
        if self._with_DCcBs:
            DCcB_index_file_path = os.path.join(self.dir_data, 'DCcB_index.txt')
            self.DCcB_index = pandas.read_csv(DCcB_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.DCcB_index.index = range(1, len(self.DCcB_index) + 1)

        # Read DRcA_index file.
        if self._with_DRcAs:
            DRcA_index_file_path = os.path.join(self.dir_data, 'DRcA_index.txt')
            self.DRcA_index = pandas.read_csv(DRcA_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.DRcA_index.index = range(1, len(self.DRcA_index) + 1)

        # Read CST_index and CST_connec files.
        if self._with_CSTs:
            CST_index_file_path = os.path.join(self.dir_data, 'CST_index.txt')
            self.CST_index = pandas.read_csv(CST_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.CST_index.index = range(1, len(self.CST_index) + 1)
            CST_MN_connec_file_path = os.path.join(self.dir_data, 'CST_connec.txt')
            self.CST_MN_connec = pandas.read_csv(
                CST_MN_connec_file_path, sep=' ', header=None, names=['CST_gidx', 'MN_gidx', 'Col'])
            self.CST_MN_connec.index = range(1, len(self.CST_MN_connec) + 1)

        # Read ST_index file.
        if self._with_STs:
            ST_index_file_path = os.path.join(self.dir_data, 'ST_index.txt')
            self.ST_index = pandas.read_csv(ST_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.ST_index.index = range(1, len(self.ST_index) + 1)

        # Read IaOD3_index file.
        if self._with_IaOD3s:
            IaOD3_index_file_path = os.path.join(self.dir_data, 'IaOD3_index.txt')
            self.IaOD3_index = pandas.read_csv(IaOD3_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.IaOD3_index.index = range(1, len(self.IaOD3_index) + 1)

        # Read MN_index file.
        if self._with_MNs:
            MN_index_file_path = os.path.join(self.dir_data, 'MN_index.txt')
            self.MN_index = pandas.read_csv(MN_index_file_path, sep=' ', header=None, names=['musc', 'seg', 'fidx'])
            self.MN_index.index = range(1, len(self.MN_index) + 1)

    ##############################################################################################################
    # The methods below are accessors of various class attributes.
    ##############################################################################################################

    def get_distrib(self, entity):
        """Return the distribution of the input entity.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).

        Return:
            distrib : nM * nS numpy.array   : muscle-seg distribution of input entity.

        """
        if entity is None: raise ValueError('entity is None. Should be str instead.')
        elif entity in {'IaFiber', 'Ia'}: distrib = self.Ia_distrib
        elif entity in {'IIFiber', 'II'}: distrib = self.II_distrib
        elif entity in {'DCcBFiber', 'DCcB'}: distrib = self.DCcB_distrib
        elif entity in {'DRcAFiber', 'DRcA'}: distrib = self.DRcA_distrib
        elif entity in {'CSTFiber', 'CST'}: distrib = self.CST_distrib
        elif entity in {'STFiber', 'ST'}: distrib = self.ST_distrib
        elif entity in {'IaFiberOD3', 'IaOD3'}: distrib = self.ST_distrib
        elif entity in {'MN', 'Motoneuron', 'MotorAxon', 'MA'}: distrib = self.IaOD3_distrib
        else: raise ValueError('Invalid value for entity.')

        return distrib

    def get_index(self, entity):
        """Return the index of the input entity.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).

        Return:
            index : pandas.DataFrame    :   index   musc    seg     fidx
                                            1       AAA     UU      1
                                            2       AAA     UU      2
                                            .
                                            .
                                            n       BBB     VVV     k

        """
        if entity is None: raise ValueError('entity is None. Should be str instead.')
        elif entity in {'IaFiber', 'Ia'}: index = self.Ia_index
        elif entity in {'IIFiber', 'II'}: index = self.II_index
        elif entity in {'DCcBFiber', 'DCcB'}: index = self.DCcB_index
        elif entity in {'DRcAFiber', 'DRcA'}: index = self.DRcA_index
        elif entity in {'CSTFiber', 'CST'}: index = self.CST_index
        elif entity in {'STFiber', 'ST'}: index = self.ST_index
        elif entity in {'IaFiberOD3', 'IaOD3'}: index = self.IaOD3_index
        elif entity in {'MN', 'Motoneuron', 'MotorAxon', 'MA'}: index = self.MN_index
        else: raise ValueError('Invalid value for entity.')

        return index

    def get_musc_segs(self, entity, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of entity.

        Arguments:
            entity  : str : name of neural entity (indifferently extended or collapsed).
            musc    : str : name of muscle.

        Return:
            segs    : list of str   : segment names.

        """
        distrib = self.get_distrib(entity)
        i = self.muscs.index(musc)
        segs = list()
        for j in range(self.seg_count):
            if distrib[i, j] > 0:
                segs.append(self.segs[j])
        return segs

    def get_Ia_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of Ias.

        segs = smc.get_Ia_musc_segs(musc)   <==>    segs = smc.get_musc_segs('Ia', musc)

        """
        return self.get_musc_segs('Ia', musc)

    def get_II_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of IIs.

        segs = smc.get_II_musc_segs(musc)   <==>    segs = smc.get_musc_segs('II', musc)

        """
        return self.get_musc_segs('II', musc)

    def get_DCcB_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of DCcBs.

        segs = smc.get_DCcB_musc_segs(musc)   <==>    segs = smc.get_musc_segs('DCcB', musc)

        """
        return self.get_musc_segs('DCcB', musc)

    def get_DRcA_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of DRcAs.

        segs = smc.get_DRcA_musc_segs(musc)   <==>    segs = smc.get_musc_segs('DRcA', musc)

        """
        return self.get_musc_segs('DCcB', musc)

    def get_CST_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of CSTs.

        segs = smc.get_CST_musc_segs(musc)   <==>    segs = smc.get_musc_segs('CST', musc)

        """
        return self.get_musc_segs('CST', musc)

    def get_ST_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of STs.

        segs = smc.get_ST_musc_segs(musc)   <==>    segs = smc.get_musc_segs('ST', musc)

        """
        return self.get_musc_segs('ST', musc)

    def get_MN_musc_segs(self, musc):
        """Retrieve and return the list of segments where the input musc possesses instances of MNs.

        segs = smc.get_MN_musc_segs(musc)   <==>    segs = smc.get_musc_segs('MN', musc)

        """
        return self.get_musc_segs('MN', musc)

    def get_obj_count(self, entity, musc=None, seg=None):
        """Return the count of instances of the input entity.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            musc    : str   : name of muscle. Default to None.
            seg     : str   : name of segment. Default to None.

        Return:
            obj_count   : int   : count of instances of the input entity.

        If musc or seg are specified, the count of instances restricted to this (musc, seg) specification is returned.

        """
        distrib = self.get_distrib(entity)

        if (musc is None) and (seg is None): obj_count = np.sum(distrib)
        elif musc is None: obj_count = np.sum(distrib[:, self.segs.index(seg)])
        elif seg is None: obj_count = np.sum(distrib[self.muscs.index(musc), :])
        else: obj_count = np.sum(distrib[self.muscs.index(musc), self.segs.index(seg)])

        return obj_count

    def get_Ia_count(self, musc=None, seg=None):
        """Return the count of Ia-fibers.

        count = smc.get_Ia_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('Ia', musc=musc, seg=seg)

        """
        return self.get_obj_count('Ia', musc=musc, seg=seg)

    def get_II_count(self, musc=None, seg=None):
        """Return the count of IIfibers.

        count = smc.get_II_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('II', musc=musc, seg=seg)

        """
        return self.get_obj_count('II', musc=musc, seg=seg)

    def get_DCcB_count(self, musc=None, seg=None):
        """Return the count of DCcB-fibers.

        count = smc.get_DCcB_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('DCcB', musc=musc, seg=seg)

        """
        return self.get_obj_count('DCcB', musc=musc, seg=seg)

    def get_DRcA_count(self, musc=None, seg=None):
        """Return the count of DRcA-fibers.

        count = smc.get_DRcA_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('DRcA', musc=musc, seg=seg)

        """
        return self.get_obj_count('DCcB', musc=musc, seg=seg)

    def get_CST_count(self, musc=None, seg=None):
        """Return the number of CST-fibers.

        count = smc.get_CST_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('CST', musc=musc, seg=seg)

        """
        return self.get_obj_count('CST', musc=musc, seg=seg)

    def get_ST_count(self, musc=None, seg=None):
        """Return the number of ST-fibers.

        count = smc.get_ST_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('ST', musc=musc, seg=seg)

        """
        return self.get_obj_count('ST', musc=musc, seg=seg)

    def get_MN_count(self, musc=None, seg=None):
        """Return the count of Motoneurons.

        count = smc.get_MN_count(musc=musc, seg=seg)   <==>    count = smc.get_obj_count('MN', musc=musc, seg=seg)

        """
        return self.get_obj_count('MN', musc=musc, seg=seg)

    def get_fidxs(self, entity, musc, seg):
        """Return the list of family indexes of entity for the (musc, seg) family.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).
            musc    : str   : name of muscle.
            seg     : str   : name of segment.

        Return:
            fidxs   : list of int   : family indexes of entity for the (musc, seg) family.

        """
        n_objs = self.get_obj_count(entity, musc=musc, seg=seg)
        return range(1, n_objs + 1)

    def get_gidxs(self, entity, musc=None, seg=None):
        """Return the list of global indexes of entity.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            musc    : str   : name of muscle. Default to None.
            seg     : str   : name of segment. Default to None.

        Return:
            gidxs   : list of int   : global indexes of entity.

        If musc or seg are specified, the restricted list of gidxs for this (musc, seg) specification is returned.

        """
        index = self.get_index(entity)

        if (musc is None) and (seg is None): gidxs = index.index.tolist()
        elif musc is None: gidxs = index[index.seg == seg].index.tolist()
        elif seg is None: gidxs = index[index.musc == musc].index.tolist()
        else: gidxs = index[(index.musc == musc) & (index.seg == seg)].index.tolist()

        return gidxs

    def get_Ia_gidxs(self, musc=None, seg=None):
        """Return the list of Ia-fiber global indexes.

        gidxs = smc.get_Ia_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('Ia', musc=musc, seg=seg)

        """
        return self.get_gidxs('Ia', musc=musc, seg=seg)

    def get_II_gidxs(self, musc=None, seg=None):
        """Return the list of II-fiber global indexes.

        gidxs = smc.get_II_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('II', musc=musc, seg=seg)

        """
        return self.get_gidxs('II', musc=musc, seg=seg)

    def get_DCcB_gidxs(self, musc=None, seg=None):
        """Return the list of DCcB-fiber global indexes.

        gidxs = smc.get_DCcB_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('DCcB', musc=musc, seg=seg)

        """
        return self.get_gidxs('DCcB', musc=musc, seg=seg)

    def get_DRcA_gidxs(self, musc=None, seg=None):
        """Return the list of DRcA-fiber global indexes.

        gidxs = smc.get_DRcA_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('DRcA', musc=musc, seg=seg)

        """
        return self.get_gidxs('DRcA', musc=musc, seg=seg)

    def get_CST_gidxs(self, musc=None, seg=None):
        """Return the list of CST-fiber global indexes.

        gidxs = smc.get_CST_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('CST', musc=musc, seg=seg)

        """
        return self.get_gidxs('CST', musc=musc, seg=seg)

    def get_ST_gidxs(self, musc=None, seg=None):
        """Return the list of ST-fiber global indexes.

        gidxs = smc.get_ST_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('ST', musc=musc, seg=seg)

        """
        return self.get_gidxs('ST', musc=musc, seg=seg)

    def get_MN_gidxs(self, musc=None, seg=None):
        """Return the list of Motoneuron global indexes.

        gidxs = smc.get_MN_gidxs(musc=musc, seg=seg)    <==>    gidxs = smc.get_gidxs('MN', musc=musc, seg=seg)

        """
        return self.get_gidxs('MN', musc=musc, seg=seg)

    def info_2_gidx(self, entity, musc, seg, fidx):
        """Return the gidx of the entity instance whose info is (musc, seg, fidx).

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).
            musc    : str   : name of muscle.
            seg     : str   : name of segment.
            fidx    : int   : family index of entity instance.

        Return:
            gidx    : int   : global index of entity instance.

        """
        index = self.get_index(entity)
        return index[(index.musc == musc) & (index.seg == seg) & (index.fidx == fidx)].index.tolist().pop(0)

    def Ia_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the Ia whose info is (musc, seg, fidx).

        gidx = smc.Ia_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('Ia', musc, seg, fidx)

        """
        return self.info_2_gidx('Ia', musc, seg, fidx)

    def II_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the II whose info is (musc, seg, fidx).

        gidx = smc.II_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('II', musc, seg, fidx)

        """
        return self.info_2_gidx('II', musc, seg, fidx)

    def DCcB_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the DCcB whose info is (musc, seg, fidx).

        gidx = smc.DCcB_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('DCcB', musc, seg, fidx)

        """
        return self.info_2_gidx('DCcB', musc, seg, fidx)

    def DRcA_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the DRcA whose info is (musc, seg, fidx).

        gidx = smc.DRcA_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('DRcA', musc, seg, fidx)

        """
        return self.info_2_gidx('DRcA', musc, seg, fidx)

    def CST_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the CST whose info is (musc, seg, fidx).

        gidx = smc.CST_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('CST', musc, seg, fidx)

        """
        return self.info_2_gidx('CST', musc, seg, fidx)

    def ST_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the ST whose info is (musc, seg, fidx).

        gidx = smc.ST_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('ST', musc, seg, fidx)

        """
        return self.info_2_gidx('CST', musc, seg, fidx)

    def MN_info_2_gidx(self, musc, seg, fidx):
        """Return the global index of the MN whose info is (musc, seg, fidx).

        gidx = smc.MN_info_2_gidx(musc, seg, fidx)  <==>    gidx = smc.info_2_gidx('MN', musc, seg, fidx)

        """
        return self.info_2_gidx('MN', musc, seg, fidx)

    def gidx_2_info(self, entity, gidx):
        """Return the info triplet (musc, seg, fidx) of the entity instance whose global index is gidx.

        Arguments:
            entity  : str   : name of neural entity (indifferently extended or collapsed).
            gidx    : int   : global index of entity instance.

        Return:
            info    : (str, str, int)   : (musc, seg, fidx) info of the entity instance.

        """
        index = self.get_index(entity)
        musc = index.loc[gidx].musc
        seg = index.loc[gidx].seg
        fidx = index.loc[gidx].fidx
        return musc, seg, fidx

    def Ia_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the Ia whose global index is gidx.

        info = smc.Ia_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('Ia', gidx)

        """
        return self.gidx_2_info('Ia', gidx)

    def II_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the II whose global index is gidx.

        info = smc.II_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('II', gidx)

        """
        return self.gidx_2_info('II', gidx)

    def DCcB_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the DCcB whose global index is gidx.

        info = smc.DCcB_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('DCcB', gidx)

        """
        return self.gidx_2_info('DCcB', gidx)

    def DRcA_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the DRcA whose global index is gidx.

        info = smc.DRcA_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('DRcA', gidx)

        """
        return self.gidx_2_info('DRcA', gidx)

    def CST_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the CST whose global index is gidx.

        info = smc.CST_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('CST', gidx)

        """
        return self.gidx_2_info('CST', gidx)

    def ST_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the ST whose global index is gidx.

        info = smc.ST_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('ST', gidx)

        """
        return self.gidx_2_info('ST', gidx)

    def MN_gidx_2_info(self, gidx):
        """Return the info triplet (musc, seg, fidx) of the MN whose global index is gidx.

        info = smc.MN_gidx_2_info(gidx)     <==>    info = smc.gidx_2_info('MN', gidx)

        """
        return self.gidx_2_info('MN', gidx)

    def get_Ia_inputs_2_MN(self, **kwargs):
        """Return a data structure giving the (Ia, Col) pairs supplying synaptic inputs to the specified MN.

        Keyword arguments:
            gidx    : int               : global index of a MN.
            info    : (str, str, int)   : (musc, seg, fidx) info of a MN.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of a MN.

        Return:
            Ia_inputs   : K * 2 pandas.DataFrame    : columns are ['Ia_gidx', 'Col'].

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """
        gidx = None
        try: gidx = self.MN_info_2_gidx(kwargs['musc'], kwargs['seg'], kwargs['fidx'])
        except: pass
        try: gidx = self.MN_info_2_gidx(*kwargs['info'])
        except: pass
        try: gidx = kwargs['gidx']
        except: pass
        if gidx is None:
            raise ValueError('Insufficient identification info provided.')

        Ia_inputs = self.Ia_MN_connec[self.Ia_MN_connec['MN_gidx'] == gidx].drop(columns='MN_gidx')
        return Ia_inputs

    ##############################################################################################################
    # The methods below deal with loading and saving results.
    ##############################################################################################################

    def load_obj(self, obj_name, entity, **kwargs):
        """Load and return the object objname of the specified entity.

        Arguments:
            obj_name    : str   : name of object to load.
            entity      : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            gidx    : int               : global index of a MN.
            info    : (str, str, int)   : (musc, seg, fidx) info of a MN.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of a MN.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """
        entity = self.expand_entity_name(entity)
        try:
            folder_path = self.get_folder_path(entity, **kwargs)
            file_path = os.path.join(folder_path, '{}.pck'.format(obj_name))
            obj = pandas.read_pickle(file_path)
        except:
            obj = self.build_empty_records(entity)

        return obj

    def load_records(self, entity, **kwargs):
        """Load and return the records of the specified entity.

        recs = smc.load_records(entity, **kwargs)    <==>    recs = smc.load_obj(ent + 'records', entity, **kwargs)

        where ent is the collapsed name of the entity.

        """
        return self.load_obj('{}records'.format(self.collapse_entity_name(entity)), entity, **kwargs)

    def save_obj(self, obj, obj_name, entity, **kwargs):
        """Save the input object of the specified entity.

        Arguments:
            obj         : pickable object   : to be saved.
            obj_name    : str               : name of saved object.
            entity      : str               : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            gidx    : int               : global index of entity.
            info    : (str, str, int)   : (musc, seg, fidx) info of entityN.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of entity.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """
        if not self._enable_save_obj(obj_name, entity, **kwargs):
            return

        folder_path = self.get_folder_path(entity, **kwargs)
        os.makedirs(folder_path, exist_ok=True)
        file_path = os.path.join(folder_path, '{}.pck'.format(obj_name))
        obj.to_pickle(file_path)

    def save_records(self, records, entity, **kwargs):
        """Save the input records of the specified entity.

        smc.save_records(records, entity, **kwargs)    <==>    smc.save_obj(records, ent + 'records', entity, **kwargs)

        where ent is the collapsed name of the entity.

        """
        self.save_obj(records, '{}records'.format(self.collapse_entity_name(entity)), entity, **kwargs)

    def _enable_save_obj(self, obj_name, entity, **kwargs):
        """Enable or forbid saving the object obj_name of the specified entity.

        Arguments:
            obj_name    : str   : name of object.
            entity      : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            gidx    : int               : global index of entity.
            info    : (str, str, int)   : (musc, seg, fidx) info of entity.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of entity.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """

        # If writing permission is minimal (=0) always forbid saving object. If maximal (=2), always enable.
        if self._wperm == 0:
            return False
        elif self._wperm == 2:
            return True

        # Otherwise, check if it is already present in the file system.
        folder_path = self.get_folder_path(entity, **kwargs)
        file_path = os.path.join(folder_path, '{}.pck'.format(obj_name))

        # If it is, forbid saving if writing permission is low (=1), ask permission to overwrite otherwise.
        if os.path.exists(file_path):
            if self._wperm == 1:
                return False
            else:
                return utilities.prompt_confirm('Object {} already exist. Do you want to overwrite ?'.format(obj_name))

        # If not, enable saving if writing permission is low (=1), ask permission to save otherwise.
        else:
            if self._wperm == 1:
                return True
            else:
                return utilities.prompt_confirm('No object {} exist yet. Save ?'.format(obj_name))

    @staticmethod
    def build_empty_records(entity):
        """Return an empty pandas.DataFrame records, appropriate for the specified entity."""
        if entity is None:
            raise ValueError('entity is None. Should be str instead.')
        elif entity in {'IaFiber', 'Ia'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'IIFiber', 'II'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'DCcBFiber', 'DCcB'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'DRcAFiber', 'DRcA'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'recr', 'delay', 'stim_type'])
        elif entity in {'CSTFiber', 'CST'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'STFiber', 'ST'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'IaFiberOD3', 'IaOD3'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'Col', 'recr', 'delay', 'stim_type'])
        elif entity in {'Motoneuron', 'MN'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'recr', 'delay', 'stim_type'])
        elif entity in {'MotorAxon', 'MA'}:
            records = pandas.DataFrame(columns=['combi', 'amp', 'recr', 'delay', 'stim_type'])
        else:
            raise ValueError('Incorrect value for entity.')

        return records

    ##############################################################################################################
    # House-keeping methods.
    ##############################################################################################################

    def get_folder_path(self, entity, **kwargs):
        """Return the path of the folder of the specified entity.

        Arguments:
            entity      : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            gidx    : int               : global index of entity.
            info    : (str, str, int)   : (musc, seg, fidx) info of entity.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of entity.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """

        musc, seg, fidx = None, None, None
        try: musc, seg, fidx = kwargs['musc'], kwargs['seg'], kwargs['fidx']
        except: pass
        try: musc, seg, fidx = kwargs['info']
        except: pass
        try: musc, seg, fidx = self.gidx_2_info(entity, kwargs['gidx'])
        except: pass
        if (musc is None) or (seg is None) or (fidx is None):
            raise ValueError('Insufficient identification info provided.')

        folder_path = os.path.join(self.dir_res, musc, seg, '{}{:d}'.format(self.expand_entity_name(entity), fidx))
        
        return folder_path

    def get_records_path(self, entity, **kwargs):
        """Return the path of the records of the specified entity.

        Arguments:
            entity      : str   : name of neural entity (indifferently extended or collapsed).

        Keyword arguments:
            gidx    : int               : global index of entity.
            info    : (str, str, int)   : (musc, seg, fidx) info of entity.
            musc    : str               : name of a muscle.
            seg     : str               : name of a segment.
            fidx    : int               : family index of entity.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx).

        """
        folder_path = self.get_folder_path(entity, **kwargs)
        records_path = os.path.join(folder_path, '{}records.pck'.format(self.collapse_entity_name(entity)))
        return records_path

    def _make_dir_res(self):
        """Make the output directory of the SMC if needed."""
        os.makedirs(self.dir_res, exist_ok=True)

    ##############################################################################################################
    # Entity name manipulations.
    ##############################################################################################################

    @staticmethod
    def expand_entity_name(entity):
        """Return the fully-qualified name of the input entity."""
        if entity in {'IaFiber', 'Ia'}: entity = 'IaFiber'
        elif entity in {'IIFiber', 'II'}: entity = 'IIFiber'
        elif entity in {'DCcB', 'DCcBFiber'}: entity = 'DCcBFiber'
        elif entity in {'DRcA', 'DRcAFiber'}: entity = 'DRcAFiber'
        elif entity in {'CST', 'CSTFiber'}: entity = 'CSTFiber'
        elif entity in {'ST', 'STFiber'}: entity = 'STFiber'
        elif entity in {'IaOD3', 'IaFiberOD3'}: entity = 'IaFiberOD3'
        elif entity in {'MN', 'Motoneuron', 'MotorAxon', 'MA'}: entity = 'MN'
        else: raise ValueError('Incorrect value for entity.')
        return entity

    @staticmethod
    def collapse_entity_name(entity):
        """Return the abbreviated/collapsed name of the input entity."""
        if entity in {'IaFiber', 'Ia'}: entity = 'Ia'
        elif entity in {'IIFiber', 'II'}: entity = 'II'
        elif entity in {'DCcB', 'DCcBFiber'}: entity = 'DCcB'
        elif entity in {'DRcA', 'DRcAFiber'}: entity = 'DRcA'
        elif entity in {'CST', 'CSTFiber'}: entity = 'CST'
        elif entity in {'ST', 'STFiber'}: entity = 'ST'
        elif entity in {'IaOD3', 'IaFiberOD3'}: entity = 'IaOD3'
        elif entity in {'MN', 'Motoneuron', 'MotorAxon', 'MA'}: entity = 'MN'
        else: raise ValueError('Incorrect value for entity.')
        return entity

###########################################################################
