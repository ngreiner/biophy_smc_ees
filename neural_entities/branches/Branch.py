# -*- coding: utf-8 -*


#############################################################################
# Class Branch.
#
# Created on: 26 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
import weakref
from neuron import h


class Branch(object):
    """
    This class regroups attributes and methods describing a nerve branch.

    Class variables defined here:
        label       : 'Branch'  : default Branch label.
        _rhoa       : 70.0      : default axoplasmic resistivity (in Ohm*cm).
        _mygm       : 0.001     : default default myelin lamella specific conductance (in S/cm^2).
        _mycm       : 0.01      : default myelin lamella specific capacitance (in uF/cm^2).
        _Epas       : -80.0     : default reversal potential of passive sections (in mV).
        _Nlength    : 1.0       : default nodes length (in um).
        _RaN        : 70.0      : default nodes axoplasmic resistivity in (Ohm*cm).
        _cmN        : 2.0       : default nodes membrane specific capacitance (in uF/cm^2).
        _xgN0       : 1e10      : default nodes 1st extracellular layer conductance (in S/cm^2).
        _xgN1       : 1e10      : default nodes 2nd extracellular layer conductance (in S/cm^2).
        _xcN0       : 0.0       : default nodes 1st extracellular layer capacitance (in uF/cm^2).
        _xcN1       : 0.0       : default nodes 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialN0  : 1e10      : default nodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialN1  : 1e10      : default nodes 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _P1length   : 3.0       : default paranodes1 length (in um).
        _space_p1   : 0.002     : default paranodes1 axon-myelin interspace width (in um).
        _xgP10      : 1e10      : default paranodes1 1st extracellular layer conductance (in S/cm^2).
        _xgP11      : 1e10      : default paranodes1 2nd extracellular layer conductance (in S/cm^2).
        _xcP10      : 0.0       : default paranodes1 1st extracellular layer capacitance (in uF/cm^2).
        _xcP11      : 0.0       : default paranodes1 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialP10 : 1e10      : default paranodes1 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialP11 : 1e10      : default paranodes1 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _space_p2   : 0.004     : default paranodes2 axon-myelin interspace width (in um).
        _xgP20      : 1e10      : default paranodes2 1st extracellular layer conductance (in S/cm^2).
        _xgP21      : 1e10      : default paranodes2 2nd extracellular layer conductance (in S/cm^2).
        _xcP20      : 0.0       : default paranodes2 1st extracellular layer capacitance (in uF/cm^2).
        _xcP21      : 0.0       : default paranodes2 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialP20 : 1e10      : default paranodes2 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialP21 : 1e10      : default paranodes2 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _space_i    : 0.004     : default internodes axon-myelin interspace width (in um).
        _xgI0       : 1e10      : default internodes 1st extracellular layer conductance (in S/cm^2).
        _xgI1       : 1e10      : default internodes 2nd extracellular layer conductance (in S/cm^2).
        _xcI0       : 0.0       : default internodes 1st extracellular layer capacitance (in uF/cm^2).
        _xcI1       : 0.0       : default internodes 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialI0  : 1e10      : default internodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialI1  : 1e10      : default internodes 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _model      : 'ModelC'  : default identifier of Branch model.
        _axflut     : False     : indicator whether mechanism 'axflut' should be inserted in paranodes2 or not.
        _extra_mech : False     : indicator whether extracellular mechanism should be inserted or not.

    Instance variables defined here:
        fiber       : weakref.ref   : weakref to the Fiber object to which the Branch belongs.
        diam        : float : diameter of the Branch (in um).
        nN          : int   : number of nodes of Ranvier of the Branch.
        nodes       : list of h.Section objects.
        paranodes1  : list of h.Section objects.
        paranodes2  : list of h.Section objects.
        internodes  : list of h.Section objects.
        _n_lamella  : int   : number of myelin lamellas.
        _Ndiam      : float : diameter of the nodes (in um).
        _xraxialN0  : float : nodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP1        : int   : number of paranodes1 of the Branch.
        _P1diam     : float : diameter of the paranodes1 (in um).
        _RaP1       : float : paranodes1 axoplasmic resistivity (in Ohm*cm).
        _cmP1       : float : paranodes1 membrane specific capacitance (in uF/cm^2).
        _gpasP1     : float : paranodes1 passive membrane conductance (in S/cm^2).
        _xgP10      : float : paranodes1 1st extracellular layer conductance (in S/cm^2).
        _xcP10      : float : paranodes1 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialP10 : float : paranodes1 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP2        : int   : number of paranodes2 of the Branch.
        _P2diam     : float : diameter of the paranodes2 (in um).
        _P2length   : float : length of the paranodes2 (in um).
        _RaP2       : float : paranodes2 axoplasmic resistivity (in Ohm*cm).
        _cmP2       : float : paranodes2 membrane specific capacitance (in uF/cm^2).
        _gpasP2     : float : paranodes2 passive membrane conductance (in S/cm^2).
        _xgP20      : float : paranodes2 1st extracellular layer conductance (in S/cm^2).
        _xcP20      : float : paranodes2 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialP20 : float : paranodes2 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nI         : int   : number of internodes of the Branch.
        _Idiam      : float : diameter of the internodes (in um).
        _Ilength    : float : length of the internodes (in um).
        _RaI        : float : internodes axoplasmic resistivity (in Ohm*cm).
        _cmI        : float : internodes membrane specific capacitance (in uF/cm^2).
        _gpasI      : float : internodes passive membrane conductance (in S/cm^2).
        _xgI0       : float : internodes 1st extracellular layer conductance (in S/cm^2).
        _xcI0       : float : internodes 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialI0  : float : internodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _deltaX     : float : node-to-node distance (in um).
        _model      : str   : default identifier of Branch model.
        _axflut     : bool  : indicator whether mechanism 'axflut' should be inserted in paranodes2 or not.
        _extra_mech : bool  : indicator whether extracellular mechanism should be inserted or not.

    Methods defined here:
        __init__(self, *args, **kwargs)
        __repr__(self)
        _set_params(self)
        get_info(self)
        build(self)
        create_sections(self)
        set_sections(self)
        get_sections(self)
        adjust_el(self)
        get_longi_xs(self)

    The Branch class provides attributes and methods to build and control a single unbranching portion of myelinated
    neurite organized as follows:

            N - P1 - P2 - I - I - I - I - I - I - P2 - P1 - N - P1 - ... - P2 - P1 - N

    where N stands for 'node', P1 for 'paranode1', P2 for 'paranode2' and I for 'internode'. This representation of a
    portion of myelinated neurite is taken from 'Modelling the effects of electric fields on nerve fibres: influence
    of the myelin sheath', Richardson et al., Medical & Biological Engineering & Computing, 2000.

    Each node, paranode1, paranode2 and internode is represented by a single HOC section, with nseg=1. Their
    dimensions are dictated by the global diameter of the Branch (which in fact is the diameter of its myelinated
    portions) and are derived from it using piecewise-linear interpolants based on the dataset originally given in the
    mentioned publication.

    The biophysical properties of the nodes, paranodes1, paranodes2, and internodes are also derived from the
    mentioned publication and from the following other publication: 'Cellular Effects of Deep Brain Stimulation:
    Model-Based Analysis of Activation and Inhibition', McIntyre et al., Translational Physiology, 2004.
    Several models are available (see method _set_params).

    A Branch usually belongs to a Fiber from which it draws its morphological parameters and model, but it can also be
    instantiated to stand alone by using the dedicated keyword arguments of the constructor.

    """

    label = 'Branch'
    _rhoa = 70.0            # Ohm * cm
    _mygm = 0.001           # S / cm^2
    _mycm = 0.1             # uF / cm^2
    _Epas = -80.0           # mV
    _Nlength = 1.0          # um
    _RaN = _rhoa
    _cmN = 2.0              # uF / cm^2
    _xgN0 = 1e10            # S / cm^2
    _xgN1 = 1e10            # S / cm^2
    _xcN0 = 0.0             # uF / cm^2
    _xcN1 = 0.0             # uF / cm^2
    _xraxialN0 = 1e10       # MOhm / cm
    _xraxialN1 = 1e10       # MOhm / cm
    _P1length = 3.0         # um
    _space_p1 = 0.002       # um
    _xgP10 = 1e10           # S / cm^2
    _xgP11 = 1e10           # S / cm^2
    _xcP10 = 0.0            # uF / cm^2
    _xcP11 = 0.0            # uF / cm^2
    _xraxialP10 = 1e10      # MOhm / cm
    _xraxialP11 = 1e10      # MOhm / cm
    _space_p2 = 0.004       # um
    _xgP20 = 1e10           # S / cm^2
    _xgP21 = 1e10           # S / cm^2
    _xcP20 = 0.0            # uF / cm^2
    _xcP21 = 0.0            # uF / cm^2
    _xraxialP20 = 1e10      # MOhm / cm
    _xraxialP21 = 1e10      # MOhm / cm
    _space_i = 0.004        # um
    _xgI0 = 1e10            # S / cm^2
    _xgI1 = 1e10            # S / cm^2
    _xcI0 = 0.0             # uF / cm^2
    _xcI1 = 0.0             # uF / cm^2
    _xraxialI0 = 1e10       # MOhm / cm
    _xraxialI1 = 1e10       # MOhm / cm
    _model = 'ModelC'
    _axflut = False
    _extra_mech = False

    # McIntyre's morphological parameters database, and interpolants ('Modelling the effects of electric fields on
    # nerve fibres: influence of the myelin sheath', Richardson et al., Medical & Biological Engineering & Computing,
    # 2000).
    _diams = np.asarray([0.0, 5.7, 7.3, 8.7, 10.0, 11.5, 12.8, 14.0, 15.0, 16.0])                       # um
    _Ndiams = np.asarray([0.0, 1.9, 2.4, 2.8, 3.3, 3.7, 4.2, 4.7, 5.0, 5.5])                            # um
    _P2diams = np.asarray([0.0, 3.4, 4.6, 5.8, 6.9, 8.1, 9.2, 10.4, 11.5, 12.7])                        # um
    _P2lengths = np.asarray([0.0, 35.0, 38.0, 40.0, 46.0, 50.0, 54.0, 56.0, 58.0, 60.0])                # um
    _deltaXs = np.asarray([0.0, 500.0, 750., 1000.0, 1150.0, 1250.0, 1350.0, 1400.0, 1450.0, 1500.0])   # um
    _n_lamellas = np.asarray([0.0, 80.0, 100.0, 110.0, 120.0, 130.0, 135.0, 140.0, 145.0, 150.0])

    _make_lambdas_list = lambda ys, xs: \
        [(lambda x, i=i: (ys[i + 1] - ys[i]) / (xs[i + 1] - xs[i]) * (x - xs[i]) + ys[i]) for i in range(len(xs) - 1)]

    _fNdiams = _make_lambdas_list(_Ndiams, _diams)
    _fP2diams = _make_lambdas_list(_P2diams, _diams)
    _fP2lengths = _make_lambdas_list(_P2lengths, _diams)
    _fdeltaXs = _make_lambdas_list(_deltaXs, _diams)
    _fnLamellas = _make_lambdas_list(_n_lamellas, _diams)
    _diams = _diams[:-1]

    def __init__(self, *args, **kwargs):
        """Class constructor method.

        Usage:
            br = Branch(fiber=some_fiber)
            br = Branch(nN=some_nN, diam=some_diam)
            br = Branch(..., model=model)
            br = Branch(label, ...)

        Optional arguments:
            label   : str   : label of Branch.

        Keyword arguments:
            fiber   : Fiber object  : to which the Branch belongs.
            nN      : int           : number of nodes of Ranvier of the Branch.
            diam    : float         : diameter of the Branch (in um).
            model   : str           : identifier of Branch model.

        If any of nN, diam or model is specified in argument, it overwrites any value eventually extracted from the
        Fiber to which the Branch belongs.

        """

        self.name = 'Branch_{:d}'.format(id(self))
        self.fiber = lambda: None
        self.p_attach = ''

        if args: self.label = args[0]

        try:
            self.fiber = weakref.ref(kwargs['cell'])
            self.name = '{}.{}'.format(self.fiber().name, self.label)
            self.nN = self.fiber().get_morpho(self.label, 'nN')
            self._diam = self.fiber().get_morpho(self.label, 'diam')
            self._model = self.fiber().model()
            try: self.p_attach = self.fiber().get_morpho(self.label, 'p_attach')
            except: pass
        except:
            pass

        try:
            self.nN = kwargs['nN']
        except:
            pass

        try:
            self._diam = kwargs['diam']
        except:
            pass

        try:
            self._model = kwargs['model']
        except:
            pass

        try:
            self.p_attach = kwargs['p_attach']
        except:
            pass

        self._n_lamella = None
        self._Ndiam = None
        self._nP1 = None
        self._P1diam = None
        self._RaP1 = None
        self._cmP1 = None
        self._gpasP1 = None
        self._nP2 = None
        self._P2diam = None
        self._P2length = None
        self._RaP2 = None
        self._cmP2 = None
        self._gpasP2 = None
        self._nI = None
        self._Idiam = None
        self._Ilength = None
        self._RaI = None
        self._cmI = None
        self._gpasI = None
        self._deltaX = None

        self.nodes = None
        self.paranodes1 = None
        self.paranodes2 = None
        self.internodes = None

        self._set_params()

    def __repr__(self):
        """Return the string representation of the Branch."""
        return self.name

    def get_info(self):
        """Return a string giving info on the Branch."""
        string = '{}:\n'.format(self.__class__.__name__)
        string += '  diam: {:f}\n'.format(self._diam)
        string += '  nN: {:d}\n'.format(self.nN)
        string += '  Ndiam: {:f}\n'.format(self._Ndiam)
        string += '  Nlength: {:f}\n'.format(self._Nlength)
        string += '  P1diam: {:f}\n'.format(self._P1diam)
        string += '  P1length: {:f}\n'.format(self._P1length)
        string += '  P2diam: {:f}\n'.format(self._P2diam)
        string += '  P2length: {:f}\n'.format(self._P2length)
        string += '  Idiam: {:f}\n'.format(self._Idiam)
        string += '  Ilength: {:f}\n'.format(self._Ilength)
        return string

    def _set_params(self):
        """Set the geometrico-biophysical attributes of the Branch.

        These are derived from the diameter, the number of nodes of Ranvier, and the model of the Branch.

        """

        # Morphological parameters common to all models.
        self._nP1 = 2 * (self.nN - 1)
        self._nP2 = 2 * (self.nN - 1)
        self._nI = 6 * (self.nN - 1)
        self._Ndiam = np.piecewise(self._diam, self._diam > self._diams, self._fNdiams)
        self._P1diam = self._Ndiam
        self._P2diam = np.piecewise(self._diam, self._diam > self._diams, self._fP2diams)
        self._P2length = np.piecewise(self._diam, self._diam > self._diams, self._fP2lengths)
        self._Idiam = self._P2diam
        self._deltaX = np.piecewise(self._diam, self._diam > self._diams, self._fdeltaXs)
        self._Ilength = (self._deltaX - 2. * self._P1length - 2. * self._P2length - self._Nlength) / 6.
        self._n_lamella = np.piecewise(self._diam, self._diam > self._diams, self._fnLamellas)

        # Biophysical parameters common to all models.
        self._RaP1 = self._rhoa * (self._diam / self._P1diam) ** 2
        self._RaP2 = self._rhoa * (self._diam / self._P2diam) ** 2
        self._RaI = self._rhoa * (self._diam / self._Idiam) ** 2

        # Models C and D.

        # In these models, the myelin sheath is represented by the first layer of NEURON's extracellular mechanism.
        # The nodes of Ranvier embed ionic channels described by the mechanism 'AXNODE'. The geometry of
        # the nodes and the geometry and passive properties of the paranodes1, paranodes2,
        # and internodes are derived from 'Modelling the effects of electric fields on nerve fibres: influence of the
        # myelin sheath', Richardson et al., Medical & Biological Engineering & Computing, 2000.

        # Model D differs from model C in that its paranodes2 (FLUT compartments in McIntyre et al.'s publication)
        # embed a K+ ionic channel described by the mechanism 'AXFLUT' (which is almost similar to the mechanism
        # 'MNAXFLUT' used by the MotorBranch class, but in which the potentials are all shifted down by 10mV,
        # such that the resting potential of a Branch is close to -80mV instead of -70mV as is the case of a
        # MotorBranch). This one is taken from 'Cellular Effects of Deep Brain Stimulation: Model-Based Analysis of
        # Activation and Inhibition', McIntyre et al., Translational Physiology, 2004.

        if self._model in {'ModelC', 'ModelD'}:

            # Inclusion of axflut in paranodes2.
            if self._model == 'ModelD':
                self._axflut = True

            # Extracellular mechanism.
            self._extra_mech = True

            # Biophysical parameters.
            self._xraxialN0 = (self._rhoa * 100) / \
                              (np.pi * ((self._Ndiam / 2. + self._space_p1) ** 2 - (self._Ndiam / 2.) ** 2))
            self._cmP1 = 2 * (self._P1diam / self._diam)
            self._gpasP1 = 0.001 * (self._P1diam / self._diam)
            self._xgP10 = self._mygm / self._n_lamella / 2.
            self._xcP10 = self._mycm / self._n_lamella / 2.
            self._xraxialP10 = (self._rhoa * 100) / \
                               (np.pi * ((self._P1diam / 2. + self._space_p1) ** 2 - (self._P1diam / 2.) ** 2))
            self._cmP2 = 2 * (self._P2diam / self._diam)
            self._gpasP2 = 0.0001 * (self._P2diam / self._diam)
            self._xgP20 = self._mygm / self._n_lamella / 2.
            self._xcP20 = self._mycm / self._n_lamella / 2.
            self._cmI = 2 * (self._Idiam / self._diam)
            self._gpasI = 0.0001 * (self._Idiam / self._diam)
            self._xgI0 = self._mygm / self._n_lamella / 2.
            self._xcI0 = self._mycm / self._n_lamella / 2.
            self._xraxialI0 = (self._rhoa * 100) / \
                              (np.pi * ((self._Idiam / 2. + self._space_i) ** 2 - (self._Idiam / 2.) ** 2))

        # Model B.

        # In this model, the myelin sheath is not explicitly represented: its properties are included in the membrane
        # properties. This notwithstanding, this model is similar to Model C. Its benefit is to allow avoiding using
        # the extracellular mechanism. It should not be used if one cares to apply an extracellular stimulation to the
        # Branch or to the Fiber to which it belongs.

        elif self._model == 'ModelB':

            # Inclusion of axflut in paranodes2.
            self._axflut = False

            # Biophysical parameters.
            self._cmP1 = 1 / (1 / (2 * self._P1diam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasP1 = 1 / (1 / (0.001 * self._P1diam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))
            self._cmP2 = 1 / (1 / (2 * self._P2diam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasP2 = 1 / (1 / (0.0001 * self._P2diam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))
            self._cmI = 1 / (1 / (2 * self._Idiam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasI = 1 / (1 / (0.0001 * self._Idiam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))

    def build(self):
        """Build the Branch."""
        self.create_sections()
        self.set_sections()

    def create_sections(self):
        """Create the sections of the Branch."""

        # Create sections.
        self.nodes = [h.Section(name='node_{:d}'.format(i), cell=self) for i in range(self.nN)]
        self.paranodes1 = [h.Section(name='paranode1_{:d}'.format(i), cell=self) for i in range(self._nP1)]
        self.paranodes2 = [h.Section(name='paranode2_{:d}'.format(i), cell=self) for i in range(self._nP2)]
        self.internodes = [h.Section(name='internode_{:d}'.format(i), cell=self) for i in range(self._nI)]

        # Connect sections together.
        for i in range(self.nN - 1):
            self.paranodes1[2 * i].connect(self.nodes[i], 1, 0)
            self.paranodes2[2 * i].connect(self.paranodes1[2 * i], 1, 0)
            self.internodes[6 * i].connect(self.paranodes2[2 * i], 1, 0)
            self.internodes[6 * i + 1].connect(self.internodes[6 * i], 1, 0)
            self.internodes[6 * i + 2].connect(self.internodes[6 * i + 1], 1, 0)
            self.internodes[6 * i + 3].connect(self.internodes[6 * i + 2], 1, 0)
            self.internodes[6 * i + 4].connect(self.internodes[6 * i + 3], 1, 0)
            self.internodes[6 * i + 5].connect(self.internodes[6 * i + 4], 1, 0)
            self.paranodes2[2 * i + 1].connect(self.internodes[6 * i + 5], 1, 0)
            self.paranodes1[2 * i + 1].connect(self.paranodes2[2 * i + 1], 1, 0)
            self.nodes[i + 1].connect(self.paranodes1[2 * i + 1], 1, 0)

    def set_sections(self):
        """Set the properties of the sections of the Branch."""

        # Nodes.
        for node in self.nodes:
            node.nseg = 1
            node.diam = self._Ndiam
            node.L = self._Nlength
            node.Ra = self._RaN
            node.cm = self._cmN
            node.insert('axnode')
            if self._extra_mech:
                node.insert('extracellular')
                node.xg[0] = self._xgN0
                node.xg[1] = self._xgN1
                node.xc[0] = self._xcN0
                node.xc[1] = self._xcN1
                node.xraxial[0] = self._xraxialN0
                node.xraxial[1] = self._xraxialN1

        # Paranodes1.
        for paranode1 in self.paranodes1:
            paranode1.nseg = 1
            paranode1.diam = self._diam
            paranode1.L = self._P1length
            paranode1.Ra = self._RaP1
            paranode1.cm = self._cmP1
            paranode1.insert('pas')
            paranode1.g_pas = self._gpasP1
            paranode1.e_pas = self._Epas
            if self._extra_mech:
                paranode1.insert('extracellular')
                paranode1.xg[0] = self._xgP10
                paranode1.xg[1] = self._xgP11
                paranode1.xc[0] = self._xcP10
                paranode1.xc[1] = self._xcP11
                paranode1.xraxial[0] = self._xraxialP10
                paranode1.xraxial[1] = self._xraxialP11

        # Paranodes2.
        for paranode2 in self.paranodes2:
            paranode2.nseg = 1
            paranode2.diam = self._diam
            paranode2.L = self._P2length
            paranode2.Ra = self._RaP2
            paranode2.cm = self._cmP2
            if self._axflut:
                paranode2.insert('axflut')
                paranode2.gl_axflut = self._gpasP2
                paranode2.el_axflut = self._Epas
            else:
                paranode2.insert('pas')
                paranode2.g_pas = self._gpasP2
                paranode2.e_pas = self._Epas
            if self._extra_mech:
                paranode2.insert('extracellular')
                paranode2.xg[0] = self._xgP20
                paranode2.xg[1] = self._xgP21
                paranode2.xc[0] = self._xcP20
                paranode2.xc[1] = self._xcP21
                paranode2.xraxial[0] = self._xraxialP20
                paranode2.xraxial[1] = self._xraxialP21

        # Internodes.
        for internode in self.internodes:
            internode.nseg = 1
            internode.diam = self._diam
            internode.L = self._Ilength
            internode.Ra = self._RaI
            internode.cm = self._cmI
            internode.insert('pas')
            internode.g_pas = self._gpasI
            internode.e_pas = self._Epas
            if self._extra_mech:
                internode.insert('extracellular')
                internode.xg[0] = self._xgI0
                internode.xg[1] = self._xgI1
                internode.xc[0] = self._xcI0
                internode.xc[1] = self._xcI1
                internode.xraxial[0] = self._xraxialI0
                internode.xraxial[1] = self._xraxialI1

        # Build sections' 3D-coordinates.
        h.define_shape()

    def get_sections(self):
        """Return a list of references to the Branch's sections ordered from proximal to distal end.

        This is:  [node_0, paranode1_0, paranode2_0, internode_0, ..., node_last].

        """
        secs = []
        for i in range(self.nN - 1):
            secs.append(self.nodes[i])
            secs.append(self.paranodes1[2 * i])
            secs.append(self.paranodes2[2 * i])
            secs.append(self.internodes[6 * i])
            secs.append(self.internodes[6 * i + 1])
            secs.append(self.internodes[6 * i + 2])
            secs.append(self.internodes[6 * i + 3])
            secs.append(self.internodes[6 * i + 4])
            secs.append(self.internodes[6 * i + 5])
            secs.append(self.paranodes2[2 * i + 1])
            secs.append(self.paranodes1[2 * i + 1])
        secs.append(self.nodes[-1])

        return secs

    def adjust_el(self):
        """Adjust the leakage reversal potential of the Branch's active compartments to reach steady-state."""

        v = self.nodes[0].v

        # Nodes.
        ina = self.nodes[0].ina_axnode
        ik = self.nodes[0].ik_axnode
        inap = self.nodes[0].inap_axnode
        gl = self.nodes[0].gl_axnode
        for node in (self.nodes + self.nodes):
            node.el_axnode = (ina + ik + inap + v * gl) / gl
        print('el node:', (ina + ik + inap + v * gl) / gl)

        # Paranodes2 if applicable.
        # if self._axflut:
        #     ik = self.paranodes2[0].ik_axflut
        #     gl = self.paranodes2[0].gl_axflut
        #     for paranode2 in (self.paranodes2 + self.paranodes2):
        #         paranode2.el_axflut = (ik + v * gl) / gl
        # print('el flut:', (ik + v * gl) / gl)

    def get_longi_xs(self):
        """Return the longitudinal abscissa of the Branch's compartments.

        Return:
            xs: numpy.array.

        """

        nN = self.nN
        Nlength = self._Nlength
        P1length = self._P1length
        P2length = self._P2length
        Ilength = self._Ilength

        nC = 11 * (nN - 1) + 1
        xs = np.zeros(nC)
        xs[0] = Nlength / 2.0
        for i in range(nN - 1):
            xs[11 * i + 1] = xs[11 * i] + Nlength / 2.0 + P1length / 2.0
            xs[11 * i + 2] = xs[11 * i + 1] + P1length / 2.0 + P2length / 2.0
            xs[11 * i + 3] = xs[11 * i + 2] + P2length / 2.0 + Ilength / 2.0
            xs[11 * i + 4] = xs[11 * i + 3] + Ilength
            xs[11 * i + 5] = xs[11 * i + 4] + Ilength
            xs[11 * i + 6] = xs[11 * i + 5] + Ilength
            xs[11 * i + 7] = xs[11 * i + 6] + Ilength
            xs[11 * i + 8] = xs[11 * i + 7] + Ilength
            xs[11 * i + 9] = xs[11 * i + 8] + Ilength / 2.0 + P2length / 2.0
            xs[11 * i + 10] = xs[11 * i + 9] + P2length / 2.0 + P1length / 2.0
            xs[11 * i + 11] = xs[11 * i + 10] + P1length / 2.0 + Nlength / 2.0

        return xs


###########################################################################
