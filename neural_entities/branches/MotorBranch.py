# -*- coding: utf-8 -*


#############################################################################
# Class MotorBranch.
#
# Created on: 21 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
import weakref
from neuron import h


class MotorBranch(object):
    """
    This class regroups attributes and methods describing a motor nerve branch.

    Class variables defined here:
        label       : 'MotorBranch' : default MotorBranch label.
        nIS         : 3         : default number of sections composing the initial segment.
        _nIntCNS    : 4         : default number of internodal sections in the CNS.
        _rhoa       : 70.0      : default axoplasmic resistivity (in Ohm*cm).
        _mygm       : 0.001     : default default myelin lamella specific conductance (in S/cm^2).
        _mycm       : 0.1       : default myelin lamella specific capacitance (in uF/cm^2).
        _Epas       : -70.0     : default reversal potential of passive sections (in mV).
        _RaIS       : 70.0      : default initial segment axoplasmic resistivity in (Ohm*cm).
        _cmIS       : 2.0       : default initial segment membrane specific capacitance (in uF/cm^2).
        _xgIS0      : 1e10      : default initial segment 1st extracellular layer conductance (in S/cm^2).
        _xgIS1      : 1e10      : default initial segment 2nd extracellular layer conductance (in S/cm^2).
        _xcIS0      : 0.0       : default initial segment 1st extracellular layer capacitance (in uF/cm^2).
        _xcIS1      : 0.0       : default initial segment 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialIS0 : 1e10      : default initial segment 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialIS1 : 1e10      : default initial segment 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _nNCNS      : 4         : default number of nodes in the CNS.
        _Nlength    : 1.0       : default nodes length (in um).
        _RaN        : 70.0      : default nodes axoplasmic resistivity in (Ohm*cm).
        _cmN        : 2.0       : default nodes membrane specific capacitance (in uF/cm^2).
        _xgN0       : 1e10      : default nodes 1st extracellular layer conductance (in S/cm^2).
        _xgN1       : 1e10      : default nodes 2nd extracellular layer conductance (in S/cm^2).
        _xcN0       : 0.0       : default nodes 1st extracellular layer capacitance (in uF/cm^2).
        _xcN1       : 0.0       : default nodes 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialN0  : 1e10      : default nodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialN1  : 1e10      : default nodes 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP1CNS     : 8         : default number of paranodes1 in the CNS.
        _P1length   : 3.0       : default paranodes1 length (in um).
        _space_p1   : 0.002     : default paranodes1 axon-myelin interspace width (in um).
        _xgP10      : 1e10      : default paranodes1 1st extracellular layer conductance (in S/cm^2).
        _xgP11      : 1e10      : default paranodes1 2nd extracellular layer conductance (in S/cm^2).
        _xcP10      : 0.0       : default paranodes1 1st extracellular layer capacitance (in uF/cm^2).
        _xcP11      : 0.0       : default paranodes1 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialP10 : 1e10      : default paranodes1 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialP11 : 1e10      : default paranodes1 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP2CNS     : 8         : default number of paranodes2 in the CNS.
        _space_p2   : 0.004     : default paranodes2 axon-myelin interspace width (in um).
        _xgP20      : 1e10      : default paranodes2 1st extracellular layer conductance (in S/cm^2).
        _xgP21      : 1e10      : default paranodes2 2nd extracellular layer conductance (in S/cm^2).
        _xcP20      : 0.0       : default paranodes2 1st extracellular layer capacitance (in uF/cm^2).
        _xcP21      : 0.0       : default paranodes2 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialP20 : 1e10      : default paranodes2 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialP21 : 1e10      : default paranodes2 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _nICNS      : 24        : default number of internodes in the CNS.
        _space_i    : 0.004     : default internodes axon-myelin interspace width (in um).
        _xgI0       : 1e10      : default internodes 1st extracellular layer conductance (in S/cm^2).
        _xgI1       : 1e10      : default internodes 2nd extracellular layer conductance (in S/cm^2).
        _xcI0       : 0.0       : default internodes 1st extracellular layer capacitance (in uF/cm^2).
        _xcI1       : 0.0       : default internodes 2nd extracellular layer capacitance (in uF/cm^2).
        _xraxialI0  : 1e10      : default internodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _xraxialI1  : 1e10      : default internodes 2nd extracellular layer periaxonal resistivity (in MOhm/cm).
        _model      : 'ModelD'  : default identifier of MotorBranch model.
        _axflut     : False     : default indicator whether mechanism 'axflut' should be inserted in paranodes2 or not.
        _extra_mech : False     : default indicator whether extracellular mechanism should be inserted or not.

    Instance variables defined here:
        axon            : weakref.ref   : weakref to the MotorAxon object to which the MotorBranch belongs.
        diam            : float : diameter of the MotorBranch (in um).
        nN              : int   : total number of nodes of the MotorBranch.
        inisegs         : list of h.Section objects.
        nodesCNS        : list of h.Section objects.
        nodesPNS        : list of h.Section objects.
        paranodes1CNS   : list of h.Section objects.
        paranodes1PNS   : list of h.Section objects.
        paranodes2CNS   : list of h.Section objects.
        paranodes2PNS   : list of h.Section objects.
        internodesCNS   : list of h.Section objects.
        internodesPNS   : list of h.Section objects.
        _nIntPNS        : int   : default number of internodal sections in the PNS.
        _n_lamella      : int   : number of myelin lamellas.
        _ISdiam         : float : diameter of the initial segment sections (in um).
        _ISlength       : float : length of the initial segment sections (in um).
        _nNPNS          : int   : number of nodes in the PNS of the MotorBranch.
        _Ndiam          : float : diameter of the nodes (in um).
        _xraxialN0      : float : nodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP1PNS         : int   : number of paranodes1 in the PNS.
        _P1diam         : float : diameter of the paranodes1 (in um).
        _RaP1           : float : paranodes1 axoplasmic resistivity (in Ohm*cm).
        _cmP1           : float : paranodes1 membrane specific capacitance (in uF/cm^2).
        _gpasP1         : float : paranodes1 passive membrane conductance (in S/cm^2).
        _xgP10          : float : paranodes1 1st extracellular layer conductance (in S/cm^2).
        _xcP10          : float : paranodes1 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialP10     : float : paranodes1 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nP2PNS         : int   : number of paranodes2 in the PNS.
        _P2diam         : float : diameter of the paranodes2 (in um).
        _P2length       : float : length of the paranodes2 (in um).
        _RaP2           : float : paranodes2 axoplasmic resistivity (in Ohm*cm).
        _cmP2           : float : paranodes2 membrane specific capacitance (in uF/cm^2).
        _gpasP2         : float : paranodes2 passive membrane conductance (in S/cm^2).
        _xgP20          : float : paranodes2 1st extracellular layer conductance (in S/cm^2).
        _xcP20          : float : paranodes2 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialP20     : float : paranodes2 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _nIPNS          : int   : number of internodes in the PNS.
        _Idiam          : float : diameter of the internodes (in um).
        _ICNSlength     : float : length of the CNS internodes (in um).
        _IPNSlength     : float : length of the PNS internodes (in um).
        _RaI            : float : internodes axoplasmic resistivity (in Ohm*cm).
        _cmI            : float : internodes membrane specific capacitance (in uF/cm^2).
        _gpasI          : float : internodes passive membrane conductance (in S/cm^2).
        _xgI0           : float : internodes 1st extracellular layer conductance (in S/cm^2).
        _xcI0           : float : internodes 1st extracellular layer capacitance (in uF/cm^2).
        _xraxialI0      : float : internodes 1st extracellular layer periaxonal resistivity (in MOhm/cm).
        _deltaX         : float : node-to-node distance in the PNS (in um).
        _model          : str   : default identifier of MotorBranch model.
        _axflut         : bool  : indicator whether mechanism 'axflut' should be inserted in paranodes2 or not.
        _extra_mech     : bool  : indicator whether extracellular mechanism should be inserted or not.

    Methods defined here:
        __init__(self, *args, **kwargs)
        __repr__(self)
        get_info(self)
        _set_params(self)
        build(self)
        create_sections(self)
        set_sections(self)
        get_sections(self)
        adjust_el(self)
        get_longi_xs(self)

    The MotorBranch class provides attributes and methods to build and control a single unbranching portion of
    myelinated neurite organized as follows:

        IS - IS - IS - P1CNS - P2CNS - ICNS - ICNS - ICNS - ICNS - ICNS - ICNS - P2CNS - P1CNS - NCNS - P1CNS - ... -
        P2CNS - P1CNS - NCNS - P1PNS - P2PNS - IPNS - IPNS - IPNS - IPNS - IPNS - IPNS - P2PNS - P1PNS - NPNS - P1PNS -
        ... - P2PNS - P1PNS - NPNS

    where IS stands for 'initial segment', NCNS for 'node CNS', P1CNS for 'paranode1 CNS', P2CNS for 'paranode2 CNS',
    ICNS for 'internode CNS' and similarly for NPNS, P1PNS, P2PNS and IPNS. This representation of a portion of
    myelinated neurite is taken from 'Extracellular Stimulation of Central Neurons: Influence of Stimulus Waveform
    and Frequency on Neuronal Output', McIntyre et al., Journal of Neurophysiology, 2002. The internodesCNS differ
    from the internodesPNS by their lengths while the nodesCNS and nodesPNS, paranodes1CNS and paranodes2PNS,
    and paranodes2CNS and paranodes2PNS are identical. The differentiation between internodesCNS and internodesPNS is
    based on an observation reported in 'Dimensions of individual alpha and gamma motor fibres in the ventral
    funiculus of the cat spinal cord', Fabricius et al., Journal of Anatomy, 1994. A MotorBranch comprises a certain
    number of internodal sections in the CNS and the remaining in the PNS.

    Each initial segment, nodeCNS, paranode1CNS, paranode2CNS, internodeCNS, nodePNS, paranode1PNS, paranode2PNS,
    and internodePNS is represented by a single HOC section, with nseg=1. Their dimensions are dictated by the global
    diameter of the MotorBranch (which in fact is the diameter of its myelinated portions) and are derived from it
    using piecewise-linear interpolants based on the dataset originally given in (McIntyre et al., 2002) and in the
    following other publication: 'Modelling the effects of electric fields on nerve fibres: influence of the myelin
    sheath', Richardson et al., Medical & Biological Engineering & Computing, 2000.

    The biophysical properties of the initial segments, nodesCNS, paranodes1CNS, paranodes2CNS, internodesCNS,
    nodesPNS, paranodes1PNS, paranodes2PNS, and internodesPNS are also derived from the mentioned publications and
    from the following additional publication: 'Cellular Effects of Deep Brain Stimulation: Model-Based Analysis of
    Activation and Inhibition', McIntyre et al., Translational Physiology, 2004.
    Several models are available (see method _set_params).

    A MotorBranch usually belongs to a MotorAxon (either a fibers.MotorAxon, either a cell_comps.MotorAxon) from
    which it draws its morphological parameters and model, but it can also be instantiated to stand alone by using
    the dedicated keyword arguments of the constructor.

    """

    label = 'MotorBranch'
    _nIntCNS = 4
    _rhoa = 70.0            # Ohm * cm
    _mygm = 0.001           # S / cm^2
    _mycm = 0.1             # uF / cm^2
    _Epas = - 70.0          # mV
    nIS = 3
    _RaIS = _rhoa
    _cmIS = 2.0             # uF / cm^2
    _xgIS0 = 1e10           # S / cm^2
    _xgIS1 = 1e10           # S / cm^2
    _xcIS0 = 0.0            # uF / cm^2
    _xcIS1 = 0.0            # uF / cm^2
    _xraxialIS0 = 1e10      # MOhm / cm
    _xraxialIS1 = 1e10      # MOhm / cm
    _nNCNS = _nIntCNS
    _Nlength = 1.0          # um
    _RaN = _rhoa
    _cmN = 2.0              # uF / cm^2
    _xgN0 = 1e10            # S / cm^2
    _xgN1 = 1e10            # S / cm^2
    _xcN0 = 0.0             # uF / cm^2
    _xcN1 = 0.0             # uF / cm^2
    _xraxialN0 = 1e10       # MOhm / cm
    _xraxialN1 = 1e10       # MOhm / cm
    _nP1CNS = 2 * _nIntCNS
    _P1length = 3.0         # um
    _space_p1 = 0.002       # um
    _xgP10 = 1e10           # S / cm^2
    _xgP11 = 1e10           # S / cm^2
    _xcP10 = 0.0            # uF / cm^2
    _xcP11 = 0.0            # uF / cm^2
    _xraxialP10 = 1e10      # MOhm / cm
    _xraxialP11 = 1e10      # MOhm / cm
    _nP2CNS = 2 * _nIntCNS
    _space_p2 = 0.004       # um
    _xgP20 = 1e10           # S / cm^2
    _xgP21 = 1e10           # S / cm^2
    _xcP20 = 0.0            # uF / cm^2
    _xcP21 = 0.0            # uF / cm^2
    _xraxialP20 = 1e10      # MOhm / cm
    _xraxialP21 = 1e10      # MOhm / cm
    _nICNS = 6 * _nIntCNS
    _space_i = 0.004        # um
    _xgI0 = 1e10            # S / cm^2
    _xgI1 = 1e10            # S / cm^2
    _xcI0 = 0.0             # uF / cm^2
    _xcI1 = 0.0             # uF / cm^2
    _xraxialI0 = 1e10       # MOhm / cm
    _xraxialI1 = 1e10       # MOhm / cm
    _model = 'ModelD'
    _axflut = False
    _extra_mech = False

    # McIntyre's morphological parameters database, and interpolants ('Modelling the effects of electric fields on
    # nerve fibres: influence of the myelin sheath', Richardson et al., Medical & Biological Engineering & Computing,
    # 2000).
    _diams = np.asarray([0.0, 5.7, 7.3, 8.7, 10.0, 11.5, 12.8, 14.0, 15.0, 16.0])                       # um
    _Ndiams = np.asarray([0.0, 1.9, 2.4, 2.8, 3.3, 3.7, 4.2, 4.7, 5.0, 5.5])                            # um
    _P2diams = np.asarray([0.0, 3.4, 4.6, 5.8, 6.9, 8.1, 9.2, 10.4, 11.5, 12.7])                        # um
    _P2lengths = np.asarray([0.0, 35.0, 38.0, 40.0, 46.0, 50.0, 54.0, 56.0, 58.0, 60.0])                # um
    _deltaXs = np.asarray([0.0, 500.0, 750., 1000.0, 1150.0, 1250.0, 1350.0, 1400.0, 1450.0, 1500.0])   # um
    _n_lamellas = np.asarray([0.0, 80.0, 100.0, 110.0, 120.0, 130.0, 135.0, 140.0, 145.0, 150.0])

    _make_lambdas_list = lambda ys, xs: \
        [(lambda x, i=i: (ys[i + 1] - ys[i]) / (xs[i + 1] - xs[i]) * (x - xs[i]) + ys[i]) for i in range(len(xs) - 1)]

    fISdiam = [lambda x: 4.0 / 11.5 * x]
    _fISlength = [lambda x: 10.0 / 11.5 * x]
    _fICNSlength = [lambda x: x / 3.0]
    _fNdiams = _make_lambdas_list(_Ndiams, _diams)
    _fP2diams = _make_lambdas_list(_P2diams, _diams)
    _fP2lengths = _make_lambdas_list(_P2lengths, _diams)
    _fdeltaXs = _make_lambdas_list(_deltaXs, _diams)
    _fnLamellas = _make_lambdas_list(_n_lamellas, _diams)
    _diams = _diams[:-1]

    def __init__(self, *args, **kwargs):
        """Class constructor method.

        Usage:
            mb = MotorBranch(axon=some_axon)
            mb = MotorBranch(nN=some_nN, diam=some_diam)
            mb = MotorBranch(..., model=model)
            mb = MotorBranch(label, ...)

        Optional arguments:
            label   : str   : label of MotorBranch.

        Keyword arguments:
            cell    : fibers.MotorAxon or cell_comps.MotorAxon object    : to which the MotorBranch belongs.
            nN      : int   : number of nodes of Ranvier of the MotorBranch.
            diam    : float : diameter of the MotorBranch (in um).
            model   : str   : identifier of MotorBranch model.

        If any of nN, diam or model is specified in argument, it overwrites any value eventually extracted from the
        MotorAxon to which the MotorBranch belongs.

        """

        self.name = 'MotorBranch_{:d}'.format(id(self))
        self.axon = lambda: None

        if args: self.label = args[0]

        try:
            self.axon = weakref.ref(kwargs['cell'])
            self.name = '{}.{}'.format(self.axon().name, self.label)
            self.nN = self.axon().get_morpho(self.label, 'nN')
            self._diam = self.axon().get_morpho(self.label, 'diam')
            self._model = self.axon().model()
        except:
            pass

        try:
            self.nN = kwargs['nN']
        except:
            pass

        try:
            self._diam = kwargs['diam']
        except:
            pass

        try:
            self._model = kwargs['model']
        except:
            pass

        self._nIntPNS = None
        self._n_lamella = None
        self._ISdiam = None
        self._ISlength = None
        self._nNPNS = None
        self._Ndiam = None
        self._nP1PNS = None
        self._P1diam = None
        self._RaP1 = None
        self._cmP1 = None
        self._gpasP1 = None
        self._nP2PNS = None
        self._P2diam = None
        self._P2length = None
        self._RaP2 = None
        self._cmP2 = None
        self._gpasP2 = None
        self._nIPNS = None
        self._Idiam = None
        self._ICNSlength = None
        self._IPNSlength = None
        self._RaI = None
        self._cmI = None
        self._gpasI = None
        self._deltaX = None

        self.inisegs = None
        self.nodesCNS = None
        self.nodesPNS = None
        self.paranodes1CNS = None
        self.paranodes1PNS = None
        self.paranodes2CNS = None
        self.paranodes2PNS = None
        self.internodesCNS = None
        self.internodesPNS = None

        self._set_params()

    def __repr__(self):
        """Return the string representation of the MotorBranch."""
        return self.name

    def get_info(self):
        """Return a string giving info on the MotorBranch."""
        string = '{}:\n'.format(self.__class__.__name__)
        string += '  diam: {:f}\n'.format(self._diam)
        string += '  nIS: {:d}\n'.format(self.nIS)
        string += '  ISdiam: {:f}\n'.format(self._ISdiam)
        string += '  ISlength: {:f}\n'.format(self._ISlength)
        string += '  nN: {:d}\n'.format(self.nN)
        string += '  Ndiam: {:f}\n'.format(self._Ndiam)
        string += '  Nlength: {:f}\n'.format(self._Nlength)
        string += '  P1diam: {:f}\n'.format(self._P1diam)
        string += '  P1length: {:f}\n'.format(self._P1length)
        string += '  P2diam: {:f}\n'.format(self._P2diam)
        string += '  P2length: {:f}\n'.format(self._P2length)
        string += '  nICNS: {:d}\n'.format(self._nICNS)
        string += '  ICNSlength: {:f}\n'.format(self._ICNSlength)
        string += '  nIPNS: {:d}\n'.format(self._nIPNS)
        string += '  IPNSlength: {:f}\n'.format(self._IPNSlength)
        string += '  Idiam: {:f}\n'.format(self._Idiam)
        return string

    def _set_params(self):
        """Set the geometrico-biophysical attributes of the MotorBranch.

        These are derived from the diameter, the number of nodes of Ranvier, and the model of the MotorBranch.

        """

        # Morphological parameters common to all models.
        self._nIntPNS = self.nN - self._nIntCNS
        self._nNPNS = self._nIntPNS
        self._nP1PNS = 2 * self._nIntPNS
        self._nP2PNS = 2 * self._nIntPNS
        self._nIPNS = 6 * self._nIntPNS
        self._ISdiam = self.fISdiam[0](self._diam)
        self._ISlength = self._fISlength[0](self._diam)
        self._Ndiam = np.piecewise(self._diam, self._diam > self._diams, self._fNdiams)
        self._P1diam = self._Ndiam
        self._P2diam = np.piecewise(self._diam, self._diam > self._diams, self._fP2diams)
        self._P2length = np.piecewise(self._diam, self._diam > self._diams, self._fP2lengths)
        self._Idiam = self._P2diam
        self._deltaX = np.piecewise(self._diam, self._diam > self._diams, self._fdeltaXs)
        self._IPNSlength = (self._deltaX - 2. * self._P1length - 2. * self._P2length - self._Nlength) / 6.
        self._ICNSlength = self._fICNSlength[0](self._IPNSlength)
        self._n_lamella = np.piecewise(self._diam, self._diam > self._diams, self._fnLamellas)

        # Biophysical parameters common to all models.
        self._RaP1 = self._rhoa * (self._diam / self._P1diam) ** 2
        self._RaP2 = self._rhoa * (self._diam / self._P2diam) ** 2
        self._RaI = self._rhoa * (self._diam / self._Idiam) ** 2

        # Models C and D.

        # In these models, the myelin sheath is represented by the first layer of NEURON's extracellular mechanism.
        # The nodes of Ranvier embed ionic channels described by the mechanism 'MNAXNODE' (which is almost similar to
        # the mechanism 'AXNODE' used by the Branch class, but in which the potentials are all shifted up by 10mV,
        # such that the resting potential of a MotorBranch is close to -70mV instead of -80mV as is the case of a
        # Branch), and the initial segments embed ionic channels described by the mechanism 'INISEG'. The geometry of
        # the initial segments and nodes and the geometry and passive properties of the paranodes1, paranodes2,
        # and internodes are derived from 'Modelling the effects of electric fields on nerve fibres: influence of the
        # myelin sheath', Richardson et al., Medical & Biological Engineering & Computing, 2000, and 'Extracellular
        # Stimulation of Central Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output',
        # McIntyre et al., Journal of Neurophysiology, 2002.

        # Model D differs from model C in that its paranodes2 (FLUT compartments in McIntyre et al.'s publication)
        # embed a K+ ionic channel described by the mechanism 'MNAXFLUT' (which is almost similar to the mechanism
        # 'AXFLUT' used by the Branch class, but in which the potentials are all shifted up by 10mV, just like the
        # 'MNAXNODE' mechanism compared to the 'AXNODE' mechanism). The 'AXFLUT' mechanism is taken from 'Cellular
        # Effects of Deep Brain Stimulation: Model-Based Analysis of Activation and Inhibition', McIntyre et al.,
        # Translational Physiology, 2004.

        if self._model in {'ModelC', 'ModelD'}:

            # Inclusion of axflut in paranodes2.
            if self._model == 'ModelD':
                self._axflut = True

            # Extracellular mechanism.
            self._extra_mech = True

            # Biophysical parameters.
            self._xraxialN0 = (self._rhoa * 100) / \
                              (np.pi * ((self._Ndiam / 2. + self._space_p1) ** 2 - (self._Ndiam / 2.) ** 2))
            self._cmP1 = 2 * (self._P1diam / self._diam)
            self._gpasP1 = 0.001 * (self._P1diam / self._diam)
            self._xgP10 = self._mygm / self._n_lamella / 2.
            self._xcP10 = self._mycm / self._n_lamella / 2.
            self._xraxialP10 = (self._rhoa * 100) / \
                               (np.pi * ((self._P1diam / 2. + self._space_p1) ** 2 - (self._P1diam / 2.) ** 2))
            self._cmP2 = 2 * (self._P2diam / self._diam)
            self._gpasP2 = 0.0001 * (self._P2diam / self._diam)
            self._xgP20 = self._mygm / self._n_lamella / 2.
            self._xcP20 = self._mycm / self._n_lamella / 2.
            self._cmI = 2 * (self._Idiam / self._diam)
            self._gpasI = 0.0001 * (self._Idiam / self._diam)
            self._xgI0 = self._mygm / self._n_lamella / 2.
            self._xcI0 = self._mycm / self._n_lamella / 2.
            self._xraxialI0 = (self._rhoa * 100) / \
                              (np.pi * ((self._Idiam / 2. + self._space_i) ** 2 - (self._Idiam / 2.) ** 2))

        # Model B.

        # In this model, the myelin sheath is not explicitly represented: its properties are included in the membrane
        # properties. This notwithstanding, this model is similar to Model C. Its benefit is to allow avoiding using
        # the extracellular mechanism. It should not be used if one cares to apply an extracellular stimulation to
        # the MotorBranch or to the MotorAxon to which it belongs.

        elif self._model == 'ModelB':

            # Inclusion of axflut in paranodes2.
            self._axflut = False

            # Biophysical parameters.
            self._cmP1 = 1 / (1 / (2 * self._P1diam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasP1 = 1 / (1 / (0.001 * self._P1diam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))
            self._cmP2 = 1 / (1 / (2 * self._P2diam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasP2 = 1 / (1 / (0.0001 * self._P2diam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))
            self._cmI = 1 / (1 / (2 * self._Idiam / self._diam) + 1 / (self._mycm / self._n_lamella / 2.))
            self._gpasI = 1 / (1 / (0.0001 * self._Idiam / self._diam) + 1 / (self._mygm / self._n_lamella / 2.))

    def build(self):
        """Build the MotorBranch."""
        self.create_sections()
        self.set_sections()

    def create_sections(self):
        """Create the sections of the MotorBranch."""

        # Create sections.
        self.inisegs = [h.Section(name='iniseg_{:d}'.format(i), cell=self) for i in range(self.nIS)]
        self.nodesCNS = [h.Section(name='nodeCNS_{:d}'.format(i), cell=self) for i in range(self._nNCNS)]
        self.nodesPNS = [h.Section(name='nodePNS_{:d}'.format(i), cell=self) for i in range(self._nNPNS)]
        self.paranodes1CNS = [h.Section(name='paranode1CNS_{:d}'.format(i), cell=self) for i in range(self._nP1CNS)]
        self.paranodes1PNS = [h.Section(name='paranode1PNS_{:d}'.format(i), cell=self) for i in range(self._nP1PNS)]
        self.paranodes2CNS = [h.Section(name='paranode2CNS_{:d}'.format(i), cell=self) for i in range(self._nP2CNS)]
        self.paranodes2PNS = [h.Section(name='paranode2PNS_{:d}'.format(i), cell=self) for i in range(self._nP2PNS)]
        self.internodesCNS = [h.Section(name='internodeCNS_{:d}'.format(i), cell=self) for i in range(self._nICNS)]
        self.internodesPNS = [h.Section(name='internodePNS_{:d}'.format(i), cell=self) for i in range(self._nIPNS)]

        # Connect sections together.
        for i in range(self.nIS - 1):
            self.inisegs[i + 1].connect(self.inisegs[i], 1, 0)

        for i in range(self._nIntCNS):
            if i == 0:
                self.paranodes1CNS[0].connect(self.inisegs[self.nIS - 1], 1, 0)
            else:
                self.paranodes1CNS[2 * i].connect(self.nodesCNS[i - 1], 1, 0)
            self.paranodes2CNS[2 * i].connect(self.paranodes1CNS[2 * i], 1, 0)
            self.internodesCNS[6 * i].connect(self.paranodes2CNS[2 * i], 1, 0)
            self.internodesCNS[6 * i + 1].connect(self.internodesCNS[6 * i], 1, 0)
            self.internodesCNS[6 * i + 2].connect(self.internodesCNS[6 * i + 1], 1, 0)
            self.internodesCNS[6 * i + 3].connect(self.internodesCNS[6 * i + 2], 1, 0)
            self.internodesCNS[6 * i + 4].connect(self.internodesCNS[6 * i + 3], 1, 0)
            self.internodesCNS[6 * i + 5].connect(self.internodesCNS[6 * i + 4], 1, 0)
            self.paranodes2CNS[2 * i + 1].connect(self.internodesCNS[6 * i + 5], 1, 0)
            self.paranodes1CNS[2 * i + 1].connect(self.paranodes2CNS[2 * i + 1], 1, 0)
            self.nodesCNS[i].connect(self.paranodes1CNS[2 * i + 1], 1, 0)

        for i in range(self._nIntPNS):
            if i == 0:
                self.paranodes1PNS[0].connect(self.nodesCNS[-1], 1, 0)
            else:
                self.paranodes1PNS[2 * i].connect(self.nodesPNS[i - 1], 1, 0)
            self.paranodes2PNS[2 * i].connect(self.paranodes1PNS[2 * i], 1, 0)
            self.internodesPNS[6 * i].connect(self.paranodes2PNS[2 * i], 1, 0)
            self.internodesPNS[6 * i + 1].connect(self.internodesPNS[6 * i], 1, 0)
            self.internodesPNS[6 * i + 2].connect(self.internodesPNS[6 * i + 1], 1, 0)
            self.internodesPNS[6 * i + 3].connect(self.internodesPNS[6 * i + 2], 1, 0)
            self.internodesPNS[6 * i + 4].connect(self.internodesPNS[6 * i + 3], 1, 0)
            self.internodesPNS[6 * i + 5].connect(self.internodesPNS[6 * i + 4], 1, 0)
            self.paranodes2PNS[2 * i + 1].connect(self.internodesPNS[6 * i + 5], 1, 0)
            self.paranodes1PNS[2 * i + 1].connect(self.paranodes2PNS[2 * i + 1], 1, 0)
            self.nodesPNS[i].connect(self.paranodes1PNS[2 * i + 1], 1, 0)

    def set_sections(self):
        """Set the properties of the sections of the MotorBranch."""

        # Inisegs.
        for iniseg in self.inisegs:
            iniseg.nseg = 1
            iniseg.diam = self._ISdiam
            iniseg.L = self._ISlength
            iniseg.Ra = self._RaIS
            iniseg.cm = self._cmIS
            iniseg.insert('initial')
            if self._extra_mech:
                iniseg.insert('extracellular')
                iniseg.xg[0] = self._xgIS0
                iniseg.xg[1] = self._xgIS1
                iniseg.xc[0] = self._xcIS0
                iniseg.xc[1] = self._xcIS1
                iniseg.xraxial[0] = self._xraxialIS0
                iniseg.xraxial[1] = self._xraxialIS1

        # Nodes.
        for node in (self.nodesCNS + self.nodesPNS):
            node.nseg = 1
            node.diam = self._Ndiam
            node.L = self._Nlength
            node.Ra = self._RaN
            node.cm = self._cmN
            node.insert('mnaxnode')
            if self._extra_mech:
                node.insert('extracellular')
                node.xg[0] = self._xgN0
                node.xg[1] = self._xgN1
                node.xc[0] = self._xcN0
                node.xc[1] = self._xcN1
                node.xraxial[0] = self._xraxialN0
                node.xraxial[1] = self._xraxialN1

        # Paranodes1.
        for paranode1 in (self.paranodes1CNS + self.paranodes1PNS):
            paranode1.nseg = 1
            paranode1.diam = self._diam
            paranode1.L = self._P1length
            paranode1.Ra = self._RaP1
            paranode1.cm = self._cmP1
            paranode1.insert('pas')
            paranode1.g_pas = self._gpasP1
            paranode1.e_pas = self._Epas
            if self._extra_mech:
                paranode1.insert('extracellular')
                paranode1.xg[0] = self._xgP10
                paranode1.xg[1] = self._xgP11
                paranode1.xc[0] = self._xcP10
                paranode1.xc[1] = self._xcP11
                paranode1.xraxial[0] = self._xraxialP10
                paranode1.xraxial[1] = self._xraxialP11

        # Paranodes2.
        for paranode2 in (self.paranodes2CNS + self.paranodes2PNS):
            paranode2.nseg = 1
            paranode2.diam = self._diam
            paranode2.L = self._P2length
            paranode2.Ra = self._RaP2
            paranode2.cm = self._cmP2
            if self._axflut:
                paranode2.insert('mnaxflut')
                paranode2.gl_mnaxflut = self._gpasP2
                paranode2.el_mnaxflut = self._Epas
            else:
                paranode2.insert('pas')
                paranode2.g_pas = self._gpasP2
                paranode2.e_pas = self._Epas
            if self._extra_mech:
                paranode2.insert('extracellular')
                paranode2.xg[0] = self._xgP20
                paranode2.xg[1] = self._xgP21
                paranode2.xc[0] = self._xcP20
                paranode2.xc[1] = self._xcP21
                paranode2.xraxial[0] = self._xraxialP20
                paranode2.xraxial[1] = self._xraxialP21

        # Internodes.
        for internode in self.internodesCNS:
            internode.nseg = 1
            internode.diam = self._diam
            internode.L = self._ICNSlength
            internode.Ra = self._RaI
            internode.cm = self._cmI
            internode.insert('pas')
            internode.g_pas = self._gpasI
            internode.e_pas = self._Epas
            if self._extra_mech:
                internode.insert('extracellular')
                internode.xg[0] = self._xgI0
                internode.xg[1] = self._xgI1
                internode.xc[0] = self._xcI0
                internode.xc[1] = self._xcI1
                internode.xraxial[0] = self._xraxialI0
                internode.xraxial[1] = self._xraxialI1

        # Internodes.
        for internode in self.internodesPNS:
            internode.nseg = 1
            internode.diam = self._diam
            internode.L = self._IPNSlength
            internode.Ra = self._RaI
            internode.cm = self._cmI
            internode.insert('pas')
            internode.g_pas = self._gpasI
            internode.e_pas = self._Epas
            if self._extra_mech:
                internode.insert('extracellular')
                internode.xg[0] = self._xgI0
                internode.xg[1] = self._xgI1
                internode.xc[0] = self._xcI0
                internode.xc[1] = self._xcI1
                internode.xraxial[0] = self._xraxialI0
                internode.xraxial[1] = self._xraxialI1

        # Build sections' 3D-coordinates.
        h.define_shape()

    def get_sections(self):
        """Return a list of references to the MotorBranch's sections ordered from proximal to distal end.

        This is:  [iniseg_0, iniseg_1, iniseg_2, paranode1CNS_0, paranode2CNS_0, internodeCNS_0, ..., paranode1CNS_1,
                   nodeCNS_0, paranode1CNS_3, paranode2CNS_3, internodeCNS_7, ..., nodeCNS_4, paranode1PNS_0, ...,
                   paranodes1PNS_1, nodePNS_0, ... nodePNS_last].

        """
        secs = list(self.inisegs)
        for i in range(self._nIntCNS):
            secs.append(self.paranodes1CNS[2 * i])
            secs.append(self.paranodes2CNS[2 * i])
            secs.append(self.internodesCNS[6 * i])
            secs.append(self.internodesCNS[6 * i + 1])
            secs.append(self.internodesCNS[6 * i + 2])
            secs.append(self.internodesCNS[6 * i + 3])
            secs.append(self.internodesCNS[6 * i + 4])
            secs.append(self.internodesCNS[6 * i + 5])
            secs.append(self.paranodes2CNS[2 * i + 1])
            secs.append(self.paranodes1CNS[2 * i + 1])
            secs.append(self.nodesCNS[i])
        for i in range(self._nIntPNS):
            secs.append(self.paranodes1PNS[2 * i])
            secs.append(self.paranodes2PNS[2 * i])
            secs.append(self.internodesPNS[6 * i])
            secs.append(self.internodesPNS[6 * i + 1])
            secs.append(self.internodesPNS[6 * i + 2])
            secs.append(self.internodesPNS[6 * i + 3])
            secs.append(self.internodesPNS[6 * i + 4])
            secs.append(self.internodesPNS[6 * i + 5])
            secs.append(self.paranodes2PNS[2 * i + 1])
            secs.append(self.paranodes1PNS[2 * i + 1])
            secs.append(self.nodesPNS[i])

        return secs

    def adjust_el(self):
        """Adjust the leakage reversal potential of the MotorBranch's active compartments to reach steady-state."""

        v = self.inisegs[0].v

        # Inisegs.
        ina = self.inisegs[0].ina_initial
        ikrect = self.inisegs[0].ikrect_initial
        inap = self.inisegs[0].inap_initial
        gl = self.inisegs[0].gl_initial
        for iniseg in self.inisegs:
            iniseg.el_initial = (ina + ikrect + inap + v * gl) / gl
        # print('el iniseg: ', (ina + ikrect + inap + v * gl) / gl)

        # Nodes.
        ina = self.nodesCNS[0].ina_mnaxnode
        ik = self.nodesCNS[0].ik_mnaxnode
        inap = self.nodesCNS[0].inap_mnaxnode
        gl = self.nodesCNS[0].gl_mnaxnode
        for node in (self.nodesCNS + self.nodesPNS):
            node.el_mnaxnode = (ina + ik + inap + v * gl) / gl
        # print('el node: ', (ina + ik + inap + v * gl) / gl)

        # # Paranodes2 if applicable.
        # if self._axflut:
        #     ik = self.paranodes2CNS[0].ik_mnaxflut
        #     gl = self.paranodes2CNS[0].gl_mnaxflut
        #     for paranode2 in (self.paranodes2CNS + self.paranodes2PNS):
        #         paranode2.el_mnaxflut = (ik + v * gl) / gl
        #     print('el paranode2: ', (ik + v * gl) / gl)

    def get_longi_xs(self):
        """Return the longitudinal abscissa of the MotorBranch's compartments.

        Return:
            xs: numpy.array.

        """

        nIS = self.nIS
        nNCNS = self._nNCNS
        nNPNS = self._nNPNS
        ISlength = self._ISlength
        Nlength = self._Nlength
        P1length = self._P1length
        P2length = self._P2length
        ICNSlength = self._ICNSlength
        IPNSlength = self._IPNSlength

        nC = nIS + 11 * nNCNS + 11 * nNPNS
        xs = np.zeros(nC)
        xs[0] = ISlength / 2.0
        for i in range(nIS - 1):
            xs[i + 1] = xs[i] + ISlength
        for i in range(nNCNS):
            if i == 0:
                xs[3] = xs[2] + ISlength / 2.0 + P1length / 2.0
            else:
                xs[11 * i + 3] = xs[11 * i + 2] + ISlength / 2.0 + P1length / 2.0
            xs[11 * i + 4] = xs[11 * i + 3] + P1length / 2.0 + P2length / 2.0
            xs[11 * i + 5] = xs[11 * i + 4] + P2length / 2.0 + ICNSlength / 2.0
            xs[11 * i + 6] = xs[11 * i + 5] + ICNSlength
            xs[11 * i + 7] = xs[11 * i + 6] + ICNSlength
            xs[11 * i + 8] = xs[11 * i + 7] + ICNSlength
            xs[11 * i + 9] = xs[11 * i + 8] + ICNSlength
            xs[11 * i + 10] = xs[11 * i + 9] + ICNSlength
            xs[11 * i + 11] = xs[11 * i + 10] + ICNSlength / 2.0 + P2length / 2.0
            xs[11 * i + 12] = xs[11 * i + 11] + P2length / 2.0 + P1length / 2.0
            xs[11 * i + 13] = xs[11 * i + 12] + P1length / 2.0 + Nlength / 2.0
        for i in range(nNPNS):
            xs[11 * i + 11 * nNCNS + 3] = xs[11 * i + 11 * nNCNS + 2] + Nlength / 2.0 + P1length / 2.0
            xs[11 * i + 11 * nNCNS + 4] = xs[11 * i + 11 * nNCNS + 3] + P1length / 2.0 + P2length / 2.0
            xs[11 * i + 11 * nNCNS + 5] = xs[11 * i + 11 * nNCNS + 4] + P2length / 2.0 + IPNSlength / 2.0
            xs[11 * i + 11 * nNCNS + 6] = xs[11 * i + 11 * nNCNS + 5] + IPNSlength
            xs[11 * i + 11 * nNCNS + 7] = xs[11 * i + 11 * nNCNS + 6] + IPNSlength
            xs[11 * i + 11 * nNCNS + 8] = xs[11 * i + 11 * nNCNS + 7] + IPNSlength
            xs[11 * i + 11 * nNCNS + 9] = xs[11 * i + 11 * nNCNS + 8] + IPNSlength
            xs[11 * i + 11 * nNCNS + 10] = xs[11 * i + 11 * nNCNS + 9] + IPNSlength
            xs[11 * i + 11 * nNCNS + 11] = xs[11 * i + 11 * nNCNS + 10] + ICNSlength / 2.0 + P2length / 2.0
            xs[11 * i + 11 * nNCNS + 12] = xs[11 * i + 11 * nNCNS + 11] + P2length / 2.0 + P1length / 2.0
            xs[11 * i + 11 * nNCNS + 13] = xs[11 * i + 11 * nNCNS + 12] + P1length / 2.0 + Nlength / 2.0

        return xs


###########################################################################
