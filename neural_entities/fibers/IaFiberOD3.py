# -*- coding: utf-8 -*


#############################################################################
# Class IaFiberOD3.
#
# Created on: 2 January 2020
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.fibers.IaFiber import IaFiber


class IaFiberOD3(IaFiber):
    """
    This class is a perfect copy of the class IaFiber. They differ only by their name, which is used in the process
    of reading the morphology information of their respective instances.
    """


###########################################################################
