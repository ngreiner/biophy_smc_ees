# -*- coding: utf-8 -*


#############################################################################
# Class DRFiber.
#
# Created on: 28 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from biophy_smc_ees.neural_entities.branches.Branch import Branch
from biophy_smc_ees.neural_entities.fibers.Fiber import Fiber


class DRFiber(Fiber):
    """
    This class refines the Fiber class to represent dorsal root (DR) fibers.

    Instance variables defined here:
        DRbranch    : Branch object.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Fiber.__init__
        get_branches(self)              -- overrides Fiber.get_branches
        _read_morpho_info(self)         -- extends Fiber._read_morpho_info
        _create_branches(self)          -- overrides Fiber._create_branches
        _create_sections(self)          -- overrides Fiber._create_sections
        _set_sections(self)             -- overrides Fiber._set_sections
        _ready_2_build(self)            -- overrides Fiber._ready_2_build

    The DRFiber class features a Fiber with a single branch: a dorsal root branch (DRbranch).

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """

        self.DRbranch = None

        super(DRFiber, self).__init__(**kwargs)

    def _read_morpho_info(self):
        """Read the morphology info of the DRFiber from the appropriate textfile.

        The textfile is found thanks to the _smc attribute and the DRFiber's identification info.
        The morphology info consists in the diameter and number of nodes of Ranvier of the dorsal root branch of the
        DRFiber.

        """

        super(DRFiber, self)._read_morpho_info()

        # Read file content.
        file_path = os.path.join(self._smc.dir_data, self._musc, self._seg,
                                 '{}{:d}'.format(self.__class__.__name__, self._fidx), 'morpho.txt')
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # DRFiber dorsal root branch info.
        self._morpho['DRbranch'] = dict()
        self._morpho['DRbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['DRbranch']['nN'] = int(file_lines.pop(0))

    def _create_branches(self):
        """Create the branches of the DRFiber.

        It only consists in a dorsal root branch.

        """
        self.DRbranch = Branch('DRbranch', cell=self)

    def _create_sections(self):
        """Create the sections of the branch of the DRFiber.

        Work is delegated to homonymous method of class Branch.

        """
        self.DRbranch.create_sections()

    def _set_sections(self):
        """Set the properties of the sections of the DRFiber branches."""
        self.DRbranch.set_sections()

    def _ready_2_build(self):
        """Return a boolean indicating whether the DRFiber is ready to be built or not.

        The method checks whether all the parameters necessary to build the dorsal root branch are defined or not.

        """
        if 'DRbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['DRbranch'].keys(): return False
        elif 'nN' not in self._morpho['DRbranch'].keys(): return False
        elif self._model is None: return False
        else: pass

        return True

    def get_branches(self):
        """Return a list of references to the branches of the DRFiber."""
        return [self.DRbranch]


###########################################################################
