# -*- coding: utf-8 -*


#############################################################################
# Class Fiber.
#
# Created on: 27 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.NeuralEntity import NeuralEntity


class Fiber(NeuralEntity):
    """
    This class refines the NeuralEntity class to represent nerve fibers.

    Class variables defined here:
        _model  : 'ModelD'  : default identifier of Fiber model.

    Instance variables defined here:
        _morpho : dict  : morphologic parameters of the Fiber.

    Methods defined here:
        __init__(self, **kwargs)    -- extends NeuralEntity.__init__
        model(self, *args)
        get_morpho(self, *args)
        _read_morpho_info(self)
        _create_branches(self)
        _create_sections(self)
        _set_sections(self)
        _build(self)
        reset(self, **kwargs)       -- extends NeuralEntity.reset
        _ready_2_build(self)        -- overrides NeuralEntity._ready_2_build
        get_branches(self)
        get_nodes(self)
        adjust_el(self)

    The Fiber class is an abstract class defining the attributes and methods common to all its derived classes.

    We represent a fiber as composed of one or several interconnected myelinated branches characterized by their
    morphology and model. The morphology of a branch is completely characterized by the number of nodes of Ranvier it
    possesses and by its diameter. On the other hand, its model dictates the biophysical properties of its
    compartments.

    A Fiber being a NeuralEntity, it is intended to belong to a sensorimotor circuit, and that its morphology be
    retrieved from the appropriate textfiles of this sensorimotor circuit dataset directory (by the method
    _read_morpho_info). However, it is also possible to instantiate a Fiber by specifying directly the morphological
    properties of its branches using the dedicated keyword arguments of the constructor.

    """

    _model = 'ModelD'

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """

        super(Fiber, self).__init__(**kwargs)

        self._morpho = dict()

        # Read Fiber morphology from appropriate textfiles if applicable.
        if self._has_ID():
            self._read_morpho_info()

        # Parse inputs.
        try:
            self.model(kwargs['model'])
        except:
            pass

        try:
            self._morpho.update(kwargs['morpho'])
        except:
            pass

        # Build Fiber if ready.
        if self._ready_2_build():
            self._build()

    def model(self, *args):
        """Return (without args) or set and return (with args) the attribute _model."""
        if len(args) > 0:
            if self._model != args[0]:
                self._model = args[0]
                self._need_2_build = True
        return self._model

    def get_morpho(self, *args):
        """Return the _morpho dictionnary (or specific values of it if args provided)."""
        morpho = self._morpho
        for arg in args:
            morpho = morpho[arg]
        return morpho

    def update_morpho(self, morpho):
        """Update the _morpho dictionnary with the input dictionnary.

        Arguments:
            morpho  : dict  : Fiber morphological parameters.

        """
        tmp = self._morpho
        self._morpho.update(morpho)
        if self._morpho != tmp:
            self._need_2_build = True

    def _read_morpho_info(self):
        """Read the morphology-related attributes of the Fiber from the appropriate textfile.

        Most of the work is delegated to homonymous methods of children classes.

        """
        self._need_2_read_info = False
        self._morpho = dict()

    def _create_branches(self):
        """Create the branches of the Fiber.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _create_sections(self):
        """Create the sections of the branches of the Fiber.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _set_sections(self):
        """Set the properties of the sections of the branches of the Fiber.

        Purely virtual method (overridden in children classes).

        """
        pass

    def _build(self):
        """Build the Fiber."""
        self._create_branches()
        self._create_sections()
        self._set_sections()
        self._need_2_build = False

    def reset(self, **kwargs):
        """Reset the Fiber.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment through which the NeuralEntity is running.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            gidx    : int           : global index of the NeuralEntity.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """
        super(Fiber, self).reset(**kwargs)

        if self._need_2_read_info and self._has_ID():
            self._read_morpho_info()

        try:
            self.model(kwargs['model'])
        except:
            pass

        try:
            self._morpho.update(kwargs['morpho'])
        except:
            pass

        if self._need_2_build and self._ready_2_build():
            self._build()

    def _ready_2_build(self):
        """Return a boolean indicating whether the Fiber is ready to be built or not.

        Purely virtual method (overridden in children classes).

        """
        pass

    def get_branches(self):
        """Return a list of references to the branches of the Fiber.

        Purely virtual method (overridden in children classes).

        """
        return []

    def get_nodes(self):
        """Return a list of references to the nodes of Ranvier of the Fiber."""
        nodes = []
        for branch in self.get_branches():
            nodes += branch.nodes
        return nodes

    def adjust_el(self):
        """Adjust the leakage reversal potential of the Fiber's active compartments to reach steady-state."""
        for branch in self.get_branches():
            branch.adjust_el()


###########################################################################
