# -*- coding: utf-8 -*


#############################################################################
# Class DRcAFiber.
#
# Created on: 25 November 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.fibers.DRFiber import DRFiber


class DRcAFiber(DRFiber):
    """
    This class is a perfect copy of the class DRFiber. They differ only by their name, which is used in the process
    of reading the morphology information of their respective instances. 'cA' stands for 'class-A': the diameter
    class of the DRFiber.
    """


###########################################################################
