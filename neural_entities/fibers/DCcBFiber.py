# -*- coding: utf-8 -*


#############################################################################
# Class DCcBFiber.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.fibers.DCFiber import DCFiber


class DCcBFiber(DCFiber):
    """
    This class is a perfect copy of the class DCFiber. They differ only by their name, which is used in the process
    of reading the morphology information of their respective instances. 'cB' stands for 'class-B': the diameter
    class of the DCFiber.
    """


###########################################################################
