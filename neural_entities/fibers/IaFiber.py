# -*- coding: utf-8 -*


#############################################################################
# Class IaFiber.
#
# Created on: 27 January 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from biophy_smc_ees.neural_entities.branches.Branch import Branch
from biophy_smc_ees.neural_entities.fibers.Fiber import Fiber


class IaFiber(Fiber):
    """
    This class refines the Fiber class to represent Ia-fibers.

    Instance variables defined here:
        Abranch     : Branch object.
        Dbranch     : Branch object.
        DRbranch    : Branch object.
        Cols        : list of Branch objects.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Fiber.__init__
        get_branches(self)              -- overrides Fiber.get_branches
        _read_morpho_info(self)         -- extends Fiber._read_morpho_info
        _create_branches(self)          -- overrides Fiber._create_branches
        _create_sections(self)          -- overrides Fiber._create_sections
        _set_sections(self)             -- overrides Fiber._set_sections
        _ready_2_build(self)            -- overrides Fiber._ready_2_build

    The IaFiber class features a Fiber with several branches: one dorsal root branch (DRbranch), one ascending branch
    (Abranch), one descending branch (Dbranch) and a series of collaterals. The Abranch and Dbranch originate from
    the tip node of Ranvier of the DRbranch, while the collaterals depart from nodes of Ranvier of the Abranch and
    Dbranch.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """

        self.Abranch = None
        self.Dbranch = None
        self.DRbranch = None
        self.Cols = []

        super(IaFiber, self).__init__(**kwargs)

    def _read_morpho_info(self):
        """Read the morphology info of the IaFiber from the appropriate textfile.

        The textfile is found thanks to the _smc attribute and the IaFiber's identification info.
        The morphology info consists in the diameter and number of nodes of Ranvier of the dorsal root branch,
        the ascending branch, the descending branch, and of the collaterals of the IaFiber. In addition, the node
        of attachment of each collateral on the ascending/descending branches should be given.

        """

        super(IaFiber, self)._read_morpho_info()

        # Read file content.
        file_path = os.path.join(self._smc.dir_data, self._musc, self._seg,
                                 '{}{:d}'.format(self.__class__.__name__, self._fidx), 'morpho.txt')
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # IaFiber global info.
        self._morpho['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['nCols'] = int(file_lines.pop(0))

        # Abranch.
        self._morpho['Abranch'] = dict()
        self._morpho['Abranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['Abranch']['nN'] = int(file_lines.pop(0))

        # Dbranch.
        self._morpho['Dbranch'] = dict()
        self._morpho['Dbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['Dbranch']['nN'] = int(file_lines.pop(0))

        # DRbranch.
        self._morpho['DRbranch'] = dict()
        self._morpho['DRbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['DRbranch']['nN'] = int(file_lines.pop(0))

        # Collaterals.
        for n in range(self._morpho['nCols']):
            self._morpho['Col{:d}'.format(n + 1)] = dict()
            self._morpho['Col{:d}'.format(n + 1)]['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
            self._morpho['Col{:d}'.format(n + 1)]['nN'] = int(file_lines.pop(0))
            self._morpho['Col{:d}'.format(n + 1)]['p_attach'] = file_lines.pop(0)

    def _create_branches(self):
        """Create the branches of the IaFiber.

        It consists in a dorsal root branch, an ascending branch, a descending branch, and a series of collaterals.

        """
        self.Abranch = Branch('Abranch', cell=self)
        self.Dbranch = Branch('Dbranch', cell=self)
        self.DRbranch = Branch('DRbranch', cell=self)
        self.Cols = [Branch('Col{:d}'.format(n + 1), cell=self) for n in range(self._morpho['nCols'])]

    def _create_sections(self):
        """Create the sections of the branches of the IaFiber.

        Most of the work is delegated to homonymous method of class Branch. In addition, this method connects the
        first node of each collateral to the mid-point of its node of attachment on the Abranch or Dbranch.

        """

        # Create and connect sections on each branch.
        self.Abranch.create_sections()
        self.Dbranch.create_sections()
        self.DRbranch.create_sections()
        for Col in self.Cols:
            Col.create_sections()

        # Connect branches together.
        self.Abranch.nodes[0].connect(self.DRbranch.nodes[0], 0, 0)
        self.Dbranch.nodes[0].connect(self.DRbranch.nodes[0], 0, 0)
        for Col in self.Cols:
            b = Col.p_attach[0]
            n = int(Col.p_attach[1:])
            if b == 'A':
                Col.nodes[0].connect(self.Abranch.nodes[n], 0.5, 0)
            elif b == 'D':
                Col.nodes[0].connect(self.Dbranch.nodes[n], 0.5, 0)

    def _set_sections(self):
        """Set the properties of the sections of the IaFiber branches."""
        self.Abranch.set_sections()
        self.Dbranch.set_sections()
        self.DRbranch.set_sections()
        for Col in self.Cols:
            Col.set_sections()

    def _ready_2_build(self):
        """Return a boolean indicating whether the IaFiber is ready to be built.

        The method checks whether all the parameters necessary to build the IaFiber's branches are defined or not.

        """

        if 'Abranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['Abranch'].keys(): return False
        elif 'nN' not in self._morpho['Abranch'].keys(): return False
        else: pass

        if 'Dbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['Dbranch'].keys(): return False
        elif 'nN' not in self._morpho['Dbranch'].keys(): return False
        else: pass

        if 'DRbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['DRbranch'].keys(): return False
        elif 'nN' not in self._morpho['DRbranch'].keys(): return False
        else: pass

        if 'nCols' not in self._morpho.keys(): return False
        for n in range(self._morpho['nCols']):
            if 'Col{:d}'.format(n + 1) not in self._morpho.keys(): return False
            elif 'diam' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'nN' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'p_attach' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            else: pass

        return True

    def get_branches(self):
        """Return a list of references to the branches of the IaFiber."""
        return [self.Abranch, self.Dbranch, self.DRbranch] + self.Cols


###########################################################################
