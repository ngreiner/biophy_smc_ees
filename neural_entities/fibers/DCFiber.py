# -*- coding: utf-8 -*


#############################################################################
# Class DCFiber.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from biophy_smc_ees.neural_entities.branches.Branch import Branch
from biophy_smc_ees.neural_entities.fibers.Fiber import Fiber


class DCFiber(Fiber):
    """
    This class refines the Fiber class to represent dorsal columns (DC) fibers.

    Instance variables defined here:
        DCbranch    : Branch object.
        Cols        : list of Branch objects.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Fiber.__init__
        get_branches(self)              -- overrides Fiber.get_branches
        _read_morpho_info(self)         -- extends Fiber._read_morpho_info
        _create_branches(self)          -- overrides Fiber._create_branches
        _create_sections(self)          -- overrides Fiber._create_sections
        _set_sections(self)             -- overrides Fiber._set_sections
        _ready_2_build(self)            -- overrides Fiber._ready_2_build

    The DCFiber class features a Fiber with several branches: one dorsal columns branch (DCbranch) and a series of
    collaterals. The collaterals depart from nodes of Ranvier of the DCbranch.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """

        self.DCbranch = None
        self.Cols = []

        super(DCFiber, self).__init__(**kwargs)

    def _read_morpho_info(self):
        """Read the morphology info of the DCFiber from the appropriate textfile.

        The textfile is found thanks to the _smc attribute and the DCFiber's identification info.
        The morphology info consists in the diameter and number of nodes of Ranvier of the dorsal column branch and
        of the collaterals of the DCFiber. In addition, the node of attachment of each collateral on the dorsal
        column branch should be given.

        """

        super(DCFiber, self)._read_morpho_info()

        # Read file content.
        file_path = os.path.join(self._smc.dir_data, self._musc, self._seg,
                                 '{}{:d}'.format(self.__class__.__name__, self._fidx), 'morpho.txt')
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # DCFiber global info.
        self._morpho['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['nCols'] = int(file_lines.pop(0))

        # DCbranch.
        self._morpho['DCbranch'] = dict()
        self._morpho['DCbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['DCbranch']['nN'] = int(file_lines.pop(0))

        # Collaterals.
        for n in range(self._morpho['nCols']):
            self._morpho['Col{:d}'.format(n + 1)] = dict()
            self._morpho['Col{:d}'.format(n + 1)]['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
            self._morpho['Col{:d}'.format(n + 1)]['nN'] = int(file_lines.pop(0))
            self._morpho['Col{:d}'.format(n + 1)]['p_attach'] = file_lines.pop(0)

    def _create_branches(self):
        """Create the branches of the DCFiber.

        It consists in a dorsal column (DC) branch and a series of collaterals.

        """

        self.DCbranch = Branch('DCbranch', cell=self)
        self.Cols = [Branch('Col{:d}'.format(n + 1), cell=self) for n in range(self._morpho['nCols'])]

    def _create_sections(self):
        """Create the sections of the branches of the DCFiber.

        Most of the work is delegated to homonymous method of class Branch. In addition, this method connects the
        first node of each collateral to the mid-point of its node of attachment on the DCbranch.

        """
        # Create and connect sections on each branch.
        self.DCbranch.create_sections()
        for Col in self.Cols:
            Col.create_sections()

        # Connect branches together.
        for Col in self.Cols:
            n = int(Col.p_attach[2:]) - 1
            Col.nodes[0].connect(self.DCbranch.nodes[n], 0.5, 0)

    def _set_sections(self):
        """Set the properties of the sections of the DCFiber branches."""
        self.DCbranch.set_sections()
        for Col in self.Cols:
            Col.set_sections()

    def _ready_2_build(self):
        """Return a boolean indicating whether the DCFiber is ready to be built or not.

        The method checks whether all the parameters necessary to build the DCFiber branches are defined or not.

        """

        if 'DCbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['DCbranch'].keys(): return False
        elif 'nN' not in self._morpho['DCbranch'].keys(): return False
        else: pass

        if 'nCols' not in self._morpho.keys(): return False
        for n in range(self._morpho['nCols']):
            if 'Col{:d}'.format(n + 1) not in self._morpho.keys(): return False
            elif 'diam' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'nN' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'p_attach' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            else: pass

        return True

    def get_branches(self):
        """Return a list of references to the branches of the DCFiber."""
        return [self.DCbranch] + self.Cols


###########################################################################
