# -*- coding: utf-8 -*


#############################################################################
# Class CSTFiber.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from biophy_smc_ees.neural_entities.branches.Branch import Branch
from biophy_smc_ees.neural_entities.fibers.Fiber import Fiber


class CSTFiber(Fiber):
    """
    This class refines the Fiber class to represent cortico-spinal tract (CST) fibers.

    Instance variables defined here:
        CSTbranch   : Branch object.
        Cols        : list of Branch objects.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Fiber.__init__
        get_branches(self)              -- overrides Fiber.get_branches
        _read_morpho_info(self)         -- extends Fiber._read_morpho_info
        _create_branches(self)          -- overrides Fiber._create_branches
        _create_sections(self)          -- overrides Fiber._create_sections
        _set_sections(self)             -- overrides Fiber._set_sections
        _ready_2_build(self)            -- overrides Fiber._ready_2_build

    The CSTFiber class features a Fiber with several branches: one cortico-spinal tract branch (CSTbranch) and a
    series of collaterals. The collaterals depart from nodes of Ranvier of the CSTbranch.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """

        self.CSTbranch = None
        self.Cols = []

        super(CSTFiber, self).__init__(**kwargs)

    def _read_morpho_info(self):
        """Read the morphology info of the CSTFiber from the appropriate textfile.

        The textfile is found thanks to the _smc attribute and the CSTFiber's identification info.
        The morphology info consists in the diameter and number of nodes of Ranvier of the CST branch and of the
        collaterals of the CSTFiber. In addition, the node of attachment of each collateral on the CST branch
        should be given.

        """

        super(CSTFiber, self)._read_morpho_info()

        # Read file content.
        file_path = os.path.join(self._smc.dir_data, self._musc, self._seg, 'CSTFiber{:d}'.format(self._fidx),
                                 'morpho.txt')
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # CSTFiber global info.
        self._morpho['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['nCols'] = int(file_lines.pop(0))

        # CSTbranch.
        self._morpho['CSTbranch'] = dict()
        self._morpho['CSTbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['CSTbranch']['nN'] = int(file_lines.pop(0))

        # Collaterals.
        for n in range(self._morpho['nCols']):
            self._morpho['Col{:d}'.format(n + 1)] = dict()
            self._morpho['Col{:d}'.format(n + 1)]['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
            self._morpho['Col{:d}'.format(n + 1)]['nN'] = int(file_lines.pop(0))
            self._morpho['Col{:d}'.format(n + 1)]['p_attach'] = file_lines.pop(0)

    def _create_branches(self):
        """Create the branches of the CSTFiber.

        It consists in a cortico-spinal tract (CST) branch and a series of collaterals.

        """
        self.CSTbranch = Branch('CSTbranch', cell=self)
        self.Cols = [Branch('Col{:d}'.format(n + 1), cell=self) for n in range(self._morpho['nCols'])]

    def _create_sections(self):
        """Create the sections of the branches of the CSTFiber.

        Most of the work is delegated to homonymous method of class Branch. In addition, this method connects the
        first node of each collateral to the mid-point of its node of attachment on the CSTbranch.

        """

        # Create and connect sections on each branch.
        self.CSTbranch.create_sections()
        for Col in self.Cols:
            Col.create_sections()

        # Connect branches together.
        for Col in self.Cols:
            n = int(Col.p_attach[3:]) - 1
            Col.nodes[0].connect(self.CSTbranch.nodes[n], 0.5, 0)

    def _set_sections(self):
        """Set the properties of the sections of the CSTFiber branches."""
        self.CSTbranch.set_sections()
        for Col in self.Cols:
            Col.set_sections()

    def _ready_2_build(self):
        """Return a boolean indicating whether the CSTFiber is ready to be built or not.

        The method checks whether all the parameters necessary to build the CSTFiber branches are defined or not.

        """

        if 'CSTbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['CSTbranch'].keys(): return False
        elif 'nN' not in self._morpho['CSTbranch'].keys(): return False
        else: pass

        if 'nCols' not in self._morpho.keys(): return False
        for n in range(self._morpho['nCols']):
            if 'Col{:d}'.format(n + 1) not in self._morpho.keys(): return False
            elif 'diam' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'nN' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            elif 'p_attach' not in self._morpho['Col{:d}'.format(n + 1)].keys(): return False
            else: pass

        return True

    def get_branches(self):
        """Return a list of references to the branches of the CSTFiber."""
        return [self.CSTbranch] + self.Cols


###########################################################################
