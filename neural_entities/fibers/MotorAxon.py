# -*- coding: utf-8 -*


#############################################################################
# Class MotorAxon.
#
# Created on: 3 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

from biophy_smc_ees.neural_entities.branches.MotorBranch import MotorBranch
from biophy_smc_ees.neural_entities.fibers.Fiber import Fiber


class MotorAxon(Fiber):
    """
    This class refines the Fiber class in order to represent motor axons (as fully-fledged neural_entities,
    rather than as compartments of motoneurons -- role which is played by the homonymous class of the subpackage
    cell_comps).

    Instance variables defined here:
        VRbranch    : Branch object : the one and only Branch of the MotorAxon.

    Methods defined here:
        __init__(self, **kwargs)        -- extends Fiber.__init__
        _read_morpho_info(self)         -- extends Fiber._read_morpho_info
        _create_branches(self)          -- overrides Fiber._create_branches
        _create_sections(self)          -- overrides Fiber._create_sections
        _set_sections(self)             -- overrides Fiber._set_sections
        _ready_2_build(self)            -- overrides Fiber._ready_2_build
        get_branches(self)              -- overrides Fiber.get_branches
        get_nodes(self)                 -- overrides Fiber.get_nodes
        adjust_el(self)

    The MotorAxon class features a Fiber with a single branch: a ventral root branch (VRbranch), which is a MotorBranch
    (contrarily to the other Fiber subclasses).

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.
            model   : str           : identifier of Fiber model.
            morpho  : dict          : morphological parameters of the Fiber branches.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.
        If morpho is specified in argument, it overwrites any morphological parameter retrieved from the textfiles of
        the NeuralEntity.

        """
        self.VRbranch = None
        super(MotorAxon, self).__init__(**kwargs)

    def _read_morpho_info(self):
        """Read the morphology info of the MotorAxon from the appropriate textfile.

        The textfile is found thanks to the _smc attribute and the MotorAxon's identification info. The morphology
        info consists in the diameter and number of nodes of Ranvier of the VRbranch of the MotorAxon.

        """

        super(MotorAxon, self)._read_morpho_info()

        # Read file content.
        file_path = os.path.join(self._smc.dir_data, self._musc, self._seg, 'MN{:d}'.format(self._fidx), 'axon',
                                 'morpho.txt')
        with open(file_path, 'r') as fileID:
            file_lines = fileID.readlines()

        # MotorAxon morpho info.
        self._morpho['VRbranch'] = dict()
        self._morpho['VRbranch']['diam'] = float(file_lines.pop(0)) * 1e3  # To get it in um.
        self._morpho['VRbranch']['nN'] = int(file_lines.pop(0))

    def _create_branches(self):
        """Create the branches of the MotorAxon.

        It only consists in a ventral root branch.

        """
        self.VRbranch = MotorBranch('VRbranch', cell=self)

    def _create_sections(self):
        """Create the sections of the branches of the MotorAxon.

        Work is delegated to homonymous method of class MotorBranch.

        """
        self.VRbranch.create_sections()

    def _set_sections(self):
        """Set the properties of the sections of the branches of the MotorAxon.

        Work is delegated to homonymous method of class MotorBranch.

        """
        self.VRbranch.set_sections()

    def _ready_2_build(self):
        """Return a boolean indicating whether the MotorAxon is ready to be built.

        The method checks whether all the parameters necessary to build the VRbranch are defined or not.

        """
        if 'VRbranch' not in self._morpho.keys(): return False
        elif 'diam' not in self._morpho['VRbranch'].keys(): return False
        elif 'nN' not in self._morpho['VRbranch'].keys(): return False
        elif self._model is None: return False
        else: pass

        return True

    def get_branches(self):
        """Return a list containing a reference to the MotorAxon VRbranch."""
        return [self.VRbranch]

    def get_nodes(self):
        """Return a list of references to the nodes of Ranvier of the MotorAxon."""
        nodes = self.VRbranch.nodesCNS + self.VRbranch.nodesPNS
        return nodes

    def adjust_el(self):
        """Adjust the leakage reversal potential of the MotorAxon's active compartments to reach steady-state.

        Work is delegated to homonymous method of class MotorBranch.

        """
        self.VRbranch.adjust_el()


###########################################################################
