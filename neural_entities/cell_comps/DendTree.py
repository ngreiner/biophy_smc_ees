# -*- coding: utf-8 -*


#############################################################################
# Class DendTree.
#
# Created on: 5 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
from scipy import random as rand
import weakref
from neuron import h

import biophy_smc_ees.utils.swc as swc
from biophy_smc_ees.neural_entities.cell_secs.DendSec import DendSec
from biophy_smc_ees.neural_entities.cell_secs.DendStem import DendStem


class DendTree(object):
    """
    This class regroups attributes and methods describing a dendritic tree of a cell.

    Class variables defined here:
        _Ra             : 200.0     : default axoplasmic resistivity (Ohm * cm).
        _Rm             : 5000.0    : default membrane specific resistance (Ohm * cm^2).
        _cm             : 2.0       : default membrane specific capacitance (uF / cm^2).
        _Epas           : -70.0     : default leakage reversal potential (mV).
        _extra_mech     : False     : default indicator whether extracellular mechanism should be used or not.
        _xg0            : 1e10      : default value for the parameter xg[0] of the extracellular mechanism.
        _xg0            : 1e10      : default value for the parameter xg[1] of the extracellular mechanism.
        _xc0            : 0.0       : default value for the parameter xc[0] of the extracellular mechanism.
        _xc1            : 0.0       : default value for the parameter xc[1] of the extracellular mechanism.
        _xraxial0       : 1e10      : default value for the parameter xraxial[0] of the extracellular mechanism.
        _xraxial1       : 1e10      : default value for the parameter xraxial[1] of the extracellular mechanism.
        _lam_nsyn_homo  : 9.6       : default mean number of synapses provided by homonymous Ia-fibers.
        _lam_nsyn_hete  : 5.9       : default mean number of synapses provided by heteronymous Ia-fibers.
        _groups_electroto_lims_unit_Rm_Ra   : 1D numpy.array    : electrotonic ranges of dendritic section groups.
        _groups_probas  : 1D numpy.array    : probabilities of the dendritic section groups to receive a synapse.
        _n_groups       : int               : number of dendritic section groups.

    Instance variables defined here:
        cell            : weakref.ref               : weakref to the Cell object to which the DendTree belongs.
        dsecs           : list of DendSec objects   : dendritic sections of the DendTree.
        name            : str                       : name of the DendTree.
        _diam           : float                     : diameter of the DendTree's cell (in um).
        _swc_file       : str                       : path to swc file.
        _model          : str                       : identifier of DendTree model.
        _dsec_groups    : list of list of int       : groups of dendritic section indexes.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        __iter__(self)
        _set_params(self)
        build(self)
        _create_sections(self)
        _set_sections(self, dsecs=None)
        _reshape(self)
        _subdivide(self, dsec)
        _calc_linear_L_elec(self, u, v, a, b)
        _find_linear_L_elec(self, f0, u, a, b)
        _make_groups(self)
        get_stems(self)
        get_dsecs(self, depth)
        get_dsec(self, name)
        pickidsecs(self, k, mode='unif')
        sampleidsecs(self, k, mode='unif')
        pickidsec(self, mode='unif')
        pickdsecs(self, k, mode='unif')
        sampledsecs(self, k, mode='unif')
        pickdsec(self, mode='unif')
        get_Gin(self)
        get_area(self)
        get_longest_dend_length(self)
        get_largest_stem_diam(self)

    The DendTree class provides attributes and methods to build and control a binary tree of HOC sections describing
    a dendritic tree of a cell. The direct control exerted by DendTree objects actually bears on DendSec objects,
    which are intermediary containers encapsulating the HOC sections (see DendSec class).

    The DendTree class relies on swc files for building such trees (see
    http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html for a description of the swc
    format). A swc file specifies the geometry of a dendritic tree, both in terms of its branching structure and in
    terms of the diameters and lengths of the unbranching sections forming the tree. While the branching structure of
    a dendritic tree is frozen and dictated by the swc file which defines it, the diameters and lengths can be
    adjusted. This is done by specifying the diameter of the soma of the cell to which the dendritic tree belongs,
    according to the following strategy: since the diameters and lengths specified by a swc file corresponds to a
    certain soma diameter (which is specified in the swc file), when specifying a different diameter for the soma,
    all these diameters and lengths are scaled as: new_dimension = old_dimension * new_soma_diameter /
    old_soma_diameter. By so doing, the electrotonic structure of the represented dendritic tree is modified in a
    non-predictable way (see 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959). Still,
    one important predictable effect this has is to modify the input resistance of the dendritic tree (a larger
    cumulative membrane area yielding a lower input resistance), in turn affecting the responsiveness of the
    dendritic tree's cell to synaptic excitation.

    The biophysical properties (which are passive) of the sections composing the dendritic tree represented by a
    DendTree object are taken from 'Extracellular Stimulation of Central Neurons: Influence of Stimulus Waveform and
    Frequency on Neuronal Output', McIntyre et al., Journal of Neurophysiology, 2002.

    The DendTree class provides a method to automatically subdivide the unbranching sections of the dendritic tree it
    describes such that each such unbranching section respects the criteria L < 0.2 * lambda, where L is the length
    of the section, and lambda its electrotonic space constant (see 'Branching dendritic trees and motoneuron
    membrane resistivity', Rall, 1959).

    The DendTree class provides methods to select dendritic sections at random based on their probability to receive
    synaptic contacts from Ia-fibers, assuming that the dendritic tree belongs to a spinal motoneuron. These
    probabilities depend upon the electrotonic distances of the dendritic sections from the soma, and are derived
    from 'Horseradish Peroxidase Study of the Spatial and Electrotonic Distribution of Group Ia Synapses on
    Type-Identified Ankle Extensor Motoneurons in the Cat', Burke and Glenn, The Journal of Comparative Neurology,
    1996.

    A DendTree usually belongs to a Cell from which it draws its morphological parameters and model, but it can also be
    instantiated to stand alone by using the dedicated keyword arguments of the constructor.

    """

    # Biophysical properties of the dendrites from 'Extracellular Stimulation of Central Neurons: Influence of
    # Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al., Journal of Neurophysiology, 2002.
    _Ra = 200.0             # Ohm * cm
    _Rm = 1.0 / 0.0002      # Ohm * cm^2
    _Cm = 2.0               # uF / cm^2
    _Epas = - 70.0          # mV

    # Connectivity parameters between Ia-fibers and motoneuronal dendritic trees from 'Horseradish Peroxidase Study
    # of the Spatial and Electrotonic Distribution of Group Ia Synapses on Type-Identified Ankle Extensor Motoneurons
    # in the Cat', Burke and Glenn, The Journal of Comparative Neurology, 1996.
    _groups_electroto_lims_unit_Rm_Ra = np.asarray([0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75,
                                                    2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75]) \
                                        * np.sqrt(2000.0 / 70.0)
    _groups_probas = np.asarray([22, 24, 33, 42, 22, 16, 10, 7, 4, 4, 7, 5, 4, 2, 1, 1]) / 204.0
    _n_groups = _groups_probas.size

    _extra_mech = False
    _xg0 = 1e10
    _xg1 = 1e10
    _xc0 = 0.0
    _xc1 = 0.0
    _xraxial0 = 1e10
    _xraxial1 = 1e10

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            cell        : Cell object   : to which the DendTree belongs.
            diam        : float         : diameter (in um) of the Cell to which the DendTree belongs.
            swc_file    : str           : path to a swc file.
            model       : str           : identifier of DendTree model.
            extra_mech  : bool          : indicator whether extracellular mechanism should be used or not.

        If cell is given in argument, each of diam, swc_file, model and extra_mech are first derived from the input
        Cell object.

        If diam, swc_file, model or extra_mech are passed as keyword arguments, they override any value derived from
        the input Cell object.

        When instantiating a DendTree object, diam, model and extra_mech can be left to their default value (None),
        but a valid path to a valid swc file must be provided for swc_file, otherwise it will not be possible to
        really build the DendTree by invoking the build() method (which will raise an exception).

        """

        # Public attributes.
        self.cell = lambda: None
        self.dsecs = None
        self.name = None

        # Private attributes.
        self._diam = None
        self._swc_file = None
        self._model = None
        self._dsec_groups = None

        try:
            self.cell = weakref.ref(kwargs['cell'])
            self.name = '{}.dtree'.format(self.cell().name)
            self._diam = self.cell().diam()
            self._swc_file = self.cell().swc_file()
            self._model = self.cell().dtree_model()
            self._extra_mech = self.cell().extra_mech()
        except:
            self.name = 'DendTree_{:d}'.format(id(self))

        try:
            self._diam = kwargs['diam']
        except:
            pass

        try:
            self._swc_file = kwargs['swc_file']
        except:
            pass

        try:
            self._model = kwargs['model']
        except:
            pass

        try:
            self._extra_mech = kwargs['extra_mech']
        except:
            pass

        self._set_params()

    def __repr__(self):
        """Return the string representation of the DendTree."""
        return self.name

    def __iter__(self):
        """Return an iterator on the dendritic sections of the DendTree."""
        return iter(self.dsecs)

    def _set_params(self):
        """Set the biophysical parameters of the DendTree according to its attribute _model.

        Not implemented. The biophysical parameters remain to their default value.

        """
        pass

    def build(self):
        """Build the DendTree."""
        self._create_sections()
        self._set_sections()
        self._reshape()
        self._make_groups()

    def _create_sections(self):
        """Create the sections of the DendTree.

        This method is limited to creating the sections and setting their geometry and interconnectivity. Setting
        their biophysical properties is delegated to another method.

        """

        self.dsecs = []
        ndend = 0

        sids, stypes, xs, ys, zs, ds, pids = swc.read_file(self._swc_file, target_diam=self._diam)

        # Dendritic sections are constructed sequentially, from stem to leaves.
        for (dsid, stype, pid) in zip(sids, stypes, pids):

            # The initial samples of the dendritic sections (stype=3) are attached to the soma (pid=1).
            if (stype == 3) & (pid == 1):

                # Retrieve branches of current dendrite.
                brs = swc.get_branches(dsid, pids)

                # We need to link the branches to their corresponding dendritic section and the samples id to their
                # corresponding branch.
                br2sec = dict()
                sid2br = dict()

                # Branch 0 is treated separately: it is the dendrite stem.
                br0 = brs[0]
                name = '{}.dend{:d}_0'.format(self.name, ndend)
                stem = DendStem(name=name)  # create stem
                stem.hsec = h.Section(name='hsec', cell=stem)  # create corresponding hoc section
                h.pt3dclear(sec=stem.hsec)  # clear 3d info
                for sid in br0:
                    h.pt3dadd(xs[sid - 1], ys[sid - 1], zs[sid - 1], ds[sid - 1], sec=stem.hsec)  # add new 3d info
                sid2br[dsid] = br0  # link first sid of branch to branch
                sid2br[sid] = br0  # link last sid of branch to branch
                br2sec[br0] = stem  # store tuple (br0, stem) in index
                self.dsecs.append(stem)

                # Subsequent branches are treated sequentially.
                for (i, br) in enumerate(brs[1:]):
                    psec = br2sec[sid2br[br[0]]]  # retrieve parent section
                    # Derive name of new section.
                    # If it is a sibling of a stem, its name is obtained by replacing the last character ('0') of the
                    # stem's name by the current number of siblings of the stem plus 1.
                    if pids[br[0]] == br0[0]:
                        name = psec.name[:-1] + str(len(psec.siblings) + 1)
                    # If the parent section of the new section does not have a lchild yet, then the new section is its
                    # lchild and its name is obtained by appending a 0 at the end of its parent's name.
                    elif psec.lchild is None:
                        name = psec.name + '0'
                    # Otherwise the new section is the rchild of its parent section, and its name is obtained by
                    # appending a 1 at the end of its parent's name.
                    else:
                        name = psec.name + '1'
                    # Create new section.
                    csec = DendSec(par=psec, name=name)
                    csec.hsec = h.Section(name='hsec', cell=csec)  # create corresponding hoc section
                    h.pt3dclear(sec=csec.hsec)  # clear 3d info
                    for sid in br:
                        h.pt3dadd(xs[sid - 1], ys[sid - 1], zs[sid - 1], ds[sid - 1], sec=csec.hsec)  # add new 3d info
                    sid2br[sid] = br  # link last sid of branch to branch
                    # Connect new section to its parent section.
                    # Branches whose 1st sample is attached to the 1st sample of the stem need special care,
                    # they need be connected to the 0 end of the stem.
                    if pids[br[0]] == br0[0]:
                        csec.hsec.connect(psec.hsec, 0, 0)
                        psec.siblings.append(csec)
                    else:
                        csec.hsec.connect(psec.hsec, 1, 0)
                        if psec.lchild is None:  # parent section doesn't have a left child yet
                            psec.lchild = csec
                        else:  # parent section already has a left child
                            psec.rchild = csec
                    br2sec[br] = csec
                    self.dsecs.append(csec)

                    if csec.par() is None and not csec.isastem:
                        pass

                # Increment dendrite counter.
                ndend += 1

    def _set_sections(self, dsecs=None):
        """Set the biophysical properties of the sections of the DendTree.

        Keyword arguments:
            dsecs   : list of DendSec objects   : those sections whose properties should be set.

        If dsecs is not passed in argument, it defaults to self.dsecs.

        """

        if dsecs is None:
            dsecs = self.dsecs

        for dsec in dsecs:
            dsec.hsec.Ra = self._Ra
            dsec.hsec.cm = self._Cm
            dsec.hsec.insert('pas')
            dsec.hsec.e_pas = self._Epas
            dsec.hsec.g_pas = 1.0 / self._Rm
            if self._extra_mech:
                dsec.hsec.insert('extracellular')
                dsec.hsec.xg[0] = self._xg0
                dsec.hsec.xg[1] = self._xg1
                dsec.hsec.xc[0] = self._xc0
                dsec.hsec.xc[1] = self._xc1
                dsec.hsec.xraxial[0] = self._xraxial0
                dsec.hsec.xraxial[1] = self._xraxial1

    def _reshape(self):
        """Reshape the DendTree to ensure that all the sections respect the criteria L < 0.2 * lambda.

        Here L denotes the length of a section, and lambda its electrotonic space constant (i.e. the ratio L / lambda
        is the electrotonic length). If some sections don't respect the criteria, they are subdivided in equally
        electrotonically long sections respecting the criteria.

        """
        dsecs = []
        while self.dsecs:
            dsec = self.dsecs.pop(0)
            if dsec.get_L_elec() < 0.2:
                dsecs.append(dsec)
            else:
                dsecs += self._subdivide(dsec)
        self.dsecs = dsecs

    def _subdivide(self, dsec):
        """Subdivide the input DendSec so as to respect the criteria L < 0.2 * lambda.

        Arguments:
            dsec    : DendSec object    : to be subdivided.

        Specific actions:
            Disconnect the DendSec from its parents/siblings/children, subdivide it into multiple new DendSecs,
            and connect the resulting DendSecs together and with the original DendSec's parents/siblings/children.

        """

        # Collect parent, siblings and children.
        par = dsec.par()
        siblings = dsec.siblings
        lchild = dsec.lchild
        rchild = dsec.rchild

        # Disconnect children and siblings.
        h.disconnect(sec=dsec.hsec)
        if siblings:
            for sibling in siblings:
                h.disconnect(sec=sibling.hsec)
        if lchild:
            dsec.lchild = None
            h.disconnect(sec=lchild.hsec)
        if rchild:
            dsec.rchild = None
            h.disconnect(sec=rchild.hsec)

        # Retrieve total electrotonic length of DendSec.
        Lelec = dsec.get_L_elec()

        # Derive the target electrotonic length of the DendSecs of the subdivision.
        nsec = int(Lelec / 0.2) + 1
        tLelec = Lelec / nsec

        # Retrieve geometric information of DendSec.
        npts = dsec.get_n3d()
        ps = dsec.get_p3ds()
        ds = dsec.get_diam3ds()
        ls = dsec.get_l3ds()

        # Derive the longitudinal abscissae of its 3d points.
        Ls = np.cumsum(np.insert(ls, 0, 0.0), axis=0)

        # Derive the coefficients As and Bs of the piecewise-linear interpolant for the diameter: d(x) = As[i] * x +
        # Bs[i] where x is denotes longitudinal abscissa, and i is the index of the DendSec portion where x is
        # lying.
        As = np.diff(ds) / ls
        Bs = np.zeros_like(As)
        Bs[0] = ds[0]
        for i in range(1, Bs.size):
            Bs[i] = Bs[i - 1] + Ls[i] * (As[i - 1] - As[i])

        # Compute DendSec's portions' electrotonic lengths, and cumulative electrotonic lengths
        fs = np.zeros(npts)
        for i in range(1, npts):
            fs[i] = self._calc_linear_L_elec(Ls[i - 1], Ls[i], As[i - 1], Bs[i - 1])
        Fs = np.cumsum(fs, axis=0)

        # If everything is ok until here, the last value of Fs should be equal to the total electrotonic length of
        # DendSec.

        # We are ready to create the DendSecs of the subdivision.
        ndsecs = []

        for i in range(nsec - 1):  # The last DendSec of the subdivision is treated separately.

            # The first DendSec of the subdivision actually need not be created: we can reuse the input DendSec,
            # which has the benefit to preserve its eventual nature of DendStem.
            if i == 0:
                ndsec = dsec

            # Otherwise, we create a new DendSec.
            else:
                ndsec = DendSec(par=ndsecs[-1])

            # Create associated HOC section. The algorithm to determine its 3D points is explained below.
            # At this stage, the variable ps contains the geometric 3D points composing the remaining piece of
            # neurite to subdivide, ds their corresponding diameters, Ls their longitudinal abscissae,
            # Fs their electrotonic longitudinal abscissae, and As and Bs the linear interpolants for the diameter in
            # each portion of this piece of neurite. Thus, the algorithm proceeds as follows:
            # 1. Determine the portion idx of the piece of neurite where the point at an electrotonic longitudinal
            # abscissa of tElec from the initial point of the piece of neurite is lying. This point, noted p0,
            # is thus lying in the segment [ps[idx], ps[idx+1]].
            # 2. Determine the geometric coordinates of p0 (by interpolating the coordinates ps[idx] and ps[idx+1]).
            # 3. Determine the corresponding diameter d0 (by interpolating the diameters ds[idx] and ds[idx+1]).
            # 4. Add the geometric points ps[0], ..., ps[idx], p0 to the HOC section.
            idx = np.flatnonzero(Fs <= tLelec)[-1]
            L0 = self._find_longi_abs(tLelec - Fs[idx], Ls[idx], As[idx], Bs[idx])
            w0 = (L0 - Ls[idx]) / (Ls[idx + 1] - Ls[idx])
            w1 = (Ls[idx + 1] - L0) / (Ls[idx + 1] - Ls[idx])
            p0 = w0 * ps[idx + 1, :] + w1 * ps[idx, :]
            d0 = w0 * ds[idx + 1] + w1 * ds[idx]
            ndsec.hsec = h.Section(name='hsec', cell=ndsec)
            h.pt3dclear(sec=ndsec.hsec)
            for k in range(idx + 1):
                h.pt3dadd(ps[k, 0], ps[k, 1], ps[k, 2], ds[k], sec=ndsec.hsec)
            h.pt3dadd(p0[0], p0[1], p0[2], d0, sec=ndsec.hsec)

            # Update variables ps, ds, Fs, Ls, As and Bs to reflect the new state of the remaining piece of neurite to
            # subdivide.
            ps = ps[idx + 1:, :]
            ps = np.insert(ps, 0, p0, axis=0)
            ds = ds[idx + 1:]
            ds = np.insert(ds, 0, d0)
            Fs = Fs[idx + 1:]
            Fs -= tLelec
            Fs = np.insert(Fs, 0, 0.0)
            Ls = Ls[idx + 1:]
            Ls -= L0
            Ls = np.insert(Ls, 0, 0.0)
            As = As[idx:]
            Bs = Bs[idx:]
            Bs += As * L0

            # Connect new DendSec and associated HOC section.

            # Here again, the first DendSec of the subdivision needs special care. Since the DendSec object itself
            # still coincides with the input DendSec and was not disconnected from its parent or siblings,
            # we only need connect its associated HOC section (which instead was just rebuilt).
            if i == 0:

                # We first need to connect it to the HOC section of the DendSec's eventual parent.
                # If the DendSec doesn't have a parent (ie when it is a DendStem not yet connected to a Soma), pass.
                if par is None:
                    pass
                # If the DendSec was a sibling of a DendStem, we need to connect the HOC section to the 0 end of this
                # DendStem's HOC section.
                elif par.siblings and dsec in par.siblings:
                    ndsec.hsec.connect(par.hsec, 0, 0)
                # Otherwise, we connect the HOC section to the 1 end of the DendSec's parent's HOC section.
                else:
                    ndsec.hsec.connect(par.hsec, 1, 0)

                # Second, if the DendSec is a DendStem and has siblings, we need to connect its siblings' HOC sections
                # to the 0 end of the HOC section.
                if siblings:
                    for sibling in siblings:
                        sibling.hsec.connect(ndsec.hsec, 0, 0)

            # For the subsequent DendSecs of the subdivision the connection is straightforward.
            else:
                ndsecs[-1].lchild = ndsec
                ndsec.hsec.connect(ndsecs[-1].hsec, 1, 0)

            # Add new DendSec to list.
            ndsecs.append(ndsec)

        # The last DendSec of the subdivision is easy to build and to connect to its parent.
        ndsec = DendSec(par=ndsecs[-1])
        ndsec.hsec = h.Section(name='hsec', cell=ndsec)
        h.pt3dclear(sec=ndsec.hsec)
        for i in range(ps.shape[0]):
            h.pt3dadd(ps[i, 0], ps[i, 1], ps[i, 2], ds[i], sec=ndsec.hsec)
        ndsecs[-1].lchild = ndsec
        ndsec.hsec.connect(ndsecs[-1].hsec, 1, 0)

        # Don't forget to reconnect the children of the original DendSec to this last new DendSec.
        if lchild:
            ndsec.lchild = lchild
            lchild.par = weakref.ref(ndsec)
            lchild.hsec.connect(ndsec.hsec, 1, 0)
        if rchild:
            ndsec.rchild = rchild
            rchild.par = weakref.ref(ndsec)
            rchild.hsec.connect(ndsec.hsec, 1, 0)

        # Add last new DendSec to list.
        ndsecs.append(ndsec)

        # Rename offspring of input DendSec.
        dsec.rename_offspring()

        # Pass electrophysiological parameters to new dendritic sections.
        self._set_sections(dsecs=ndsecs)

        # Return.
        return ndsecs

    def _calc_linear_L_elec(self, u, v, a, b):
        """Return the electrotonic length of a portion of dendritic neurite.

        Arguments:
            u, v    : float : longitudinal abscissae of the endpoints of the section portion.
            a, b    : float : coefficients of the linear interpolant for the diameter of the portion between u and v:
                              diam(x) = a * x + b for x in [u, v].

        Return:
            L_elec  : float : electrotonic length of the portion of dendritic neurite.

        """
        if a == 0:
            return np.sqrt(4e-4 * self._Ra / self._Rm) * (v - u) / b ** 0.5
        else:
            return np.sqrt(4e-4 * self._Ra / self._Rm) * 2.0 / a * ((a * v + b) ** 0.5 - (a * u + b) ** 0.5)

    def _find_longi_abs(self, f0, u, a, b):
        """Return the longitudinal abscissa v such that _calc_linear_L_elec(u, v, a, b) = f0.

        Arguments:
            f0      : float : electrotonic length.
            u       : float : longitudinal abscissa of the 1st endpoint of the section portion.
            a, b    : float : coefficients of the linear interpolant for the diameter of the portion between u and the
                              looked-for v: diam(x) = a * x + b for x in [u, v].

        Return:
            v   : float : longitudinal abscissa yielding the input electrotonic length.

        """
        if a == 0:
            return f0 / np.sqrt(4e-4 * self._Ra / self._Rm) * b ** 0.5 + u
        else:
            return ((a / 2.0 * f0 / np.sqrt(4e-4 * self._Ra / self._Rm) + (a * u + b) ** 0.5) ** 2 - b) / a

    def _make_groups(self):
        """Form groups of dendritic sections (set _dsec_groups) based on their electrotonic distance from the soma.

        The attribute _dsec_groups are actually made of the indexes of the DendSecs, not the DendSecs themselves.

        The 'unitary' electrotonic distance ranges of the groups (_groups_electroto_lims_unit_Rm_Ra) and associated
        probabilities to receive a synapse (_groups_probas) are taken from Fig. 16 of 'Horseradish Peroxidase Study
        of the Spatial and Electrotonic Distribution of Group Ia Synapses on Type-Identified Ankle Extensor
        Motoneurons in the Cat', Burke and Glenn, The Journal of Comparative Neurology, 1996.

        The definition that is adopted in the mentioned publication for the electrotonic distance from the soma of
        any point of the dendritic tree, and which is also the one implemented in this package (see methods
        get_L_elec and get_elec_dist_from_soma of class DendSec) is the sum of the electrotonic lengths of every
        segment joining the soma to the considered point. Since all of these are proportional to the factor sqrt(
        Ra/Rm), the sum itself is proportional to sqrt(Ra/Rm) (see methods of the publication). Thus the electrotonic
        distances estimated in the mentioned publication can and should be reevaluated for the specific Rm and Ra of
        the present DendTree. Division by the factor sqrt(Ra/Rm) estimated with the Rm and Ra of the publication is
        already entailed in the Class attribute _groups_electroto_lims_unit_Rm_Ra. There simply remains to multiply
        back these 'unitary' electrotonic distances by the factor sqrt(Ra/Rm) estimated with the Rm and Ra of the
        present DendTree. This is achieved below, prior to distributing the synapses into the different groups
        according to their electrotonic lengths.

        """
        scale_fact = np.sqrt(self.dsecs[0].get_Ri() / self.dsecs[0].get_Rm() * 1e4)
        lims = self._groups_electroto_lims_unit_Rm_Ra * scale_fact

        dsec_groups = [list() for _ in range(self._n_groups)]
        for i, dsec in enumerate(self.dsecs):
            d_elec = dsec.get_elec_dist_from_soma()
            idx = np.flatnonzero(lims <= d_elec)[-1]
            dsec_groups[idx].append(i)

        self._dsec_groups = dsec_groups

    ##############################################################################################################
    # The methods below deal with selecting specific dendritic sections.
    ##############################################################################################################

    def get_stems(self):
        """Return a list of the DendStems of the DendTree."""
        return [dsec for dsec in self.dsecs if dsec.isastem]

    def get_dsecs(self, depth):
        """Return a list of the DendSecs whose depths in the tree are lower or equal to the specified depth.

        Arguments:
            depth   : int   : threshold depth.

        The soma is considered at depth -1, the dendrite stems at depth 0, etc.

        """
        if depth < 0:
            return []
        elif depth == 0:
            return self.get_stems()
        else:
            dsecs = self.get_dsecs(depth - 1)
            dsecs += [dsec.lchild for dsec in dsecs if dsec.lchild is not None]
            dsecs += [dsec.rchild for dsec in dsecs if dsec.rchild is not None]
            return dsecs

    def get_dsec(self, name):
        """Return the DendSec with specified name."""
        for dsec in self.dsecs:
            if dsec.name == name:
                break
        try:
            return dsec
        except:
            raise ValueError('No DendSec with specified name.')

    ##############################################################################################################
    # The methods below deal with selecting dendritic sections at random.
    ##############################################################################################################

    def pick_idsecs(self, k, mode='unif'):
        """Return k section indexes of the DendTree, picked at random with replacement.

        Arguments:
            k   : int   : the number of dendritic sections indexes to pick.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        """

        if mode == 'unif':
            idsecs = rand.choice(range(len(self.dsecs)), k, replace=True)

        elif mode == 'groups':
            idsecs = []
            for _ in range(k):
                while True:
                    try:
                        igroup = rand.choice(range(self._n_groups), p=self._groups_probas)
                        idsec = rand.choice(self._dsec_groups[igroup])
                    except:
                        continue
                    break
                idsecs.append(idsec)

        elif mode[0:5] == 'group':
            igroup = int(mode[5:]) - 1
            while igroup >= 0:
                try:
                    idsecs = [rand.choice(self._dsec_groups[igroup])]
                    break
                except:
                    igroup -= 1

        else:
            raise ValueError('Invalid input mode: {}'.format(mode))

        return idsecs

    def sample_idsecs(self, k, mode='unif'):
        """Return k section indexes of the DendTree, sampled at random without replacement.

        Arguments:
            k   : int   : the number of dendritic section indexes to sample.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        """

        if mode == 'unif':
            idsecs = rand.choice(range(len(self.dsecs)), k, replace=False)

        elif mode == 'groups':
            idsecs = []
            for _ in range(k):
                while True:
                    try:
                        igroup = rand.choice(range(self._n_groups), p=self._groups_probas)
                        idsec = rand.choice(self._dsec_groups[igroup])
                    except:
                        continue
                    if idsec not in idsecs:
                        break
                idsecs.append(idsec)

        elif mode[0:5] == 'group':
            igroup = int(mode[5:]) - 1
            while igroup >= 0:
                try:
                    idsecs = [rand.choice(self._dsec_groups[igroup])]
                except:
                    igroup -= 1

        else:
            raise ValueError('Invalid input mode: {}'.format(mode))

        return idsecs

    def pick_idsec(self, mode='unif'):
        """Return one section index of the DendTree, picked at random.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        """
        return self.pick_idsecs(1, mode=mode)[0]

    def pick_dsecs(self, k, mode='unif'):
        """Return k sections of the DendTree, picked at random with replacement.

        Arguments:
            k   : int   : number of dendritic sections to pick.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        See method `pickidsecs`.

        """
        idsecs = self.pick_idsecs(k, mode=mode)
        return [self.dsecs[idsec] for idsec in idsecs]

    def sample_dsecs(self, k, mode='unif'):
        """Return k sections of the DendTree, sampled at random without replacement.

        Arguments:
            k   : int   : the number of dendritic sections to sample.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        See method `sampleidsecs`.

        """
        idsecs = self.sample_idsecs(k, mode=mode)
        return [self.dsecs[idsec] for idsec in idsecs]

    def pick_dsec(self, mode='unif'):
        """Return one section of the DendTree, picked at random.

        Keyword arguments:
            mode    : str   : indicator of method for the random selection.

        See method `pickdsecs`.

        """
        return self.dsecs[self.pick_idsec(mode=mode)]

    ##############################################################################################################
    # The methods below deal with probing properties of the dendrites.
    ##############################################################################################################

    def get_Gin(self):
        """Return the input conductance of the DendTree (in Ohm).

        See 'Branching dendritic trees and motoneuron membrane resistivity', Rall, 1959.

        """
        Gins = [stem.get_Gin() for stem in self.get_stems()]
        return sum(Gins)

    def get_area(self):
        """Return the total area of the DendTree (in um^2)."""
        areas = [dsec.get_area() for dsec in self.dsecs]
        return sum(areas)

    def get_longest_dend_length(self):
        """Return the longest path from soma to leaf of the DendTree (in um)."""
        return max([stem.get_longest_path_length() for stem in self.get_stems()])

    def get_largest_stem_diam(self):
        """Return the largest diameter across the dendrite stems (in um)."""
        return max([stem.get_D() for stem in self.get_stems()])

#############################################################################
