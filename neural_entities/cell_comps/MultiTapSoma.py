# -*- coding: utf-8 -*


#############################################################################
# Class MultiTapSoma.
#
# Created on: 27 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np
import weakref
from neuron import h

from biophy_smc_ees.neural_entities.cell_secs.SomaSec import SomaSec
import biophy_smc_ees.utils.swc as swc


class MultiTapSoma(object):
    """
    This class regroups attributes and methods describing a cellular soma made of multiple tappered cylindrical
    compartments.

    Class attributes defined here:
        _Ra         : 200.0 : default axoplasmic resistivity (Ohm * cm).
        _Rm         : 500.0 : default membrane specific resistance (Ohm * cm^2).
        _cm         : 2.0   : default membrane specific capacitance (uF / cm^2).
        _Epas       : -70.0 : default leakage reversal potential (mV).
        _extra_mech : False : default indicator whether extracellular mechanism should be used or not.
        _xg0        : 1e10  : default value for the parameter xg[0] of the extracellular mechanism.
        _xg0        : 1e10  : default value for the parameter xg[1] of the extracellular mechanism.
        _xc0        : 0.0   : default value for the parameter xc[0] of the extracellular mechanism.
        _xc1        : 0.0   : default value for the parameter xc[1] of the extracellular mechanism.
        _xraxial0   : 1e10  : default value for the parameter xraxial[0] of the extracellular mechanism.
        _xraxial1   : 1e10  : default value for the parameter xraxial[1] of the extracellular mechanism.

    Instance variables defined here:
        name        : str                       : name of the MultiTapSoma.
        cell        : weakref.ref               : weakref to the Cell object to which the MultiTapSoma belongs.
        tsecs       : list of SomaSec objects   : composing the MultiTapSoma.
        _diam       : float                     : diameter of the MultiTapSoma.
        _swc_file   : str                       : path to swc file.
        _model      : str                       : identifier of MultiTapSoma model.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        _set_params(self)
        build(self)
        _create_sections(self)
        _set_sections(self)
        adjust_el(self)
        get_Gin(self)
        get_area(self)

    The MultiTapSoma class provides a container allowing to build and control the truncated conical HOC sections
    composing the soma of a cell. It is intended to be used to represent the somata of cells possessing a dendritic
    tree.

    As such, to each MultiTapSoma instance is associated a dendritic tree, described by a swc file. Thus,
    the MultiTapSoma class relies on swc files to determine the number and dimensions of the compartments of its
    instances.

    The rules to derive the number and dimensions of the compartments composing a MultiTapSoma are as follows:
        1. The total area of a MultiTapSoma is that of a spherical soma having the same diameter.
        2. There are as many compartments as there are dendrites in the dendritic tree associated to the MultiTapSoma.
        3. All the compartments composing the MultiTapSoma possess the same area, equal to the total area divided by
        the number of compartments.
        4. All the compartments composing the MultiTapSoma are connected together at one of their ends, where they
        share the same diameter.
        5. All the compartments composing the MultiTapSoma are connected to a dendrite stem at their other end, where
        they share the diameter of that dendrite stem.
        6. The lengths of the compartments composing the MultiTapSoma are such that rule #2 is fulfilled.
        7. If the MultiTapSoma belongs to a cell possessing an axon, a compartment needs be added to be connected
        to that axon, in compliance with the previous rules.

    The biophysical properties of the HOC sections composing a MultiTapSoma are taken from 'Extracellular Stimulation
    of Central Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al.,
    Journal of Neurophysiology, 2002. The ionic channels of the soma membrane are described by the 'MOTONEURON.mod'
    file which is based upon the same publication.

    A MultiTapSoma usually belongs to a Cell from which it draws its morphological parameters (including the path to
    an swc file) and model, but it can also be instantiated to stand alone by using the dedicated keyword arguments
    of the constructor.

    """

    # Biophysical properties of the soma from 'Extracellular Stimulation of Central Neurons: Influence of
    # Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al., Journal of Neurophysiology, 2002.
    _Ra = 200.0         # Ohm * cm
    _Rm = 1.0 / 0.002   # Ohm * cm^2
    _Cm = 2.0           # uF / cm^2
    _Epas = - 70.0      # mV

    _extra_mech = False
    _xg0 = 1e10
    _xg1 = 1e10
    _xc0 = 0.0
    _xc1 = 0.0
    _xraxial0 = 1e10
    _xraxial1 = 1e10

    fISdiam = [lambda x: 4.0 / 11.5 * x]

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            cell        : Cell object   : to which the MultiTapSoma belongs.
            diam        : float         : diameter (in um) of the Cell to which the MultiTapSoma belongs.
            swc_file    : str           : path to a swc file.
            model       : str           : identifier of DendTree model.
            extra_mech  : bool          : indicator whether extracellular mechanism should be used or not.

        If cell is passed in argument, each of diam, axon_diam, swc_file, model and extra_mech are first derived from
        the input Cell object.

        If diam, axon_diam, swc_file, model or extra_mech are passed in argument, they override any value derived
        from the input Cell object.

        When instantiating a MultiTapSoma object, diam, swc_file, model and extra_mech can be left to their default
        value (None). However, a valid path to a valid swc file must be provided for swc_file, otherwise it will not
        be possible to effectively build the MultiTapSoma by invoking the build() method (which will raise an
        exception).

        """

        # Public attributes.
        self.name = '{}_{:d}'.format(self.__class__.__name__, id(self))
        self.cell = lambda: None
        self.tsecs = None

        # Private object attributes.
        self._diam = None
        self._swc_file = None
        self._model = None

        try:
            self.cell = weakref.ref(kwargs['cell'])
            self.name = '{}.multi_tap_soma'.format(self.cell().name)
            self._diam = self.cell().diam()
            self._swc_file = self.cell().swc_file()
            self._model = self.cell().soma_model()
            self._extra_mech = self.cell().extra_mech()
        except:
            pass

        try:
            self._diam = kwargs['diam']
        except:
            pass

        try:
            self._swc_file = kwargs['swc_file']
        except:
            pass

        try:
            self._model = kwargs['model']
        except:
            pass

        try:
            self._extra_mech = kwargs['extra_mech']
        except:
            pass

        self._set_params()

    def __repr__(self):
        """Return the string representation of the MultiTapSoma."""
        return self.name

    def _set_params(self):
        """Set the biophysical parameters of the MultiTapSoma according to its attribute _model.

        Not implemented. The biophysical parameters remain to their default value.

        """
        pass

    def build(self):
        """Build the MultiTapSoma."""
        self._create_sections()
        self._set_sections()

    def _create_sections(self):
        """Create the sections of the MultiTapSoma.

        This method is limited to creating the sections and setting their geometry and interconnectivity. Setting
        their biophysical properties is delegated to another method.

        """

        self.tsecs = []

        # Retrieve diameters of dendrite stems connected to the MultiTapSoma.
        ds = swc.get_stem_diams(self._swc_file, target_diam=self._diam)
        ndend = len(ds)

        # If the MultiTapSoma belongs to a cell which comprises an axon, add this axon's diameter to the previous list.
        cell = self.cell()
        if cell is not None and cell.with_axon():
            d = self.fISdiam[0](cell.axon_diam())
            ds.insert(0, d)
            ndend = ndend + 1

        # Total soma area (omitting multiplication by pi).
        S0 = self._diam ** 2

        # Area allocated to each tappered compartment.
        S0 = S0 / ndend

        # Radius of the tappered compartments at the center of the cell (the 0 end).
        # Note: the choice below is arbitrary, but it dictates the lengths of the compartments.
        r0 = np.sqrt(S0 / np.sqrt(2.0))

        # Build tappered somatic compartments.
        for i in range(ndend):

            tsec = SomaSec(name='{}.tsec{:d}'.format(self.name, i))
            tsec.hsec = h.Section(name='hsec', cell=tsec)
            ri = ds[i] / 2.0
            # The area of a truncated cone of length L and base disks' radii r1 and r2 is given by
            #   A = pi * (r1 + r2) * sqrt(L^2 + (r1-r2)^2)
            # The variable L below is obtained by inverting the previous formula (and pi is omitted because it was
            # omitted in S0 as well).
            L = np.sqrt((S0 / (ri + r0)) ** 2 - (ri - r0) ** 2)
            x = np.cos(2 * np.pi * float(i) / float(ndend)) * L
            y = np.sin(2 * np.pi * float(i) / float(ndend)) * L

            h.pt3dadd(0.0, 0.0, 0.0, r0 * 2.0, sec=tsec.hsec)
            h.pt3dadd(  x,   y, 0.0, ri * 2.0, sec=tsec.hsec)

            self.tsecs.append(tsec)

            if i != 0:
                tsec.hsec.connect(self.tsecs[0].hsec, 0, 0)

    def _set_sections(self):
        """Set the biophysical properties of the sections of the MultiTapSoma."""
        for tsec in self.tsecs:
            tsec.hsec.Ra = self._Ra
            tsec.hsec.cm = self._Cm
            tsec.hsec.insert('motoneuron')
            if self._extra_mech:
                tsec.hsec.insert('extracellular')
                tsec.hsec.xg[0] = self._xg0
                tsec.hsec.xg[1] = self._xg1
                tsec.hsec.xc[0] = self._xc0
                tsec.hsec.xc[1] = self._xc1
                tsec.hsec.xraxial[0] = self._xraxial0
                tsec.hsec.xraxial[1] = self._xraxial1

    def adjust_el(self):
        """Adjust the leakage reversal potential of the sections of the MultiTapSoma to reach steady-state."""
        ina = self.tsecs[0].hsec.ina_motoneuron
        ikrect = self.tsecs[0].hsec.ikrect_motoneuron
        ikca = self.tsecs[0].hsec.ikca_motoneuron
        icaN = self.tsecs[0].hsec.icaN_motoneuron
        icaL = self.tsecs[0].hsec.icaL_motoneuron
        v = self.tsecs[0].hsec.v
        gl = self.tsecs[0].hsec.gl_motoneuron

        for tsec in self.tsecs:
            tsec.hsec.el_motoneuron = (ina + ikrect + ikca + icaN + icaL + v * gl) / gl

    def get_Gin(self):
        """Return the total passive input conductance of the MultiTapSoma (in Ohm)."""
        gs = [tsec.get_Gin() for tsec in self.tsecs]
        return sum(gs)

    def get_area(self):
        """Return the total area of the MultiTapSoma (in um^2)."""
        areas = [tsec.get_area() for tsec in self.tsecs]
        return sum(areas)

#############################################################################
