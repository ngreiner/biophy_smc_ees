# -*- coding: utf-8 -*


#############################################################################
# Class Soma.
#
# Created on: 5 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import weakref
from neuron import h

from biophy_smc_ees.neural_entities.cell_secs.SomaSec import SomaSec


class Soma(object):
    """
    This class regroups attributes and methods describing a cellular soma made of a single cylindrical compartment.

    Class variables defined here:
        _Ra         : 200.0 : default axoplasmic resistivity (Ohm * cm).
        _Rm         : 500.0 : default membrane specific resistance (Ohm * cm^2).
        _cm         : 2.0   : default membrane specific capacitance (uF / cm^2).
        _Epas       : -70.0 : default leakage reversal potential (mV).
        _extra_mech : False : default indicator whether extracellular mechanism should be used or not.
        _xg0        : 1e10  : default value for the parameter xg[0] of the extracellular mechanism.
        _xg0        : 1e10  : default value for the parameter xg[1] of the extracellular mechanism.
        _xc0        : 0.0   : default value for the parameter xc[0] of the extracellular mechanism.
        _xc1        : 0.0   : default value for the parameter xc[1] of the extracellular mechanism.
        _xraxial0   : 1e10  : default value for the parameter xraxial[0] of the extracellular mechanism.
        _xraxial1   : 1e10  : default value for the parameter xraxial[1] of the extracellular mechanism.

    Instance variables defined here:
        cell    : weakref.ref       : weakref to the Cell object to which the Soma belongs.
        somasec : SomaSec object    : composing the Soma.
        name    : str               : name of the Soma.
        _diam   : float             : diameter of the Soma.
        _model  : str               : identifier of the Soma model.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        _set_params(self)
        build(self)
        _create_sections(self)
        _set_sections(self)
        adjust_el(self)
        get_Gin(self)
        get_area(self)

    The Soma class provides a container to encapsulate the single cylindrical HOC section composing the soma of a cell.

    The biophysical properties of the HOC section composing a soma are taken from 'Extracellular Stimulation of
    Central Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al., Journal of
    Neurophysiology, 2002. The ionic channels of the soma membrane are described by the 'MOTONEURON.mod' file which
    is based upon the same publication.

    A Soma usually belongs to a Cell from which it draws its morphological parameters and model, but it can also be
    instantiated to stand alone by using the dedicated keyword arguments of the constructor.

    """

    # Biophysical properties of the soma from 'Extracellular Stimulation of Central Neurons: Influence of
    # Stimulus Waveform and Frequency on Neuronal Output', McIntyre et al., Journal of Neurophysiology, 2002.
    _Ra = 200.0         # Ohm * cm
    _Rm = 1.0 / 0.002   # Ohm * cm^2
    _cm = 2.0           # uF / cm^2
    _Epas = - 70.0      # mV

    _extra_mech = False
    _xg0 = 1e10
    _xg1 = 1e10
    _xc0 = 0.0
    _xc1 = 0.0
    _xraxial0 = 1e10
    _xraxial1 = 1e10

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            cell        : Cell object   : to which the Soma belongs.
            diam        : float         : diameter (in um) of the Cell to which the Soma belongs.
            model       : str           : identifier of Soma model.
            extra_mech  : bool          : indicator whether extracellular mechanism should be used or not.

        If cell is passed as a keyword argument, each of diam, model and extra_mech are first derived from the input
        Cell object.

        If diam, model or extra_mech are passed as keyword arguments, they override any value derived from the input
        Cell object.

        When instantiating a Soma object, diam, model and extra_mech can be left to their default value (None),
        but a positive float must be provided for diam, otherwise it will not be possible to really build the Soma by
        invoking the build() method (which will raise an exception).

        """

        # Public attributes.
        self.name = '{}_{:d}'.format(self.__class__.__name__, id(self))
        self.cell = lambda: None
        self.somasec = None

        # Private object attributes.
        self._diam = None
        self._model = None

        try:
            self.cell = weakref.ref(kwargs['cell'])
            self.name = '{}.soma'.format(self.cell().name)
            self._diam = self.cell().diam()
            self._model = self.cell().soma_model()
            self._extra_mech = self.cell().extra_mech()
        except:
            pass

        try:
            self._diam = kwargs['diam']
        except:
            pass

        try:
            self._model = kwargs['model']
        except:
            pass

        try:
            self._extra_mech = kwargs['extra_mech']
        except:
            pass

        self._set_params()

    def __repr__(self):
        """Return the string representation of the Soma."""
        return self.name

    def _set_params(self):
        """Set the biophysical parameters of the Soma according to its attribute _model.

        Not implemented. The biophysical parameters remain to their default value.

        """
        pass

    def build(self):
        """Build the Soma."""
        self._create_sections()
        self._set_sections()

    def _create_sections(self):
        """Create the SomaSec composing the Soma.

        This method is limited to creating the section and setting its geometry. Setting its biophysical properties
        is delegated to another method.

        """
        self.somasec = SomaSec(name='{}.somasec'.format(self.name))
        self.somasec.hsec = h.Section(name='hsec', cell=self.somasec)
        rad = self._diam / 2.0
        h.pt3dadd( rad, 0.0, 0.0, self._diam, sec=self.somasec.hsec)
        h.pt3dadd( 0.0, 0.0, 0.0, self._diam, sec=self.somasec.hsec)
        h.pt3dadd(-rad, 0.0, 0.0, self._diam, sec=self.somasec.hsec)

    def _set_sections(self):
        """Set the biophysical properties of the section of the Soma."""
        self.somasec.hsec.Ra = self._Ra
        self.somasec.hsec.cm = self._cm
        self.somasec.hsec.insert('motoneuron')
        if self._extra_mech:
            self.somasec.hsec.insert('extracellular')
            self.somasec.hsec.xg[0] = self._xg0
            self.somasec.hsec.xg[1] = self._xg1
            self.somasec.hsec.xc[0] = self._xc0
            self.somasec.hsec.xc[1] = self._xc1
            self.somasec.hsec.xraxial[0] = self._xraxial0
            self.somasec.hsec.xraxial[1] = self._xraxial1

    def adjust_el(self):
        """Adjust the leakage reversal potential of the section of the Soma to reach steady-state."""
        ina = self.somasec.hsec.ina_motoneuron
        ikrect = self.somasec.hsec.ikrect_motoneuron
        ikca = self.somasec.hsec.ikca_motoneuron
        icaN = self.somasec.hsec.icaN_motoneuron
        icaL = self.somasec.hsec.icaL_motoneuron
        v = self.somasec.hsec.v
        gl = self.somasec.hsec.gl_motoneuron
        self.somasec.hsec.el_motoneuron = (ina + ikrect + ikca + icaN + icaL + v * gl) / gl

    def get_Gin(self):
        """Return the total passive input conductance of the Soma (in Ohm)."""
        return self.somasec.get_Gin()

    def get_area(self):
        """Return the area of the Soma (in um^2)."""
        return self.somasec.hsec(0.5).area()

#############################################################################
