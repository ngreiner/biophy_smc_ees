# -*- coding: utf-8 -*


#############################################################################
# Class Soma.
#
# Created on: 5 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import weakref

from biophy_smc_ees.neural_entities.branches.MotorBranch import MotorBranch


class MotorAxon(object):
    """
    This class regroups attributes and methods describing a motor axon (as a compartment of a motoneuron, rather than
    as a fully-fledged NeuralEntity -- role which is played by the homonymous class of the subpackage fibers).

    Class variables defined here:
        _model  : str   : default identifier of MotorAxon model.

    Instance variables defined here:
        name        : str                   : name of the MotorAxon.
        cell        : weakref.ref           : weakref to the Cell object to which the MotorAxon belongs.
        VRbranch    : MotorBranch object    : composing the MotorAxon.
        _morpho     : dict                  : morphological parameters of the MotorAxon.

    Methods defined here:
        __init__(self, **kwargs)
        model(self, *args)
        get_morpho(self, *args)
        build(self)
        adjust_el(self)
        _create_branches(self)
        _create_sections(self)
        _set_sections(self)

    The MotorAxon class provides a container to encapsulate the MotorBranch representing the unbranched myelinated
    axon of a motoneuron. It was in fact developed to allow representing motor axons presenting more complex (
    branching) structures (like axons comprising collaterals contacting Renshaw cells, or comprising terminal
    arborizations) at a later time.

    """

    _model = 'ModelD'

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            cell        : Cell object   : to which the MotorAxon belongs.
            diam        : float         : diameter (in um) of the MotorAxon.
            nN          : int           : number of nodes of Ranvier of the MotorAxon.
            model       : str           : identifier of MotorAxon model.
            extra_mech  : bool          : indicator whether extracellular mechanism should be used or not.

        If cell is passed in argument, each of diam, nN, model and extra_mech are first derived from the input Cell
        object.

        If diam, nN, model or extra_mech are passed in argument, they override any value derived from the input Cell
        object.

        When instantiating a MotorAxon object, model and extra_mech can be left to their default value (None),
        but diam and nN must be assigned with appropriate values to allow effectively building the MotorAxon by
        invoking the build() method (which will otherwise raise an exception).

        """

        # Public attributes.
        self.name = '{}_{:d}'.format(self.__class__.__name__, id(self))
        self.cell = lambda: None
        self.VRbranch = None

        # Private object attributes.
        self._morpho = dict()
        self._morpho['VRbranch'] = dict()

        try:
            self.cell = weakref.ref(kwargs['cell'])
            self.name = '{}.motoraxon'.format(self.cell().name)
            self._morpho['VRbranch']['diam'] = self.cell().axon_diam()
            self._morpho['VRbranch']['nN'] = self.cell().axon_nN()
            self._model = self.cell().axon_model()
        except:
            pass

        try:
            self._morpho['VRbranch']['diam'] = kwargs['diam']
        except:
            pass

        try:
            self._morpho['VRbranch']['nN'] = kwargs['nN']
        except:
            pass

        try:
            self.model(kwargs['model'])
        except:
            pass

    def __repr__(self):
        """Return the string representation of the MotorAxon."""
        return self.name

    def model(self, *args):
        """Return (without args) or set and return (with args) the attribute _model."""
        if len(args) > 0:
            if self._model != args[0]:
                self._model = args[0]
        return self._model

    def get_morpho(self, *args):
        """Return the _morpho dictionnary (or specific keys of it if args provided)."""
        morpho = self._morpho
        for arg in args:
            morpho = morpho[arg]
        return morpho

    def build(self):
        """Build the MotorAxon."""
        self._create_branches()
        self._create_sections()
        self._set_sections()

    def _create_branches(self):
        """Create the branches of the MotorAxon.

        It only consists in a ventral root branch.

        """
        self.VRbranch = MotorBranch('VRbranch', cell=self)

    def _create_sections(self):
        """Create the sections of the MotorBranch composing the MotorAxon.

        This method is limited to creating the sections and setting their geometries and interconnectivity,
        for which the work is delegated to the homonymous method of class MotorBranch.

        Setting their biophysical properties is delegated to the method _set_sections.

        """
        self.VRbranch.build()

    def _set_sections(self):
        """Set the biophysical properties of the sections of the MotorAxon.

        Work is delegated to homonymous method of class MotorBranch.

        """
        self.VRbranch.set_sections()

    def adjust_el(self):
        """Adjust the leakage reversal potential of the MotorAxon's active compartments to reach steady-state.

        Work is delegated to homonymous method of class MotorBranch.

        """
        self.VRbranch.adjust_el()

    def get_nodes(self):
        """Return a list of references to the nodes of Ranvier of the MotorAxon."""
        nodes = self.VRbranch.nodesCNS + self.VRbranch.nodesPNS
        return nodes

#############################################################################
