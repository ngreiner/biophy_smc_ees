# -*- coding: utf-8 -*


#############################################################################
# Class NeuralEntity.
#
# Created on: 17 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


class NeuralEntity(object):
    """
    This class regroups attributes and methods describing a neural entity of a sensorimotor circuit.

    Instance variables defined here:
        _smc                : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
        _musc               : str           : name of the muscle to which the NeuralEntity belongs.
        _seg                : str           : name of the segment in which the NeuralEntity is located.
        _fidx               : int           : index of the NeuralEntity in the (musc, seg) family.
        _gidx               : int           : global index of the NeuralEntity in the sensorimotor circuit.
        _need_2_read_info   : bool          : indicator whether NeuralEntity info shall be read from database.
        _need_2_build       : bool          : indicator whether NeuralEntity shall be built.

    Methods defined here:
        __init__(self, **kwargs)
        __str__(self)
        smc(self, *args)
        musc(self, *args)
        seg(self, *args)
        fidx(self, *args)
        info(self, *args)
        gidx(self, *args)
        reset(self, **kwargs)
        _has_ID(self)
        _ready_2_build(self)

    The NeuralEntity class is a base class for describing various neural entities, such as nerve fibers or motoneurons.
    The purpose of the NeuralEntity class is to provide the attributes and methods to identify the neural entity within
    a sensorimotor circuit. Such identification is ensured by an SMC object and the global index of the neural entity
    within the sensorimotor circuit described by that SMC object. The type of the neural entity is readily given by the
    name of the specific subclass of the neural entity.

    """

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment in which the NeuralEntity is positioned.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            info    : 3-tuple       : (musc, seg, fidx) identifying the NeuralEntity.
            gidx    : int           : global index of the NeuralEntity in the sensorimotor circuit.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.

        """

        # Public attributes.
        self.name = '{}_{:d}'.format(self.__class__.__name__, id(self))

        # Private attributes.
        self._smc = None
        self._musc = None
        self._seg = None
        self._fidx = None
        self._gidx = None
        self._need_2_read_info = False
        self._need_2_build = False

        # Parse inputs.
        try:
            self.smc(kwargs['smc'])
        except:
            pass

        try:
            self.musc(kwargs['musc'])
        except:
            pass

        try:
            self.seg(kwargs['seg'])
        except:
            pass

        try:
            self.fidx(kwargs['fidx'])
        except:
            pass

        try:
            self.info(kwargs['info'])
        except:
            pass

        try:
            self.gidx(kwargs['gidx'])
        except:
            pass

        try:
            self.name = '{}_{}_{}_{}'.format(self.__class__.__name__, self._musc, self._seg, self._fidx)
        except:
            pass

    def __repr__(self):
        """Return the string representation of the NeuralEntity."""
        return self.name

    ##############################################################################################################
    # Mutators and accessors.
    ##############################################################################################################

    def smc(self, *args):
        """Return (without args) or set and return (with args) the attribute _smc.

        Optional arguments:
            smc : SMC object.

        Additional actions:
            Set _need_2_read_info and _need_2_build to True if _smc is modified.

        """
        if len(args) > 0:
            if self._smc != args[0]:
                self._smc = args[0]
                self._need_2_read_info = True
                self._need_2_build = True
        return self._smc

    def musc(self, *args):
        """Return (without args) or set and return (with args) the attribute _musc.

        Optional arguments:
            musc    : str   : name of muscle.

        Additional actions:
            Update _gidx and set _need_2_read_info and _need_2_build to True if _musc is modified.

        """
        if len(args) > 0:
            if self._musc != args[0]:
                self._musc = args[0]
                self._need_2_read_info = True
                self._need_2_build = True
                try:
                    self._gidx = self._smc.info_2_gidx(self.__class__.__name__, self._musc, self._seg, self._fidx)
                except:
                    pass
        return self._musc

    def seg(self, *args):
        """Return (without args) or set and return (with args) the attribute _seg.

        Optional arguments:
            seg : str   : name of segment.

        Additional actions:
            Update _gidx and set _need_2_read_info and _need_2_build to True if _seg is modified.

        """
        if len(args) > 0:
            if self._seg != args[0]:
                self._seg = args[0]
                self._need_2_read_info = True
                self._need_2_build = True
                try:
                    self._gidx = self._smc.info_2_gidx(self.__class__.__name__, self._musc, self._seg, self._fidx)
                except:
                    pass
        return self._seg

    def fidx(self, *args):
        """Return (without args) or set and return (with args) the attribute _fidx.

        Optional arguments:
            fidx    : int   : family index.

        Additional actions:
            Update _gidx and set _need_2_read_info and _need_2_build to True if _fidx is modified.

        """
        if len(args) > 0:
            if self._fidx != args[0]:
                self._fidx = args[0]
                self._need_2_read_info = True
                self._need_2_build = True
                try:
                    self._gidx = self._smc.info_2_gidx(self.__class__.__name__, self._musc, self._seg, self._fidx)
                except:
                    pass
        return self._fidx

    def info(self, *args):
        """Return (without args) or set and return (with args) the attributes _musc, _seg, _fidx.

        Optional arguments:
            info    : 3-tuple   : (musc, seg, fidx) triplet.

        Additional actions:
            Update _gidx and set _need_2_read_info and _need_2_build to True if any of _musc, _seg or _fidx is modified.

        """
        if len(args) > 0:
            if (self._musc, self._seg, self._fidx) != args[0]:
                self._musc, self._seg, self._fidx = args[0]
                self._gidx = self._smc.info_2_gidx(self.__class__.__name__, *args[0])
                self._need_2_read_info = True
                self._need_2_build = True
        return self._musc, self._seg, self._fidx

    def gidx(self, *args):
        """Return (without args) or set and return (with args) the attribute _gidx.

        Optional arguments:
            info    : 3-tuple   : (musc, seg, fidx) triplet.

        Additional actions:
            Update _musc, _seg and _fidx, and set _need_2_read_info and _need_2_build to True if _gidx is modified.

        """
        if len(args) > 0:
            if self._gidx != args[0]:
                self._gidx = args[0]
                self._musc, self._seg, self._fidx = self._smc.gidx_2_info(self.__class__.__name__, args[0])
                self._need_2_read_info = True
                self._need_2_build = True
        return self._gidx

    def reset(self, **kwargs):
        """Reset the NeuralEntity.

        Keyword arguments:
            smc     : SMC object    : sensorimotor circuit to which the NeuralEntity belongs.
            musc    : str           : name of the muscle to which the NeuralEntity belongs.
            seg     : str           : name of the segment through which the NeuralEntity is running.
            fidx    : int           : index of the NeuralEntity in the (musc, seg) family.
            gidx    : int           : global index of the NeuralEntity.

        If gidx is provided in argument, it has precedence over info, which has precedence over the triplet
        (musc, seg, fidx) to set the identity of the NeuralEntity.

        """

        try:
            self.smc(kwargs['smc'])
        except:
            pass

        try:
            self.musc(kwargs['musc'])
        except:
            pass

        try:
            self.seg(kwargs['seg'])
        except:
            pass

        try:
            self.fidx(kwargs['fidx'])
        except:
            pass

        try:
            self.info(kwargs['info'])
        except:
            pass

        try:
            self.gidx(kwargs['gidx'])
        except:
            pass

        try:
            self.name = '{}_{}_{}_{}'.format(self.__class__.__name__, self._musc, self._seg, self._fidx)
        except:
            pass

    ##############################################################################################################
    # The methods below deal with probing the entity's status.
    ##############################################################################################################

    def _has_ID(self):
        """Return a boolean indicating whether the NeuralEntity is properly identified or not.

        It is the case if the entity has a global index in a sensorimotor circuit.

        """
        return (self._smc is not None) and (self._gidx is not None)

    def _ready_2_build(self):
        """Return a boolean indicating whether the NeuralEntity is ready to be built or not.

        Purely virtual method (overridden in children classes).

        """
        pass
