# -*- coding: utf-8 -*


#############################################################################
# Class Bootstrap.
#
# Created on: 8 June 2017
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import random as rnd
import numpy as np


class Bootstrap(object):
    """
    This class regroups attributes and methods to perform bootstrapping statistical analyses.

    Class variables defined here:
        pop_size    : 1000  : default number of random sequences forming the bootstrapped population.

    Instance variables defined here:
        data_sequence   : 1D array-like     : original data sequence drawn from unknown distribution.
        dtype           : type              : type of the data values.
        sequence_size   : int               : size of the data sequence.
        pop_size        : int               : number of random sequences forming the bootstrapped population.
        pop             : 2D numpy.array    : bootstrapped population.

    """

    pop_size = 1000

    def __init__(self, *args, **kwargs):
        """Class-constructor method.

        Optional arguments:
            data_sequence   : 1D array-like : original data sequence drawn from unknown distribution.

        Keyword arguments:
            pop_size    : int   : number of random sequences forming the bootstrapped population.

        """

        self.data_sequence = None
        self.dtype = None
        self.sequence_size = None
        self.pop = None

        try:
            self.set_data_sequence(args[0])
        except:
            pass

        try:
            self.pop_size = kwargs['pop_size']
        except:
            pass

    def set_data_sequence(self, data_sequence):
        """Set data_sequence, original data sequence onto which the bootstrapping is performed.

        Arguments:
            data_sequence : sequence  : sequence of values.

        Sets the attributes data_sequence, dtype, and sequence_size.

        """
        self.data_sequence = data_sequence
        self.dtype = np.asarray(data_sequence).dtype
        self.sequence_size = len(data_sequence)

    def gen_sequence(self):
        """Generate and return a random sequence by sampling (with replacement) the original data sequence.

        Return:
            sequence    : 1D numpy.array    : random sequence obtained by sampling the original sequence.

        """
        sequence = np.zeros(self.sequence_size, dtype=self.dtype)
        for i in range(self.sequence_size):
            sequence[i] = rnd.choice(self.data_sequence)
        return sequence

    def gen_pop(self):
        """Generate the bootstrap population (sets the attribute pop).

        pop is a 2D numpy.array with sequence_size lines and pop_size columns.
        Each column of pop is a random sequence of the same size as the original data sequence, and obtained by
        sampling with replacement this very data sequence.

        """
        self.pop = np.zeros((self.sequence_size, self.pop_size), dtype=self.dtype)
        for i in range(self.pop_size): self.pop[:, i] = self.gen_sequence()

    def get_mean(self):
        """Return an estimate of the mean of the distribution from which the original data sequence is drawn.

        Return:
            m   : float : estimated mean of the unknown distribution.

        Description: m = np.mean(np.mean(self.pop, axis=0))

        """
        return np.mean(np.mean(self.pop, axis=0))

    def get_std(self):
        """Return an estimate of the std of the distribution from which the original data sequence is originating.

        Return:
            s   : float : estimated standard deviation of the unknown distribution.

        Description: s = np.std(np.mean(self.pop, axis=0))

        """
        return np.std(np.mean(self.pop, axis=0))

    def get_mean_sum(self):
        """Return an estimate of the mean of the sum of #sequence_size realizations of the distribution from which
        the original data sequence is originating.

        Return:
            m   : float : estimated mean sum of #sequence_size realizations drawn from the unknown distribution.

        Description: m = np.mean(np.sum(self.pop, axis=0))

        """
        return np.mean(np.sum(self.pop, axis=0))

    def get_sum_std(self):
        """Return an estimate of the standard deviation of the sum of #sequence_size realizations of the
        distribution from which the original data sequence is originating.

        Return:
            s   : float : estimated standard deviation of the sum of #sequence_size realizations drawn from the unknown
                          distribution.

        Description: s = np.std(np.sum(self.pop, axis=0))

        """
        return np.std(np.sum(self.pop, axis=0))
