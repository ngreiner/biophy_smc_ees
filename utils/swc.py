# -*- coding: utf-8 -*


#############################################################################
# Module swc.py.
#
# Created on: 4 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


def read_file(file_path, target_diam=None):
    """Read the swc file at file_path and return its extracted information.

    Arguments:
        file_path   : str   :  path to swc file.

    Keyword arguments:
        target_diam : float : the diameter of the cell which the user intends to build using the swc file at file_path.

    Return:
        sids    : list of int   : sample identifiers. A sample is a 3D point belonging to one section of the MN.
        stypes  : list of int   : sample types.
        xs      : list of float : sample x-coordinates.
        ys      : list of float : sample y-coordinates.
        zs      : list of float : sample z-coordinates.
        ds      : list of float : sample diameters.
        pids    : list of int   : sample parent identifiers.

    Description: see http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html for a
    description of the swc format. If target_diam is specified, target_diam refers to the diameter of the soma of the
    cell which the user intends to build with the help of the swc file at file_path. In this case, the geometric
    parameters contained in the swc file at file_path are uniformly scaled such that the soma diameter given by these
    parameters match the specified target_diam.

    """
    with open(file_path, 'r') as fileID:
        lines = fileID.readlines()

    # Split lines around blank spaces.
    lines = [line.split() for line in lines]

    # Remove first lines (which are comments).
    while lines[0][0] == '#':
        lines.pop(0)

    # Convert line elements from strings to int/float.
    sids = [int(line[0]) for line in lines]
    stypes = [int(line[1]) for line in lines]
    xs = [float(line[2]) for line in lines]
    ys = [float(line[3]) for line in lines]
    zs = [float(line[4]) for line in lines]
    ds = [2 * float(line[5]) for line in lines]
    pids = [int(line[6]) for line in lines]

    # Scale geometric parameters if target_diam is specified.
    if target_diam:
        f = target_diam / ds[0]
        xs = [elt * f for elt in xs]
        ys = [elt * f for elt in ys]
        zs = [elt * f for elt in zs]
        ds = [elt * f for elt in ds]

    return sids, stypes, xs, ys, zs, ds, pids


def get_branches(sid, pids):
    """Return the branches originating from sample sid.

    Arguments:
        sid     : int           : sample.
        pids    : list of int   : list of parent samples. pids[i] = parent sample of sample i.

    Return:
        branches    : list of tuple of int  : branches originating from sample sid.

    Description:

        A branch is a tuple of samples linearly connected where:

        1. The first sample is either the first sample of an entire dendrite (in which case its parent sample is a
        somatic sample), either a sample where bifurcation occurs (in which case it has two children samples and it
        is the last sample of the branch directly upstream of the branch of which it is the first sample, and it will
        also be the first sample of another branch). {Note: first samples of entire dendrites present the peculiarity
        that they might be bifurcation points from which more than two branches bifurcate, ie they might have more
        than two children samples.}

        2. The last sample is either a leaf (it is the parent of no sample), either a sample where bifurcation occurs
        (in which case it has two children samples and it will be the first sample of the two branches directly
        downstream of the branch of which it is the last sample).

        3. The other samples are not the first sample of an entire dendrite (their parent sample is not a somatic
        sample), and they are the parent sample of exactly one other sample.

        4. If n is the length of branch (number of samples it contains), we have: for i = 0 ... n-2,  branch[i] is
        the parent of branch[i+1].

        This method retrieves the branches that originate from any input sample. If the input sample is an inner
        sample of a branch (not the first, neither the last), we extend the definition of a branch to the linear
        connection of samples joining this sample to the first bifurcation sample downstream to it. The branches
        originating from the input sample will thus comprise this degenerate/incomplete branch and the branches
        originating from this first bifurcation sample downstream to it.

    """
    cids = get_children(sid, pids)  # direct children of sid

    # If sid doesn't have any children, no branch originates from sid.
    if len(cids) == 0:
        return []

    # If sid has a single child, which we call c0, we need to inspect the offspring of this child.
    elif len(cids) == 1:
        ccids = get_children(cids[0], pids)     # direct children of c0

        # If c0 doesn't have any children, there's only 1 branch originating from sid, formed by itself and c0.
        if len(ccids) == 0:
            return [(sid, cids[0])]

        # If c0 has a single child, c0 is not a bifurcation point of the dendritic tree. We look for the branches
        # originating from c0. The first of these branches is the branch connecting c0 to the closest bifurcation or
        # leaf sample descending from it (which is also the closest bifurcation or leaf sample descending from sid).
        # If it is a leaf, then there is actually only one branch originating from c0: the one connecting c0 to this
        # leaf. If it is a bifurcation point, there are more than one branch originating from c0, the others being
        # the branches originating from this bifurcation point. In both cases, we need to prepend sid to the first
        # branch originating from c0.
        elif len(ccids) == 1:
            cbr0 = get_branches(cids[0], pids)
            return [(sid,) + cbr0[0]] + cbr0[1:]

        # If c0 has two children, c0 is a bifurcation point of the dendritic tree. sid and c0 form a branch,
        # and the other branches originating from sid are the branches originating from c0.
        else:
            return [(sid, cids[0])] + get_branches(cids[0], pids)

    # If sid has two children, which we call c0 and c1, we need to inspect the offspring of these children.
    else:
        c0cids = get_children(cids[0], pids)
        c1cids = get_children(cids[1], pids)

        # If c0 doesn't have any children, sid and c0 form a branch, and there are no other branches descending from sid
        # on the side of c0.
        if len(c0cids) == 0:
            br0 = [(sid, cids[0])]

        # If c0 has a single child, the branches originating from sid on the side of c0 are again obtained by
        # getting the branches originating from c0, and prepending sid to the first of these branches.
        elif len(c0cids) == 1:
            cbr0 = get_branches(cids[0], pids)
            br0 = [(sid,) + cbr0[0]] + cbr0[1:]

        # If c0 has two children, sid and c0 form a branch, and the other branches descending from sid on the side of c0
        # are the branches originating from c0.
        else:
            br0 = [(sid, cids[0])] + get_branches(cids[0], pids)

        # Same reasoning applies to get the branches originating from sid on the side of c1.
        if len(c1cids) == 0:
            br1 = [(sid, cids[1])]

        elif len(c1cids) == 1:
            cbr1 = get_branches(cids[1], pids)
            br1 = [(sid,) + cbr1[0]] + cbr1[1:]

        else:
            br1 = [(sid, cids[1])] + get_branches(cids[1], pids)

        # The branches originating from sid are the union of those on the side of c0 and those on the side of c1.
        return br0 + br1


def get_children(sid, pids):
    """Return the children samples of the input sample.

    Arguments:
        sid     : int           : sample.
        pids    : list of int   : table of parent samples. pids[i] = parent sample of sample i.

    Return:
        cids    : list of int   : children samples of the input sample.

    Description: the children of the input sample are those samples whose parent is that sample.

    """
    cids = [-1]
    while True:
        try:
            cids.append(pids.index(sid, cids[-1] + 1) + 1)
        except:
            break
    cids.pop(0)
    return cids


def get_stem_diams(file_path, target_diam=None):
    """Return the initial diameters of the stems of the dendritic tree described by the input swc file.

    Arguments:
        file_path   : str   : swc file path.
        target_diam : float : the diameter of the cell which the user intends to build using the swc file at file_path.

    Return:
        diams   : list of float : list of diameters of the stems of the dendritic tree.

    Description: the initial samples of the stems have their stype==3 and pid==1, allowing easy identification. If
    target_diam is specified, the diameters associated with these initial samples (here above referred to as 'initial
    diameters') are first adjusted (ie scaled) to comply with this target_diam before being returned.

    """
    sids, stypes, xs, ys, zs, ds, pids = read_file(file_path, target_diam=target_diam)
    ds = [d for stype, d, pid in zip(stypes, ds, pids) if stype == 3 and pid == 1]
    return ds
