# -*- coding: utf-8 -*


###########################################################################
# Class Plotter.
#
# Created on: 29 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D


class Plotter:
    """
    This class regroups attributes and methods which allow building and plotting matplotlib figures.

    Class variables defined here:
        _subplot_width = 3.5
        _subplot_height = 3.5
        _cmap = 'jet'

    Methods defined here:
        __init__(self, **kwargs)
        build_figure(self, nsubplots, ncol=None, projection=None, mask=None)
        plot_lines(self, ax, *args, **kwargs)
        plot_bounded_lines(self, ax, *args, **kwargs)
        plot_bars_with_errors(self, ax, *args, **kwargs)
        plot_data_cmap(self, ax, *args, **kwargs)
        plot_mn(self, ax, mn, synidsecs=None, *args, **kwargs)
        format_ax(ax, **kwargs)                                                     -- static
        format_fig(fig, **kwargs)                                                   -- static
        get_colors(cmapname, ncolors, lthresh=0, hthresh=1, reverse=False)          -- static
        show(block=False)                                                           -- static
        tight_layout(pad=2)                                                         -- static
        savefig(file_path, **kwargs)                                                -- static
        renew_manager(**kwargs)                                                     -- static

    """

    _subplot_width = 3.5
    _subplot_height = 3.5
    _cmap = 'jet'

    def __init__(self, **kwargs):
        """Class constructor method.

        Inputs: variable keyword arguments.

        """
        pass

    def build_figure(self, nsubplots, ncol=None, projection=None, mask=None):
        """Build a matplotlib Figure with `nsubplots` subplots.

        Arguments:
            nsubplots   : int   : number of subplots to add in Figure.

        Keyword arguments:
            ncol        : int   : number of column of subplots.
            projection  : str   : identifier of projection type.

        Return:
            fig : matplotlib.Figure.figure instance         : a handle to the built figure.
            axs : list of  matplotlib.Axis.axis instances   : handles to the axes of the Figure, ordered from left to
                                                              right first, top to bottom then.

        By default, the subplots are arranged in the most square possible arrangement, but specifying ncol can alter
        this behavior.

        """

        if mask is None:
            mask = [True] * nsubplots

        if ncol is None:
            p = np.floor(np.sqrt(nsubplots))
            x = np.sqrt(nsubplots) - p
            if x == 0:
                ncol = int(p)
                nrow = int(p)
            elif (2 * p + x) * x <= p:
                ncol = int(p) + 1
                nrow = int(p)
            elif abs((2 * p + x) * x - p) < 1e-8:
                ncol = int(p) + 1
                nrow = int(p)
            else:
                ncol = int(p) + 1
                nrow = int(p) + 1
        else:
            nrow = int(nsubplots / ncol)
            if nsubplots % ncol != 0:
                nrow += 1

        # Build figure with multiple subplots using GridSpec.
        fig = plt.figure(figsize=(self._subplot_width * ncol, self._subplot_height * nrow))
        gs = gridspec.GridSpec(nrow, ncol)
        axs = []
        cnt = 0
        for irow in range(nrow):
            for icol in range(ncol):
                if cnt == nsubplots:
                    break
                if mask[cnt]:
                    axs.append(fig.add_subplot(gs[irow, icol], projection=projection))
                cnt += 1

        return fig, axs

    def plot_lines(self, ax, *args, **kwargs):
        """Plot 2D lines on the input ax."""

        assert(len(args) % 2 == 0)
        n = int(len(args) / 2)

        try: lws = kwargs['line_widths']
        except: lws = [1.0] * n
        try: colors = kwargs['colors']
        except: colors = self.get_colors(self._cmap, n)
        try: styles = kwargs['styles']
        except: styles = ['-'] * n
        try: labels = kwargs['labels']
        except: labels = ['line_{:d}'.format(i + 1) for i in range(n)]
            
        for i in range(n):
            x, y = args[2 * i], args[2 * i + 1]
            ax.plot(x, y, c=colors[i], ls=styles[i], lw=lws[i], label=labels[i])

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_3d_lines(self, ax, *args, **kwargs):
        """Plot 3D lines on the input ax."""

        assert(len(args) % 3 == 0)
        n = int(len(args) / 3)

        try: lws = kwargs['line_widths']
        except: lws = [1.0] * n
        try: colors = kwargs['colors']
        except: colors = self.get_colors(self._cmap, n)
        try: styles = kwargs['styles']
        except: styles = ['-'] * n
        try: labels = kwargs['labels']
        except: labels = ['line_{:d}'.format(i + 1) for i in range(n)]

        for i in range(n):
            x, y, z = args[3 * i], args[3 * i + 1], args[3 * i + 2]
            ax.plot(x, y, z, c=colors[i], ls=styles[i], lw=lws[i], label=labels[i])

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_bounded_lines(self, ax, *args, **kwargs):
        """Plot 2D bounded lines on the input ax."""

        assert(len(args) % 3 == 0)
        n = int(len(args) / 3)

        try: lws = kwargs['line_widths']
        except: lws = [1.0] * n
        try: colors = kwargs['colors']
        except: colors = self.get_colors(self._cmap, n)
        try: styles = kwargs['styles']
        except: styles = ['-'] * n
        try: labels = kwargs['labels']
        except: labels = ['line_{:d}'.format(i + 1) for i in range(n)]
        try: color_fills = kwargs['color_fills']
        except: color_fills = colors
        try: alpha_fills = kwargs['alpha_fills']
        except: alpha_fills = [0.5] * n

        for i in range(n):
            x, y, std = args[3 * i], args[3 * i + 1], args[3 * i + 2]
            ax.plot(x, y, c=colors[i], ls=styles[i], lw=lws[i], label=labels[i])
            ax.fill_between(x, y + std, y - std, color=color_fills[i], alpha=alpha_fills[i], lw=0)

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_bars_with_errors(self, ax, *args, **kwargs):
        """Plot bars with errors on the input ax."""

        assert(len(args) % 3 == 0)
        n = int(len(args) / 3)

        try: colors = kwargs['colors']
        except: colors = self.get_colors(self._cmap, n)
        try: alpha_fills = kwargs['alpha_fills']
        except: alpha_fills = [1] * n
        try: labels = kwargs['labels']
        except: labels = ['line_{:d}'.format(i + 1) for i in range(n)]
        try: cap_size = kwargs['cap_size']
        except: cap_size = 3.0
        try: cap_thick = kwargs['cap_thick']
        except: cap_thick = 0.5

        for i in range(n):
            x, y, std = args[3 * i], args[3 * i + 1], args[3 * i + 2]
            yerr = np.array([std, std]).reshape((2, 1))
            ax.bar(x, y, yerr=yerr, color=colors[i], label=labels[i], alpha=alpha_fills[i],
                   error_kw=dict(capsize=cap_size, capthick=cap_thick, barsabove=True))

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_dots(self, ax, *args, **kwargs):
        """Plot dots on the input ax."""

        assert(len(args) % 2 == 0)
        n = int(len(args) / 2)

        try: colors = kwargs['colors']
        except: colors = self.get_colors(self._cmap, n)
        try: alpha_fills = kwargs['alpha_fills']
        except: alpha_fills = [1.0] * n
        try: edge_colors = kwargs['edge_colors']
        except: edge_colors = ['k'] * n
        try: sizes = kwargs['sizes']
        except: sizes = 36.0
        try: lws = kwargs['line_widths']
        except: lws = [1.0] * n

        for i in range(n):
            x, y = args[2 * i], args[2 * i + 1]
            ax.scatter(x, y, s=sizes[i], c=colors[i], alpha=alpha_fills[i], linewidths=lws[i], edgecolors=edge_colors[i])

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_pie(self, ax, values, **kwargs):
        """Plot a pie chart on the input ax."""

        try: counterclock = kwargs['counterclock']
        except: counterclock = False
        try: labels = kwargs['labels']
        except: labels = values
        try: startangle = kwargs['startangle']
        except: startangle = 90
        try: axis = kwargs['axis']
        except: axis = 'equal'

        ax.pie(values, labels=labels, counterclock=counterclock, startangle=startangle)
        ax.axis(axis)

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_data_cmap(self, ax, mat2d, **kwargs):
        """Build a 2D color image representing the data given in input on the input ax.

        Arguments:
            mat2d   : 2D numpy.array    : the data to display.

        """

        try: cmap = kwargs['cmap']
        except: cmap = self._cmap
        try: extent = kwargs['extent']
        except: extent = None
        try: interp = kwargs['interp']
        except: interp = 'nearest'
        try: alpha = kwargs['alpha']
        except: alpha = 1.0
        try: aspect = kwargs['aspect']
        except: aspect = 'auto'

        # Plot matrix with colormap.
        ax.imshow(mat2d, cmap=cmap, extent=extent, interpolation=interp, alpha=alpha)

        # Set aspect ratio.
        ax.set_aspect(aspect)

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    def plot_mn(self, ax, mn, synidsecs=None, **kwargs):
        """Build a 3D plot displaying the input MN with its dendritic tree and synapses.

        Arguments:
            ax  : Axes3D handle.
            mn  : Motoneuron object.

        Keyword arguments:
            synidsecs   : list  : indexes of MN's dendritic sections where to draw a synapse.

        Description: TO BE COMPLETED.

        """

        # Derive coordinates and linewidths of each dendritic section.
        maxD = mn.dtree.get_largest_stem_diam()
        lws = []
        xs = []
        ys = []
        zs = []
        for dsec in mn.dtree.dsecs:
            lw = 2.0 * np.sqrt(dsec.get_D() / maxD)
            x = dsec.get_x3ds()
            y = dsec.get_y3ds()
            z = dsec.get_z3ds()
            if dsec.isastem:
                x = np.insert(x, 0, 0.0)
                y = np.insert(y, 0, 0.0)
                z = np.insert(z, 0, 0.0)
            lws.append(lw)
            xs.append(x)
            ys.append(y)
            zs.append(z)

        for (x, y, z, lw) in zip(xs, ys, zs, lws):
            ax.plot(x, y, z, lw=lw, c='k', ls='-')

        if synidsecs:
            # Derive coordinates of synapses.
            synxs = [mn.dtree.dsecs[idsec].get_x3ds().mean() for idsec in synidsecs]
            synys = [mn.dtree.dsecs[idsec].get_y3ds().mean() for idsec in synidsecs]
            synzs = [mn.dtree.dsecs[idsec].get_z3ds().mean() for idsec in synidsecs]
            ax.scatter(synxs, synys, synzs, c='b', s=10)

        ax.scatter(0.0, 0.0, 0.0, c='k', s=100)

        self.format_ax(ax, **kwargs)
        self.format_fig(ax.get_figure(), **kwargs)

        if 'show' not in kwargs or kwargs['show']:
            block = 'block' in kwargs and kwargs['block']
            plt.show(block=block)
            plt.pause(0.01)

    @staticmethod
    def format_ax(ax, **kwargs):
        """Format the input ax according to the parameters given in **kwargs."""

        # Set ax title.
        if 'ax_title' in kwargs:
            ax.set_title(kwargs['ax_title'])

        if 'ax_title_size' in kwargs:
            ax.set_title(kwargs['ax_title'], fontdict={'fontsize': kwargs['ax_title_size']})

        # Set axes ticks and ranges.
        if 'xmargin' in kwargs:
            ax.set_xmargin(kwargs['xmargin'])

        if 'ymargin' in kwargs:
            ax.set_ymargin(kwargs['ymargin'])

        if 'xlim' in kwargs:
            ax.set_xlim(kwargs['xlim'])

        if 'ylim' in kwargs:
            ax.set_ylim(kwargs['ylim'])

        if 'zlim' in kwargs:
            ax.set_zlim(kwargs['zlim'])

        if 'xlabel' in kwargs:
            ax.set_xlabel(kwargs['xlabel'])

        if 'ylabel' in kwargs:
            ax.set_ylabel(kwargs['ylabel'])

        if 'with_xtick' in kwargs:
            ax.tick_params(axis='x', bottom=kwargs['with_xtick'], labelbottom=kwargs['with_xtick'])

        if 'with_ytick' in kwargs:
            ax.tick_params(axis='y', left=kwargs['with_ytick'], labelleft=kwargs['with_ytick'])

        if 'n_xtick' in kwargs:
            xmin, xmax = ax.get_xlim()
            ax.set_xticks(np.round(np.linspace(xmin, xmax, kwargs['n_xtick']), 2))

        if 'n_ytick' in kwargs:
            ymin, ymax = ax.get_ylim()
            ax.set_yticks(np.round(np.linspace(ymin, ymax, kwargs['n_ytick']), 2))

        if 'xticks' in kwargs:
            ax.set_xticks(kwargs['xticks'])

        if 'yticks' in kwargs:
            ax.set_yticks(kwargs['yticks'])

        if 'with_xaxis' in kwargs:
            ax.spines['bottom'].set_visible(kwargs['with_xaxis'])

        if 'with_yaxis' in kwargs:
            ax.spines['left'].set_visible(kwargs['with_yaxis'])

        if 'aspect' in kwargs:
            if kwargs['aspect'] == 'equal3d':
                xlim, ylim, zlim = Plotter.get_equal_3d_lims(ax)
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                ax.set_zlim(zlim)

        # Set ax legend.
        if 'with_ax_legend' in kwargs and kwargs['with_ax_legend']:
            lgd = ax.legend()
            # if 'ax_legend_loc' in kwargs: ax.legend(loc=kwargs['ax_legend_loc'])
            # if 'ax_legend_bbox_to_anchor' in kwargs: ax.legend(bbox_to_anchor=kwargs['ax_legend_bbox_to_anchor'])
            # if 'ax_legend_ncol' in kwargs: ax.legend(ncol=kwargs['ax_legend_ncol'])
            if 'ax_legend_loc' in kwargs:
                lgd = ax.legend(loc=kwargs['ax_legend_loc'])
            if 'ax_legend_bbox_to_anchor' in kwargs:
                # lgd.update(prop={'bbox_to_anchor': kwargs['ax_legend_bbox_to_anchor']})
                # lgd = ax.legend(bbox_to_anchor=kwargs['ax_legend_bbox_to_anchor'])
                lgd.set_bbox_to_anchor(kwargs['ax_legend_bbox_to_anchor'])
            if 'ax_legend_ncol' in kwargs:
                lgd.update(prop={'ncol': kwargs['ax_legend_ncol']})

    @staticmethod
    def format_fig(fig, **kwargs):
        """Format the input fig according to the parameters given in **kwargs."""

        if 'with_fig_legend' in kwargs and kwargs['with_fig_legend']:
            lgd = fig.legend(kwargs['fig_legend_lines'], kwargs['fig_legend_labels'])
            if 'fig_legend_loc' in kwargs:
                lgd.update(prop={'loc': kwargs['fig_legend_loc']})
            if 'fig_legend_bbox_to_anchor' in kwargs:
                lgd.update(prop={'bbox_to_anchor': kwargs['fig_legend_bbox_to_anchor']})
            if 'fig_legend_ncol' in kwargs:
                lgd.update(prop={'ncol': kwargs['fig_legend_ncol']})

        if 'tight_layout' in kwargs and kwargs['tight_layout']:
            if 'pad' in kwargs:
                plt.tight_layout(pad=kwargs['pad'])
            else:
                plt.tight_layout()

    @staticmethod
    def get_colors(cmapname, ncolors, lthresh=0, hthresh=1, reverse=False):
        """Return a list of color tuples.

        Arguments:
            cmapname    : str   : name of a matplotlib colormap or color indicator from which colormap is derived.
            ncolors     : int   : number of colors to return.
            lthresh     : float : in the range [0,1]. Lower bound to pick colors from colormap.
            hthresh     : float : in the range [0,1]. Higher bound to pick colors from colormap.
            reverse     : bool  : indicate whether the returned list of colors should be reversed.

        Return:
            colors  : list of tuples    : ncolors color tuples.

        """

        try:
            colors = [plt.get_cmap(cmapname)(x) for x in np.linspace(lthresh, hthresh, ncolors)]

        except:
            if cmapname == 'reds':
                colors = [plt.get_cmap('Reds')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark reds', 'darkreds']:
                colors = [plt.get_cmap('Reds')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light reds', 'lightreds']:
                colors = [plt.get_cmap('Reds')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'reds2':
                colors = [plt.get_cmap('YlOrRd')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark reds2', 'darkreds2']:
                colors = [plt.get_cmap('YlOrRd')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light reds2', 'lightreds2']:
                colors = [plt.get_cmap('YlOrRd')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'reds3':
                colors = [plt.get_cmap('OrRd')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark reds3', 'darkreds3']:
                colors = [plt.get_cmap('OrRd')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light reds3', 'lightreds3']:
                colors = [plt.get_cmap('OrRd')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'blues':
                colors = [plt.get_cmap('Blues')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark blues', 'darkblues']:
                colors = [plt.get_cmap('Blues')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light blues', 'lightblues']:
                colors = [plt.get_cmap('Blues')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'blues2':
                colors = [plt.get_cmap('PuBu')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark blues2', 'darkblues2']:
                colors = [plt.get_cmap('PuBu')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light blues2', 'lightblues2']:
                colors = [plt.get_cmap('PuBu')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'greens':
                colors = [plt.get_cmap('Greens')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark greens', 'darkgreens']:
                colors = [plt.get_cmap('Greens')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light greens', 'lightgreens']:
                colors = [plt.get_cmap('Greens')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'greens2':
                colors = [plt.get_cmap('YlGn')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark greens2', 'darkgreens2']:
                colors = [plt.get_cmap('YlGn')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light greens2', 'lightgreens2']:
                colors = [plt.get_cmap('YlGn')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'oranges':
                colors = [plt.get_cmap('Oranges')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark oranges', 'darkoranges']:
                colors = [plt.get_cmap('Oranges')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light oranges', 'lightoranges']:
                colors = [plt.get_cmap('Oranges')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'oranges2':
                colors = [plt.get_cmap('YlOrBr')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark oranges2', 'darkoranges2']:
                colors = [plt.get_cmap('YlOrBr')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light oranges2', 'lightoranges2']:
                colors = [plt.get_cmap('YlOrBr')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'purples':
                colors = [plt.get_cmap('Purples')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark purples', 'darkpurples']:
                colors = [plt.get_cmap('Purples')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light purples', 'lightpurples']:
                colors = [plt.get_cmap('Purples')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'purples2':
                colors = [plt.get_cmap('BuPu')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark purples2', 'darkpurples2']:
                colors = [plt.get_cmap('BuPu')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light purples2', 'lightpurples2']:
                colors = [plt.get_cmap('BuPu')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'magentas':
                colors = [plt.get_cmap('PuRd')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark magentas', 'darkmagentas']:
                colors = [plt.get_cmap('PuRd')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light magentas', 'lightmagentas']:
                colors = [plt.get_cmap('PuRd')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'magentas2':
                colors = [plt.get_cmap('RdPu')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark magentas2', 'darkmagentas2']:
                colors = [plt.get_cmap('RdPu')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light magentas2', 'lightmagentas2']:
                colors = [plt.get_cmap('RdPu')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'bluegreens':
                colors = [plt.get_cmap('GnBu')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark bluegreens', 'darkbluegreens']:
                colors = [plt.get_cmap('GnBu')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light bluegreens', 'lightbluegreens']:
                colors = [plt.get_cmap('GnBu')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'bluegreens2':
                colors = [plt.get_cmap('YlGnBu')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark bluegreens2', 'darkbluegreens2']:
                colors = [plt.get_cmap('YlGnBu')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light bluegreens2', 'lightbluegreens2']:
                colors = [plt.get_cmap('YlGnBu')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'greenblues':
                colors = [plt.get_cmap('PuBuGn')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark greenblues', 'darkgreenblues']:
                colors = [plt.get_cmap('PuBuGn')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light greenblues', 'lightgreenblues']:
                colors = [plt.get_cmap('PuBuGn')(x) for x in np.linspace(0, 0.5, ncolors)]
            elif cmapname == 'greenblues2':
                colors = [plt.get_cmap('BuGn')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark greenblues2', 'darkgreenblues2']:
                colors = [plt.get_cmap('BuGn')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light greenblues2', 'lightgreenblues2']:
                colors = [plt.get_cmap('BuGn')(x) for x in np.linspace(0, 0.5, ncolors)]

            elif cmapname == 'greys':
                colors = [plt.get_cmap('Greys')(x) for x in np.linspace(lthresh, hthresh, ncolors)]
            elif cmapname in ['dark greys', 'darkgreys']:
                colors = [plt.get_cmap('Greys')(x) for x in np.linspace(0.5, 1, ncolors)]
            elif cmapname in ['light greys', 'lightgreys']:
                colors = [plt.get_cmap('Greys')(x) for x in np.linspace(0, 0.5, ncolors)]

            else:
                raise ValueError

        if reverse:
            colors.reverse()

        return colors

    @staticmethod
    def get_equal_3d_lims(ax):
        """Return xlim, ylim and zlim appropriate to make the input ax look with an equal aspect."""
        xmid = 0.5 * max(line._verts3d[0].max() for line in ax.lines) + 0.5 * min(line._verts3d[0].min() for line in ax.lines)
        max_xrange = 0.5 * max(line._verts3d[0].max() for line in ax.lines) - 0.5 * min(line._verts3d[0].min() for line in ax.lines)
        ymid = 0.5 * max(line._verts3d[1].max() for line in ax.lines) + 0.5 * min(line._verts3d[1].min() for line in ax.lines)
        max_yrange = 0.5 * max(line._verts3d[1].max() for line in ax.lines) - 0.5 * min(line._verts3d[1].min() for line in ax.lines)
        zmid = 0.5 * max(line._verts3d[2].max() for line in ax.lines) + 0.5 * min(line._verts3d[2].min() for line in ax.lines)
        max_zrange = 0.5 * max(line._verts3d[2].max() for line in ax.lines) - 0.5 * min(line._verts3d[2].min() for line in ax.lines)
        max_range = max(max_xrange, max_yrange, max_zrange)
        return [xmid - max_range, xmid + max_range], [ymid - max_range, ymid + max_range], [zmid - max_range, zmid + max_range]

    @staticmethod
    def show(block=False):
        """Call matplotlib.pyplot.show."""
        plt.show(block=block)
        plt.pause(0.001)

    @staticmethod
    def pause(dur=0.001):
        """Call matplotlib.pyplot.pause."""
        plt.pause(dur)

    @staticmethod
    def tight_layout(pad=2):
        """Call matplotlib.pyplot.tight_layout."""
        plt.tight_layout(pad=pad)

    @staticmethod
    def savefig(file_path, **kwargs):
        """Call matplotlib.pyplot.savefig."""
        plt.savefig(file_path, **kwargs)

    @staticmethod
    def close(fig):
        """Call matplotlib.pyplot.close."""
        plt.close(fig)

    @staticmethod
    def renew_manager(fig):
        """Renew the window manager of the input figure."""
        if plt.fignum_exists(fig.number):
            return
        else:
            dummy = plt.figure()
            new_manager = dummy.canvas.manager
            new_manager.canvas.figure = fig
            fig.set_canvas(new_manager.canvas)


###########################################################################
