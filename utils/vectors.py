# -*- coding: utf-8 -*


#############################################################################
# Module vectors.
#
# Created on: 4 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################

from neuron import h


def build_t_vector():
    """Return a HOC vector recording the time steps during a NEURON simulation."""
    tvec = h.Vector()
    tvec.record(h._ref_t)
    return tvec


def build_v_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's membrane potential during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    """
    vvec = h.Vector()
    vvec.record(hsec(loc)._ref_v)
    return vvec


def build_vext0_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's vext[0] during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'extracellular' mechanism.

    """
    vext0vec = h.Vector()
    vext0vec.record(hsec(loc)._ref_vext[0])
    return vext0vec


def build_vext1_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's vext[1] during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'extracellular' mechanism.

    """
    vext1vec = h.Vector()
    vext1vec.record(hsec(loc)._ref_vext[1])
    return vext1vec


def build_eext_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's eext during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'extracellular' mechanism.

    """
    eextvec = h.Vector()
    eextvec.record(hsec(loc)._ref_e_extracellular)
    return eextvec


def build_ina_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's sodium current during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'axnode' mechanism.

    """
    inavec = h.Vector()
    inavec.record(hsec(loc)._ref_ina_axnode)
    return inavec


def build_ik_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's potassium current during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'axnode' mechanism.

    """
    ikvec = h.Vector()
    ikvec.record(hsec(loc)._ref_ik_axnode)
    return ikvec


def build_ipas_vector(hsec, loc=0.5):
    """Return a HOC vector recording the input HOC section's passive current during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.

    Only valid if hsec possesses the 'pas' mechanism.

    """
    ipasvec = h.Vector()
    ipasvec.record(hsec(loc)._ref_i_pas)
    return ipasvec


def build_spike_times_vector(hsec, loc=0.5, threshold=0.0):
    """Return a HOC vector recording the spike times of the input HOC section during a NEURON simulation.

    Arguments:
        hsec    : h.Section object  : the section to record.

    Keyword arguments:
        loc : float in [0, 1]   : position along the section where to record.
        threshold   : float : potential level to cross to discriminate an action potential.

    """
    nc = h.NetCon(hsec(loc)._ref_v, None, sec=hsec)
    nc.threshold = threshold
    spikeTimes = h.Vector()
    nc.record(spikeTimes)
    return spikeTimes
