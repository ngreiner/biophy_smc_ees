

# -*- coding: utf-8 -*


#############################################################################
# Module stats.py.
#
# Created on: 12 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np


def ttest(mu1, sd1, n1, mu2, sd2, n2):
    """?

    Arguments:
        mu1 : float   : estimated mean of first normal distribution.
        sd1 : float   : estimated standard deviation of first normal distribution.
        n1  : float   : number of samples from which mu1 and sd1 were evaluated.
        mu2 : float   : estimated mean of second normal distribution.
        sd2 : float   : estimated standard deviation of second normal distribution.
        n2  : float   : number of samples from which mu2 and sd2 were evaluated.

    Return:
        p   : float : ?.

    """
    return (mu1 - mu2) / np.sqrt(sd1 ** 2 / (n1 - 1) + sd2 ** 2 / (n2 - 1))


def bhattacharyya_distance(m1, s1, m2, s2):
    return 0.25 * (m1 - m2) ** 2 / (s1 + s2) + 0.5 * np.log(0.5 * (s1 + s2) / np.sqrt(s1 * s2))


def residuals(mu1, sd1, mu2, sd2, x=1):
    mu = mu1 - x * mu2
    sd = np.sqrt(sd1 ** 2 + x ** 2 * sd2 ** 2)
    rv = norm(mu, sd)
    return rv.cdf(0)