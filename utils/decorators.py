import time
import traceback
from functools import wraps


def print_exec(func):
    """Decorate function to print starting and completion messages in console."""
    def func_timed(*args, **kw):
        print('BEGIN: {!r} ...'.format(func.__name__))
        ts = time.time()
        result = func(*args, **kw)
        te = time.time()
        print('END: {!r}.  (Execution time: {:.3f} s)'.format(func.__name__, te - ts))
        return result
    return func_timed


def control_exec(func):
    """Decorate function to print exception information and stack trace."""
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            traceback.print_exc()
            raise
    return func_wrapper
