# -*- coding: utf-8 -*


#############################################################################
# Module maths.py.
#
# Created on: 4 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import numpy as np


def gcd(*args):
    """Return the Greatest Common Divisor of the input arguments.

    Usage:
        gcd(x1, x2, x3, ...)
        gcd(L)

    Arguments:
        *args   : variable length argument list :
            x1,2,3,...  : int-convertible objects.
            L           : list of int-convertible objects.

    Return:
        gcd : float : greatest common divisor of the input arguments.

    """

    if len(args) == 1:
        x = args[0]
        if isinstance(x, int): return float(abs(x))
        elif isinstance(x, float): return abs(x)
        elif isinstance(x, list): return gcd(*x)
        else: return gcd(*list(x))

    elif len(args) == 2:
        try: a = float(gcd(args[0]))
        except: a = 0.0
        try: b = float(gcd(args[1]))
        except: b = 0.0
        while b:
            a, b = b, a % b
        return float(a)

    else:
        return gcd(gcd(args[0]), gcd(args[1 : -1]))


def get_uniform_subordinate(xs):
    """Return a regularly-spaced subdivision subordinated to the input subdivisions.

    Usage:
        sx = get_subordinate(xs)

    Arguments:
        xs  : list  of 1D numpy.arrays  : each element is a subdivision.

    Return:
        sx  : 1D numpy.array    : the subordinated subdivision.

    """
    if not isinstance(xs, list):
        xs = list(xs)
    xmin = np.min([np.min(x) for x in xs])
    xmax = np.max([np.max(x) for x in xs])
    step = gcd(xs)
    nstep = (xmax - xmin) / step
    sx = np.linspace(xmin, xmax, nstep + 1)
    return sx


def get_subordinate(xs):
    """Return a regularly-spaced subdivision subordinated to the input subdivisions.

    Usage:
        sx = get_subordinate(xs)

    Arguments:
        xs  : list  of 1D numpy.arrays  : each element is a subdivision.

    Return:
        sx  : 1D numpy.array    : the subordinated subdivision.

    """
    sx = np.unique(np.concatenate(xs))
    return sx


def refine_subdivisions(xs):
    """Return numpy.arrays representing refined subdivisions of those represented by the input numpy.arrays.

    Usage:
        rxs = refine_subdivisions(xs)

    Arguments:
        xs : list  of 1D numpy.arrays : each numpy.array is a subdivision.

    Return:
        rxs : list of 1D numpy.array : the refined subdivisions.

    Description:
        The refinement is such that each returned numpy.array is the image of any other by an affine transformation.

    """
    coeffs = list()
    tmp = list()
    for x in xs:
        rx, a, b = scale_shift_to_0_1(x)
        tmp.append(rx)
        coeffs.append((a, b))
    sx = get_subordinate(tmp)
    rxs = [sx / a - b for (a, b) in coeffs]
    return rxs


def get_common(xs):
    """Return the common elements among multiple arrays.

    Usage:
        cx = get_common(xs)

    Arguments:
        xs  : list  of 1D numpy.arrays.

    Return:
        cx  : 1D numpy.array    : common elements of input arrays.

    """
    if not isinstance(xs, list):
        xs = list(xs)
    cx = list(xs.pop(0))
    cxbis = list(cx)
    for el in cxbis:
        for x in xs:
            if el not in x:
                cx.remove(el)
                break
    cx = np.array(cx)
    return cx


def resample(ys, xs=None):
    """Resample input arrays.

    Arguments:
        ys  : list  of 1D numpy.arrays  : ordinates.
        xs  : list  of 1D numpy.arrays  : abscissae.

    Return:
        rys : list of 1D numpy.arrays   : resampled ordinate arrays.
        sx  : 1D numpy.array            : regularly-spaced abscissae subordinated to every input abscissa array.

    Description:
        The arrays in `xs` and `ys` constitute abscissa-ordinates vector pairs.
        The resampling of the arrays in `ys` is performed over a regular subdivision subordinated to every array in
        `xs`.

    """

    if xs is None:
        xs = [np.linspace(0.0, 1.0, num=y.size) for y in ys]
        sx = get_subordinate(xs)
        rxs = [sx.copy() for _ in xs]
    else:
        rxs = refine_subdivisions(xs)

    rys = list()
    for x, y, rx in zip(xs, ys, rxs):
        ry = np.zeros(rx.size)
        for i, xi in enumerate(rx):
            if xi < x[0]:
                dy = y[0] - y[1]
                dx = x[0] - x[1]
                slope = dy / dx
                yi = y[0] + slope * (xi - x[0])
                ry[i] = yi
            elif xi > x[-1]:
                dy = y[-1] - y[-2]
                dx = x[-1] - x[-2]
                slope = dy / dx
                yi = y[-1] + slope * (xi - x[-1])
                ry[i] = yi
            else:
                try:
                    ih = np.where(x >= xi)[0][0]
                    il = np.where(x <= xi)[0][-1]
                    dy = y[ih] - y[il]
                    dx = x[ih] - x[il]
                    slope = dy / dx if dx != 0. else 0.
                    yi = y[il] + slope * (xi - x[il])
                    ry[i] = yi
                except:
                    debug = True
        rys.append(ry)

    return rys, rxs


def rectify_0_100(ys):
    """Rectify the input arrays to the range [0.0, 100.0].

    Usage:
        rys = rectify_0_100(ys)

    Arguments:
        ys  : 1D numpy.array/list of 1D numpy.arrays    : arrays to rectify.

    Return:
        rys : 1D numpy.array/list of 1D numpy.arrays    : rectified arrays.

    """

    if not isinstance(ys, list): ys = list(ys)

    rys = list()
    for y in ys:
        y[np.where(y > 100.0)] = 100.0
        y[np.where(y < 0.0)] = 0.0
        rys.append(y)

    if not isinstance(ys, list): rys = rys.pop(0)

    return rys


def scale_shift_to_0_1(x):
    """Scale and shift the input numpy.array to the interval [0, 1].

    Usage:
        rx, a, b = scale_shift_to_0_1(x)

    Arguments:
        x : 1D numpy.array : should be sorted in increasing order.

    Return:
        rx : 1D numpy.array : scaled-shifted array.
        a : float : scaling coefficient.
        b : float : shifting coefficient.

    Description:
        rx contains the image of the elements of x by the affine transformation mapping the smallest element of x to
        0.0 and the largest to 1.0.
        rx = a * x + b, with the smallest and largest elements of rx being 0.0 and 1.0.

    """
    b = - x[0]
    a = 1.0 / (x[-1] + b)
    rx = a * (x + b)
    return rx, a, b


def invert_permutation(x):
    """Invert the permutation x.

    Usage:
        inv_x = invert_permutation(x)

    Arguments:
        x : 1D numpy.array : a permutation of [1, len(x)].

    Return:
        inv_x : 1D numpy.array : inverse permutation of x.

    Description:
        inv_x is such that inv_x[x[i]] == i for all i in [1, len(x)].

    """
    inv_x = np.zeros_like(x)
    for i in range(len(x)):
        inv_x[x[i] - 1] = i + 1
    return inv_x


def compose_permutations(x1, x2):
    """Compose the permutations x1 and x2.

    Usage:
        x3 = compose_permutations(x1, x2)

    Arguments:
        x1 : 1D numpy.array : a permutation of [1, len(x1)].
        x2 : 1D numpy.array : a permutation of [1, len(x1)].

    Return:
        x3 : 1D numpy.array : composed permutation x1 o x2.

    Description:
        x3[i] == x1[x2[i]] for all i in [1, len(x1)].
        x1 and x2 must be of the same size.

    """
    x3 = np.zeros_like(x1)
    for i in range(len(x1)):
        x3[i] = x1[x2[i] - 1]
    return x3


def find_inversions(x):
    """Find the inversions of the permutation x.

    Usage:
        invs = find_inversions(x)

    Arguments:
        x : 1D numpy.array : a permutation of [1, len(x)].

    Return:
        invs : list of tuples : inversions of x.

    Description:
        An inversion of a permutation x is a couple (i, j) with i < j and x[i] > x[j].

    """
    invs = list()
    for i in range(len(x)):
        for j in range(i + 1, len(x)):
            if x[i] > x[j]:
                invs.append((i + 1, j + 1))
    return invs


def get_inversion_number(x):
    """Get the inversion number of the permutation x.

    Usage:
        n_inv = get_inversion_number(x)

    Arguments:
        x : 1D numpy.array : a permutation of [1, len(x)].

    Return:
        n_inv : int : inversion number of x (the number of inversions of x).

    Description:
        The inversion number of a permutation x is the cardinality of its inversion set (the set of all the inversions
        of x).

    """
    return len(find_inversions(x))


def running_mean(x, window=3):
    """Compute the running mean of the 1D array x.

    Usage:
        rx = running_mean(x)
        rx = running_mean(x, window=window)

    Arguments:
        x : 1D numpy.array : data sequence.
        window : int : size of window.

    Return:
        rx : 1D numpy.array : the moving mean of x.

    Note:
        Window sizes different than 3 are not yet supported.

    """
    x1 = np.roll(x, 1)
    x2 = np.roll(x, -1)
    rx = (x + x1 + x2) / 3.0
    rx[0] = (x[0] + x[1]) / 2.0
    rx[-1] = (x[-2] + x[-1]) / 2.0
    return rx
