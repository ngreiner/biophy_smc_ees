# -*- coding: utf-8 -*


##############################################################################################################
# Class FileLock.
#
# Created on: 27 September 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
# Credits: this module is largely inspired by the module 'lazyflow/utility/fileLock.py'
# (https://github.com/ilastik/lazyflow/blob/master/lazyflow/utility/fileLock.py).
#
##############################################################################################################


import os


class FileLock:
    """
    Naive context manager to lock file access during the execution of code blocks encapsulated in with statements.

    Instance variables defined here:
        acquired    : bool  : indicate whether the lock is indeed acquired by the FileLock or not.
        lock_file   : str   : path to lock file.

    Methods defined here:
        __init__(self, file_path)
        __enter__(self)
        __exit__(self, exc_type, exc_value, traceback)
        available(self)
        acquire(self)
        release(self)

    """

    def __init__(self, file_path):
        """Class-constructor method.

        Arguments:
            file_path   : str   : path to file to be locked.

        """
        self.acquired = False
        self.lock_file = file_path + '.lock'

    def __enter__(self):
        """Acquire lock on the desired file when the FileLock is used in the with statement."""
        self.acquire()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Release lock when the end of the with statement is reached."""
        self.release()

    def available(self):
        """Returns True if and only if the file is currently available to be locked."""
        return not os.path.exists(self.lock_file)

    def acquire(self):
        """Acquire lock on the desired file, if possible."""
        if self.available():
            self.acquired = True
            open(self.lock_file, 'a').close()

    def release(self):
        """Release lock by deleting the lock file, if lock is held by the FileLock."""
        if self.acquired:
            self.acquired = False
            os.unlink(self.lock_file)
