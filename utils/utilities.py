# -*- coding: utf-8 -*


#############################################################################
# Module utilities.py.
#
# Created on: 4 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


def gen_seed(*args):
    """Return an integer intended to be used as a seed for a pseudo-random number generator.

    Optional arguments:
        args : str or int.

    Return:
        seed : int.

    Description:
    Each str argument is converted to the sum of the integers coding each of its characters.
    The returned integer is the product of every int argument and str argument converted as described above.

    """
    seed = 1
    for arg in args:
        if isinstance(arg, int):
            seed *= 1
        elif isinstance(arg, str):
            seed *= sum(ord(c) for c in arg)
        else:
            raise TypeError('Input arguments should be str or int.')
    return seed


def prompt_confirm(msg):
    """Return a boolean describing the answer of the user to the yes/no question contained in `msg`.

    Arguments:
        msg : str   : question prompted to user.

    Return:
        outbool : bool  : True if user replies positively, False otherwise.

    """
    while True:
        ans = raw_input(msg)
        if ans in ['y', 'Y', 'yes', 'Yes', 'YES']:
            return True
        elif ans in ['n', 'N', 'no', 'No', 'NO']:
            return False
        else:
            pass


def combi_2_str(combi):
    """Return a string encrypting the input combi.

    Arguments:
        combi   : int/str/list/dict : stimulation combination descriptor.

    Return:
        scombi  : str   : encrypted combi.

    The input combi is either an int --giving the index of an electrode--, a str --giving the full name of an
    electrode, as in 'elec12'--, a list --where each element is the activation level of the corresponding electrode,
    ie combi[i] is the activation level of elec #(i+1)-- or a dict --where each item is made of a str-typed key and
    a value of type int or float.

    The encryption into a str goes as follows:
        1. if combi is a str, scombi is the same str
        2. if combi is an int, scombi is the str 'elec{:d}'.format(combi)
        3. if combi is a list, scombi is the concatenation of terms of the form 'elec{1}:{2}{3}' separated by '/',
           where {1} is one more than a list index i, {2} is either 'p' or 'n', and {3} is the formatted value combi[i],
           with three decimals and as many figures as necessary before the decimal point. 'p' and 'n' in {2} denotes
           'positive' and 'negative' respectively, and are chosen according to combi[i]. If combi[i] is null, the term
           'elec{1}:{2}{3}' for that index i is discarded altogether.
        4. if combi is a dict, scombi is the concatenation of terms of the form '{1}:{2}{3}' separated by '/', where {1}
           is a key of the dict, {2} is either 'p' or 'n', and {3} is the formatted value associated to the key, with
           three decimals and as many figures as necessary before the decimal point. 'p' and 'n' in {2} denotes
           'positive' and 'negative' respectively, and are chosen according to the value associated to the key.
           If it is null, the term '{1}:{2}{3}' for that key is discarded altogether.

    """
    if isinstance(combi, str):
        return combi

    elif isinstance(combi, int):
        return 'elec{:d}'.format(combi)

    elif isinstance(combi, list):
        tmp = list()
        for i, w in enumerate(combi):
            if w == 0.0: continue
            elif w > 0.0: s = 'p'
            else: s = 'n'
            word = 'elec{:d}:{:}{:.3f}'.format(i + 1, s, abs(w))
            tmp.append(word)
        if not tmp:
            raise ValueError('combi is the null vector.')
        return '/'.join(tmp)

    else:
        assert(isinstance(combi, dict))
        tmp = list()
        for e, w in combi.items():
            if w > 0.0: s = 'p'
            else: s = 'n'
            word = '{:}:{:}{:.3f}'.format(e, s, abs(w))
            tmp.append(word)
        if not tmp:
            raise ValueError('combi is the null vector.')
        return '/'.join(tmp)


def str_2_combi(scombi):
    """Return the combi encrypted by the input str.

    Arguments:
        scombi  : str   : encrypted stimulation combination descriptor.

    Return:
        combi   : str/dict  : decrypted stimulation combination descriptor.

    See function 'combi_2_str' for details on the encryption process to understand the decryption process.
    The latter only ever produce str or dict objects, even when alternative representations of stimulation
    combinations as int or list objects are possible.

    """

    words = scombi.split('/')

    if len(words) == 1:
        return scombi

    else:
        combi = dict()
        for word in words:
            parts = word.split(':')
            if parts[1][0] == 'p': s = 1.0
            else: s = -1.0
            combi[parts[0]] = s * float(parts[1][1:])
        return combi
