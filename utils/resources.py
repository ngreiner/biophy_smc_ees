# -*- coding: utf-8 -*


#############################################################################
# Module resources.py.
#
# Created on: 26 February 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################

import os
import biophy_smc_ees


swc_folder = os.path.join(biophy_smc_ees.__path__[0], 'resources', 'swc_files')
swc_files = [os.path.join(swc_folder, 'v_e_moto1.CNG.swc'),
             os.path.join(swc_folder, 'v_e_moto2.CNG.swc'),
             os.path.join(swc_folder, 'v_e_moto3.CNG.swc'),
             os.path.join(swc_folder, 'v_e_moto4.CNG.swc'),
             os.path.join(swc_folder, 'v_e_moto5.CNG.swc'),
             os.path.join(swc_folder, 'v_e_moto6.CNG.swc')]
