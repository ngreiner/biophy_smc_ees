BIOPHY_SMC_EES
==============

Author: _Nathan GREINER_, PhD  
email: <mailto:nathan.greiner@m4x.org>

ABSTRACT
--------
This package provides modules allowing to run neurophysical simulations and estimate the effects of epidural electrical stimulation on various spinal neural entities.

It was originally developped to be used in conjunction with the following package: <https://bitbucket.org/ngreiner/fem_smc_ees/src/master/>

The latter can be used to generate sensorimotor circuit datasets. These describe the geometry, morphology and connectivity of various neural entities. They also specify the electric potential distributions resulting from epidural electrical stimulation along the trajectories of these neural entities.

MINIMAL REQUIREMENTS
--------------------
This package is compatible with Python 3.7.x distributions.  
It requires the following packages to be installed:

- numpy
- scipy
- pandas
- matplotlib

These can be installed by running the following commands in a terminal :

	cd <biophy_smc_ees>
	pip install -r requirements.txt

where _<biophy_smc_ees>_ is the path to the **biophy_smc_ees** package on your system.

NEURON PACKAGE
--------------
This package is essentially a wrapper around the _neuron_ simulator (Hines et al., 2009). The _neuron_ simulator thus needs to be importable as a Python package (<https://neuron.yale.edu/neuron/docs>).  
When imported, the _neuron_ package loads the compiled .mod files found in the working directory.  
The neural entities that can be simulated with the present package make use of specific .mod files. These can be found in _<biophy_smc_ees>/resources/mod_files/_.  
They should be compiled in the directory where the Python scripts will be executed.  
On Unix-based platforms, this can be done by running the following commands in a terminal:

	cd /path/to/execution/directory/
	cp -r <biophy_smc_ees>/resources/mod_files/ .
	nrnivmodl mod_files/

REFERENCES
----------
This package builds up on, and was inspired by, the following works:

- _Central terminations of muscle afferents on motoneurones in the cat spinal cord_, Iles, J. F., The Journal of Physiology, 1976.
- _The morphology of group Ia afferent fibre collaterals in the spinal cord of the cat_, Brown, A. G. and Fyffe, R. E., The Journal of Physiology, 1978.
- _Relations between cell body size, axon diameter and axon conduction velocity of cat sciatic alpha-motoneurons stained with horseradish peroxidase_, Cullheim, S., Neuroscience letters, 1978.
- _The synaptic current evoked in cat spinal motoneurones by impulses in single group 1a axons_, Finkel, A. S. and Redman, S. J., The Journal of Physiology, 1983.
- _Membrane area and dendritic structure in type-identified triceps surae alpha motoneurons_, Cullheim, S. et al., The Journal of Comparative Neurology, 1987.
- _Computer simulation of group Ia EPSPs using morphologically realistic models of cat alpha-motoneurons_, Segev, I. et al., Journal of Neurophysiology, 1990.
- _Recruitment of dorsal column fibers in spinal cord stimulation: influence of collateral branching_, Struijk, J. J. et al., IEEE Transactions on Biomedical Engineering, 1992.
- _Horseradish peroxidase study of the spatial and electrotonic distribution of group Ia synapses on type-identified ankle extensor motoneurons in the cat_, Burke, R. E. and Glenn, L. L., The Journal of Comparative Neurology, 1996.
- _The NEURON Simulation Environment_, Hines, M. L. and Carnevale, N. T., Neural Computation, 1997.
- _The basic mechanism for the electrical stimulation of the nervous system_, Rattay, F., Neuroscience, 1999.
- _Modelling the effects of electric fields on nerve fibres: Influence of the myelin sheath_, Richardson, A. G. et al., Medical & Biological Engineering & Computing, 2000.
- _Extracellular Stimulation of Central Neurons: Influence of Stimulus Waveform and Frequency on Neuronal Output_, McIntyre, C. C. and Grill, W. M., Journal of Neurophysiology, 2002.
- _Modeling the excitability of mammalian nerve fibers: influence of afterpotentials on the recovery cycle_, McIntyre, C. C. et al., Journal of Neurophysiology, 2002.
- _Cellular effects of deep brain stimulation: model-based analysis of activation and inhibition_, McIntyre, C. C. et al., Journal of Physiology, 2004.
- _NEURON and Python_, Hines, M. L., Davison A. P., and Muller E., Frontiers in neuroinformatics, Volume 3, 2009.