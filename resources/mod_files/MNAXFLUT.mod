TITLE Motor Axon FLUT compartments channels

: 5 June 2018
: Nathan GREINER
:
: Fast K+ currents ensuring small-diameter MRG fibers stability.
:
: This model is described in detail in:
:
: McIntyre CC, Grill WM, Sherman DL and Thakor NV.
: Cellular Effects of Deep Brain Stimulation:
: Model-Based Analysis of Activation and Inhibition.
: Journal of Neurophysiology: 91: 1457-1469, 2004.
:
: Note:
: Same as AXFLUT.mod but for a resting potential of -70 mV.

INDEPENDENT {t FROM 0 TO 1 WITH 1 (ms)}

NEURON {
	SUFFIX mnaxflut
	NONSPECIFIC_CURRENT ik
	NONSPECIFIC_CURRENT il
	RANGE gkbar, gl, ek, el
	RANGE n_inf
	RANGE tau_n
}


UNITS {
	(mA) = (milliamp)
	(mV) = (millivolt)
}

PARAMETER {
	gkbar   = 0.08 	(mho/cm2)
	gl		= 0.007 (mho/cm2)
	ek      = -80.0 (mV)
	el		= -70.0 (mV)
	celsius			(degC)
	dt              (ms)
	v               (mV)
	a_A = 0.0462
	a_B = 83.2
	a_C = 1.1
	b_A = 0.0824
	b_B = 66
	b_C = 10.5
}

STATE {
	n
}

ASSIGNED {
	ik      (mA/cm2)
	il      (mA/cm2)
	n_inf
	tau_n
	q10
}

BREAKPOINT {
	SOLVE states METHOD cnexp
	ik   = gkbar * n * n * n * n * (v - ek)
	il   = gl * (v - el)
}

DERIVATIVE states {
    evaluate_fct(v)
	n' = (n_inf - n) / tau_n
}

UNITSOFF

INITIAL {
	q10 = 3.0 ^ ((celsius-36)/ 10 )
	evaluate_fct(v)
	n = n_inf
}

PROCEDURE evaluate_fct(v(mV)) { LOCAL a,b,v2
	a = q10 * vtrap1(v)
	b = q10 * vtrap2(v)
	tau_n = 1 / (a + b)
	n_inf = a / (a + b)
}

FUNCTION vtrap1(x) {
	if (fabs((x + a_B) / a_C) < 1e-6) {
		vtrap1 = a_A * a_C
	}else{
		vtrap1 = (a_A * (x + a_B)) / (1 - Exp( - (x + a_B) / a_C))
	}
}

FUNCTION vtrap2(x) {
	if (fabs((x + b_B) / b_C) < 1e-6) {
		vtrap2 = b_A * b_C
	}else{
		vtrap2 = (b_A * ( - (x + b_B))) / (1 - Exp((x + b_B) / b_C))
	}
}

FUNCTION Exp(x) {
	if (x < -100) {
		Exp = 0
	}else{
		Exp = exp(x)
	}
}

UNITSON