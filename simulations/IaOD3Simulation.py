# -*- coding: utf-8 -*


###########################################################################
# Class IaOD3Simulation.
#
# Created on: 2 January 2020
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.IaSimulation import IaSimulation


class IaOD3Simulation(IaSimulation):
    """
    This class is an exact copy of the class IaSimulation. It is intended to be used in combination with IaFiberOD3s
    instead of IaFibers.
    """
    pass

###########################################################################
