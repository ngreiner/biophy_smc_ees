# -*- coding: utf-8 -*


###########################################################################
# Class IaSimulation.
#
# Created on: 10 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import pandas
from neuron import h

from biophy_smc_ees.simulations.FiberSimulation import FiberSimulation

h.load_file('stdrun.hoc')


class IaSimulation(FiberSimulation):
    """
    This class refines the FiberSimulation class to control simulations whose fibers are IaFibers.

    Methods defined here:
        init(self)          -- extends Simulation.init
        get_recr(self)
    """

    def init(self):
        """Initialize the simulation.

        This consists in building h.Vectors to record the spiking activity of the terminal nodes of Ranvier of the
        IaFiber collaterals, and executing the initialization common to all Simulation-derived classes.

        """
        for Col in self.fiber.Cols:
            self.record_spike_times(Col.nodes[-1])
        super(IaSimulation, self).init()

    def get_recr(self):
        """Return the recruitment summary of the IaFiber.

        Return:
            recr    : pandas.DataFrame  :   index   Col     recr    delay
                                            0       1       r1      d1
                                            1       2       r2      d2
                                            .
                                            .
                                            n-1     n       rn      dn

        This method should be called after the simulation has been ran.

        """
        recr = [pandas.DataFrame(index=[0]) for _ in range(len(self.fiber.Cols))]
        for i, Col in enumerate(self.fiber.Cols):
            recr[i]['Col'] = i + 1
            st_vec = self.get_records(Col.nodes[-1], 'spike_times')
            if st_vec.size > 0:
                recr[i]['recr'] = 1.0
                recr[i]['delay'] = st_vec[0]
            else:
                recr[i]['recr'] = 0.0
                recr[i]['delay'] = 0.0
        recr = pandas.concat(recr, ignore_index=True)
        return recr

###########################################################################
