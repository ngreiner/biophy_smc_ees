# -*- coding: utf-8 -*


###########################################################################
# Class Simulation.
#
# Created on: 25 June 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import numpy as np

from biophy_smc_ees.utils.vectors import *

from neuron import h

h.load_file('stdrun.hoc')


class Simulation(object):
    """
    This class regroups attributes and methods which allow controlling simulations with the numerical solver NEURON.

    Class variables defined here:
        _tstop  : 10.0  : default end time of simulation (in ms).
        _dt     : 0.01  : default timestep of simulation (in ms) if _use_var_dt==False.
        _v0     : -70.0 : default initial value for membrane potential (in mV).
        _celsius    : 37.0  : default temperature used in the simulation (in Celsius).
        _use_var_dt : False : default indicator for employment of the variable time step method.
        _minstep    : 0.0   : default minimal time step (in ms) when _use_var_dt==True. (defectuous)
        _maxstep    : 0.5   : default maximal time step (in ms) when _use_var_dt==True.

    Instance variables defined here:
        _records    : dict  : holding recorded quantities while the Simulation runs.

    Methods defined here:
        __init__(self, **kwargs)
        __repr__(self)
        get_info(self)
        tstop(self, *args)
        dt(self, *args)
        v0(self, *args)
        celsius(self, *args)
        use_var_dt(self, *args)
        minstep(self, *args)
        maxstep(self, *args)
        init(self)
        reset(self)
        _advance(self)                  -- static
        _continuetil(self, t)           -- static
        _continuefor(self, dur)         -- static
        run(self)
        record_t(self)
        record_v(self, sec, loc=0.5)
        record_eext(self, sec, loc=0.5)
        record_spike_times(self, sec, loc=0.5, threshold=0.0)
        get_records(self, *args)

    """

    _tstop = 10.0
    _dt = 0.01
    _v0 = -70.0
    _celsius = 37.0
    _use_var_dt = 0
    _minstep = 0.0
    _maxstep = 0.5

    def __init__(self, **kwargs):
        """Class constructor method.

        Keyword arguments:
            tstop   : float : end time of simulation (in ms).
            dt      : float : timestep of simulation (in ms) if _use_var_dt==False.
            v0      : float : initial value for membrane potential (in mV).
            celsius     : float : temperature used in the simulation (in Celsius).
            use_var_dt  : bool  : indicator for employment of the variable time step method.
            minstep     : float : minimal time step (in ms) when _use_var_dt==True. (defectuous)
            maxstep     : float : maximal time step (in ms) when _use_var_dt==True.

        """

        self._records = dict()

        h.tstop = self._tstop
        h.dt = self._dt
        h.celsius = self._celsius
        h.cvode_active(self._use_var_dt)
        h.cvode.minstep(self._minstep)
        h.cvode.maxstep(self._maxstep)

        try:
            self.tstop(kwargs['tstop'])
        except:
            pass

        try:
            self.dt(kwargs['dt'])
        except:
            pass

        try:
            self.v0(kwargs['v0'])
        except:
            pass

        try:
            self.celsius(kwargs['celsius'])
        except:
            pass

        try:
            self.use_var_dt(kwargs['use_var_dt'])
        except:
            pass

        try:
            self.minstep(kwargs['minstep'])
        except:
            pass

        try:
            self.maxstep(kwargs['maxstep'])
        except:
            pass

    def __repr__(self):
        """Return the string representation of the Simulation."""
        return '{}_{:d}'.format(self.__class__.__name__, id(self))

    def get_info(self):
        """Return a string giving information about the Stimulation."""
        string = '{} object with following attributes:\n'.format(self.__class__.__name__)
        string += ' - python id:{:d}\n'.format(id(self))
        string += ' - tstop: {:f}\n'.format(self._tstop)
        string += ' - dt: {:f}\n'.format(self._dt)
        string += ' - v0: {:f}\n'.format(self._v0)
        string += ' - celsius: {:f}\n'.format(self._celsius)
        string += ' - use_var_dt: {:f}\n'.format(self._use_var_dt)
        string += ' - minstep: {:f}\n'.format(self._minstep)
        string += ' - maxstep: {:f}\n'.format(self._maxstep)
        return string

    def tstop(self, *args):
        """Return (without args) or set and return (with args) the attribute _tstop.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._tstop = args[0]
            h.tstop = self._tstop
        return self._tstop

    def dt(self, *args):
        """Return (without args) or set and return (with args) the attribute _dt.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._dt = args[0]
            h.dt = self._dt
        return self._dt

    def v0(self, *args):
        """Return (without args) or set and return (with args) the attribute _v0."""
        if len(args) > 0:
            self._v0 = args[0]
        return self._v0

    def celsius(self, *args):
        """Return (without args) or set and return (with args) the attribute _celsius.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._celsius = args[0]
            h.celsius = self._celsius
        return self._celsius

    def use_var_dt(self, *args):
        """Return (without args) or set and return (with args) the attribute _use_var_dt.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._use_var_dt = args[0]
            h.cvode_active(self._use_var_dt)
            h.cvode.maxstep(self._maxstep)
            h.cvode.minstep(self._minstep)
        return self._use_var_dt

    def maxstep(self, *args):
        """Return (without args) or set and return (with args) the attribute _maxstep.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._maxstep = args[0]
            h.cvode.maxstep(self._maxstep)
        return self._maxstep

    def minstep(self, *args):
        """Return (without args) or set and return (with args) the attribute _minstep.

        Update NEURON solver accordingly.

        """
        if len(args) > 0:
            self._minstep = args[0]
            h.cvode.minstep(self._minstep)
        return self._minstep

    def init(self):
        """Initialize the NEURON solver.

        Which consists in setting the integration method (fixed or variable time step) and calling finitialize with
        _v0 in argument.

        """
        h.cvode_active(self._use_var_dt)
        h.cvode.maxstep(self._maxstep)
        h.cvode.minstep(self._minstep)
        h.finitialize(self._v0)

    def reset(self):
        """Reset the NEURON solver.

        Which consists in a call to finitialize to which _v0 is passed in argument.

        """
        h.finitialize(self._v0)

    @staticmethod
    def _advance():
        """Advance the simulation by one timestep."""
        h.fadvance()

    @staticmethod
    def _continuetil(t):
        """Advance the simulation until `t`.

        Arguments:
            t   : float : time point until which the Simulation should advance.
        """
        h.continuerun(t)

    @staticmethod
    def _continuefor(dur):
        """Advance the simulation for `dur`.

        Arguments:
            dur : float : duration for which the Simulation should advance.
        """
        h.continuerun(h.t + dur)

    def run(self):
        """Advance the Simulation until _tstop."""
        h.continuerun(self._tstop)

    ##############################################################################################################
    # The methods below deal with recording (and retrieving the records of) quantities during the Simulation.
    ##############################################################################################################

    def record_t(self):
        """Record the simulation time steps."""
        self._records['t'] = build_t_vector()

    def record_v(self, hsec, loc=0.5):
        """Record the input HOC section's membrane potential during simulation.

        Arguments:
            hsec    : HOC section   : whose membrane potential shall be recorded.

        Keyword arguments:
            loc : float in [0, 1]   : position along the section where to record.

        """
        try:
            self._records[hsec.name()]['v'] = build_v_vector(hsec, loc=loc)
        except:
            self._records[hsec.name()] = dict()
            self._records[hsec.name()]['v'] = build_v_vector(hsec, loc=loc)

    def record_eext(self, hsec, loc=0.5):
        """Record the input HOC section's extracellular potential during simulation.

        Arguments:
            hsec    : HOC section   : whose membrane potential shall be recorded.

        Keyword arguments:
            loc : float in [0, 1]   : position along the section where to record.

        """
        try:
            self._records[hsec.name()]['eext'] = build_eext_vector(hsec, loc=loc)
        except:
            self._records[hsec.name()] = dict()
            self._records[hsec.name()]['eext'] = build_eext_vector(hsec, loc=loc)

    def record_spike_times(self, hsec, loc=0.5, threshold=0.0):
        """Record the input HOC section's spike times during simulation.

        Arguments:
            hsec    : HOC section   : whose membrane potential shall be recorded.

        Keyword arguments:
            loc : float in [0, 1]   : position along the section where to record.
            threshold   : float : potential level to cross to discriminate an action potential.

        """
        try:
            self._records[hsec.name()]['spike_times'] = build_spike_times_vector(hsec, loc=loc, threshold=threshold)
        except:
            self._records[hsec.name()] = dict()
            self._records[hsec.name()]['spike_times'] = build_spike_times_vector(hsec, loc=loc, threshold=threshold)

    def get_records(self, *args):
        """Return the dict _records or specific keys of it.

        Usage:
            records = simu.get_records()
            t_vec = simu.get_records('t')
            v_vec = simu.get_records(hsec, 'v')
            st_vec = simu.get_records(hsec, 'spike_times')
            ...

        Optional arguments:
            't' : to get the recorded time steps of the Simulation.
            hsec    : h.Section object  : some of whose properties were recorded during the simulation.

        The optional arguments should be given in an order forming a valid path to explore the tree-like structure of
        _records. If providing a h.Section in the list of arguments, it is first converted to its string
        representation to explore the tree. If the provided list of arguments leads to a leaf of the tree,
        it is converted to a numpy.array before being returned (all the leaves of the _records tree are h.Vector
        objects which recorded some quantity during the Simulation and which can be converted to numpy.arrays).
        Otherwise it leads to a subdictionnary of _records which is returned as is.

        """
        records = self._records

        for arg in args:
            records = records[str(arg)]

        try:
            records = np.asarray(records.to_python())
        except:
            pass

        return records

##############################################################################################################
