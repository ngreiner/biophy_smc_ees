# -*- coding: utf-8 -*


###########################################################################
# Class MNSimulation.
#
# Created on: 17 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import pandas
import numpy as np
from neuron import h

from biophy_smc_ees.simulations.Simulation import Simulation

h.load_file('stdrun.hoc')


class MNSimulation(Simulation):
    """
    This class refines the Simulation class to control simulations involving a single Motoneuron.

    Instance variables defined here:
        mn: Motoneuron object.

    Methods defined here:
        __init__(self, mn, **kwargs)        -- extends Simulation.__init__
        __repr__(self)                      -- overrides Simulation.__repr__
        init(self)                          -- extends Simulation.init
        reset(self, **kwargs)               -- overrides Simulation.reset
        get_recr(self)
        find_activation_spot(self)

    """

    def __init__(self, mn, **kwargs):
        """Class constructor method.

        Arguments:
            mn  : Motoneuron object.

        Keyword arguments:
            tstop   : float : end time of simulation (in ms).
            dt      : float : timestep of simulation (in ms) if _use_var_dt==False.
            v0      : float : initial value for membrane potential (in mV).
            celsius     : float : temperature used in the simulation (in Celsius).
            use_var_dt  : bool  : indicator for employment of the variable time step method.
            minstep     : float : minimal time step (in ms) when _use_var_dt==True. (defectuous)
            maxstep     : float : maximal time step (in ms) when _use_var_dt==True.

        """
        super(MNSimulation, self).__init__(**kwargs)
        self.mn = mn

    def __repr__(self):
        """Return the string representation of the MNSimulation."""
        return 'MNSimulation.{}'.format(self.mn.name)

    def init(self):
        """Initialize the NEURON solver.

        This consists in:
            1. building a h.Vector to record the spiking activity of the terminal node of Ranvier of the
               Motoneuron's axon if it possesses one, or of one of its somatic compartments otherwise
            2. executing the initialization common to all Simulation-derived classes
            3. adjusting the leakage reversal potentials of the active compartments of the Motoneuron to reach
               steady-state instantaneously (thereby starting the simulation with a Motoneuron effectively at rest)

        """
        if self.mn.with_axon():
            self.record_spike_times(self.mn.axon.VRbranch.nodesPNS[-1])
        elif self.mn.soma.__class__.__name__ == 'Soma':
            self.record_spike_times(self.mn.soma.somasec.hsec, threshold=-20.0)
        elif self.mn.soma.__class__.__name__ == 'MultiTapSoma':
            self.record_spike_times(self.mn.soma.tsecs[0].hsec, threshold=-20.0)
        super(MNSimulation, self).init()
        h.fcurrent()
        self.mn.adjust_el()

    def reset(self, **kwargs):
        """Reset the NEURON solver.

        Which consists in calling finitialize with _v0 in argument and adjusting the leakage reversal potentials of
        the active compartments of the Motoneuron to reach steady-state instantaneously (thereby starting the
        simulation with a Motoneuron effectively at rest).

        """
        h.finitialize(self._v0)
        h.fcurrent()
        self.mn.adjust_el()

    def get_recr(self):
        """Return the recruitment summary of the Motoneuron.

        Return:
            recr    : pandas.DataFrame  :   index   recr    delay
                                            0       r       d

        This method should be called after the simulation has been ran.

        """
        recr = pandas.DataFrame(index=[0])
        if self.mn.with_axon():
            st_vec = self.get_records(self.mn.axon.VRbranch.nodesPNS[-1], 'spike_times')
        elif self.mn.soma.__class__.__name__ == 'Soma':
            st_vec = self.get_records(self.mn.soma.somasec.hsec, 'spike_times')
        elif self.mn.soma.__class__.__name__ == 'MultiTapSoma':
            st_vec = self.get_records(self.mn.soma.tsecs[0].hsec, 'spike_times')
        else:
            raise TypeError('Unknown MN soma type.')
        if st_vec.size > 0:
            recr['recr'] = 1.0
            recr['delay'] = st_vec[0]
        else:
            recr['recr'] = 0.0
            recr['delay'] = 0.0
        return recr

    def find_activation_spot(self):
        """Find the node of the Motoneuron's axon where a spike first emerged during simulation.

        Return:
            label   : str   : label of the Branch to which the node belongs.
            idx     : int   : index of node in the Branch.

        Notes:
            1. this method should be called after the simulation has been ran
            2. all the nodes of the Motoneuron's axon should have had their spiking activity recorded during the
               simulation
            3. the Motoneuron should have been recruited during the simulation

        """
        nodes = self.mn.axon.get_nodes()
        st_nodes = [self.get_records(node, 'spike_times')[0] for node in nodes]
        idx = st_nodes.index(min(st_nodes))
        node = nodes[idx]
        try:
            idx = self.mn.axon.VRbranch.nodesPNS.index(node)
            return self.mn.axon.VRbranch.name + '_PNS', idx
        except:
            idx = self.mn.axon.VRbranch.nodesCNS.index(node)
            return self.mn.axon.VRbranch.name + '_CNS', idx


###########################################################################
