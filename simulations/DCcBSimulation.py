# -*- coding: utf-8 -*


###########################################################################
# Class DCcBSimulation.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.IaSimulation import IaSimulation


class DCcBSimulation(IaSimulation):
    """
    This class is an exact copy of the class IaSimulation. It is intended to be used in combination with DCcBFibers
    instead of IaFibers.
    """
    pass

###########################################################################
