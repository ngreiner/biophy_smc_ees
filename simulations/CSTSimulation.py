# -*- coding: utf-8 -*


###########################################################################
# Class CSTSimulation.
#
# Created on: 13 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.IaSimulation import IaSimulation


class CSTSimulation(IaSimulation):
    """
    This class is an exact copy of the class IaSimulation. It is intended to be used in combination with CSTFibers
    instead of IaFibers.
    """
    pass

###########################################################################
