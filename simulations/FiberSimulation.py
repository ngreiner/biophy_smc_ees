# -*- coding: utf-8 -*


###########################################################################
# Class FiberSimulation.
#
# Created on: 10 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import pandas
import numpy as np
from neuron import h

from biophy_smc_ees.simulations.Simulation import Simulation

h.load_file('stdrun.hoc')


class FiberSimulation(Simulation):
    """
    This class refines the Simulation class to control simulations involving a single Fiber.

    Class variables defined here:
        _v0 : -80.0 : default initial value for membrane potential (in mV).           -- overrides Simulation._v0

    Instance variables defined here:
        fiber: Fiber object: which is simulated.

    Methods defined here:
        __init__(self, fiber, **kwargs)     -- extends Simulation.__init__
        find_activation_spot(self)

    """

    _v0 = -80.0

    def __init__(self, fiber, **kwargs):
        """Class constructor method.

        Arguments:
            fiber   : Fiber object.

        Keyword arguments:
            tstop   : float : end time of simulation (in ms).
            dt      : float : timestep of simulation (in ms) if _use_var_dt==False.
            v0      : float : initial value for membrane potential (in mV).
            celsius     : float : temperature used in the simulation (in Celsius).
            use_var_dt  : bool  : indicator for employment of the variable time step method.
            minstep     : float : minimal time step (in ms) when _use_var_dt==True. (defectuous)
            maxstep     : float : maximal time step (in ms) when _use_var_dt==True.

        """
        super(FiberSimulation, self).__init__(**kwargs)
        self.fiber = fiber

    def find_activation_spot(self):
        """Find the node of the Fiber where a spike first emerged during simulation.

        Return:
            label: str: label of the Branch to which the node belongs.
            idx: int: index of node in the Branch.

        Notes:
            1. this method should be called after the simulation has been ran
            2. all the nodes of the Fiber should have had their spiking activity recorded during the
               simulation
            3. the Fiber should have been recruited during the simulation

        """
        nodes = self.fiber.get_nodes()
        spike_times = [self.get_records(node, 'spike_times')[0] for node in nodes
                       if self.get_records(node, 'spike_times').size > 0]
        if len(spike_times) == 0:
            raise ValueError('No spike time recorded for {:}.'.format(self.fiber))
        idx = spike_times.index(min(spike_times))
        node = nodes[idx]
        for branch in self.fiber.get_branches():
            try:
                idx = branch.nodes.index(node)
                break
            except:
                pass
        return branch.label, idx

###########################################################################
