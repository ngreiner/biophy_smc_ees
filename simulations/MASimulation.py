# -*- coding: utf-8 -*


###########################################################################
# Class MASimulation.
#
# Created on: 20 August 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import pandas
from neuron import h

from biophy_smc_ees.simulations.Simulation import Simulation

h.load_file('stdrun.hoc')


class MASimulation(Simulation):
    """
    This class refines the Simulation class to control simulations involving a single MotorAxon.

    Instance variables defined here:
        axon: MotorAxon object.

    Methods defined here:
        __init__(self, axon, **kwargs)      -- extends Simulation.__init__
        init(self)                          -- extends Simulation.init
        get_recr(self)
        find_activation_spot(self)

    """

    def __init__(self, axon, **kwargs):
        """Class constructor method.

        Arguments:
            axon    : MotorAxon object.

        Keyword arguments:
            tstop       : float : end time of simulation (in ms).
            dt          : float : timestep of simulation (in ms) if _use_var_dt==False.
            v0          : float : initial value for membrane potential (in mV).
            celsius     : float : temperature used in the simulation (in Celsius).
            use_var_dt  : bool  : indicator for employment of the variable time step method.
            minstep     : float : minimal time step (in ms) when _use_var_dt==True. (defectuous)
            maxstep     : float : maximal time step (in ms) when _use_var_dt==True.

        """
        super(MASimulation, self).__init__(**kwargs)
        self.axon = axon

    def init(self):
        """Initialize the NEURON solver.

        This consists in building an h.Vector to record the spiking activity of the terminal node of the VRbranch of
        the MotorAxon, executing the initialization common to all Simulation-derived classes, and adjusting the
        leakage reversal potentials of the active compartments of the MotorAxon to reach steady-state instantaneously
        (thereby starting the simulation with a MotorAxon effectively at rest).

        """
        self.record_spike_times(self.axon.VRbranch.nodesPNS[-1])
        super(MASimulation, self).init()
        h.fcurrent()
        self.axon.adjust_el()

    def reset(self, **kwargs):
        """Reset the NEURON solver.

        Which consists in calling finitialize with _v0 in argument and adjusting the leakage reversal potentials of
        the active compartments of the MotorAxon to reach steady-state instantaneously (thereby starting the
        simulation with a MotorAxon effectively at rest).

        """
        h.finitialize(self._v0)
        h.fcurrent()
        self.axon.adjust_el()

    def get_recr(self):
        """Return the recruitment summary of the MotorAxon.

        Return:
            recr    : pandas.DataFrame  :   index   recr    delay
                                            0       r       d

        This method should be called after the simulation has been ran.

        """
        recr = pandas.DataFrame(index=[0])
        st_vec = self.get_records(self.axon.VRbranch.nodesPNS[-1], 'spike_times')
        if st_vec.size > 0:
            recr['recr'] = 1.0
            recr['delay'] = st_vec[0]
        else:
            recr['recr'] = 0.0
            recr['delay'] = 0.0

    def find_activation_spot(self):
        """Find the node of the MotorAxon where a spike first emerged during simulation.

        Return:
            label   : str   : label of the Branch to which the node belongs.
            idx     : int   : index of node in the Branch.

        Notes:
            1. this method should be called after the simulation has been ran
            2. all the nodes of the MotorAxon should have had their spiking activity recorded during the
               simulation
            3. the MotorAxon should have been recruited during the simulation

        """
        nodes = self.axon.get_nodes()
        st_vec = [self.get_records(node, 'spike_times')[0] for node in nodes]
        idx = st_vec.index(min(st_vec))
        node = nodes[idx]
        try:
            idx = self.axon.VRbranch.nodesPNS.index(node)
            return self.axon.VRbranch.name + '_PNS', idx
        except:
            idx = self.axon.VRbranch.nodesCNS.index(node)
            return self.axon.VRbranch.name + '_CNS', idx


###########################################################################
