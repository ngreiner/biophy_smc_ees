# -*- coding: utf-8 -*


###########################################################################
# Class IISimulation.
#
# Created on: 2 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.IaSimulation import IaSimulation


class IISimulation(IaSimulation):
    """
    This class is an exact copy of the class IaSimulation. It is intended to be used in combination with IIFibers
    instead of IaFibers.
    """
    pass

###########################################################################
