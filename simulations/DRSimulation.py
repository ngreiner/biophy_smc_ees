
# -*- coding: utf-8 -*


###########################################################################
# Class DRSimulation.
#
# Created on: 2 July 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


import pandas
from neuron import h

from biophy_smc_ees.simulations.FiberSimulation import FiberSimulation

h.load_file('stdrun.hoc')


class DRSimulation(FiberSimulation):
    """
    This class refines the FiberSimulation class to control simulations whose fibers are DRFibers.

    Methods defined here:
        init(self)          -- extends Simulation.init
        get_recr(self)
    """

    def init(self):
        """Initialize the simulation.

        This consists in building an h.Vector to record the spiking activity of the terminal node of the DRbranch of
        the DRFiber, and executing the initialization common to all Simulation-derived classes.

        """
        self.record_spike_times(self.fiber.DRbranch.nodes[0])
        super(DRSimulation, self).init()

    def get_recr(self):
        """Return the recruitment summary of the DRFiber.

        Return:
            recr    : pandas.DataFrame  :   index   recr    delay
                                            0       r       d

        This method should be called after the simulation has been ran.

        """
        recr = pandas.DataFrame(index=[0])
        st_vec = self.get_records(self.fiber.DRbranch.nodes[0], 'spike_times')
        if st_vec.size > 0:
            recr['recr'] = 1.0
            recr['delay'] = st_vec[0]
        else:
            recr['recr'] = 0.0
            recr['delay'] = 0.0
        return recr


###########################################################################
