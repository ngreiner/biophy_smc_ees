# -*- coding: utf-8 -*


###########################################################################
# Class DRcASimulation.
#
# Created on: 25 November 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.DRSimulation import DRSimulation


class DRcASimulation(DRSimulation):
    """
    This class is an exact copy of the class DRSimulation.
    It is intended to be used in combination with DRcAFibers.
    """
    pass

###########################################################################
