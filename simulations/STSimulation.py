# -*- coding: utf-8 -*


###########################################################################
# Class STSimulation.
#
# Created on: 23 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from biophy_smc_ees.simulations.IaSimulation import IaSimulation


class STSimulation(IaSimulation):
    """
    This class is an exact copy of the class IaSimulation. It is intended to be used in combination with STFibers
    instead of IaFibers.
    """
    pass

###########################################################################
