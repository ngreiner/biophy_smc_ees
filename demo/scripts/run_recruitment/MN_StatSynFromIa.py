# -*- coding: utf-8 -*-


#############################################################################
# Script: .../run_recruitment/MN_StatSynFromIa.py
#
# Created on: 22 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.recruitment_builders.MNStatSynFromIaRecruitmentBuilder import MNStatSynFromIaRecruitmentBuilder


dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

params = dict()
params['neuralobj_params'] = dict(extra_mech=False, with_axon=True)
params['stim_params'] = dict(connec_type='fcd', gmax=7.625)
params['gidxs'] = smc.get_MN_gidxs(musc='DEL')
params['combis'] = ['elec10']
params['amps'] = -25.0 * np.linspace(1, 80, num=80)
params['simu_params'] = dict(tstop=15.0, use_var_dt=1)
params['skip_when_recr'] = True
params['runperm'] = 1
params['stim_type'] = 'stat_syn_from_Ia_gmax=7.625'
rb = MNStatSynFromIaRecruitmentBuilder(smc=smc, **params)

rb.run_recr_serial()

input('Press ENTER to exit program ...')
