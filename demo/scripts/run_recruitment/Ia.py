# -*- coding: utf-8 -*


#############################################################################
# Script: .../run_recruitment/Ia.py
#
# Created on: 21 September 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np

import biophy_smc_ees
from biophy_smc_ees.recruitment_builders.IaRecruitmentBuilder import IaRecruitmentBuilder
from biophy_smc_ees.neural_entities.SMC import SMC


dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

rbparams = dict()
rbparams['neuralobj_params'] = dict(model='ModelD')
rbparams['gidxs'] = smc.get_Ia_gidxs(musc='DEL')
rbparams['combis'] = ['elec10']
rbparams['amps'] = - 25.0 * np.linspace(1, 80, num=80)
rbparams['simu_params'] = dict(use_var_dt=1, tstop=10.0)
rbparams['skip_when_recr'] = True
rbparams['runperm'] = 1
rb = IaRecruitmentBuilder(smc, **rbparams)

rb.run_recr_serial()

input('Press ENTER to exit program ...')
