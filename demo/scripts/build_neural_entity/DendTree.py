# -*- coding: utf-8 -*


#############################################################################
# Script: .../build_neural_entity/DendTree.py
#
# Created on: 9 March 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.cell_comps.DendTree import DendTree
from biophy_smc_ees.utils.resources import swc_files


params = dict()
params['swc_file'] = swc_files[0]

dtree = DendTree(**params)
dtree.build()

pass
