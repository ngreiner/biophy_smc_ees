# -*- coding: utf-8 -*


#############################################################################
# Script: .../build_neural_entity/Motoneuron.py
#
# Created on: 10 March 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.cells.Motoneuron import Motoneuron
from biophy_smc_ees.utils.resources import swc_files


mnparams = dict()
mnparams['diam'] = 55.0
mnparams['swc_file'] = swc_files[0]
mnparams['with_axon'] = False
mnparams['extra_mech'] = False

mn = Motoneuron(**mnparams)

# Get total area:
soma_area = mn.soma.get_area()
dtree_area = mn.dtree.get_area()
print('somatic area: {:.1f} um^2'.format(soma_area))
print('dendritic area: {:.1f} um^2'.format(dtree_area))
print('total area: {:.1f} um^2'.format(soma_area + dtree_area))

pass
