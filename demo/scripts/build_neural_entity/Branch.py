# -*- coding: utf-8 -*


#############################################################################
# Script: .../build_neural_entity/Branch.py
#
# Created on: 9 March 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# nathan.greiner@epfl.ch
#
#############################################################################


from biophy_smc_ees.neural_entities.branches.Branch import Branch


label = 'MyBranch'
bparams = dict()
bparams['nN'] = 19
bparams['diam'] = 10.0

branch = Branch(label, **bparams)

pass
