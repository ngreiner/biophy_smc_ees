# -*- coding: utf-8 -*-


#############################################################################
# Script: .../run_simu/Ia.py
#
# Created on: 21 September 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.neural_entities.fibers.IaFiber import IaFiber
from biophy_smc_ees.simulations.IaSimulation import IaSimulation
from biophy_smc_ees.stims.IaFiberEextStim import IaFiberEextStim
from biophy_smc_ees.utils.Plotter import Plotter


def main():

    # SMC dataset.
    dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
    dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=0)

    # Ia fiber.
    fiberparams = dict()
    fiberparams['smc'] = smc
    fiberparams['musc'] = 'DEL'
    fiberparams['seg'] = 'C6'
    fiberparams['fidx'] = 1
    fiberparams['model'] = 'ModelD'
    fiber = IaFiber(**fiberparams)

    # Stim.
    stimparams = dict()
    stimparams['combi'] = 'elec10'
    stimparams['amp'] = -475.0
    stim = IaFiberEextStim(fiber, **stimparams)

    # Simu.
    simuparams = dict()
    simuparams['tstop'] = 15.0
    simuparams['use_var_dt'] = 1
    simu = IaSimulation(fiber, **simuparams)
    simu.record_t()

    # Record sections of Ia fiber.
    secs = fiber.Cols[0].nodes
    for sec in secs:
        simu.record_v(sec)

    # Run simu.
    stim.activate()
    simu.init()
    simu.run()

    # Plotting section.
    plot = True

    if plot:

        # Define global plotting parameters.
        plotter = Plotter()

        pparams = dict()
        pparams['xlabel'] = 'time (ms)'
        pparams['ylabel'] = 'membrane potential (mV)'
        pparams['ylim'] = [-100.0, 50.0]
        pparams['tight_layout'] = True

        t = simu.get_records('t')
        vs = [simu.get_records(sec, 'v') for sec in secs]

        fig, axs = plotter.build_figure(len(secs))
        for sec, v, ax in zip(secs, vs, axs):
            pparams['ax_title'] = sec.name()
            plotter.plot_lines(ax, t, v, **pparams)

        plotter.show(block=True)

    # # The end.
    # raw_input('Press ENTER to exit program.')


if __name__ == '__main__':

    main()
