# -*- coding: utf-8 -*


#############################################################################
# Script: .../find_amp/threshold.py
#
# Created on: 1 August 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np

from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.utils.Bootstrap import Bootstrap


# Choose and load sensorimotor circuit dataset.
dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
smc = SMC(dir_data=dir_data, dir_res=dir_res)

# Choose entities.
entity = 'Ia'
muscs = ['DEL']

# Choose combi and amplitudes.
combis = ['elec10']
amps = - 25.0 * np.linspace(1, 80, num=80)

# Parameters for statistical analyses.
pop_size = 100

# Set threshold and saturation ratios.
xthresh = 0.1
xsat = 0.9

# Load entity instances' records and prepare dictionary to store results.
records = dict()
results = dict()
for musc in muscs:
    gidxs = smc.get_gidxs(entity, musc=musc)
    for gidx in gidxs:
        records[gidx] = smc.load_obj(entity + 'records', entity, gidx=gidx)
    results[musc] = dict()

for musc in muscs:

    print('\n')
    print('*' * 50)
    print('musc = {:}\n'.format(musc))

    gidxs = smc.get_gidxs(entity, musc=musc)

    # Build Bootstrap object to perform statistical analyses.
    bstrap = Bootstrap(pop_size=pop_size)
    bstrap.set_data_sequence(gidxs)
    bstrap.gen_pop()

    for combi in combis:

        print('combi = {:}'.format(combi))

        # Dictionnary to store the amplitudes yielding specific recruitments.
        results[musc][combi] = dict()

        # Dictionnary to cache the recruitments of specific gidxs.
        recrs = dict()
        for gidx in gidxs:
            recrs[gidx] = dict()

        for i, amp in enumerate(amps):
            amp_recs = np.zeros((len(gidxs), pop_size))

            for j in range(pop_size):

                for k, gidx in enumerate(bstrap.pop[:, j]):
                    try:
                        amp_recs[k, j] = recrs[gidx][amp]
                    except:
                        record = records[gidx].loc[(records[gidx].combi == combi) & (records[gidx].amp == amp)]
                        amp_recs[k, j] = 1.0 if record['recr'].any() else 0.0
                        recrs[gidx][amp] = amp_recs[k, j]

                tmp = np.mean(amp_recs, axis=0)
                for rec in tmp:
                    try:
                        results[musc][combi][rec].append(abs(amp))
                    except:
                        results[musc][combi][rec] = [abs(amp)]

# Extract mean and standard deviation of threshold and saturation amplitudes for each combi.
for combi in combis:
    Ithresh_mean = np.Inf
    Ithresh_std = np.Inf
    Isat_mean = np.Inf
    Isat_std = np.Inf

    for musc in muscs:

        x1 = min(x for x in results[musc][combi].keys() if x >= xthresh)
        tmp = np.asarray(results[musc][combi][x1])
        if np.mean(tmp) < Ithresh_mean:
            Ithresh_mean = np.mean(tmp)
            Ithresh_std = np.std(tmp)

        try:
            x2 = min(x for x in results[musc][combi].keys() if x >= xsat)
            tmp = np.asarray(results[musc][combi][x2])
            if np.mean(tmp) < Isat_mean:
                Isat_mean = np.mean(tmp)
                Isat_std = np.std(tmp)
        except:
            Isat_mean = amps[-1]
            Isat_std = 0.0

    print('\n')
    print('Combi: {:}'.format(combi))
    print('Threshold: {:f} +- {:f} uA'.format(abs(Ithresh_mean), Ithresh_std))
    print('Saturation: {:f} +- {:f} uA\n'.format(abs(Isat_mean), Isat_std))

input('Press ENTER to exit program...')
