# -*- coding: utf-8 -*


#############################################################################
# Script: .../find_amp/full_recruitment.py
#
# Created on: 12 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC


# SMC dataset.
dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=0)

# Entities which should all be recruited.
entity = 'Ia'
musc = 'DEL'
gidxs = smc.get_gidxs(entity, musc=musc)

# Stimulation characteristics.
stim_type = 'eext'
scombi = 'elec10'

# Find amplitude of full recruitment.
amps = list()
for gidx in gidxs:
    records = smc.load_records(entity, gidx=gidx)
    records = records.loc[(records.combi == scombi) & (records.stim_type == stim_type) & (records.recr == 1.0)]
    vals = records['amp'].values
    amps.append(np.min(np.abs(vals)))
full_rec_amp = max(amps)

print(full_rec_amp)

input('Press ENTER to exit program ...')
