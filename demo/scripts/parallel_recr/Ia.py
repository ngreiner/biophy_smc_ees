# -*- coding: utf-8 -*-


###########################################################################
# Script: .../parallel_recr/Ia.py
#
# Created on: 29 June 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from mpi4py import MPI

import os
import numpy as np

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.recruitment_builders.IaRecruitmentBuilder import IaRecruitmentBuilder


if __name__ == '__main__':

    comm = MPI.COMM_WORLD
    size_comm = comm.Get_size()
    rank = comm.Get_rank()

    if rank == 0:
        tstart = MPI.Wtime()

    dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
    dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

    params = dict()
    params['neuralobj_params'] = dict(model='ModelD')
    params['gidxs'] = [gidx for gidx in smc.get_Ia_gidxs() if gidx % size_comm == rank]
    params['combis'] = ['elec10']
    params['amps'] = -25.0 * np.linspace(1, 80, num=80)
    params['simu_params'] = dict(use_var_dt=1, tstop=10.0)
    params['skip_when_recr'] = True
    params['runperm'] = 1
    rb = IaRecruitmentBuilder(smc=smc, **params)
    rb.run_recr_serial()

    if rank == 0:
        tstop = MPI.Wtime()
        print('Time elapsed: {:f} s'.format(tstop - tstart))
