# -*- coding: utf-8 -*-


###########################################################################
# Script: .../parallel_recr/test_mpi_implementation.py
#
# Created on: 25 June 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
###########################################################################


from mpi4py import MPI


comm = MPI.COMM_WORLD
size_comm = comm.Get_size()
rank = comm.Get_rank()


print('mpi4py thinks I am host {:d} on {:d}.'.format(rank, size_comm))
