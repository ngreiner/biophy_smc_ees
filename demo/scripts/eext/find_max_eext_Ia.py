# -*- coding: utf-8 -*-


#############################################################################
# Script: .../eext/find_max_eext_Ia.py
#
# Created on: 9 March 2018
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.neural_entities.fibers.IaFiber import IaFiber
from biophy_smc_ees.stims.IaFiberEextStim import IaFiberEextStim


def main():

    # SMC dataset.
    dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
    dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=0)

    # Ia fiber.
    fiberparams = dict()
    fiberparams['smc'] = smc
    fiberparams['musc'] = 'DEL'
    fiberparams['seg'] = 'C6'
    fiberparams['fidx'] = 1
    fiber = IaFiber(**fiberparams)

    # Stim.
    stimparams = dict()
    stimparams['combi'] = 'elec10'
    stimparams['amp'] = -400.0
    stim = IaFiberEextStim(fiber, **stimparams)

    # Find node where eext reaches its max.
    eext_Abranch = stim.eext['Abranch'][0: -1: 11]
    eext_Dbranch = stim.eext['Dbranch'][0: -1: 11]
    eext_DRbranch = stim.eext['DRbranch'][0: -1: 11]
    eext = np.concatenate([eext_Abranch, eext_Dbranch, eext_DRbranch])
    M = np.min(eext)
    if M in eext_Abranch:
        idx = np.flatnonzero(eext_Abranch == M)[0]
        sec = fiber.Abranch.nodes[idx]
    elif M in eext_Dbranch:
        idx = np.flatnonzero(eext_Dbranch == M)[0]
        sec = fiber.Dbranch.nodes[idx]
    else:
        idx = np.flatnonzero(eext_DRbranch == M)[0]
        sec = fiber.DRbranch.nodes[idx]

    # The end.
    input('Press ENTER to exit program.')


if __name__ == '__main__':
    main()
