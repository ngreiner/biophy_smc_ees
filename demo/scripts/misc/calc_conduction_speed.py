# -*- coding: utf-8 -*-


#############################################################################
# Script: .../run_simu/CST.py
#
# Created on: 28 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC
from biophy_smc_ees.neural_entities.fibers.IaFiber import IaFiber
from biophy_smc_ees.simulations.IaSimulation import IaSimulation
from biophy_smc_ees.stims.IaFiberEextStim import IaFiberEextStim


def main():

    # SMC dataset.
    dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
    dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
    smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=0)

    # CST fiber.
    fiberparams = dict()
    fiberparams['smc'] = smc
    fiberparams['musc'] = 'DEL'
    fiberparams['seg'] = 'C6'
    fiberparams['fidx'] = 1
    fiber = IaFiber(**fiberparams)

    # Stim.
    stimparams = dict()
    stimparams['combi'] = 'elec10'
    stimparams['amp'] = -400.0
    stim = IaFiberEextStim(fiber, **stimparams)

    # Simu.
    simuparams = dict()
    simuparams['tstop'] = 15.0
    simuparams['use_var_dt'] = 1
    simu = IaSimulation(fiber, **simuparams)
    simu.record_t()

    # Record sections of Ia fiber.
    secs = fiber.get_nodes()
    for sec in secs:
        simu.record_spike_times(sec)

    # Run simu.
    stim.activate()
    simu.init()
    simu.run()

    # Get recruitment value.
    recr = simu.get_recr()
    print('recr: {}\n'.format(recr['recr'].any()))

    # Find activation spot.
    blabel, idx = simu.find_activation_spot()

    # Compute conduction speeds in the fiber's branches.
    if blabel == 'DRbranch':

        # DRbranch.
        xs = fiber.DRbranch.get_longi_xs()
        L = xs[11 * idx] - xs[0]
        t1 = simu.get_records(fiber.DRbranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.DRbranch.nodes[0], 'spike_times')[0]
        vdr1 = L / (t2 - t1) * 1e-3
        L = xs[-1] - xs[11 * idx]
        t1 = simu.get_records(fiber.DRbranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.DRbranch.nodes[-1], 'spike_times')[0]
        vdr2 = L / (t2 - t1) * 1e-3

        # Abranch.
        xs = fiber.Abranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.Abranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.Abranch.nodes[-1], 'spike_times')[0]
        va = L / (t2 - t1) * 1e-3

        # Dbranch.
        xs = fiber.Dbranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.Dbranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.Dbranch.nodes[-1], 'spike_times')[0]
        vd = L / (t2 - t1) * 1e-3

        print('Recruitment in DRbranch.')
        print('  - cv in DRbranch:   cv1 = {:.1f}   /   cv2 = {:.1f}'.format(vdr1, vdr2))
        print('  - cv in Abranch:     cv = {:.1f}'.format(va))
        print('  - cv in Dbranch:     cv = {:.1f}'.format(vd))

    elif blabel == 'Abranch':

        # DRbranch.
        xs = fiber.DRbranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.DRbranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.DRbranch.nodes[-1], 'spike_times')[0]
        vdr = L / (t2 - t1) * 1e-3

        # Abranch.
        xs = fiber.Abranch.get_longi_xs()
        L = xs[11 * idx] - xs[0]
        t1 = simu.get_records(fiber.Abranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.Abranch.nodes[0], 'spike_times')[0]
        va1 = L / (t2 - t1) * 1e-3
        L = xs[-1] - xs[11 * idx]
        t1 = simu.get_records(fiber.Abranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.Abranch.nodes[-1], 'spike_times')[0]
        va2 = L / (t2 - t1) * 1e-3

        # Dbranch.
        xs = fiber.Dbranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.Dbranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.Dbranch.nodes[-1], 'spike_times')[0]
        vd = L / (t2 - t1) * 1e-3

        print('Recruitment in Abranch.')
        print('  - cv in DRbranch:   cv = {:.1f}'.format(vdr))
        print('  - cv in Abranch:   cv1 = {:.1f}   /   cv2 = {:.1f}'.format(va1, va2))
        print('  - cv in Dbranch:    cv = {:.1f}'.format(vd))

    elif blabel == 'Dbranch':

        # DRbranch.
        xs = fiber.DRbranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.DRbranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.DRbranch.nodes[-1], 'spike_times')[0]
        vdr = L / (t2 - t1) * 1e-3

        # Abranch.
        xs = fiber.Abranch.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(fiber.Abranch.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(fiber.Abranch.nodes[-1], 'spike_times')[0]
        va = L / (t2 - t1) * 1e-3

        # Dbranch.
        xs = fiber.Dbranch.get_longi_xs()
        L = xs[11 * idx] - xs[0]
        t1 = simu.get_records(fiber.Dbranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.Abranch.nodes[0], 'spike_times')[0]
        vd1 = L / (t2 - t1) * 1e-3
        L = xs[-1] - xs[11 * idx]
        t1 = simu.get_records(fiber.Dbranch.nodes[idx], 'spike_times')[0]
        t2 = simu.get_records(fiber.Dbranch.nodes[-1], 'spike_times')[0]
        vd2 = L / (t2 - t1) * 1e-3

        print('Recruitment in Dbranch.')
        print('  - cv in DRbranch:   cv = {:.1f}'.format(vdr))
        print('  - cv in Abranch:    cv = {:.1f}'.format(va))
        print('  - cv in Dbranch:   cv1 = {:.1f}   /   cv2 = {:.1f}'.format(vd1, vd2))

    else:
        print('Fiber first recruited in collaterals.')

    for Col in fiber.Cols:
        xs = Col.get_longi_xs()
        L = xs[-1] - xs[0]
        t1 = simu.get_records(Col.nodes[0], 'spike_times')[0]
        t2 = simu.get_records(Col.nodes[-1], 'spike_times')[0]
        cv = L / (t2 - t1) * 1e-3
        print('  - cv in {}:   cv = {:.1f}'.format(Col.label, cv))

    # The end.
    print('\nThe end.')


if __name__ == '__main__':

    main()
