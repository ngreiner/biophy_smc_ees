# -*- coding: utf-8 -*


#############################################################################
# Script: .../records/update.py
#
# Created on: 28 April 2019
#
# Author: Nathan GREINER, PhD candidate
# Institution: EPFL BMI SV UPCOURTINE
# email: nathan.greiner@epfl.ch
#
#############################################################################


import os
import numpy as np
import pandas as pd

import biophy_smc_ees
from biophy_smc_ees.neural_entities.SMC import SMC


# Choose dataset whose records should be updated.
dir_data = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'datasets', 'smcd_Ias_MNs')
dir_res = os.path.join(biophy_smc_ees.__path__[0], 'demo', 'results', 'smcd_Ias_MNs')
smc = SMC(dir_data=dir_data, dir_res=dir_res, wperm=2)

# Choose entity and specific instances of it whose records should be updated.
entity = 'Ia'
gidxs = smc.get_gidxs(entity)

# Choose whether to save back records or to perform a dry run.
save = True

for gidx in gidxs:

    # Load records.
    records = smc.load_records(entity, gidx=gidx)
    # records2 = smc2.load_records(entity, gidx=gidx)
    debug = True

    # Sort records.
    records = records.sort_values(['stim_type', 'combi', 'amp'], ascending=[True, True, False])
    debug = True

    # # Update stim_type.
    # records['stim_type'] = 'eext'
    # debug = True

    # # Keep only certain stim_types.
    # records = records[records['stim_type'].apply(lambda x: 'stat_syn_from_Ia' in x)]
    # debug = True

    # Reset index.
    records.reset_index(drop=True, inplace=True)
    debug = True

    # Save back records.
    if save:
        smc.save_records(records, entity, gidx=gidx)

print('The end.')
